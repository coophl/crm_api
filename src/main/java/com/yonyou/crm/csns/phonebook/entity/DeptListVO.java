package com.yonyou.crm.csns.phonebook.entity;

import java.io.Serializable;

public class DeptListVO implements Serializable  {
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 部门名称
     */
    private String name;

    /**
     * 图片id
     */
    private String pic;
    
    /**
     * 是否我的部门(Y,N)
     */
    private String flag;

	/**
     * 人数
     */
    private Integer num;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

}
