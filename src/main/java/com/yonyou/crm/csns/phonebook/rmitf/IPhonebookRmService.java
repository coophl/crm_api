package com.yonyou.crm.csns.phonebook.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.csns.phonebook.entity.PersonListVO;

public interface IPhonebookRmService {
	
	public void synPersonData();
	
	public List<PersonListVO> getPersonList(Map<String, Object> paraMap);
	
	public Map<String, Object> getOrganization(Long deptId);
	
	public Map<String, Integer> countOneByDeptIds();
	
	public List<Map<String, Object>> getDeptWithSub(Long deptId);
	
	public List<Map<String, Object>> getOrgTree();
	
	/*
	 * 通讯录搜索，关键字可以是部门名称或者人员姓名，结果包含部门列表和人员列表
	 */
	public Map<String, Object> searchList(String searchKey);

}
