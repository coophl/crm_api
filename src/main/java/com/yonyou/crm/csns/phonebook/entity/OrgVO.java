package com.yonyou.crm.csns.phonebook.entity;

import java.io.Serializable;

public class OrgVO implements Serializable  {
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 部门名称
     */
    private String name;
    
    /**
     * 部门编码
     */
    private String code;

    /**
     * 图片id
     */
    private String pic;
    
    /**
     * 父id
     */
    private Long fatherorgId;

    /**
     * 启用状态
     */
    private Byte enablestate;
    
    private Integer num;


    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getFatherorgId() {
		return fatherorgId;
	}

	public void setFatherorgId(Long fatherorgId) {
		this.fatherorgId = fatherorgId;
	}

	public Byte getEnablestate() {
		return enablestate;
	}

	public void setEnablestate(Byte enablestate) {
		this.enablestate = enablestate;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

}
