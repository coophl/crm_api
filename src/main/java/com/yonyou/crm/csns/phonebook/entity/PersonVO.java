package com.yonyou.crm.csns.phonebook.entity;

import java.io.Serializable;
import java.util.Date;

public class PersonVO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 姓名拼音
     */
    private String namePinyin;

    /**
     * 来源，1.user，2.联系人
     */
    private Byte source;

    /**
     * 来源为1时，为用户表id，来源为2时，为联系人id
     */
    private Long sourceId;

    /**
     * 所属公司id
     */
    private Long orgId;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 电话
     */
    private String phone;

    /**
     * 性别,1男，2女
     */
    private Integer gender;

    /**
     * 性别
     */
    private String genderName;

    /**
     * 头像
     */
    private String pic;

    /**
     * 是否负责人，Y or N, 对应user中的job值1,员工，2负责人，3其他负责人
     */
    private String manage;

    /**
     * 职位,对应角色id
     */
    private String job;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * csns_phonebook_person
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 姓名拼音
     * @return name_pinyin 姓名拼音
     */
    public String getNamePinyin() {
        return namePinyin;
    }

    /**
     * 姓名拼音
     * @param namePinyin 姓名拼音
     */
    public void setNamePinyin(String namePinyin) {
        this.namePinyin = namePinyin;
    }

    /**
     * 来源，1.user，2.联系人
     * @return source 来源，1.user，2.联系人
     */
    public Byte getSource() {
        return source;
    }

    /**
     * 来源，1.user，2.联系人
     * @param source 来源，1.user，2.联系人
     */
    public void setSource(Byte source) {
        this.source = source;
    }

    /**
     * 来源为1时，为用户表id，来源为2时，为联系人id
     * @return source_id 来源为1时，为用户表id，来源为2时，为联系人id
     */
    public Long getSourceId() {
        return sourceId;
    }

    /**
     * 来源为1时，为用户表id，来源为2时，为联系人id
     * @param sourceId 来源为1时，为用户表id，来源为2时，为联系人id
     */
    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * 所属公司id
     * @return org_id 所属公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 所属公司id
     * @param orgId 所属公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 部门id
     * @return dept_id 部门id
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 部门id
     * @param deptId 部门id
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
     * 邮箱
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 电话
     * @return phone 电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 电话
     * @param phone 电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 性别,1男，2女
     * @return gender 性别,1男，2女
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别,1男，2女
     * @param gender 性别,1男，2女
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getGenderName() {
		return genderName;
	}

	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}

	/**
     * 头像
     * @return pic 头像
     */
    public String getPic() {
        return pic;
    }

    /**
     * 头像
     * @param pic 头像
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 是否负责人，
     * @return manage 是否负责人
     */
    public String getManage() {
        return manage;
    }

    /**
     * 是否负责人
     * @param manage 是否负责人
     */
    public void setManage(String manage) {
        this.manage = manage;
    }

    /**
     * 职位
     * @return job 职位
     */
    public String getJob() {
        return job;
    }

    /**
     * 职位
     * @param job 职位
     */
    public void setJob(String job) {
        this.job = job;
    }
    
    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }
}