package com.yonyou.crm.csns.phonebook.entity;

import java.io.Serializable;
import java.util.Date;

public class PersonListVO implements Serializable {
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 部门名称
     */
    private String department;

    /**
     * 名称
     */
    private String name;

    /**
     * 手机
     */
    private String phonenum;

    /**
     * 头像id
     */
    private String pic;
    
    /**
     * 是否负责人，Y是，N否(对应的user表中的job字段，1员工，2负责人，3其他负责人)
     */
    private String manage;
    
    /**
     * 职位名称(对应person表中的job字段)
     */
    private String job;
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getManage() {
		return manage;
	}

	public void setManage(String manage) {
		this.manage = manage;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(String phonenum) {
		this.phonenum = phonenum;
	}
    
}
