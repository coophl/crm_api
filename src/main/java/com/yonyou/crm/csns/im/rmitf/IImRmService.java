package com.yonyou.crm.csns.im.rmitf;

import java.util.Map;

import com.yonyou.crm.csns.im.entity.ImVO;

public interface IImRmService {

	public Map<String, Object> getImInfo(Long id);
	public ImVO getImDetail(Long id);
}
