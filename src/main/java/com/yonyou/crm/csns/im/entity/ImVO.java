package com.yonyou.crm.csns.im.entity;

import java.io.Serializable;
import java.util.Date;

public class ImVO implements Serializable {
    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 企业id
     */
    private String eptId;

    /**
     * 应用id
     */
    private String appId;

    /**
     * 客户端id，euc生成
     */
    private String clientId;

    /**
     * 客户端加密，euc生成
     */
    private String clientSecret;

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 企业id
     * @return ept_id 企业id
     */
    public String getEptId() {
        return eptId;
    }

    /**
     * 企业id
     * @param eptId 企业id
     */
    public void setEptId(String eptId) {
        this.eptId = eptId;
    }

    /**
     * 应用id
     * @return app_id 应用id
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 应用id
     * @param appId 应用id
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * 客户端id，euc生成
     * @return client_id 
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * 客户端id，euc生成
     * @param clientId 
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * 客户端加密，euc生成
     * @return client_secret 
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * 客户端加密，euc生成
     * @param clientSecret 
     */
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}