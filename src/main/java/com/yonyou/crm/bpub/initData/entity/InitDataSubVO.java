package com.yonyou.crm.bpub.initData.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InitDataSubVO implements Serializable {

	/*
	 * 表体分组唯一标识
	 */
	private String bodymark;
	
	/*
	 * 表体修改动作类型描述
	 */
	private String action;
	
	/*
	 * 表体数据列表
	 */
	private List<InitDatasListItemVO> bodydatalist = new ArrayList<InitDatasListItemVO>();

	public String getBodymark() {
		return bodymark;
	}

	public void setBodymark(String bodymark) {
		this.bodymark = bodymark;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<InitDatasListItemVO> getBodydatalist() {
		return bodydatalist;
	}

	public void setBodydatalist(List<InitDatasListItemVO> bodydatalist) {
		this.bodydatalist = bodydatalist;
	}

}
