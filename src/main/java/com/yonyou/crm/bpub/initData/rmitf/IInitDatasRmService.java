package com.yonyou.crm.bpub.initData.rmitf;

import java.util.Map;

import com.yonyou.crm.bpub.initData.entity.*;

public interface IInitDatasRmService {
	
	public InitDatasVO getInitData(Map<String,String> param);
}
