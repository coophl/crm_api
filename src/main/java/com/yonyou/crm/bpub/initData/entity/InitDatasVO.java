package com.yonyou.crm.bpub.initData.entity;

import java.io.Serializable;
import java.util.*;

public class InitDatasVO  implements Serializable{

	/*
	 * bodydefaultdatalist
	 */
	private ArrayList<InitDatasListItemVO> headerlist = new ArrayList<InitDatasListItemVO>();
	
	/*
	 * 表体分组数据值列表
	 */
	private ArrayList<InitDatasBodyVO> bodylist = new ArrayList<InitDatasBodyVO>();
	
	/*
	 * 表体分组默认数据值列表
	 */
	private ArrayList<InitDatasBodyVO> bodydefaultdatalist = new ArrayList<>();

	public ArrayList<InitDatasListItemVO> getHeaderlist() {
		return headerlist;
	}

	public void setHeaderlist(ArrayList<InitDatasListItemVO> headerlist) {
		this.headerlist = headerlist;
	}

	public ArrayList<InitDatasBodyVO> getBodylist() {
		return bodylist;
	}

	public void setBodylist(ArrayList<InitDatasBodyVO> bodylist) {
		this.bodylist = bodylist;
	}

	public ArrayList<InitDatasBodyVO> getBodydefaultdatalist() {
		return bodydefaultdatalist;
	}

	public void setBodydefaultdatalist(ArrayList<InitDatasBodyVO> bodydefaultdatalist) {
		this.bodydefaultdatalist = bodydefaultdatalist;
	}
	
}
