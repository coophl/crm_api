package com.yonyou.crm.bpub.initData.entity;

import java.io.Serializable;
import java.util.List;


public class InitDatasItemVO implements Serializable{
	
	/**
	 * 数据项的id
	 */
	private Object dataid = "";
	
	/**
	 * 数据项的value
	 */
	private Object datavalue = "";
	
	/**
	 * 文件内容，base64编码，文件类型的字段时，此值才不为空
	 */
	private String datacontent = "";
	
	/**
	 * 扩展结构，如经纬度信息
	 */
	private List<InitParamItemVO> paramlist;
	
	public InitDatasItemVO() {
		
	}
	
	public InitDatasItemVO(String datavalue) {
		this.datavalue = datavalue;
	}
	
	public InitDatasItemVO(String dataid, String datavalue) {
		this.dataid = dataid;
		this.datavalue = datavalue;
	}

	public Object getDataid() {
		return dataid;
	}

	public void setDataid(Object dataid) {
		this.dataid = dataid;
	}

	public Object getDatavalue() {
		return datavalue;
	}

	public void setDatavalue(Object datavalue) {
		this.datavalue = datavalue;
	}

	public String getDatacontent() {
		return datacontent;
	}

	public void setDatacontent(String datacontent) {
		this.datacontent = datacontent;
	}

	public List<InitParamItemVO> getParamlist() {
		return paramlist;
	}

	public void setParamlist(List<InitParamItemVO> paramlist) {
		this.paramlist = paramlist;
	}
	
}