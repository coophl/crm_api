package com.yonyou.crm.bpub.initData.entity;

import java.io.Serializable;
import java.util.*;

public class InitDatasListItemVO implements Serializable{
	
	/*
	 * 字段的key
	 */
	private String itemkey;
	
	/*
	 * 数据项值列表
	 */
	private ArrayList<InitDatasItemVO> itemdatalist;
	
	public InitDatasListItemVO() {
	}
	
	public InitDatasListItemVO(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public ArrayList<InitDatasItemVO> getItemdatalist() {
		return itemdatalist;
	}

	public void setItemdatalist(ArrayList<InitDatasItemVO> itemdatalist) {
		this.itemdatalist = itemdatalist;
	}

}