package com.yonyou.crm.bpub.initData.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InitDatasBodyVO implements Serializable {

	/*
	 * 表体分组唯一标识
	 */
	private String bodymark;
	
	/*
	 * 表体数据列表
	 */
	private List<List<InitDatasListItemVO>> bodydatalist = new ArrayList<List<InitDatasListItemVO>>();

	public String getBodymark() {
		return bodymark;
	}

	public void setBodymark(String bodymark) {
		this.bodymark = bodymark;
	}

	public List<List<InitDatasListItemVO>> getBodydatalist() {
		return bodydatalist;
	}

	public void setBodydatalist(List<List<InitDatasListItemVO>> bodydatalist) {
		this.bodydatalist = bodydatalist;
	}
}
