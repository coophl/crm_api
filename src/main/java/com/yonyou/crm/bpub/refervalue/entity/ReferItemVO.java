package com.yonyou.crm.bpub.refervalue.entity;

import java.io.Serializable;

public class ReferItemVO implements Serializable {

	private String itemkey;
	
	private String id;
	
	private String value;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
