package com.yonyou.crm.bpub.refervalue.entity;

import java.io.Serializable;
import java.util.List;

public class ReferDataVO implements Serializable {

	private String id;
	
	private String value;
	
	/*
	 * 其他字段
	 */
	private List<String> itemdatalist;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<String> getItemdatalist() {
		return itemdatalist;
	}

	public void setItemdatalist(List<String> itemdatalist) {
		this.itemdatalist = itemdatalist;
	}

}
