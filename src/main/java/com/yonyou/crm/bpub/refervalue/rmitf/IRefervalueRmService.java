package com.yonyou.crm.bpub.refervalue.rmitf;

import java.io.Serializable;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;

public interface IRefervalueRmService {

	public <T extends Serializable> Map<String, Object> getRefervalues(Page<T> page, Map<String, Object> paraMap);
}
