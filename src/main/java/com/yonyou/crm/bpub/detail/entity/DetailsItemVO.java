package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;
import java.util.List;

public class DetailsItemVO implements Serializable {

	/**
	 * 字段的key
	 */
	private String itemkey = "";
	
	/**
	 * 显示名称
	 */
	private String name = "";
	
	/**
	 * 显示值对应的id(枚举、参照类型)
	 */
	private Object id = "";
	
	/**
	 * 显示值
	 */
	private Object value = "";
	
	/**
	 * 数据类型
	 */
	private String type = "";
	
	/**
	 * 扩展结构，如经纬度信息
	 */
	private List<DetailsParamItemVO> paramlist;
	
	private static final long serialVersionUID = 1L;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<DetailsParamItemVO> getParamlist() {
		return paramlist;
	}

	public void setParamlist(List<DetailsParamItemVO> paramlist) {
		this.paramlist = paramlist;
	}
	
}
