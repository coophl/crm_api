package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;

public class DetailsParamItemVO implements Serializable {

	/**
	 * 扩展结构某一项的key, 例如经度longitude, 纬度latitude
	 */
	private String paramkey;
	
	/**
	 * 扩展结构某一项的值
	 */
	private Object paramvalue;
	
	public DetailsParamItemVO() {
		
	}
	
	public DetailsParamItemVO(String paramkey, Object paramvalue) {
		this.paramkey = paramkey;
		this.paramvalue = paramvalue;
	}

	public String getParamkey() {
		return paramkey;
	}

	public void setParamkey(String paramkey) {
		this.paramkey = paramkey;
	}

	public Object getParamvalue() {
		return paramvalue;
	}

	public void setParamvalue(Object paramvalue) {
		this.paramvalue = paramvalue;
	}
	
}
