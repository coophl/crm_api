package com.yonyou.crm.bpub.detail.rmitf;

import com.yonyou.crm.bpub.detail.entity.DetailsVO;
import com.yonyou.crm.common.exception.CrmMessageException;

public interface IDetailsRmService {

	public DetailsVO getDetailData(String module, String id) throws CrmMessageException;
}
