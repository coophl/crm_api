package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;
import java.util.*;

public class DetailsVO implements Serializable {

	/**
	 * 表头数据值列表
	 */
	private List<DetailsGroupVO> headerlist = new ArrayList<>();
	
	/**
	 * 表体分组数据值列表
	 */
	private List<DetailsGroupVO> bodylist = new ArrayList<>();
	
	/**
	 * 标识性参数
	 */
	private List<DetailsFlagVO> flags = new ArrayList<>();
	
	/**
	 * 操作列表
	 */
	private List<String> actions = new ArrayList<>();
	
	private static final long serialVersionUID = 1L;
	
	public List<DetailsGroupVO> getHeaderlist() {
		return headerlist;
	}
	
	public void setHeaderlist(List<DetailsGroupVO> headerlist) {
		this.headerlist = headerlist;
	}
	
	public List<DetailsGroupVO> getBodylist() {
		return bodylist;
	}
	
	public void setBodylist(List<DetailsGroupVO> bodylist) {
		this.bodylist = bodylist;
	}

	public List<DetailsFlagVO> getFlags() {
		return flags;
	}

	public void setFlags(List<DetailsFlagVO> flags) {
		this.flags = flags;
	}

	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}
	
}
