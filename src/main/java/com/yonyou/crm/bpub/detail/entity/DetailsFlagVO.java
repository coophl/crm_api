package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;

public class DetailsFlagVO implements Serializable {

	private String des = "";
	
	private String value = "";
	
	private static final long serialVersionUID = 1L;

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
