package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;
import java.util.List;

public class DetailsFilterVO implements Serializable {
	
	/**
	 * DetailsFilterVO
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 模板中参与项的key
	 */
	private String key = "";
	
	/**
	 * 数据项列表
	 */
	private List<DetailsFilterDataItemVO> datalist;
	
	public DetailsFilterVO() {	
	}
	
	public DetailsFilterVO(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<DetailsFilterDataItemVO> getDatalist() {
		return datalist;
	}

	public void setDatalist(List<DetailsFilterDataItemVO> datalist) {
		this.datalist = datalist;
	}
	
}
