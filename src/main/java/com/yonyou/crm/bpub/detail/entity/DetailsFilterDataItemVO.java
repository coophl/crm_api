package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;

public class DetailsFilterDataItemVO implements Serializable {
	
	/**
	 * DetailsFilterDataItemVO
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 字段的id
	 */
	private String id;
	
	/**
	 * 字段的值
	 */
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
