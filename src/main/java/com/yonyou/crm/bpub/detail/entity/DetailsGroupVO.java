package com.yonyou.crm.bpub.detail.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DetailsGroupVO implements Serializable {

	/**
	 * 分组名称
	 */
	private String groupname = "";
	
	/**
	 * 分组Id
	 */
	private String groupid = "";
	
	/**
	 * 条目内容
	 */
	private List<DetailsItemVO> groupdata = new ArrayList<DetailsItemVO>();
	
	private static final long serialVersionUID = 1L;

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public List<DetailsItemVO> getGroupdata() {
		return groupdata;
	}

	public void setGroupdata(List<DetailsItemVO> groupdata) {
		this.groupdata = groupdata;
	}
	
}
