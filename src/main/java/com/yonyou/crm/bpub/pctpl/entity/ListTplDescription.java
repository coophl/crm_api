package com.yonyou.crm.bpub.pctpl.entity;

import java.util.List;

/**
 * 列模板描述
 * @author litcb
 *
 */
public class ListTplDescription {
	private List<ListFieldDescription> columns;

	
	public ListTplDescription(List<ListFieldDescription> columns) {
		super();
		this.columns = columns;
	}

	public List<ListFieldDescription> getColumns() {
		return columns;
	}

	public void setColumns(List<ListFieldDescription> columns) {
		this.columns = columns;
	}
	
	
}
