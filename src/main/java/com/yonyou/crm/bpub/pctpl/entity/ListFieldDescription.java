package com.yonyou.crm.bpub.pctpl.entity;

/**
 * 列字段描述
 * @author Administrator
 *
 */
public class ListFieldDescription {
	//显示名称
	private String title;
	//字段编码
	private String dataIndex;
	//渲染方式
	private String render;
	
	
	public ListFieldDescription(String title, String dataIndex, String render) {
		super();
		this.title = title;
		this.dataIndex = dataIndex;
		this.render = render;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDataIndex() {
		return dataIndex;
	}
	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}
	public String getRender() {
		return render;
	}
	public void setRender(String render) {
		this.render = render;
	}

}
