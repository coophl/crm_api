package com.yonyou.crm.bpub.pctpl.entity;

import java.util.Map;

/**
 * 参照字段描述
 * @author litcb
 *
 */
public class FormRefFieldDescription extends FormFieldDescription{

	//参照值的数据集code
	private String dataSource;
	//<读入字段,写入字段>
	private Map<String,String> readWriteFields;
	//过滤字段
	private String filter;
	
	public FormRefFieldDescription(String name, String code, FieldsType type,
			String render,String dataSource,Map<String,String> readWriteFields) {
		super(name, code, type, render);
		
		this.readWriteFields = readWriteFields;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	

	public Map<String, String> getReadWriteFields() {
		return readWriteFields;
	}

	public void setReadWriteFields(Map<String, String> readWriteFields) {
		this.readWriteFields = readWriteFields;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	
}
