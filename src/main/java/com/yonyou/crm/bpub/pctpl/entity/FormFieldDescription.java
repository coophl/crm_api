package com.yonyou.crm.bpub.pctpl.entity;


/**
 * 表单字段描述
 * @author litcb
 *
 */
public class FormFieldDescription {
	//布局信息
	
	//显示名称
	private String name;
	//字段编码
	private String code;
	//字段类型
	private FieldsType type;
	//渲染方式
	private String render;
	//是否可见
	private boolean visible;
	//是否禁用
	private boolean disabled;
	//默认值
	private String defaultValue;
	//关联项
	private String relation;
	//是否必输
	private boolean nullAble = true;
	//校验规则
	private String validateType;
	
	public FormFieldDescription(String name, String code, FieldsType type,
			String render, boolean visible) {
		super();
		this.name = name;
		this.code = code;
		this.type = type;
		this.render = render;
		this.visible = visible;
	}
	public FormFieldDescription(String name, String code, FieldsType type,
			String render) {
		super();
		this.name = name;
		this.code = code;
		this.type = type;
		this.render = render;
		this.visible = true;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public FieldsType getType() {
		return type;
	}
	public void setType(FieldsType type) {
		this.type = type;
	}
	public String getRender() {
		return render;
	}
	public void setRender(String render) {
		this.render = render;
	}
	public boolean isNullAble() {
		return nullAble;
	}
	public void setNullAble(boolean nullAble) {
		this.nullAble = nullAble;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getValidateType() {
		return validateType;
	}
	public void setValidateType(String validateType) {
		this.validateType = validateType;
	}

	
	/*public void init() {
		switch(this.render) {
		case Ref:
			//参照类型
			break;
		case Enum:
			//枚举类型
			break;
		default:
			break;
			
		}
		
	}*/
	
}
