package com.yonyou.crm.bpub.pctpl.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 枚举字段描述
 * @author litcb
 *
 */
public class FormEnumFieldDescription extends FormFieldDescription {
	// 枚举值数据集的code
	private String dataSource;
	//<读入字段,写入字段>
	private Map<String,String> readWriteFields;
	// 枚举集
	private List<EnumDescription> enumeration;

	public FormEnumFieldDescription(String name, String code, FieldsType type,
			String render,String dataSource,List<EnumDescription> enumeration) {
		super(name, code, type, render);
		this.dataSource = dataSource;
		this.enumeration = enumeration;
		this.readWriteFields = new HashMap<String,String>();
		this.readWriteFields.put("id", code);
		this.readWriteFields.put("title", code + "Name");
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public List<EnumDescription> getEnumeration() {
		return enumeration;
	}

	public void setEnumeration(List<EnumDescription> enumeration) {
		this.enumeration = enumeration;
	}

	public Map<String, String> getReadWriteFields() {
		return readWriteFields;
	}

	public void setReadWriteFields(Map<String, String> readWriteFields) {
		this.readWriteFields = readWriteFields;
	}
	
}
