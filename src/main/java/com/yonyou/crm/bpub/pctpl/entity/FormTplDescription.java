package com.yonyou.crm.bpub.pctpl.entity;

import java.util.List;

/**
 * 表单模板描述
 * @author litcb
 *
 */
public class FormTplDescription {
	List<FormFieldDescription> fields ;

	
	public FormTplDescription(List<FormFieldDescription> fields) {
		super();
		this.fields = fields;
	}

	public List<FormFieldDescription> getFields() {
		return fields;
	}

	public void setFields(List<FormFieldDescription> fields) {
		this.fields = fields;
	}
	
	
}
