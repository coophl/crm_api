package com.yonyou.crm.bpub.pctpl.entity;

public enum FieldsType {
	String(1,"单行文本"),Ref(2,"参照"),Enum(3,"枚举"),DateTime(4,"日期时间");
	
	//字段类型对应内部值
	int value;
	//字段类型名称
	String name;

	// 构造方法
	private FieldsType(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
}
