package com.yonyou.crm.bpub.pctpl.entity;

public class EnumDescription {
	private int key;
	private String title;
	
	public EnumDescription(int key, String title) {
		super();
		this.key = key;
		this.title = title;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
