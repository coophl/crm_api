package com.yonyou.crm.bpub.approval.itf;

public abstract interface ISuperVO {
	public abstract Object getAttributeValue(String paramString);
	  
	public abstract void setAttributeValue(String paramString, Object paramObject);
	  
	public abstract String getPrimaryKey();
	
}
