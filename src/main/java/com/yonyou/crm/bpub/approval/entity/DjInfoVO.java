package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DjInfoVO implements Serializable {
	
	private int id;

	/*
	 * 实例id
	 */
	private String instanceId;
	
	/*
	 * 实例名称
	 */
	private String name;
	
	/*
	 * 单据id
	 */
	private Long djId;
	
	/*
	 * 单据类型key
	 */
	private int djType;
	
	/*
	 * 提交时间
	 */
	private Date createTime;
	
	/*
	 * 当前阶段停留时长(未完成状态时需要此值)
	 */
	private String stayTimeLength;
	
	/*
	 * 完成时间
	 */
	private Date completeTime;
	
	/*
	 * 审批状态（1.发起，2.审批进行中，3.审批完成）
	 */
	private int status;
	
	/*
	 * 审批状态名称
	 */
	private String statusName;
	
	/*
	 * 审批意见
	 */
	private String comment;
	
	/*
	 * 是否可提醒0,1标识
	 */
	private int isRemind;
	
	private String operate;

	/*
	 * 当前环节待审人
	 */
	private List<AssigneeVO> approvalUserList;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDjId() {
		return djId;
	}

	public void setDjId(Long djId) {
		this.djId = djId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getStayTimeLength() {
		return stayTimeLength;
	}

	public void setStayTimeLength(String stayTimeLength) {
		this.stayTimeLength = stayTimeLength;
	}

	public Date getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(Date completeTime) {
		this.completeTime = completeTime;
	}

	public int getDjType() {
		return djType;
	}

	public void setDjType(int djType) {
		this.djType = djType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(int isRemind) {
		this.isRemind = isRemind;
	}

	public List<AssigneeVO> getApprovalUserList() {
		return approvalUserList;
	}

	public void setApprovalUserList(List<AssigneeVO> approvalUserList) {
		this.approvalUserList = approvalUserList;
	}

	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}
	
}
