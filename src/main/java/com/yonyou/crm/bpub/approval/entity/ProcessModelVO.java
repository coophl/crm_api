package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;

public class ProcessModelVO implements Serializable {
	
	private Long id;

	private Long tenantId;
	
	private Long bizTypeId;
	
	private String bizTypeName;
	
	private Integer djType;
	
	private String djTypeName;
	
	private String ysModelName;
	
	private String ysModelKey;
	
	private String ysModelId;
	
	private String ysUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getBizTypeId() {
		return bizTypeId;
	}

	public void setBizTypeId(Long bizTypeId) {
		this.bizTypeId = bizTypeId;
	}

	public String getBizTypeName() {
		return bizTypeName;
	}

	public void setBizTypeName(String bizTypeName) {
		this.bizTypeName = bizTypeName;
	}

	public Integer getDjType() {
		return djType;
	}

	public void setDjType(Integer djType) {
		this.djType = djType;
	}

	public String getDjTypeName() {
		return djTypeName;
	}

	public void setDjTypeName(String djTypeName) {
		this.djTypeName = djTypeName;
	}

	public String getYsModelName() {
		return ysModelName;
	}

	public void setYsModelName(String ysModelName) {
		this.ysModelName = ysModelName;
	}

	public String getYsModelId() {
		return ysModelId;
	}

	public void setYsModelId(String ysModelId) {
		this.ysModelId = ysModelId;
	}

	public String getYsModelKey() {
		return ysModelKey;
	}

	public void setYsModelKey(String ysModelKey) {
		this.ysModelKey = ysModelKey;
	}

	public String getYsUrl() {
		return ysUrl;
	}

	public void setYsUrl(String ysUrl) {
		this.ysUrl = ysUrl;
	}
}
