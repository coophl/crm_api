package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;

public class ProcessInstanceVO implements Serializable {
	
	/*
	 * 租户id
	 */
	private Long tenantId;

	/*
	 * 单据id
	 */
	private Long businessId;
	
	/*
	 * 单据类型key
	 */
	private Integer djType;
	
	/*
	 * 单据类型key
	 */
	private Long bizTypeId;
	
	/*
	 * 提交的单据对应的云审流程实例id
	 */
	private String ysInstanceId;
	
	/*
	 * 提交的单据对应的云审流程模型key
	 */
	private String ysModelKey;
	
	/*
	 * 提交人id
	 */
	private Long userId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public Integer getDjType() {
		return djType;
	}

	public void setDjType(Integer djType) {
		this.djType = djType;
	}

	public String getYsInstanceId() {
		return ysInstanceId;
	}

	public void setYsInstanceId(String ysInstanceId) {
		this.ysInstanceId = ysInstanceId;
	}

	public String getYsModelKey() {
		return ysModelKey;
	}

	public void setYsModelKey(String ysModelKey) {
		this.ysModelKey = ysModelKey;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBizTypeId() {
		return bizTypeId;
	}

	public void setBizTypeId(Long bizTypeId) {
		this.bizTypeId = bizTypeId;
	}
	
}
