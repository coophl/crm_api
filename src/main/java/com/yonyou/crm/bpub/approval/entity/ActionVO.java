package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;

public class ActionVO implements Serializable {

	private String action;
	
	private String name;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
