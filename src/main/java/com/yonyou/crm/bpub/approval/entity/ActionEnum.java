package com.yonyou.crm.bpub.approval.entity;

public enum ActionEnum {
	SUBMIT("submit", "提交"),
	CANCEL("cancel", "撤回"),
	AGREE("agree", "同意"),
	REJECT("reject", "驳回到上一步"),
	REJECTALL("rejectall", "驳回到制单人");
	
	//枚举项对应的值
	String value;
	//枚举项显示值
	String name;
	
	private ActionEnum(String value, String name){
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
