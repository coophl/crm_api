package com.yonyou.crm.bpub.approval.entity;

public enum DjTypeEnum {
	CUM_PRMT(1,"客户升级申请单"),
	TERMINAL_POINT_ADD(2,"网点新增申请单"),
	TERMINAL_POINT_STOP(3,"网点停用申请单"),
	SALE_PRICE(4,"促销价申请单"),
	CREDICT(5,"信用申请单"),
	CUM_PRICE(6,"客户费用");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private DjTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
