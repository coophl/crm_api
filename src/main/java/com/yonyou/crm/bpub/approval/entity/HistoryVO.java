package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;
import java.util.List;

public class HistoryVO implements Serializable {

	/*
	 * 审批人列表
	 */
	private List<AssigneeVO> personlist;
	
	/*
	 * 审批状态
	 */
	private String status;
	
	/*
	 * 审批意见
	 */
	private String comment;
	
	/*
	 * 审批时间描述，非当前状态取年月日时分描述，当前状态取等待时间
	 */
	private String time;
	
	/*
	 * 是否可提醒（Y可以N不可以）
	 */
	private String isremind;

	public List<AssigneeVO> getPersonlist() {
		return personlist;
	}

	public void setPersonlist(List<AssigneeVO> personlist) {
		this.personlist = personlist;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getIsremind() {
		return isremind;
	}

	public void setIsremind(String isremind) {
		this.isremind = isremind;
	}
	
}
