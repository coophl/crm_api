package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;
import java.util.Date;

public class TaskVO implements Serializable {
	
	private int id;

	private String taskId;
	
	private String instanceId;
	
	private String name;
	
	/*
	 * 任务接收时间
	 */
	private Date createTime;
	
	/*
	 * 当前状态停留时长
	 */
	private String stayTimeLength;
	
	/*
	 * 审批时间
	 */
	private Date completeTime;
	
	/*
	 * 单据id
	 */
	private Long djId;
	
	/*
	 * 单据类型key
	 */
	private int djType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getStayTimeLength() {
		return stayTimeLength;
	}

	public void setStayTimeLength(String stayTimeLength) {
		this.stayTimeLength = stayTimeLength;
	}

	public Date getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(Date completeTime) {
		this.completeTime = completeTime;
	}

	public Long getDjId() {
		return djId;
	}

	public void setDjId(Long djId) {
		this.djId = djId;
	}

	public int getDjType() {
		return djType;
	}

	public void setDjType(int djType) {
		this.djType = djType;
	}
	
}
