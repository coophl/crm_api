package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.yonyou.crm.bpub.approval.itf.ISuperVO;


public abstract class SuperVO implements ISuperVO, Serializable {
	private transient Map<String, Object> valueIndex = new HashMap();
	/**
	 * 
	 */
	private static final long serialVersionUID = -3871430045826862190L;

	public void setAttributeValue(String name, Object value) {
		valueIndex.put(name, value);
	}
	public Object getAttributeValue(String name) {
		
		return valueIndex.get(name);
	}
	
	  /**
     * 最后审批人
     * @return approval_user_id 最后审批人
     */
    public Long getApprovalUserId() {
    	return (Long) valueIndex.get("approvalUserId");
    }

    /**
     * 最后审批人
     * @param approvalUserId 最后审批人
     */
    public void setApprovalUserId(Long approvalUserId) {
    	valueIndex.put("approvalUserId", approvalUserId);
    }
    /**
     * 最后审批人名称
	 * @return approvalUserName
	 */
	public String getApprovalUserName() {
		return (String) valueIndex.get("approvalUserName");
	}

	/**
	 * @param approvalUserName 最后审批人名称
	 */
	public void setApprovalUserName(String approvalUserName) {
		valueIndex.put("approvalUserName", approvalUserName);
	}
}
