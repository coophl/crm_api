package com.yonyou.crm.bpub.approval.entity;

public enum ApprovalStatusEnum {
	NOT_COMMIT(1,"未提交"),
	COMMITED(2,"已提交"),
	APPROVAL(3,"审批中"),
	FINISHED(4,"已完成");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private ApprovalStatusEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
