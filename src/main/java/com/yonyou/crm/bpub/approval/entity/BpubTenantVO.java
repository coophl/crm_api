package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;

public class BpubTenantVO implements Serializable {

	private Long tenantId;
	
	private String ysTenantId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getYsTenantId() {
		return ysTenantId;
	}

	public void setYsTenantId(String ysTenantId) {
		this.ysTenantId = ysTenantId;
	}
}
