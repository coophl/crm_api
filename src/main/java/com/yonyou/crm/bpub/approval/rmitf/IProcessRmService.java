package com.yonyou.crm.bpub.approval.rmitf;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.yonyou.crm.bpub.approval.entity.ActionVO;
import com.yonyou.crm.bpub.approval.entity.AssigneeVO;
import com.yonyou.crm.bpub.approval.entity.DjInfoVO;
import com.yonyou.crm.bpub.approval.entity.HistoryVO;
import com.yonyou.crm.bpub.approval.entity.ProcessModelVO;
import com.yonyou.crm.bpub.approval.entity.TaskVO;
import com.yonyou.crm.bpub.detail.entity.DetailsVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IProcessRmService {
	
	public String getTenant();

	/*
	 * 同步部门和人员
	 */
	public void synDeptsAndUsers();
	
	/*
	 * 创建流程模型(返回流程模型设计器url)
	 */
	public String createProcess(Map<String, Object> paraMap);
	
	/*
	 * 删除流程定义模型
	 */
	public boolean deleteProcessModel(String modelId);
	
	/*
	 * 获取单点登录云审批设计器的url
	 */
//	public String getUrlForLoginYs(String modelId);
	
	/*
	 * 返回审批流列表
	 */
	public List<ProcessModelVO> getProcessModelList(Map<String, Object> paraMap);
	
	/*
	 * 获取审批历史
	 */
	public List<HistoryVO> getHistoryTasks(Long businessId, int djType, String order);
	
	/*
	 * 执行任务
	 */
	public boolean actionTask(Long businessId, int djType, String instanceId, String taskId, String comment, String action);
	
	/*
	 * 获取未完成流程实例列表
	 */
	public Page<DjInfoVO> getNotFinished(Map<String, Object> paraMap);
	
	/*
	 * 获取已完成流程实例列表
	 */
	public Page<DjInfoVO> getFinished(Map<String, Object> paraMap);
	
	/*
	 * 查询单个流程实例(详情)
	 */
	public <T extends Serializable> T getDetail(Long businessId, int djType);
	
	/*
	 * 获取审批动作列表
	 */
	public List<ActionVO> getActions(String instanceId, String taskId);
	
	/*
	 * 提醒审批
	 */
	public void remind(Long businessId, int djType, List<String> personlist);
	
	/*
	 * 查询待办任务列表
	 */
	public Page<TaskVO> getTasksTodo(Map<String, Object> paraMap);
	
	/*
	 * 查询已办任务列表
	 */
	public Page<TaskVO> getTasksDone(Map<String, Object> paraMap);
	
	public int getProcessInstancesCount(Boolean isFinished);
	
	public int getTasksCount(Boolean isFinished);
	/**
	 * 
	 * @param tenantId
	 */
	public void saveTenant(Long tenantId) ;
}
