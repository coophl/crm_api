package com.yonyou.crm.bpub.approval.entity;

import java.io.Serializable;

public class ApprovalUserVO implements Serializable {
	
	private Long tenantId;
	
	private Long userId;
	
	private String ysUserId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getYsUserId() {
		return ysUserId;
	}

	public void setYsUserId(String ysUserId) {
		this.ysUserId = ysUserId;
	}
}
