package com.yonyou.crm.bpub.template.rmitf;

import java.util.Map;

import com.yonyou.crm.sys.modules.entity.LayoutVO;
import com.yonyou.crm.sys.modules.entity.ListLayoutVO;

public interface IPCTemplatesRmService {
	
	public LayoutVO getLayout(Map<String, Object> param);
	
	public ListLayoutVO getListLayout(Map<String, Object> param);
}
