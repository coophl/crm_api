package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;

public class TemplatesParttakeVO implements Serializable {

	/**
	 * 触发项的唯一标识，对应模版中某一字段的Key
	 */
	private String itemkey;
	
	/**
	 * 取数范围描述，0为表头，1为取表体单行，2为只取表体全部行, 3为只传参数但不取值的
	 */
	private String range;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}
	
}
