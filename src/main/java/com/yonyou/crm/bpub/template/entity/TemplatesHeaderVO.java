package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class TemplatesHeaderVO implements Serializable{
	
	/**
	 * 分组名称
	 */
	private String headername = "";
	
	/**
	 * 预留字段：分组展现样式
	 */
	private String style = "";
	
	/**
	 * 预留字段：是否展开
	 */
	private String spread = "";
	
	/**
	 * 字段实体列表
	 */
	private ArrayList<TemplatesItemVO> itemlist = new ArrayList<TemplatesItemVO>();

	public String getHeadername() {
		return headername;
	}

	public void setHeadername(String headername) {
		this.headername = headername;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public ArrayList<TemplatesItemVO> getItemlist() {
		return itemlist;
	}

	public void setItemlist(ArrayList<TemplatesItemVO> itemlist) {
		this.itemlist = itemlist;
	}
	
}