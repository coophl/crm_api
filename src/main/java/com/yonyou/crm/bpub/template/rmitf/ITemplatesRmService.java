package com.yonyou.crm.bpub.template.rmitf;

import java.util.Map;

import com.yonyou.crm.bpub.template.entity.TemplatesVO;

public interface ITemplatesRmService {
	public TemplatesVO getTemplate(Map<String,Object> param);
}
