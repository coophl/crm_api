package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.List;

public class TemplatesFilterVO implements Serializable {
	
	/**
	 * 筛选项的唯一标识，对应模版中某一字段的Key
	 */
	private String itemkey;
	
	/**
	 * 发送参照请求的必要项列表，当所有字段都不为空时才可以出发请求
	 */
	private List<TemplatesParttakeVO> necessarylist;
	
	/**
	 * 参照请求时的参与项列表
	 */
	private List<TemplatesParttakeVO> parttakelist;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public List<TemplatesParttakeVO> getNecessarylist() {
		return necessarylist;
	}

	public void setNecessarylist(List<TemplatesParttakeVO> necessarylist) {
		this.necessarylist = necessarylist;
	}

	public List<TemplatesParttakeVO> getParttakelist() {
		return parttakelist;
	}

	public void setParttakelist(List<TemplatesParttakeVO> parttakelist) {
		this.parttakelist = parttakelist;
	}
	
}
