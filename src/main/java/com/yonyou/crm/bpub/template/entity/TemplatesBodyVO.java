package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TemplatesBodyVO implements Serializable {

	/**
	 * 分组名称
	 */
	private String bodyname;
	
	/**
	 * 表体分组唯一标识
	 */
	private String bodymark;
	
	/**
	 * 预留字段:分组展现样式
	 */
	private String style;
	
	/**
	 * 字段实体列表
	 */
	private List<TemplatesItemVO> itemlist = new ArrayList<TemplatesItemVO>();

	public String getBodyname() {
		return bodyname;
	}

	public void setBodyname(String bodyname) {
		this.bodyname = bodyname;
	}

	public String getBodymark() {
		return bodymark;
	}

	public void setBodymark(String bodymark) {
		this.bodymark = bodymark;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public List<TemplatesItemVO> getItemlist() {
		return itemlist;
	}

	public void setItemlist(List<TemplatesItemVO> itemlist) {
		this.itemlist = itemlist;
	}
}
