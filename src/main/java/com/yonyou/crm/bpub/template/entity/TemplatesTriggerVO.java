package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TemplatesTriggerVO implements Serializable {

	/**
	 * 触发项的唯一标识，对应模版中某一字段的Key
	 */
	private String itemkey;
	
	/**
	 * 表体分组唯一标识
	 */
	private String bodymark;
	
	/**
	 * 触发请求的必要项列表，列表中的String对应模版中某一字段的key，当所有字段都不为空时才可以触发请求
	 */
	private List<TemplatesParttakeVO> necessarylist;
	
	/**
	 * 触发请求时的参与项列表
	 */
	private List<TemplatesParttakeVO> parttakelist;
	
	/**
	 * 影响项列表
	 */
	private List<TemplatesParttakeVO> effectlist;
	
	
	/**
	 * 触发类型，后台返回，触发请求时原样发送到后台，可对应到原有的业务计算、参照关联,可包含多个值，由后台去决定处理顺序。含义待明确
	 */
	private List<String> type;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getBodymark() {
		return bodymark;
	}

	public void setBodymark(String bodymark) {
		this.bodymark = bodymark;
	}

	public List<TemplatesParttakeVO> getNecessarylist() {
		return necessarylist;
	}

	public void setNecessarylist(List<TemplatesParttakeVO> necessarylist) {
		this.necessarylist = necessarylist;
	}

	public List<TemplatesParttakeVO> getParttakelist() {
		return parttakelist;
	}

	public void setParttakelist(List<TemplatesParttakeVO> parttakelist) {
		this.parttakelist = parttakelist;
	}

	public List<TemplatesParttakeVO> getEffectlist() {
		return effectlist;
	}

	public void setEffectlist(List<TemplatesParttakeVO> effectlist) {
		this.effectlist = effectlist;
	}

	public List<String> getType() {
		return type;
	}

	public void setType(List<String> type) {
		this.type = type;
	}
	
}
