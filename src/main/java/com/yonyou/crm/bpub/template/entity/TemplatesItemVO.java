package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.*;

public class TemplatesItemVO implements Serializable{
	
	/**
	 * 字段的唯一标识
	 */
	private String itemkey = "";
	
	/**
	 * 字段名称
	 */
	private String itemname = "";
	
	/**
	 * 字段类型
	 */
	private String itemtype = "";
	
	/**
	 * 是否允许编辑”Y”or”N”
	 */
	private String iseditable = "";
	
	/**
	 * 是否必填”Y”or”N”
	 */
	private String isrequired = "";
	
	/**
	 * 是否允许多选”Y”or”N”
	 */
	private String ismultiselect = "N";
	
	/**
	 * 图片最大录入张数
	 */
	private String maxpicnum = "";
	
	/**
	 * 是否允许从相册选择
	 */
	private String issurportalbum = "";
	
	/**
	 * 精度
	 */
	private String precision = "";
	
	/**
	 * 是否显示”Y”or”N”
	 */
	private String isdisplay = "Y";
	
	/**
	 * 最大字符输入长度控制
	 */
	private String maxlenth = "";
	
	/**
	 * 参照的数据类型，用于获取参照列表
	 */
	private String referto = "";
	
	/**
	 * 编辑公式，移动端根据此公式做简单的四则运算
	 */
	private String editformula = "";
	
	/**
	 * 数据项全值列表，只有当此项为枚举时此结构才存在
	 */
	private ArrayList<TemplatesEnumVO> enumdatalist = new ArrayList();
	
	/**
	 * 同一种字段类型，可能包含多种展现类型；
	 * 比如引用数据类型根据数据量进行差异化展示、Date类型的差异化展示、String类型字段是否支持换行等。
	 * （这地方需要考虑后台模版配置的设计方案）
	 */
	// private String style = "";

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	public String getIseditable() {
		return iseditable;
	}

	public void setIseditable(String iseditable) {
		this.iseditable = iseditable;
	}

	public String getIsrequired() {
		return isrequired;
	}

	public void setIsrequired(String isrequired) {
		this.isrequired = isrequired;
	}

	public String getIsmultiselect() {
		return ismultiselect;
	}

	public void setIsmultiselect(String ismultiselect) {
		this.ismultiselect = ismultiselect;
	}

	public String getMaxpicnum() {
		return maxpicnum;
	}

	public void setMaxpicnum(String maxpicnum) {
		this.maxpicnum = maxpicnum;
	}

	public String getIssurportalbum() {
		return issurportalbum;
	}

	public void setIssurportalbum(String issurportalbum) {
		this.issurportalbum = issurportalbum;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public String getIsdisplay() {
		return isdisplay;
	}

	public void setIsdisplay(String isdisplay) {
		this.isdisplay = isdisplay;
	}

	public String getMaxlenth() {
		return maxlenth;
	}

	public void setMaxlenth(String maxlenth) {
		this.maxlenth = maxlenth;
	}

	public String getReferto() {
		return referto;
	}

	public void setReferto(String referto) {
		this.referto = referto;
	}

	public String getEditformula() {
		return editformula;
	}

	public void setEditformula(String editformula) {
		this.editformula = editformula;
	}

	public ArrayList<TemplatesEnumVO> getEnumdatalist() {
		return enumdatalist;
	}

	public void setEnumdatalist(ArrayList<TemplatesEnumVO> enumdatalist) {
		this.enumdatalist = enumdatalist;
	}
	
	/*
	public void setStyle(String style){
		this.style = style;
	}
	
	public String getStyle(){
		return style;
	}
	*/
}