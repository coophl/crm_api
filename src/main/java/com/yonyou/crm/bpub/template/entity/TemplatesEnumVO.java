package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;

public class TemplatesEnumVO implements Serializable {

	private String dataid = "";
	
	private String datavalue = "";

	public TemplatesEnumVO (String datavalue) {
		this.datavalue = datavalue;
	}
	
	public TemplatesEnumVO (String dataid, String datavalue) {
		this.dataid = dataid;
		this.datavalue = datavalue;
	}
	
	public String getDataid() {
		return dataid;
	}

	public void setDataid(String dataid) {
		this.dataid = dataid;
	}

	public String getDatavalue() {
		return datavalue;
	}

	public void setDatavalue(String datavalue) {
		this.datavalue = datavalue;
	}
	
}
