package com.yonyou.crm.bpub.template.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class TemplatesVO implements Serializable{
	private ArrayList<TemplatesHeaderVO> headerlist = new ArrayList<>();
	
	private ArrayList<TemplatesBodyVO> bodylist = new ArrayList<>();
	
	private ArrayList<TemplatesTriggerVO> triggerlist = new ArrayList<>();
	
	private ArrayList<TemplatesFilterVO> filterlist = new ArrayList<>();

	public ArrayList<TemplatesHeaderVO> getHeaderlist() {
		return headerlist;
	}

	public void setHeaderlist(ArrayList<TemplatesHeaderVO> headerlist) {
		this.headerlist = headerlist;
	}

	public ArrayList<TemplatesBodyVO> getBodylist() {
		return bodylist;
	}

	public void setBodylist(ArrayList<TemplatesBodyVO> bodylist) {
		this.bodylist = bodylist;
	}

	public ArrayList<TemplatesTriggerVO> getTriggerlist() {
		return triggerlist;
	}

	public void setTriggerlist(ArrayList<TemplatesTriggerVO> triggerlist) {
		this.triggerlist = triggerlist;
	}

	public ArrayList<TemplatesFilterVO> getFilterlist() {
		return filterlist;
	}

	public void setFilterlist(ArrayList<TemplatesFilterVO> filterlist) {
		this.filterlist = filterlist;
	}
	
	
}
