package com.yonyou.crm.bpub.list.entity;

import java.io.Serializable;

public class AppListSpecialVO implements Serializable {
	
	private String type;
	
	private String position;
	
	private Object value;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
}
