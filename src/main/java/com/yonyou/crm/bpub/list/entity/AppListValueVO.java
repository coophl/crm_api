package com.yonyou.crm.bpub.list.entity;

import java.io.Serializable;

public class AppListValueVO implements Serializable{
	
	private String itemkey;
	
	private Object displayvalue;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public Object getDisplayvalue() {
		return displayvalue;
	}

	public void setDisplayvalue(Object displayvalue) {
		this.displayvalue = displayvalue;
	}

}
