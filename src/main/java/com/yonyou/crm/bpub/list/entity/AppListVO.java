package com.yonyou.crm.bpub.list.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class AppListVO implements Serializable {
	
	private Long id;
	
	private List<AppListSpecialVO> speciallist;
	
	private List<List<AppListValueVO>> valuelist;
	
	private Map<String, String> hidelist;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<AppListSpecialVO> getSpeciallist() {
		return speciallist;
	}

	public void setSpeciallist(List<AppListSpecialVO> speciallist) {
		this.speciallist = speciallist;
	}

	public List<List<AppListValueVO>> getValuelist() {
		return valuelist;
	}

	public void setValuelist(List<List<AppListValueVO>> valuelist) {
		this.valuelist = valuelist;
	}

	public Map<String, String> getHidelist() {
		return hidelist;
	}

	public void setHidelist(Map<String, String> hidelist) {
		this.hidelist = hidelist;
	}

}
