package com.yonyou.crm.bpub.list.entity;

import java.io.Serializable;
import java.util.List;

public class AppCodeListVO implements Serializable {
	
	private String id;
	
	private List<AppListValueVO> valuelist;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<AppListValueVO> getValuelist() {
		return valuelist;
	}

	public void setValuelist(List<AppListValueVO> valuelist) {
		this.valuelist = valuelist;
	}

}
