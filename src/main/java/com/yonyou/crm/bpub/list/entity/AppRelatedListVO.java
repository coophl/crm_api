package com.yonyou.crm.bpub.list.entity;

import java.io.Serializable;
import java.util.List;

public class AppRelatedListVO implements Serializable {

	/*
	 * 相关对象类型，如：contact
	 */
	private String type;
	
	/*
	 * 相关对象名称，如：联系人
	 */
	private String name;
	
	/*
	 * 相关对象数量
	 */
	private String num;
	
	/*
	 * 是否支持新增（Y支持,N不支持）
	 */
	private String isnew;
	
	/*
	 * 具体相关对象的行数据显示
	 */
	private List<AppListVO> detaillist;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getIsnew() {
		return isnew;
	}

	public void setIsnew(String isnew) {
		this.isnew = isnew;
	}

	public List<AppListVO> getDetaillist() {
		return detaillist;
	}

	public void setDetaillist(List<AppListVO> detaillist) {
		this.detaillist = detaillist;
	}
}
