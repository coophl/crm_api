package com.yonyou.crm.bpub.topbar.rmitf;

import com.yonyou.crm.bpub.topbar.entity.TopbarVO;


public interface ITopbarRmService {

	public TopbarVO getTopbar(String module);
	
}
