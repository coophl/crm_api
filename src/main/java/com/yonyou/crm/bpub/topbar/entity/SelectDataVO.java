package com.yonyou.crm.bpub.topbar.entity;

import java.io.Serializable;
import java.util.*;

public class SelectDataVO implements Serializable{
	
	/**
	 * APP  item of select data
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 筛选关键字的字段名称(多选为复数，其他类型为单数)
	 */
	private String key;
	
	/**
	 * 筛选关键字的名称
	 */
	private String title;
	
	/**
	 * 筛选关键字的类型
	 */
	private String type;
	
	/**
	 * 参照referto
	 */
	private String referto;
	
	/**
	 * 筛选项的取值列表
	 */
	private List<DataItemVO> data = new ArrayList<>();

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReferto() {
		return referto;
	}

	public void setReferto(String referto) {
		this.referto = referto;
	}

	public List<DataItemVO> getData() {
		return data;
	}

	public void setData(List<DataItemVO> data) {
		this.data = data;
	}
	
}
