package com.yonyou.crm.bpub.topbar.entity;

import java.io.Serializable;
import java.util.*;

public class TopbarVO implements Serializable{
	
	/**
	 * APP topbar
	 */
	private static final long serialVersionUID = 1L;

	private PlanVO presetOptions;
	
	private String searchPlaceholder;
	
	private PlanVO sortData;
	
	private List<SelectDataVO> selectData;

	public PlanVO getPresetOptions() {
		return presetOptions;
	}

	public void setPresetOptions(PlanVO presetOptions) {
		this.presetOptions = presetOptions;
	}

	public String getSearchPlaceholder() {
		return searchPlaceholder;
	}

	public void setSearchPlaceholder(String searchPlaceholder) {
		this.searchPlaceholder = searchPlaceholder;
	}

	public PlanVO getSortData() {
		return sortData;
	}

	public void setSortData(PlanVO sortData) {
		this.sortData = sortData;
	}

	public List<SelectDataVO> getSelectData() {
		return selectData;
	}

	public void setSelectData(List<SelectDataVO> selectData) {
		this.selectData = selectData;
	}
	
}
 