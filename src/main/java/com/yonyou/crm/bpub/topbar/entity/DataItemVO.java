package com.yonyou.crm.bpub.topbar.entity;

import java.io.Serializable;

public class DataItemVO implements Serializable{
	
	/**
	 * APP data item of search plan, sort plan, select data  
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 区间取值的key、排序或预置查询方案的值、筛选项的值
	 */
	private String id;
	
	/**
	 * 区间取值的文本、排序或预置查询方案的名称、筛选项的名称
	 */
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public DataItemVO(String id, String value) {
		this.id = id;
		this.value = value;
	}
	
	
}
