package com.yonyou.crm.bpub.topbar.entity;

import java.io.Serializable;
import java.util.*;

public class PlanVO implements Serializable{
	
	/**
	 * APP search plan or sort plan
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 排序或预置查询方案列表的名称
	 */
	private String title;
	
	/**
	 * 排序或预置查询方案的列表
	 */
	private List<DataItemVO> data;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<DataItemVO> getData() {
		return data;
	}

	public void setData(List<DataItemVO> data) {
		this.data = data;
	}
	
	
}
