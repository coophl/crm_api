package com.yonyou.crm.tpub.excel.rmitf;

import com.yonyou.crm.tpub.excel.entity.ExcelDataRuleVO;

public interface IExcelRmService {

	public String getValueByName(int dataType, ExcelDataRuleVO ruleVO, String cellValue);
}
