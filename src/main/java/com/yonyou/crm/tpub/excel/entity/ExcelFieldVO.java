package com.yonyou.crm.tpub.excel.entity;

import java.io.Serializable;

public class ExcelFieldVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 字段名
	 */
	private String fieldName;
	
	/**
	 * 字段类型
	 */
	private Class<?> fieldType;
	
	/**
	 * 列名
	 */
	private String cellName;
	
	/**
	 * 是否必输
	 */
	private boolean isRequired;
	
	/**
	 * 数据类型
	 */
	private Integer dataType;
	
	/**
	 * 数据规则
	 */
	private ExcelDataRuleVO dataRule;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<?> getFieldType() {
		return fieldType;
	}

	public void setFieldType(Class<?> fieldType) {
		this.fieldType = fieldType;
	}

	public String getCellName() {
		return cellName;
	}

	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public int getDataType() {
		
		if (dataType == null || dataType.intValue() <= 0) {
			return ExcelDataTypeEnum.STRING.getValue();
		}
		
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public ExcelDataRuleVO getDataRule() {
		return dataRule;
	}

	public void setDataRule(ExcelDataRuleVO dataRule) {
		this.dataRule = dataRule;
	}
}
