package com.yonyou.crm.tpub.excel.entity;

import java.io.Serializable;
import java.util.List;

public class ExcelTemplateVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 字段列表
	 */
	private List<ExcelFieldVO> fieldList;
	
	/**
	 * 实体名称
	 */
	private String entityName;
	
	/**
	 * 实体类路径
	 */
	private Class<?> entityClass;
	
	/**
	 * 新增、查询服务
	 */
	private Class<?> service;
	
	/**
	 * 新增、查询方法
	 */
	private String method;
	
	/**
	 * 值处理器，各自模板对值的处理
	 */
	private Class<?> valueHandler;
	
	public List<ExcelFieldVO> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<ExcelFieldVO> fieldList) {
		this.fieldList = fieldList;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public Class<?> getService() {
		return service;
	}

	public void setService(Class<?> service) {
		this.service = service;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Class<?> getValueHandler() {
		return valueHandler;
	}

	public void setValueHandler(Class<?> valueHandler) {
		this.valueHandler = valueHandler;
	}
	
}
