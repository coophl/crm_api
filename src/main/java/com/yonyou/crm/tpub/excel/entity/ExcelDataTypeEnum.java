package com.yonyou.crm.tpub.excel.entity;

public enum ExcelDataTypeEnum {

	STRING(1,"文本"),NUMERIC(2,"数值"),DATE(3,"日期"),REF(4,"参照"),ENUM(5,"枚举"),DOC(6,"档案");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private ExcelDataTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
