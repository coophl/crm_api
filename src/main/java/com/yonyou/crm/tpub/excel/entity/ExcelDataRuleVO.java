package com.yonyou.crm.tpub.excel.entity;

import java.io.Serializable;

public class ExcelDataRuleVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 参照表名
	 */
	private String refTableName;
	
	/**
	 * 参照列
	 */
	private String refColumnName;

	/**
	 * 枚举code
	 */
	private String enumCode;
	
	/**
	 * 格式
	 */
	private String format;
	
	public ExcelDataRuleVO() {}
	
	//参照
	public ExcelDataRuleVO(String refTableName, String refColumnName) {
		this.refTableName = refTableName;
		this.refColumnName = refColumnName;
	}
	
	//枚举、档案
	public ExcelDataRuleVO(String enumCode) {
		this.enumCode = enumCode;
	}


	public String getRefTableName() {
		return refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getRefColumnName() {
		return refColumnName;
	}

	public void setRefColumnName(String refColumnName) {
		this.refColumnName = refColumnName;
	}

	public String getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
