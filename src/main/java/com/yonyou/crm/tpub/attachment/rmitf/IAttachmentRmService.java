package com.yonyou.crm.tpub.attachment.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.tpub.attachment.entity.AttachmentVO;

public interface IAttachmentRmService {
	
	public List<AttachmentVO> getList(Map<String, Object> param);
	
	public AttachmentVO save(AttachmentVO vo);

	public AttachmentVO idGet(Long id);

	public int batchDelete(String[] fileNames);
	
	public boolean isExistName(Integer objType, Long objId, String name);
	
	public int getCountByObjId(Integer objType, Long objId);
}
