package com.yonyou.crm.tpub.attachment.entity;

public class AttachmentConstant {

	public static final String ERROR_EXCEL_DIR = "ErrorExcel";
	public static final String ATTACHMENT_DIR = "Attachment";
	public static final String APPROVAL_DIR = "Approval";
}
