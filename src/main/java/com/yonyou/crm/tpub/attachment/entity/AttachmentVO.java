package com.yonyou.crm.tpub.attachment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AttachmentVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 服务器
     */
    private Long serverId;
    
    /**
     * 业务对象类型ID
     */
    private Integer objType;
    
    /**
     * 业务对象具体单据ID
     */
    private Long objId;
    /**
     * 名称
     */
    private String name;

    /**
     * url
     */
    private String url;

    /**
     * 类型
     */
    private String type;

    /**
     * 大小
     */
    private BigDecimal size;

    /**
     * 
     */
    private Date uploadTime;

    /**
     * 上传人
     */
    private Long uploadUserId;
    
    /**
     * 上传人名称
     */
    private String uploadUserName;

    /**
     * tpub_attachment
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 服务器
     * @return server_id 服务器
     */
    public Long getServerId() {
        return serverId;
    }

    /**
     * 服务器
     * @param serverId 服务器
     */
    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * url
     * @return url url
     */
    public String getUrl() {
        return url;
    }

    /**
     * url
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 类型
     * @return type 类型
     */
    public String getType() {
        return type;
    }

    /**
     * 类型
     * @param type 类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 大小
     * @return size 大小
     */
    public BigDecimal getSize() {
        return size;
    }

    /**
     * 大小
     * @param size 大小
     */
    public void setSize(BigDecimal size) {
        this.size = size;
    }

    /**
     * 
     * @return upload_time 
     */
    public Date getUploadTime() {
        return uploadTime;
    }

    /**
     * 
     * @param uploadTime 
     */
    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    /**
     * 上传人
     * @return upload_user_id 上传人
     */
    public Long getUploadUserId() {
        return uploadUserId;
    }

    /**
     * 上传人
     * @param uploadUserId 上传人
     */
    public void setUploadUserId(Long uploadUserId) {
        this.uploadUserId = uploadUserId;
    }

	/**
	 * 上传人名称
	 * @return uploadUserName
	 */
	public String getUploadUserName() {
		return uploadUserName;
	}

	/**
	 * @param uploadUserName 上传人名称
	 */
	public void setUploadUserName(String uploadUserName) {
		this.uploadUserName = uploadUserName;
	}

	public Integer getObjType() {
		return objType;
	}

	public void setObjType(Integer objType) {
		this.objType = objType;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}
}