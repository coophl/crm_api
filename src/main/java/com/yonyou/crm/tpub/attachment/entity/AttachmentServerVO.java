package com.yonyou.crm.tpub.attachment.entity;

import java.io.Serializable;
import java.util.Date;

public class AttachmentServerVO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private Long tenantId;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private String config;

    /**
     * 停启用状态，1启用2停用
     */
    private Integer enableState;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * tpub_attachment_server
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return tenant_id 
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 
     * @param tenantId 
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 
     * @return name 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return config 
     */
    public String getConfig() {
        return config;
    }

    /**
     * 
     * @param config 
     */
    public void setConfig(String config) {
        this.config = config;
    }

    /**
     * 停启用状态，1启用2停用
     * @return enable_state 停启用状态，1启用2停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 停启用状态，1启用2停用
     * @param enableState 停启用状态，1启用2停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }
}