package com.yonyou.crm.util;

public class QueryUtils {
	
	private StringBuilder sql;
	
	public QueryUtils() {
		this.sql = new StringBuilder();
	}
	/**
	 * 对sql添加arg返回QueryUtils
	 * @param arg
	 * @return
	 */
	public QueryUtils append(String arg) {
		if(arg != null) {
			this.sql.append(arg+" ");			
		}
		return this;
	}
	/**
	 * 对sql添加arg(Object.toString())返回QueryUtils
	 * @param arg
	 * @return
	 */
	public QueryUtils append(Object arg) {
		if(arg != null) {
			this.sql.append(arg.toString());			
		}
		return this;
	}
	/**
	 * 对sql添加(key,value)=> "key = 'value'"返回QueryUtils
	 * @param arg
	 * @return
	 */
	public QueryUtils append(String name, String value) {
        if (value != null) {
            this.sql.append(name);
            this.sql.append("='");
            this.sql.append(value);
            this.sql.append("' ");
        }
        return this;
    }
	/**
	 * 对sql添加(key,value(Object.toString))=> "key = 'value'"返回QueryUtils
	 * @param arg
	 * @return
	 */
	public QueryUtils append(String name, Object value) {
        if (value != null) {
            this.sql.append(name);
            this.sql.append("='");
            this.sql.append(value.toString());
            this.sql.append("' ");
        }
        return this;
    }
	
	/**
     * 对于String数组值构造in条件
     * 
     * @param name 字段名
     * @param values String数组值
     */
    public QueryUtils append(String name, String[] values) {
        if (values.length == 1) {
            this.append(name, values[0]);
        } else {
            this.sql.append(name);
            this.in(name, values);
        }
        return this;
    }
    /**
     * 对于主键数组值构造in条件
     * 
     * @param name 字段名
     * @param values String数组值
     */
    public QueryUtils append(String name, Long[] values) {
        if (values.length == 1) {
            this.append(name, values[0]);
        } else {
            this.sql.append(name);
            this.in(name, values);
        }
        return this;
    }
	
    private QueryUtils in(String name,String[] values) {
    	this.inString(values);
    	return this;
    }
    
    private QueryUtils in(String name,Long[] values) {
    	this.inString(values);
    	return this;
    }
    
    private void inString(String[] values) {
    	int length = values.length;
        this.sql.append(" in (");
        if (values != null && values.length > 0) {
            for (int i = 0; i < length; i++) {
                this.sql.append("'");
                this.sql.append(values[i]);
                this.sql.append("'");
                this.sql.append(",");
            }
            length = this.sql.length();
            this.sql.deleteCharAt(length - 1);
            this.sql.append(") ");
        }
    }
    private void inString(Long[] values) {
    	int length = values.length;
        this.sql.append(" in (");
        if (values != null && values.length > 0) {
            for (int i = 0; i < length; i++) {
                this.sql.append(values[i]);
                this.sql.append(",");
            }
            length = this.sql.length();
            this.sql.deleteCharAt(length - 1);
            this.sql.append(") ");
        }
    }
	@Override
	public String toString() {
		return this.sql.toString();
	}
}
