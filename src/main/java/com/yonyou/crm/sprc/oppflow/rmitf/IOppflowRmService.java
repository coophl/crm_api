package com.yonyou.crm.sprc.oppflow.rmitf;


import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sprc.oppflow.entity.OppflowVO;

import java.util.List;
import java.util.Map;

public interface IOppflowRmService {

	Page<OppflowVO> getList(Page<OppflowVO> requestPage, Map<String, Object> searchMap);
	OppflowVO detail(String id);
	OppflowVO insert(OppflowVO vo, List<Map<String, Object>> flowStageList, List<Map<String, Object>> stageActionList);
	OppflowVO update(OppflowVO vo, List<Map<String, Object>> flowStageList, List<Map<String, Object>> stageActionList);
	List<OppflowVO> batchUpdateEnableState(String[] idArray, Integer enableState, Map<String, Object> searchMap);

   int delete(String idStr);

    Map<String,Object> getDetail(Long id);

    List<OppflowVO> getList(Map<String, Object> searchMap);

	Map<String,Object> getBizType();

    public int checkBiztypeRepeat(String biztype);
}
