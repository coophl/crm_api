package com.yonyou.crm.sprc.oppflow.entity;

import java.io.Serializable;
import java.util.Date;

public class OppflowVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否预置,1是2否
     */
    private Integer isPreseted;

    private String isPresetedName;

    /**
     * 使用业务类型
     */
    private Long biztype;

    private String biztypeName;

    /**
     * 流程状态
     */
    private Integer flowState;

    private String flowStateName;

    /**
     * 流程生效时间
     */
    private Date effectTime;

    /**
     * 启用标识(启用1，未启用2)
     */
    private Integer enableState;

    private String enableStateName;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 租户
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Integer isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * sprc_oppflow
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 描述
     * @return description 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 是否预置,1是2否
     * @return is_preseted 是否预置,1是2否
     */
    public Integer getIsPreseted() {
        return isPreseted;
    }

    /**
     * 是否预置,1是2否
     * @param isPreseted 是否预置,1是2否
     */
    public void setIsPreseted(Integer isPreseted) {
        this.isPreseted = isPreseted;
    }

    /**
     * 使用业务类型
     * @return biztype 使用业务类型
     */
    public Long getBiztype() {
        return biztype;
    }

    /**
     * 使用业务类型
     * @param biztype 使用业务类型
     */
    public void setBiztype(Long biztype) {
        this.biztype = biztype;
    }

    /**
     * 流程状态
     * @return flow_state 流程状态
     */
    public Integer getFlowState() {
        return flowState;
    }

    /**
     * 流程状态
     * @param flowState 流程状态
     */
    public void setFlowState(Integer flowState) {
        this.flowState = flowState;
    }

    /**
     * 流程生效时间
     * @return effect_time 流程生效时间
     */
    public Date getEffectTime() {
        return effectTime;
    }

    /**
     * 流程生效时间
     * @param effectTime 流程生效时间
     */
    public void setEffectTime(Date effectTime) {
        this.effectTime = effectTime;
    }

    /**
     * 启用标识(启用1，未启用2)
     * @return enable_state 启用标识(启用1，未启用2)
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 启用标识(启用1，未启用2)
     * @param enableState 启用标识(启用1，未启用2)
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 租户
     * @return tenant_id 租户
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户
     * @param tenantId 租户
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    public String getIsPresetedName() {
        return isPresetedName;
    }

    public void setIsPresetedName(String isPresetedName) {
        this.isPresetedName = isPresetedName;
    }

    public String getBiztypeName() {
        return biztypeName;
    }

    public void setBiztypeName(String biztypeName) {
        this.biztypeName = biztypeName;
    }

    public String getFlowStateName() {
        return flowStateName;
    }

    public void setFlowStateName(String flowStateName) {
        this.flowStateName = flowStateName;
    }

    public String getEnableStateName() {
        return enableStateName;
    }

    public void setEnableStateName(String enableStateName) {
        this.enableStateName = enableStateName;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */

    public void setTs(Date ts) {
        this.ts = ts;
    }


}