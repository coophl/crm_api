package com.yonyou.crm.sprc.oppdimension.rmitf;


import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sprc.oppdimension.entity.OppdimensionVO;

import java.util.List;
import java.util.Map;

public interface IOppdimensionRmService {

	Page<OppdimensionVO> getList(Page<OppdimensionVO> requestPage, Map<String, Object> searchMap);
	OppdimensionVO detail(String id);
	OppdimensionVO insert(OppdimensionVO vo);
	OppdimensionVO update(OppdimensionVO vo);
	List<OppdimensionVO> getAll();
	Page<OppdimensionVO> batchDelete(String[] idArray, Page<OppdimensionVO> requestPage, Map<String, Object> searchMap);
	Page<OppdimensionVO> batchUpdateEnableState(String[] idArray, Integer enableState, Page<OppdimensionVO> requestPage, Map<String, Object> searchMap);
}
