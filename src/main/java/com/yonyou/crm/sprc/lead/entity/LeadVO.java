package com.yonyou.crm.sprc.lead.entity;

import java.io.Serializable;
import java.util.Date;

import com.yonyou.crm.sys.user.entity.UserVO;

public class LeadVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Integer isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别,1男2女
     */
    private Integer gender;
    
    /**
     * 性别名称
     */
    private String genderName;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 职务
     */
    private Long post;
    
    /**
     * 职务名称
     */
    private String postName;

    /**
     * 固话
     */
    private String tel;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 网址
     */
    private String website;

    /**
     * 省
     */
    private Integer province;
    
    /**
     * 省名称
     */
    private String provinceName;

    /**
     * 市
     */
    private Integer city;
    
    /**
     * 市名称
     */
    private String cityName;

    /**
     * 区县
     */
    private Integer district;
    
    /**
     * 区县名称
     */
    private String districtName;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 档案，线索来源
     */
    private Long source;
    
    /**
     * 来源名称
     */
    private String sourceName;

    /**
     * 状态，1未处理2成功转化3失败关闭
     */
    private Integer state;
    
    /**
     * 状态名称
     */
    private String stateName;

    /**
     * 档案，线索等级
     */
    private Long level;
    
    /**
     * 等级名称
     */
    private String levelName;

    /**
     * 行业
     */
    private Long industryId;
    
    /**
     * 行业名称
     */
    private String industryName;

    /**
     * 负责人
     */
    private Long ownerUserId;
    
    /**
     * 负责人
     */
    private UserVO ownerUserInfo;
    

    /**
     * 失败原因
     */
    private Integer closeReason;
    
    /**
     * 失败原因名称
     */
    private String closeReasonName;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 跟进时间
     */
    private Date followTime;
    
    /**
     * 关注标识
     */
    private String followFlag;

    /**
     * sprc_lead
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 性别,1男2女
     * @return gender 性别,1男2女
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别,1男2女
     * @param gender 性别,1男2女
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 性别名称
	 * @return genderName
	 */
	public String getGenderName() {
		return genderName;
	}

	/**
	 * @param genderName 性别名称
	 */
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}

	/**
     * 公司名称
     * @return company_name 公司名称
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 公司名称
     * @param companyName 公司名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 职务
     * @return post 职务
     */
    public Long getPost() {
        return post;
    }

    /**
     * 职务
     * @param post 职务
     */
    public void setPost(Long post) {
        this.post = post;
    }

    public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

    /**
     * 固话
     * @return tel 固话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 固话
     * @param tel 固话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 手机
     * @return mobile 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 邮箱
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 网址
     * @return website 网址
     */
    public String getWebsite() {
        return website;
    }

    /**
     * 网址
     * @param website 网址
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * 省
     * @return province 省
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 省
     * @param province 省
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 省名称
	 * @return provinceName
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * @param provinceName 省名称
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
     * 市
     * @return city 市
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 市
     * @param city 市
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 市名称
	 * @return cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName 市名称
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
     * 区县
     * @return district 区县
     */
    public Integer getDistrict() {
        return district;
    }

    /**
     * 区县
     * @param district 区县
     */
    public void setDistrict(Integer district) {
        this.district = district;
    }

    /**
     * 区县名称
	 * @return districtName
	 */
	public String getDistrictName() {
		return districtName;
	}

	/**
	 * @param districtName 区县名称
	 */
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
     * 详细地址
     * @return address 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 详细地址
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 档案，线索来源
     * @return source
     */
    public Long getSource() {
        return source;
    }

    /**
     * 档案，线索来源
     * @param source
     */
    public void setSource(Long source) {
        this.source = source;
    }

    /**
     * 来源名称
	 * @return sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName 来源名称
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
     * 状态，1未处理2成功转化3失败关闭
     * @return state 状态，1未处理2成功转化3失败关闭
     */
    public Integer getState() {
        return state;
    }

    /**
     * 状态，1未处理2成功转化3失败关闭
     * @param state 状态，1未处理2成功转化3失败关闭
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 状态名称
	 * @return stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName 状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
     * 档案，线索等级
     * @return level
     */
    public Long getLevel() {
        return level;
    }

    /**
     * 档案，线索等级
     * @param level
     */
    public void setLevel(Long level) {
        this.level = level;
    }

    /**
     * 等级名称
	 * @return levelName
	 */
	public String getLevelName() {
		return levelName;
	}

	/**
	 * @param levelName 等级名称
	 */
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	/**
     * 行业
     * @return industry_id 行业
     */
    public Long getIndustryId() {
        return industryId;
    }

    /**
     * 行业
     * @param industryId 行业
     */
    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    /**
     * 行业名称
	 * @return industryName
	 */
	public String getIndustryName() {
		return industryName;
	}

	/**
	 * @param industryName 行业名称
	 */
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	/**
     * 负责人
     * @return owner_user_id 负责人
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }
    
	/**
     * 负责人
     * @param ownerUserId 负责人
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }
    
    /**
     * 负责人信息
     * @return owneruserinfo 负责人信息
     */
    public UserVO getOwnerUserInfo() {
		return ownerUserInfo;
	}

    /**
     * 负责人信息
     * @param ownerUserId 负责人信息
     */
	public void setOwnerUserInfo(UserVO ownerUserInfo) {
		this.ownerUserInfo = ownerUserInfo;
	}

    /**
     * 失败原因
     * @return close_reason 失败原因
     */
    public Integer getCloseReason() {
        return closeReason;
    }

    /**
     * 失败原因
     * @param closeReason 失败原因
     */
    public void setCloseReason(Integer closeReason) {
        this.closeReason = closeReason;
    }

    /**
     * 失败原因名称
	 * @return closeReasonName
	 */
	public String getCloseReasonName() {
		return closeReasonName;
	}

	/**
	 * @param closeReasonName 失败原因名称
	 */
	public void setCloseReasonName(String closeReasonName) {
		this.closeReasonName = closeReasonName;
	}

	/**
     * 备注
     * @return remarks 备注
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 备注
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 跟进时间
     * @return follow_time 跟进时间
     */
    public Date getFollowTime() {
        return followTime;
    }

    /**
     * 跟进时间
     * @param followTime 跟进时间
     */
    public void setFollowTime(Date followTime) {
        this.followTime = followTime;
    }

	public String getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(String followFlag) {
		this.followFlag = followFlag;
	}
}