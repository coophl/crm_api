package com.yonyou.crm.sprc.lead.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sprc.lead.entity.LeadVO;

public interface ILeadRmService {

	public Page<LeadVO> page(Page<LeadVO> page,Map<String, Object> paraMap);
	public LeadVO detail(Long id);
	public LeadVO insert(LeadVO vo);
	
	/**
	 * 关联业务对象新增
	 */
	public LeadVO insert(LeadVO vo, Map<String, Object> param);
	
	public LeadVO update(LeadVO vo);
	public String batchDelete(String[] ids);
	public Page<LeadVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<LeadVO> page,Map<String, Object> paraMap);
	
	public int insertFollow(Long id);
	public int deleteFollow(Long id);
}
