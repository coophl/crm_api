package com.yonyou.crm.sprc.oppaction.rmitf;


import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sprc.oppaction.entity.OppactionVO;
import com.yonyou.crm.sprc.oppdimension.entity.OppdimensionVO;

import java.util.List;
import java.util.Map;

public interface IOppactionRmService {

	Page<OppactionVO> getList(Page<OppactionVO> requestPage, Map<String, Object> searchMap);
	OppactionVO detail(String id);
	OppactionVO insert(OppactionVO vo);
	OppactionVO update(OppactionVO vo);
	Page<OppactionVO> batchDelete(String[] idArray, Page<OppactionVO> requestPage, Map<String, Object> searchMap);
	Page<OppactionVO> batchUpdateEnableState(String[] idArray, Integer enableState, Page<OppactionVO> requestPage, Map<String, Object> searchMap);

    List<OppactionVO> getByDimension(List dimIds);
}
