package com.yonyou.crm.sprc.oppstage.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sprc.oppstage.entity.OppstageVO;

public interface IOppstageRmService {
	
	List<OppstageVO> list(Map<String, Object> paraMap);
	OppstageVO detail(Long id);
	OppstageVO insert(OppstageVO vo);
	OppstageVO update(OppstageVO vo);
	List<OppstageVO> batchDelete(String[] ids, Map<String, Object> paraMap);
	List<OppstageVO> batchUpdateEnableState(String[] ids, Integer enableState, Map<String, Object> paraMap);

    List<OppstageVO> getStageByBiztype(String biztype);
    List<OppstageVO> getStageByTenant();
}
