package com.yonyou.crm.sprc.opportunity.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sprc.opportunity.entity.AppOpportunityStageVO;
import com.yonyou.crm.sprc.opportunity.entity.OpportunityBVO;
import com.yonyou.crm.sprc.opportunity.entity.OpportunityVO;

public interface IOpportunityRmService {

	Page<OpportunityVO> getList(Page<OpportunityVO> requestPage, Map<String, Object> map);

	OpportunityVO getDetail(Long id);

	OpportunityVO insert(OpportunityVO opportunity);

	OpportunityVO insert(OpportunityVO opportunity,List<Map<String,Object>> stageList,List<Map<String,Object>> actionList);
	
	OpportunityVO insert(OpportunityVO opportunity, Map<String, Object> param);

	OpportunityVO update(OpportunityVO opportunity);

	Map <String, Object> batchDelete(String[] idArray, Page<OpportunityVO> requestPage, Map<String, Object> map);

	Page<OpportunityBVO> getChildList(Page<OpportunityBVO> childRequestPage, Map<String, Object> searchMap, Long opportunityID);

	public int delete(Long id);

    List getFunnel( Map<String, Object> searchMap);
    
    public Map<String, Object> getFilterConditon();
    
    List<AppOpportunityStageVO> getStageListForOpportunity(Long id);
    
    public List<AppOpportunityStageVO> stageInfo(Map<String, Object> param);
    
    public int saveAction(Map<String, Object> param);
    
    public int insertFollow(Long id);
    
	public int deleteFollow(Long id);

	List<Map<String,Object>> getStageBybiztype(Long type);

	List<Map<String,Object>> getActionBybiztype(Long type);

	List<Map<String,Object>> getResultById(Long id);

	List<Map<String,Object>> getStageById(Long id);
	
	public void win(Map<String, Object> paramMap);
	
	public void lose(Map<String, Object> paramMap);

    public int finishAction(Map<String,Object> paramMap);

	public int setCurrentStage(Map<String,Object> paramMap);

	public Map<String,Object> getFunnelMoney(Map<String, Object> searchMap);


	public OpportunityVO winOpp(OpportunityVO vo);

	public OpportunityVO lostOpp(OpportunityVO vo);
}
