package com.yonyou.crm.sprc.opportunity.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sys.user.entity.UserVO;

public class OpportunityVO implements Serializable {
	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 商机名称
	 */
	private String name;

	/**
	 * 编码
	 */
	private String code;

	/**
	 * 客户
	 */
	private Long customerId;
	
	/**
	 * 客户信息
	 */
	private CustomerVO customerInfo;

	/**
	 * 客户名称  参照显示使用
	 */
	private String customerName;
	/**
	 * 商机时间
	 */
	private Date createdTime;
	/**
	 * 商机类型
	 */
	private Long type;

	/**
	 * 商机类型名称	参照显示使用
	 */
	private String typeName;

	/**
	 * 商机状态(单选，枚举)
	 */
	private Integer state;
	/**
	 * 商机状态名称 	枚举显示
	 */
	private String stateName;

	/**
	 * 负责人
	 */
	private Long ownerUserId;
	/**
	 * 负责人名称		参照显示使用
	 */
	private String ownerUserName;

	/**
	 * 商机阶段(单选，参照商机类型创建的商机阶段枚举)
	 */
	private Integer saleStage;
	/**
	 * 商机阶段名称(单选，参照商机类型创建的商机阶段枚举)	枚举显示
	 */
	private String saleStageName;

	/**
	 * 客户预算
	 */
	private BigDecimal customerBudget;

	/**
	 * 赢单概率
	 */
	private String winProbability;

	/**
	 * 阶段开始时间
	 */
	private Date stageStartTime;


	/**
	 * 阶段停留时间
	 */
	private String stageStayTime;

	public String getStageStayTime() {
		return stageStayTime;
	}

	public void setStageStayTime(String stageStayTime) {
		this.stageStayTime = stageStayTime;
	}

	/**
	 * 预计签单时间
	 */
	private Date expectSignTime;

	/**
	 * 预计签单金额
	 */
	private BigDecimal expectSignMoney;

	/**
	 * 实际完成时间
	 */
	private Date actualSignTime;

	/**
	 * 实际签单金额
	 */
	private BigDecimal actualSignMoney;

	/**
	 * 商机来源(单选，枚举)
	 */
	private Integer source;

	/**
	 *  商机来源(单选，枚举)	枚举显示
	 */
	private String sourceName;

	/**
	 * 部门
	 */
	private Long deptId;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 赢单原因（多选，枚举 逗号分隔）
	 */
	private Long winReason;

	/**
	 * 赢单原因（多选，枚举 逗号分隔）	多枚举值显示
	 */
	private String winReasonName;

	/**
	 * 丢单原因 （多选，枚举 逗号分隔）
	 */
	private Long failReason;
	/**
	 * 丢单原因 （多选，枚举 逗号分隔）	多枚举值显示
	 */
	private String failReasonName;
	/**
	 * 客户需求
	 */
	private String customerRequest;

	/**
	 * 备注
	 */
	private String description;
	
	/**
     * 关注标识
     */
    private String followFlag;
    
    /**
     * 是否有商机阶段标识
     */
    private Integer stageFlag;

	/**
	 * 组织
	 */
	private Long orgId;

	/**
	 * 组织名称
	 */
	private String orgName;

	/**
	 * 租户
	 */
	private Long tenantId;

	/**
	 * 是否已删除
	 */
	private Byte isDeleted;

	/**
	 * 删除人
	 */
	private Long deletedUserId;

	/**
	 * 删除时间
	 */
	private Date deletedTime;

	/**
	 * 记录的创建时间
	 */
	private Date sysCreatedTime;

	/**
	 * 记录的创建人
	 */
	private Long sysCreatedUserId;

	/**
	 * 记录的修改时间
	 */
	private Date sysModifiedTime;

	/**
	 * 记录的修改人
	 */
	private Long sysModifiedUserId;

	/**
	 * 自定义项1
	 */
	private String def1;

	/**
	 * 自定义项2
	 */
	private String def2;

	/**
	 * 自定义项3
	 */
	private String def3;

	/**
	 * 自定义项4
	 */
	private String def4;

	/**
	 * 自定义项5
	 */
	private String def5;

	/**
	 * 自定义项6
	 */
	private String def6;

	/**
	 * 自定义项7
	 */
	private String def7;

	/**
	 * 自定义项8
	 */
	private String def8;

	/**
	 * 自定义项9
	 */
	private String def9;

	/**
	 * 自定义项10
	 */
	private String def10;

	/**
	 * 自定义项11
	 */
	private String def11;

	/**
	 * 自定义项12
	 */
	private String def12;

	/**
	 * 自定义项13
	 */
	private String def13;

	/**
	 * 自定义项14
	 */
	private String def14;

	/**
	 * 自定义项15
	 */
	private String def15;

	/**
	 * 自定义项16
	 */
	private String def16;

	/**
	 * 自定义项17
	 */
	private String def17;

	/**
	 * 自定义项18
	 */
	private String def18;

	/**
	 * 自定义项19
	 */
	private String def19;

	/**
	 * 自定义项20
	 */
	private String def20;

	/**
	 * 自定义项21
	 */
	private String def21;

	/**
	 * 自定义项22
	 */
	private String def22;

	/**
	 * 自定义项23
	 */
	private String def23;

	/**
	 * 自定义项24
	 */
	private String def24;

	/**
	 * 自定义项25
	 */
	private String def25;

	/**
	 * 自定义项26
	 */
	private String def26;

	/**
	 * 自定义项27
	 */
	private String def27;

	/**
	 * 自定义项28
	 */
	private String def28;

	/**
	 * 自定义项29
	 */
	private String def29;

	/**
	 * 自定义项30
	 */
	private String def30;

	/**
	 * 自定义项31
	 */
	private String def31;

	/**
	 * 自定义项32
	 */
	private String def32;

	/**
	 * 自定义项33
	 */
	private String def33;

	/**
	 * 自定义项34
	 */
	private String def34;

	/**
	 * 自定义项35
	 */
	private String def35;

	/**
	 * 自定义项36
	 */
	private String def36;

	/**
	 * 自定义项37
	 */
	private String def37;

	/**
	 * 自定义项38
	 */
	private String def38;

	/**
	 * 自定义项39
	 */
	private String def39;

	/**
	 * 自定义项40
	 */
	private String def40;

	/**
	 * 自定义项41
	 */
	private String def41;

	/**
	 * 自定义项42
	 */
	private String def42;

	/**
	 * 自定义项43
	 */
	private String def43;

	/**
	 * 自定义项44
	 */
	private String def44;

	/**
	 * 自定义项45
	 */
	private String def45;

	/**
	 * 自定义项46
	 */
	private String def46;

	/**
	 * 自定义项47
	 */
	private String def47;

	/**
	 * 自定义项48
	 */
	private String def48;

	/**
	 * 自定义项49
	 */
	private String def49;

	/**
	 * 自定义项50
	 */
	private String def50;

	/**
	 * 时间戳
	 */
	private Date ts;

	/**
	 * 商机明细集合
	 */

	private List<OpportunityBVO> childList;
	private Page<OpportunityBVO> childPage;
	
	/*
	 * 参与人列表
	 */
	private List<UserVO> relUserList;
	

	public List<OpportunityBVO> getChildList() {
		return childList;
	}

	public void setChildList(List<OpportunityBVO> childList) {
		this.childList = childList;
	}

	public Page<OpportunityBVO> getChildPage() {
		return childPage;
	}

	public void setChildPage(Page<OpportunityBVO> childPage) {
		this.childPage = childPage;
	}

	/**
	 * sprc_opportunity
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 * 
	 * @return id 主键
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 主键
	 * 
	 * @param id
	 *            主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 商机名称
	 * 
	 * @return name 商机名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 商机名称
	 * 
	 * @param name
	 *            商机名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 编码
	 * 
	 * @return code 编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 编码
	 * 
	 * @param code
	 *            编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 客户
	 * 
	 * @return customer_id 客户
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * 客户
	 * 
	 * @param customerId
	 *            客户
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public CustomerVO getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerVO customerInfo) {
		this.customerInfo = customerInfo;
	}

	/**
	 * 商机类型
	 * 
	 * @return type 商机类型
	 */
	public Long getType() {
		return type;
	}

	/**
	 * 商机类型
	 * 
	 * @param type
	 *            商机类型
	 */
	public void setType(Long type) {
		this.type = type;
	}

	/**
	 * 商机状态(单选，枚举)
	 * 
	 * @return state 商机状态(单选，枚举)
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * 商机状态(单选，枚举)
	 * 
	 * @param state
	 *            商机状态(单选，枚举)
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * 负责人
	 * 
	 * @return owner_user_id 负责人
	 */
	public Long getOwnerUserId() {
		return ownerUserId;
	}

	/**
	 * 负责人
	 * 
	 * @param ownerUserId
	 *            负责人
	 */
	public void setOwnerUserId(Long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

	/**
	 * 商机阶段(单选，参照商机类型创建的商机阶段枚举)
	 * 
	 * @return sale_stage 商机阶段(单选，参照商机类型创建的商机阶段枚举)
	 */
	public Integer getSaleStage() {
		return saleStage;
	}

	/**
	 * 商机阶段(单选，参照商机类型创建的商机阶段枚举)
	 * 
	 * @param saleStage
	 *            商机阶段(单选，参照商机类型创建的商机阶段枚举)
	 */
	public void setSaleStage(Integer saleStage) {
		this.saleStage = saleStage;
	}

	/**
	 * 客户预算
	 * 
	 * @return customer_budget 客户预算
	 */
	public BigDecimal getCustomerBudget() {
		return customerBudget;
	}

	/**
	 * 客户预算
	 * 
	 * @param customerBudget
	 *            客户预算
	 */
	public void setCustomerBudget(BigDecimal customerBudget) {
		this.customerBudget = customerBudget;
	}

	/**
	 * 赢单概率
	 * 
	 * @return win_probability 赢单概率
	 */
	public String getWinProbability() {
		return winProbability;
	}

	/**
	 * 赢单概率
	 * 
	 * @param winProbability
	 *            赢单概率
	 */
	public void setWinProbability(String winProbability) {
		this.winProbability = winProbability;
	}

	/**
	 * 阶段开始时间
	 * 
	 * @return stageStartTime 阶段停留时间(*天)
	 */
	public Date getStageStartTime() {
		return stageStartTime;
	}

	/**
	 * 阶段开始时间
	 * 
	 * @param stageStartTime
	 *            stageStartTimeTime(*天)
	 */
	public void setStageStartTime(Date stageStartTime) {
		this.stageStartTime = stageStartTime;
	}

	/**
	 * 预计签单时间
	 * 
	 * @return expect_sign_time 预计签单时间
	 */
	public Date getExpectSignTime() {
		return expectSignTime;
	}

	/**
	 * 预计签单时间
	 * 
	 * @param expectSignTime
	 *            预计签单时间
	 */
	public void setExpectSignTime(Date expectSignTime) {
		this.expectSignTime = expectSignTime;
	}

	/**
	 * 预计签单金额
	 * 
	 * @return expect_sign_money 预计签单金额
	 */
	public BigDecimal getExpectSignMoney() {
		return expectSignMoney;
	}

	/**
	 * 预计签单金额
	 * 
	 * @param expectSignMoney
	 *            预计签单金额
	 */
	public void setExpectSignMoney(BigDecimal expectSignMoney) {
		this.expectSignMoney = expectSignMoney;
	}

	/**
	 * 实际完成时间
	 * 
	 * @return actual_sign_time 实际完成时间
	 */
	public Date getActualSignTime() {
		return actualSignTime;
	}

	/**
	 * 实际完成时间
	 * 
	 * @param actualSignTime
	 *            实际完成时间
	 */
	public void setActualSignTime(Date actualSignTime) {
		this.actualSignTime = actualSignTime;
	}

	/**
	 * 实际签单金额
	 * 
	 * @return actual_sign_money 实际签单金额
	 */
	public BigDecimal getActualSignMoney() {
		return actualSignMoney;
	}

	/**
	 * 实际签单金额
	 * 
	 * @param actualSignMoney
	 *            实际签单金额
	 */
	public void setActualSignMoney(BigDecimal actualSignMoney) {
		this.actualSignMoney = actualSignMoney;
	}

	/**
	 * 商机来源(单选，枚举)
	 * 
	 * @return source 商机来源(单选，枚举)
	 */
	public Integer getSource() {
		return source;
	}

	/**
	 * 商机来源(单选，枚举)
	 * 
	 * @param source
	 *            商机来源(单选，枚举)
	 */
	public void setSource(Integer source) {
		this.source = source;
	}

	/**
	 * 部门
	 * 
	 * @return dept_id 部门
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 部门
	 * 
	 * @param deptId
	 *            部门
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * 赢单原因（多选，枚举 逗号分隔）
	 * 
	 * @return win_reason 赢单原因（多选，枚举 逗号分隔）
	 */
	public Long getWinReason() {
		return winReason;
	}

	/**
	 * 赢单原因（多选，枚举 逗号分隔）
	 * 
	 * @param winReason
	 *            赢单原因（多选，枚举 逗号分隔）
	 */
	public void setWinReason(Long winReason) {
		this.winReason = winReason;
	}

	/**
	 * 丢单原因 （多选，枚举 逗号分隔）
	 * 
	 * @return fail_reason 丢单原因 （多选，枚举 逗号分隔）
	 */
	public Long getFailReason() {
		return failReason;
	}

	/**
	 * 丢单原因 （多选，枚举 逗号分隔）
	 * 
	 * @param failReason
	 *            丢单原因 （多选，枚举 逗号分隔）
	 */
	public void setFailReason(Long failReason) {
		this.failReason = failReason;
	}

	public String getCustomerRequest() {
		return customerRequest;
	}

	public void setCustomerRequest(String customerRequest) {
		this.customerRequest = customerRequest;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(String followFlag) {
		this.followFlag = followFlag;
	}

	public Integer getStageFlag() {
		return stageFlag;
	}

	public void setStageFlag(Integer stageFlag) {
		this.stageFlag = stageFlag;
	}

	/**
	 * 组织
	 * 
	 * @return org_id 组织
	 */
	public Long getOrgId() {
		return orgId;
	}

	/**
	 * 组织
	 * 
	 * @param orgId
	 *            组织
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * 租户
	 * 
	 * @return tenant_id 租户
	 */
	public Long getTenantId() {
		return tenantId;
	}

	/**
	 * 租户
	 * 
	 * @param tenantId
	 *            租户
	 */
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getOwnerUserName() {
		return ownerUserName;
	}

	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}

	public String getSaleStageName() {
		return saleStageName;
	}

	public void setSaleStageName(String saleStageName) {
		this.saleStageName = saleStageName;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getWinReasonName() {
		return winReasonName;
	}

	public void setWinReasonName(String winReasonName) {
		this.winReasonName = winReasonName;
	}

	public String getFailReasonName() {
		return failReasonName;
	}

	public void setFailReasonName(String failReasonName) {
		this.failReasonName = failReasonName;
	}

	/**
	 * 是否已删除
	 * 
	 * @return is_deleted 是否已删除
	 */
	public Byte getIsDeleted() {
		return isDeleted;
	}

	/**
	 * 是否已删除
	 * 
	 * @param isDeleted
	 *            是否已删除
	 */
	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * 删除人
	 * 
	 * @return deleted_user_id 删除人
	 */
	public Long getDeletedUserId() {
		return deletedUserId;
	}

	/**
	 * 删除人
	 * 
	 * @param deletedUserId
	 *            删除人
	 */
	public void setDeletedUserId(Long deletedUserId) {
		this.deletedUserId = deletedUserId;
	}

	/**
	 * 删除时间
	 * 
	 * @return deleted_time 删除时间
	 */
	public Date getDeletedTime() {
		return deletedTime;
	}

	/**
	 * 删除时间
	 * 
	 * @param deletedTime
	 *            删除时间
	 */
	public void setDeletedTime(Date deletedTime) {
		this.deletedTime = deletedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @return sys_created_time 记录的创建时间
	 */
	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @param sysCreatedTime
	 *            记录的创建时间
	 */
	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}

	/**
	 * 记录的创建人
	 * 
	 * @return sys_created_user_id 记录的创建人
	 */
	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}

	/**
	 * 记录的创建人
	 * 
	 * @param sysCreatedUserId
	 *            记录的创建人
	 */
	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @return sys_modified_time 记录的修改时间
	 */
	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @param sysModifiedTime
	 *            记录的修改时间
	 */
	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}

	/**
	 * 记录的修改人
	 * 
	 * @return sys_modified_user_id 记录的修改人
	 */
	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	/**
	 * 记录的修改人
	 * 
	 * @param sysModifiedUserId
	 *            记录的修改人
	 */
	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	/**
	 * 自定义项1
	 * 
	 * @return def1 自定义项1
	 */
	public String getDef1() {
		return def1;
	}

	/**
	 * 自定义项1
	 * 
	 * @param def1
	 *            自定义项1
	 */
	public void setDef1(String def1) {
		this.def1 = def1;
	}

	/**
	 * 自定义项2
	 * 
	 * @return def2 自定义项2
	 */
	public String getDef2() {
		return def2;
	}

	/**
	 * 自定义项2
	 * 
	 * @param def2
	 *            自定义项2
	 */
	public void setDef2(String def2) {
		this.def2 = def2;
	}

	/**
	 * 自定义项3
	 * 
	 * @return def3 自定义项3
	 */
	public String getDef3() {
		return def3;
	}

	/**
	 * 自定义项3
	 * 
	 * @param def3
	 *            自定义项3
	 */
	public void setDef3(String def3) {
		this.def3 = def3;
	}

	/**
	 * 自定义项4
	 * 
	 * @return def4 自定义项4
	 */
	public String getDef4() {
		return def4;
	}

	/**
	 * 自定义项4
	 * 
	 * @param def4
	 *            自定义项4
	 */
	public void setDef4(String def4) {
		this.def4 = def4;
	}

	/**
	 * 自定义项5
	 * 
	 * @return def5 自定义项5
	 */
	public String getDef5() {
		return def5;
	}

	/**
	 * 自定义项5
	 * 
	 * @param def5
	 *            自定义项5
	 */
	public void setDef5(String def5) {
		this.def5 = def5;
	}

	/**
	 * 自定义项6
	 * 
	 * @return def6 自定义项6
	 */
	public String getDef6() {
		return def6;
	}

	/**
	 * 自定义项6
	 * 
	 * @param def6
	 *            自定义项6
	 */
	public void setDef6(String def6) {
		this.def6 = def6;
	}

	/**
	 * 自定义项7
	 * 
	 * @return def7 自定义项7
	 */
	public String getDef7() {
		return def7;
	}

	/**
	 * 自定义项7
	 * 
	 * @param def7
	 *            自定义项7
	 */
	public void setDef7(String def7) {
		this.def7 = def7;
	}

	/**
	 * 自定义项8
	 * 
	 * @return def8 自定义项8
	 */
	public String getDef8() {
		return def8;
	}

	/**
	 * 自定义项8
	 * 
	 * @param def8
	 *            自定义项8
	 */
	public void setDef8(String def8) {
		this.def8 = def8;
	}

	/**
	 * 自定义项9
	 * 
	 * @return def9 自定义项9
	 */
	public String getDef9() {
		return def9;
	}

	/**
	 * 自定义项9
	 * 
	 * @param def9
	 *            自定义项9
	 */
	public void setDef9(String def9) {
		this.def9 = def9;
	}

	/**
	 * 自定义项10
	 * 
	 * @return def10 自定义项10
	 */
	public String getDef10() {
		return def10;
	}

	/**
	 * 自定义项10
	 * 
	 * @param def10
	 *            自定义项10
	 */
	public void setDef10(String def10) {
		this.def10 = def10;
	}

	/**
	 * 自定义项11
	 * 
	 * @return def11 自定义项11
	 */
	public String getDef11() {
		return def11;
	}

	/**
	 * 自定义项11
	 * 
	 * @param def11
	 *            自定义项11
	 */
	public void setDef11(String def11) {
		this.def11 = def11;
	}

	/**
	 * 自定义项12
	 * 
	 * @return def12 自定义项12
	 */
	public String getDef12() {
		return def12;
	}

	/**
	 * 自定义项12
	 * 
	 * @param def12
	 *            自定义项12
	 */
	public void setDef12(String def12) {
		this.def12 = def12;
	}

	/**
	 * 自定义项13
	 * 
	 * @return def13 自定义项13
	 */
	public String getDef13() {
		return def13;
	}

	/**
	 * 自定义项13
	 * 
	 * @param def13
	 *            自定义项13
	 */
	public void setDef13(String def13) {
		this.def13 = def13;
	}

	/**
	 * 自定义项14
	 * 
	 * @return def14 自定义项14
	 */
	public String getDef14() {
		return def14;
	}

	/**
	 * 自定义项14
	 * 
	 * @param def14
	 *            自定义项14
	 */
	public void setDef14(String def14) {
		this.def14 = def14;
	}

	/**
	 * 自定义项15
	 * 
	 * @return def15 自定义项15
	 */
	public String getDef15() {
		return def15;
	}

	/**
	 * 自定义项15
	 * 
	 * @param def15
	 *            自定义项15
	 */
	public void setDef15(String def15) {
		this.def15 = def15;
	}

	/**
	 * 自定义项16
	 * 
	 * @return def16 自定义项16
	 */
	public String getDef16() {
		return def16;
	}

	/**
	 * 自定义项16
	 * 
	 * @param def16
	 *            自定义项16
	 */
	public void setDef16(String def16) {
		this.def16 = def16;
	}

	/**
	 * 自定义项17
	 * 
	 * @return def17 自定义项17
	 */
	public String getDef17() {
		return def17;
	}

	/**
	 * 自定义项17
	 * 
	 * @param def17
	 *            自定义项17
	 */
	public void setDef17(String def17) {
		this.def17 = def17;
	}

	/**
	 * 自定义项18
	 * 
	 * @return def18 自定义项18
	 */
	public String getDef18() {
		return def18;
	}

	/**
	 * 自定义项18
	 * 
	 * @param def18
	 *            自定义项18
	 */
	public void setDef18(String def18) {
		this.def18 = def18;
	}

	/**
	 * 自定义项19
	 * 
	 * @return def19 自定义项19
	 */
	public String getDef19() {
		return def19;
	}

	/**
	 * 自定义项19
	 * 
	 * @param def19
	 *            自定义项19
	 */
	public void setDef19(String def19) {
		this.def19 = def19;
	}

	/**
	 * 自定义项20
	 * 
	 * @return def20 自定义项20
	 */
	public String getDef20() {
		return def20;
	}

	/**
	 * 自定义项20
	 * 
	 * @param def20
	 *            自定义项20
	 */
	public void setDef20(String def20) {
		this.def20 = def20;
	}

	/**
	 * 自定义项21
	 * 
	 * @return def21 自定义项21
	 */
	public String getDef21() {
		return def21;
	}

	/**
	 * 自定义项21
	 * 
	 * @param def21
	 *            自定义项21
	 */
	public void setDef21(String def21) {
		this.def21 = def21;
	}

	/**
	 * 自定义项22
	 * 
	 * @return def22 自定义项22
	 */
	public String getDef22() {
		return def22;
	}

	/**
	 * 自定义项22
	 * 
	 * @param def22
	 *            自定义项22
	 */
	public void setDef22(String def22) {
		this.def22 = def22;
	}

	/**
	 * 自定义项23
	 * 
	 * @return def23 自定义项23
	 */
	public String getDef23() {
		return def23;
	}

	/**
	 * 自定义项23
	 * 
	 * @param def23
	 *            自定义项23
	 */
	public void setDef23(String def23) {
		this.def23 = def23;
	}

	/**
	 * 自定义项24
	 * 
	 * @return def24 自定义项24
	 */
	public String getDef24() {
		return def24;
	}

	/**
	 * 自定义项24
	 * 
	 * @param def24
	 *            自定义项24
	 */
	public void setDef24(String def24) {
		this.def24 = def24;
	}

	/**
	 * 自定义项25
	 * 
	 * @return def25 自定义项25
	 */
	public String getDef25() {
		return def25;
	}

	/**
	 * 自定义项25
	 * 
	 * @param def25
	 *            自定义项25
	 */
	public void setDef25(String def25) {
		this.def25 = def25;
	}

	/**
	 * 自定义项26
	 * 
	 * @return def26 自定义项26
	 */
	public String getDef26() {
		return def26;
	}

	/**
	 * 自定义项26
	 * 
	 * @param def26
	 *            自定义项26
	 */
	public void setDef26(String def26) {
		this.def26 = def26;
	}

	/**
	 * 自定义项27
	 * 
	 * @return def27 自定义项27
	 */
	public String getDef27() {
		return def27;
	}

	/**
	 * 自定义项27
	 * 
	 * @param def27
	 *            自定义项27
	 */
	public void setDef27(String def27) {
		this.def27 = def27;
	}

	/**
	 * 自定义项28
	 * 
	 * @return def28 自定义项28
	 */
	public String getDef28() {
		return def28;
	}

	/**
	 * 自定义项28
	 * 
	 * @param def28
	 *            自定义项28
	 */
	public void setDef28(String def28) {
		this.def28 = def28;
	}

	/**
	 * 自定义项29
	 * 
	 * @return def29 自定义项29
	 */
	public String getDef29() {
		return def29;
	}

	/**
	 * 自定义项29
	 * 
	 * @param def29
	 *            自定义项29
	 */
	public void setDef29(String def29) {
		this.def29 = def29;
	}

	/**
	 * 自定义项30
	 * 
	 * @return def30 自定义项30
	 */
	public String getDef30() {
		return def30;
	}

	/**
	 * 自定义项30
	 * 
	 * @param def30
	 *            自定义项30
	 */
	public void setDef30(String def30) {
		this.def30 = def30;
	}

	/**
	 * 自定义项31
	 * 
	 * @return def31 自定义项31
	 */
	public String getDef31() {
		return def31;
	}

	/**
	 * 自定义项31
	 * 
	 * @param def31
	 *            自定义项31
	 */
	public void setDef31(String def31) {
		this.def31 = def31;
	}

	/**
	 * 自定义项32
	 * 
	 * @return def32 自定义项32
	 */
	public String getDef32() {
		return def32;
	}

	/**
	 * 自定义项32
	 * 
	 * @param def32
	 *            自定义项32
	 */
	public void setDef32(String def32) {
		this.def32 = def32;
	}

	/**
	 * 自定义项33
	 * 
	 * @return def33 自定义项33
	 */
	public String getDef33() {
		return def33;
	}

	/**
	 * 自定义项33
	 * 
	 * @param def33
	 *            自定义项33
	 */
	public void setDef33(String def33) {
		this.def33 = def33;
	}

	/**
	 * 自定义项34
	 * 
	 * @return def34 自定义项34
	 */
	public String getDef34() {
		return def34;
	}

	/**
	 * 自定义项34
	 * 
	 * @param def34
	 *            自定义项34
	 */
	public void setDef34(String def34) {
		this.def34 = def34;
	}

	/**
	 * 自定义项35
	 * 
	 * @return def35 自定义项35
	 */
	public String getDef35() {
		return def35;
	}

	/**
	 * 自定义项35
	 * 
	 * @param def35
	 *            自定义项35
	 */
	public void setDef35(String def35) {
		this.def35 = def35;
	}

	/**
	 * 自定义项36
	 * 
	 * @return def36 自定义项36
	 */
	public String getDef36() {
		return def36;
	}

	/**
	 * 自定义项36
	 * 
	 * @param def36
	 *            自定义项36
	 */
	public void setDef36(String def36) {
		this.def36 = def36;
	}

	/**
	 * 自定义项37
	 * 
	 * @return def37 自定义项37
	 */
	public String getDef37() {
		return def37;
	}

	/**
	 * 自定义项37
	 * 
	 * @param def37
	 *            自定义项37
	 */
	public void setDef37(String def37) {
		this.def37 = def37;
	}

	/**
	 * 自定义项38
	 * 
	 * @return def38 自定义项38
	 */
	public String getDef38() {
		return def38;
	}

	/**
	 * 自定义项38
	 * 
	 * @param def38
	 *            自定义项38
	 */
	public void setDef38(String def38) {
		this.def38 = def38;
	}

	/**
	 * 自定义项39
	 * 
	 * @return def39 自定义项39
	 */
	public String getDef39() {
		return def39;
	}

	/**
	 * 自定义项39
	 * 
	 * @param def39
	 *            自定义项39
	 */
	public void setDef39(String def39) {
		this.def39 = def39;
	}

	/**
	 * 自定义项40
	 * 
	 * @return def40 自定义项40
	 */
	public String getDef40() {
		return def40;
	}

	/**
	 * 自定义项40
	 * 
	 * @param def40
	 *            自定义项40
	 */
	public void setDef40(String def40) {
		this.def40 = def40;
	}

	/**
	 * 自定义项41
	 * 
	 * @return def41 自定义项41
	 */
	public String getDef41() {
		return def41;
	}

	/**
	 * 自定义项41
	 * 
	 * @param def41
	 *            自定义项41
	 */
	public void setDef41(String def41) {
		this.def41 = def41;
	}

	/**
	 * 自定义项42
	 * 
	 * @return def42 自定义项42
	 */
	public String getDef42() {
		return def42;
	}

	/**
	 * 自定义项42
	 * 
	 * @param def42
	 *            自定义项42
	 */
	public void setDef42(String def42) {
		this.def42 = def42;
	}

	/**
	 * 自定义项43
	 * 
	 * @return def43 自定义项43
	 */
	public String getDef43() {
		return def43;
	}

	/**
	 * 自定义项43
	 * 
	 * @param def43
	 *            自定义项43
	 */
	public void setDef43(String def43) {
		this.def43 = def43;
	}

	/**
	 * 自定义项44
	 * 
	 * @return def44 自定义项44
	 */
	public String getDef44() {
		return def44;
	}

	/**
	 * 自定义项44
	 * 
	 * @param def44
	 *            自定义项44
	 */
	public void setDef44(String def44) {
		this.def44 = def44;
	}

	/**
	 * 自定义项45
	 * 
	 * @return def45 自定义项45
	 */
	public String getDef45() {
		return def45;
	}

	/**
	 * 自定义项45
	 * 
	 * @param def45
	 *            自定义项45
	 */
	public void setDef45(String def45) {
		this.def45 = def45;
	}

	/**
	 * 自定义项46
	 * 
	 * @return def46 自定义项46
	 */
	public String getDef46() {
		return def46;
	}

	/**
	 * 自定义项46
	 * 
	 * @param def46
	 *            自定义项46
	 */
	public void setDef46(String def46) {
		this.def46 = def46;
	}

	/**
	 * 自定义项47
	 * 
	 * @return def47 自定义项47
	 */
	public String getDef47() {
		return def47;
	}

	/**
	 * 自定义项47
	 * 
	 * @param def47
	 *            自定义项47
	 */
	public void setDef47(String def47) {
		this.def47 = def47;
	}

	/**
	 * 自定义项48
	 * 
	 * @return def48 自定义项48
	 */
	public String getDef48() {
		return def48;
	}

	/**
	 * 自定义项48
	 * 
	 * @param def48
	 *            自定义项48
	 */
	public void setDef48(String def48) {
		this.def48 = def48;
	}

	/**
	 * 自定义项49
	 * 
	 * @return def49 自定义项49
	 */
	public String getDef49() {
		return def49;
	}

	/**
	 * 自定义项49
	 * 
	 * @param def49
	 *            自定义项49
	 */
	public void setDef49(String def49) {
		this.def49 = def49;
	}

	/**
	 * 自定义项50
	 * 
	 * @return def50 自定义项50
	 */
	public String getDef50() {
		return def50;
	}

	/**
	 * 自定义项50
	 * 
	 * @param def50
	 *            自定义项50
	 */
	public void setDef50(String def50) {
		this.def50 = def50;
	}

	/**
	 * 时间戳
	 * 
	 * @return ts 时间戳
	 */
	public Date getTs() {
		return ts;
	}

	/**
	 * 时间戳
	 * 
	 * @param ts
	 *            时间戳
	 */
	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public List<UserVO> getRelUserList() {
		return relUserList;
	}

	public void setRelUserList(List<UserVO> relUserList) {
		this.relUserList = relUserList;
	}
}