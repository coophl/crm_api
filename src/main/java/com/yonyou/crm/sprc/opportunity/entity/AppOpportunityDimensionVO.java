package com.yonyou.crm.sprc.opportunity.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.user.entity.UserVO;

public class AppOpportunityDimensionVO implements Serializable {
	/**
	 * AppOpportunityDimension
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 维度id
	 */
	private String id;
	
	/**
	 * 维度名称
	 */
	private String name;
	
	/**
	 * 维度分数10满分
	 */
	private String score;
	
	/**
	 * 动作列表
	 */
	private List<AppOpportunityActionVO> actionlist;
	
	public AppOpportunityDimensionVO(){
	}
	
	public AppOpportunityDimensionVO(String id, String name, String score) {
		this.id = id;
		this.name = name;
		this.score = score;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public List<AppOpportunityActionVO> getActionlist() {
		return actionlist;
	}

	public void setActionlist(List<AppOpportunityActionVO> actionlist) {
		this.actionlist = actionlist;
	}

}