package com.yonyou.crm.sprc.opportunity.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.user.entity.UserVO;

public class AppOpportunityActionVO implements Serializable {
	/**
	 * AppOpportunityAction
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 动作id
	 */
	private String id;
	
	/**
	 * 动作名称
	 */
	private String name;
	
	/**
	 * 是否选择 Y已选N未选
	 */
	private String isselect;
	
	/**
	 * 是否是期望值
	 */
	private String isexpected;
	
	/**
	 * 动作分数
	 */
	private String score;
	
	public AppOpportunityActionVO(){
	}
	
	public AppOpportunityActionVO(String id, String name, String isselect, String isexpected, String score) {
		this.id = id;
		this.name = name;
		this.isselect = isselect;
		this.isexpected = isexpected;
		this.score = score;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsselect() {
		return isselect;
	}

	public void setIsselect(String isselect) {
		this.isselect = isselect;
	}

	public String getIsexpected() {
		return isexpected;
	}

	public void setIsexpected(String isexpected) {
		this.isexpected = isexpected;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

}