package com.yonyou.crm.sprc.opportunity.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.user.entity.UserVO;

public class AppOpportunityStageVO implements Serializable {
	/**
	 * AppOpportunityStage
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 阶段id
	 */
	private String id;
	
	/**
	 * 阶段名称
	 */
	private String name;
	
	/**
	 * 是否是当前阶段
	 */
	private String iscurrent;
	
	/**
	 * 是否已完成
	 */
	private String iscomplete;
	
	/**
	 * 维度列表
	 */
	private List<AppOpportunityDimensionVO> dimensionlist;
	
	public AppOpportunityStageVO() {	
	}
	
	public AppOpportunityStageVO(String id, String name, String iscurrent, String iscomplete) {
		this.id = id;
		this.name = name;
		this.iscurrent = iscurrent;
		this.iscomplete = iscomplete;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIscurrent() {
		return iscurrent;
	}

	public void setIscurrent(String iscurrent) {
		this.iscurrent = iscurrent;
	}

	public String getIscomplete() {
		return iscomplete;
	}

	public void setIscomplete(String iscomplete) {
		this.iscomplete = iscomplete;
	}

	public List<AppOpportunityDimensionVO> getDimensionlist() {
		return dimensionlist;
	}

	public void setDimensionlist(List<AppOpportunityDimensionVO> dimensionlist) {
		this.dimensionlist = dimensionlist;
	}

}