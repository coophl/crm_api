package com.yonyou.crm.sprc.opportunity.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.yonyou.crm.base.product.entity.ProductVO;

public class OpportunityBVO implements Serializable {
    /**
     * 商机明细主键
     */
    private Long id;

    /**
     * 商机
     */
    private Long opportunityId;

    /**
     * 产品
     */
    private Long productId;
    
    /**
     * 产品信息
     */
    private ProductVO productInfo;

    private String productName;

    /**
     * 产品分类
     */
    private Long productTypeId;

    private String productTypeName;

    /**
     * 品牌
     */
    private Long brandId;

    private String brandName;

    /**
     * 计量单位
     */
    private Long measureId;

    private String measureName;

    /**
     * 销售单价
     */
    private BigDecimal price;

    /**
     * 产品数量
     */
    private Integer number;

    /**
     * 合计金额
     */
    private BigDecimal sumMoney;

    /**
     * 租户
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 自定义项1
     */
    private String def1;

    /**
     * 自定义项2
     */
    private String def2;

    /**
     * 自定义项3
     */
    private String def3;

    /**
     * 自定义项4
     */
    private String def4;

    /**
     * 自定义项5
     */
    private String def5;

    /**
     * 自定义项6
     */
    private String def6;

    /**
     * 自定义项7
     */
    private String def7;

    /**
     * 自定义项8
     */
    private String def8;

    /**
     * 自定义项9
     */
    private String def9;

    /**
     * 自定义项10
     */
    private String def10;

    /**
     * 自定义项11
     */
    private String def11;

    /**
     * 自定义项12
     */
    private String def12;

    /**
     * 自定义项13
     */
    private String def13;

    /**
     * 自定义项14
     */
    private String def14;

    /**
     * 自定义项15
     */
    private String def15;

    /**
     * 自定义项16
     */
    private String def16;

    /**
     * 自定义项17
     */
    private String def17;

    /**
     * 自定义项18
     */
    private String def18;

    /**
     * 自定义项19
     */
    private String def19;

    /**
     * 自定义项20
     */
    private String def20;

    /**
     * 自由项1
     */
    private String free1;

    /**
     * 自由项2
     */
    private String free2;

    /**
     * 自由项3
     */
    private String free3;

    /**
     * 自由项4
     */
    private String free4;

    /**
     * 自由项5
     */
    private String free5;

    /**
     * 自由项6
     */
    private String free6;

    /**
     * 自由项7
     */
    private String free7;

    /**
     * 自由项8
     */
    private String free8;

    /**
     * 自由项9
     */
    private String free9;

    /**
     * 自由项10
     */
    private String free10;

    /**
     * 自由项11
     */
    private String free11;

    /**
     * 自由项12
     */
    private String free12;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 备注
     */
    private String description;

    /**
     * 编辑状态 add：新增，update：修改，delete:删除
     */
    private String editState;

    /**
     * sprc_opportunity_b
     */
    private static final long serialVersionUID = 1L;

    /**
     * 商机明细主键
     *
     * @return id 商机明细主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 商机明细主键
     *
     * @param id 商机明细主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 商机
     *
     * @return opportunity_id 商机
     */
    public Long getOpportunityId() {
        return opportunityId;
    }

    /**
     * 商机
     *
     * @param opportunityId 商机
     */
    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

    /**
     * 产品
     *
     * @return product_id 产品
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * 产品
     *
     * @param productId 产品
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public ProductVO getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductVO productInfo) {
		this.productInfo = productInfo;
	}

	/**
     * 产品分类
     *
     * @return product_type_id 产品分类
     */
    public Long getProductTypeId() {
        return productTypeId;
    }

    /**
     * 产品分类
     *
     * @param productTypeId 产品分类
     */
    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    /**
     * 品牌
     *
     * @return brand_id 品牌
     */
    public Long getBrandId() {
        return brandId;
    }

    /**
     * 品牌
     *
     * @param brandId 品牌
     */
    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    /**
     * 计量单位
     *
     * @return measure_id 计量单位
     */
    public Long getMeasureId() {
        return measureId;
    }

    /**
     * 计量单位
     *
     * @param measureId 计量单位
     */
    public void setMeasureId(Long measureId) {
        this.measureId = measureId;
    }

    /**
     * 销售单价
     *
     * @return price 销售单价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 销售单价
     *
     * @param price 销售单价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 产品数量
     *
     * @return number 产品数量
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 产品数量
     *
     * @param number 产品数量
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 合计金额
     *
     * @return sum_money 合计金额
     */
    public BigDecimal getSumMoney() {
        return sumMoney;
    }

    /**
     * 合计金额
     *
     * @param sumMoney 合计金额
     */
    public void setSumMoney(BigDecimal sumMoney) {
        this.sumMoney = sumMoney;
    }

    /**
     * 租户
     *
     * @return tenant_id 租户
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户
     *
     * @param tenantId 租户
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     *
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     *
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     *
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     *
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     *
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     *
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     *
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     *
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     *
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     *
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     *
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     *
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     *
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     *
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 自定义项1
     *
     * @return def1 自定义项1
     */
    public String getDef1() {
        return def1;
    }

    /**
     * 自定义项1
     *
     * @param def1 自定义项1
     */
    public void setDef1(String def1) {
        this.def1 = def1;
    }

    /**
     * 自定义项2
     *
     * @return def2 自定义项2
     */
    public String getDef2() {
        return def2;
    }

    /**
     * 自定义项2
     *
     * @param def2 自定义项2
     */
    public void setDef2(String def2) {
        this.def2 = def2;
    }

    /**
     * 自定义项3
     *
     * @return def3 自定义项3
     */
    public String getDef3() {
        return def3;
    }

    /**
     * 自定义项3
     *
     * @param def3 自定义项3
     */
    public void setDef3(String def3) {
        this.def3 = def3;
    }

    /**
     * 自定义项4
     *
     * @return def4 自定义项4
     */
    public String getDef4() {
        return def4;
    }

    /**
     * 自定义项4
     *
     * @param def4 自定义项4
     */
    public void setDef4(String def4) {
        this.def4 = def4;
    }

    /**
     * 自定义项5
     *
     * @return def5 自定义项5
     */
    public String getDef5() {
        return def5;
    }

    /**
     * 自定义项5
     *
     * @param def5 自定义项5
     */
    public void setDef5(String def5) {
        this.def5 = def5;
    }

    /**
     * 自定义项6
     *
     * @return def6 自定义项6
     */
    public String getDef6() {
        return def6;
    }

    /**
     * 自定义项6
     *
     * @param def6 自定义项6
     */
    public void setDef6(String def6) {
        this.def6 = def6;
    }

    /**
     * 自定义项7
     *
     * @return def7 自定义项7
     */
    public String getDef7() {
        return def7;
    }

    /**
     * 自定义项7
     *
     * @param def7 自定义项7
     */
    public void setDef7(String def7) {
        this.def7 = def7;
    }

    /**
     * 自定义项8
     *
     * @return def8 自定义项8
     */
    public String getDef8() {
        return def8;
    }

    /**
     * 自定义项8
     *
     * @param def8 自定义项8
     */
    public void setDef8(String def8) {
        this.def8 = def8;
    }

    /**
     * 自定义项9
     *
     * @return def9 自定义项9
     */
    public String getDef9() {
        return def9;
    }

    /**
     * 自定义项9
     *
     * @param def9 自定义项9
     */
    public void setDef9(String def9) {
        this.def9 = def9;
    }

    /**
     * 自定义项10
     *
     * @return def10 自定义项10
     */
    public String getDef10() {
        return def10;
    }

    /**
     * 自定义项10
     *
     * @param def10 自定义项10
     */
    public void setDef10(String def10) {
        this.def10 = def10;
    }

    /**
     * 自定义项11
     *
     * @return def11 自定义项11
     */
    public String getDef11() {
        return def11;
    }

    /**
     * 自定义项11
     *
     * @param def11 自定义项11
     */
    public void setDef11(String def11) {
        this.def11 = def11;
    }

    /**
     * 自定义项12
     *
     * @return def12 自定义项12
     */
    public String getDef12() {
        return def12;
    }

    /**
     * 自定义项12
     *
     * @param def12 自定义项12
     */
    public void setDef12(String def12) {
        this.def12 = def12;
    }

    /**
     * 自定义项13
     *
     * @return def13 自定义项13
     */
    public String getDef13() {
        return def13;
    }

    /**
     * 自定义项13
     *
     * @param def13 自定义项13
     */
    public void setDef13(String def13) {
        this.def13 = def13;
    }

    /**
     * 自定义项14
     *
     * @return def14 自定义项14
     */
    public String getDef14() {
        return def14;
    }

    /**
     * 自定义项14
     *
     * @param def14 自定义项14
     */
    public void setDef14(String def14) {
        this.def14 = def14;
    }

    /**
     * 自定义项15
     *
     * @return def15 自定义项15
     */
    public String getDef15() {
        return def15;
    }

    /**
     * 自定义项15
     *
     * @param def15 自定义项15
     */
    public void setDef15(String def15) {
        this.def15 = def15;
    }

    /**
     * 自定义项16
     *
     * @return def16 自定义项16
     */
    public String getDef16() {
        return def16;
    }

    /**
     * 自定义项16
     *
     * @param def16 自定义项16
     */
    public void setDef16(String def16) {
        this.def16 = def16;
    }

    /**
     * 自定义项17
     *
     * @return def17 自定义项17
     */
    public String getDef17() {
        return def17;
    }

    /**
     * 自定义项17
     *
     * @param def17 自定义项17
     */
    public void setDef17(String def17) {
        this.def17 = def17;
    }

    /**
     * 自定义项18
     *
     * @return def18 自定义项18
     */
    public String getDef18() {
        return def18;
    }

    /**
     * 自定义项18
     *
     * @param def18 自定义项18
     */
    public void setDef18(String def18) {
        this.def18 = def18;
    }

    /**
     * 自定义项19
     *
     * @return def19 自定义项19
     */
    public String getDef19() {
        return def19;
    }

    /**
     * 自定义项19
     *
     * @param def19 自定义项19
     */
    public void setDef19(String def19) {
        this.def19 = def19;
    }

    /**
     * 自定义项20
     *
     * @return def20 自定义项20
     */
    public String getDef20() {
        return def20;
    }

    /**
     * 自定义项20
     *
     * @param def20 自定义项20
     */
    public void setDef20(String def20) {
        this.def20 = def20;
    }

    public String getFree1() {
        return free1;
    }

    public void setFree1(String free1) {
        this.free1 = free1;
    }

    public String getFree2() {
        return free2;
    }

    public void setFree2(String free2) {
        this.free2 = free2;
    }

    public String getFree3() {
        return free3;
    }

    public void setFree3(String free3) {
        this.free3 = free3;
    }

    public String getFree4() {
        return free4;
    }

    public void setFree4(String free4) {
        this.free4 = free4;
    }

    public String getFree5() {
        return free5;
    }

    public void setFree5(String free5) {
        this.free5 = free5;
    }

    public String getFree6() {
        return free6;
    }

    public void setFree6(String free6) {
        this.free6 = free6;
    }

    public String getFree7() {
        return free7;
    }

    public void setFree7(String free7) {
        this.free7 = free7;
    }

    public String getFree8() {
        return free8;
    }

    public void setFree8(String free8) {
        this.free8 = free8;
    }

    public String getFree9() {
        return free9;
    }

    public void setFree9(String free9) {
        this.free9 = free9;
    }

    public String getFree10() {
        return free10;
    }

    public void setFree10(String free10) {
        this.free10 = free10;
    }

    public String getFree11() {
        return free11;
    }

    public void setFree11(String free11) {
        this.free11 = free11;
    }

    public String getFree12() {
        return free12;
    }

    public void setFree12(String free12) {
        this.free12 = free12;
    }

    /**
     * 时间戳
     *
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     *
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 备注
     *
     * @return description 备注
     */
    public String getDescription() {
        return description;
    }

    /**
     * 备注
     *
     * @param description 备注
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditState() {
        return editState;
    }

    public void setEditState(String editState) {
        this.editState = editState;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getMeasureName() {
        return measureName;
    }

    public void setMeasureName(String measureName) {
        this.measureName = measureName;
    }
}