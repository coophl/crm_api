package com.yonyou.crm.sprc.opportunity.entity;

public enum OpportunityStateEnum {
	WIN(1,"赢单"),LOSE(2,"输单"),FOLLOW(3,"跟进中");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private OpportunityStateEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
