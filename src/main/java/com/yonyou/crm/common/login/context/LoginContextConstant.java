package com.yonyou.crm.common.login.context;

public class LoginContextConstant {

	public final static String TenantId = "tenantid"; 
	public final static String UserId = "id"; 
	public final static String RoleId = "roleid";
	public final static String ROLETYPE = "roletype";
	public final static String OrgId = "orgid"; 
	public final static String DeptId = "deptid";
	public final static String UserType = "usertype";
}
