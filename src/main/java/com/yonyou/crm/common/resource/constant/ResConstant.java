package com.yonyou.crm.common.resource.constant;

public class ResConstant {

	/**
	 * 客户
	 */
	public final static String Customer = "1";
	/**
	 * 联系人
	 */
	public final static String Contact = "2";
	/**
	 * 商机
	 */
	public final static String Opportunity = "3";
	/**
	 * 客户拜访
	 */
	public final static String CustomerVisit = "4";


}
