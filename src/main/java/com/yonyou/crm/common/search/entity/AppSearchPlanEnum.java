package com.yonyou.crm.common.search.entity;

public enum AppSearchPlanEnum {
	ALL(1,"全部"),OWNER(2,"我负责"),REL(3,"我参与"),FOLLOW(4,"我关注"),VIEW(5,"最近查看"),
	WEEKFOLLOWUP(6,"一周未跟进"),OVERDUE(7,"已逾期"),BIRTHMONTH(8,"本月生日"),CREATE(9,"最近创建"),
	CONVERSION(10,"成功转化"), CLOSE(11,"失败关闭"), ASSIGNED(12,"已分配"), UNASSIGNED(13,"未分配"),
	DYNAMICACTION(14,"行为动态"), DYNAMICTRANSACTION(15,"交易动态"), MYCOMMIT(16, "我提交"), 
	MYAPPROVAL(17,"我审批");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private AppSearchPlanEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
