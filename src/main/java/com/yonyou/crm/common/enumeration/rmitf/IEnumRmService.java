package com.yonyou.crm.common.enumeration.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.enumeration.entity.EnumVO;

public interface IEnumRmService {

	public List<Map<String, Object>> getEnumByCode(String code);
	
	public Map<String, List<EnumVO>> getEnumByCodes(String[] codes);
	
	public Object getEnumByValue(String code, Integer value);
	
	public List<Map<String, Object>> selectFieldsByIds(String code, Object[] values);
	
}
