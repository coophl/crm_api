package com.yonyou.crm.common.enumeration.entity;

public enum EnableStateEnum {

	ENABLE(1,"启用"),DISABLE(2,"停用");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private EnableStateEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
