package com.yonyou.crm.common.exception;
/**
 * 此类将异常信息分成hint和message.其中hint将是对用户直接可见的友好提示信息.
 * message 和stacktrace是对客户端调试有用的信息.
 */
public class CrmBusinessException extends RuntimeException {
	private static final long serialVersionUID = 6461253948274704817L;
	private String hint;
	private String code;
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	//标题
	private String title = null;
	public CrmBusinessException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CrmBusinessException(String code,String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public CrmBusinessException(String code,String message, String hint, Throwable cause) {
		super(message, cause);
		this.hint = hint;
		this.code = code;
	}
	
	public CrmBusinessException(String message) {
		super(message);
	}
	public CrmBusinessException(String code ,String message) {
		super(message);
		this.code = code;
	}
	
	public CrmBusinessException(String code,String message, String hint){
		super(message);
		this.hint = hint;
		this.code = code;
	}

	public CrmBusinessException(Throwable cause) {		
		super(cause.getMessage(),cause);
	}

	public String getHint() {
		if(hint == null)
			hint = this.getMessage();
		if(hint == null){
			if(this.getCause() != null)
				hint = this.getCause().getMessage();
		}
		return hint;
	}

	public CrmBusinessException setHint(String hint) {
		this.hint = hint;
		return this;
	}
	
	public CrmBusinessException setTitle(String title) {
		this.title = title;
		return this;
	}
	
	public String getTitle(){
		return this.title;
	}
}
