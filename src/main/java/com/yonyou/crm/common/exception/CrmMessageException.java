package com.yonyou.crm.common.exception;

public class CrmMessageException extends Exception{
	
	private static final long serialVersionUID = 8555532622167927179L;

	private String jsonDate;
	private String code;
	
	
	public CrmMessageException(String msg) {
        super(msg);
    }
	public CrmMessageException(String code,String msg) {
        super(msg);
        this.code = code;
    }
	
	public CrmMessageException(String code,String msg,String jsonDate) {
        super(msg);
        this.jsonDate=jsonDate;
        this.code = code;
    }

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
