package com.yonyou.crm.common.sort.entity;

public enum AppSortPlanEnum {
	NAME(1, "名称"), OWNER(2, "负责人"), CREATETIME(3, "创建时间"), FOLLOWUPTIME(4,"跟进时间"),
	VISITTIME(5, "拜访时间"), CODE(6, "编码"), MODIFYTIME(7,"修改时间"),WINPROBABILITY(8,"赢单概率"),
	EXPECTEDSIGNTIME(9,"预计签单时间"),EXPECTEDSIGNAMOUNT(10,"预计签单金额"),BUYPOTENTIAL(11,"购买能力");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private AppSortPlanEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
