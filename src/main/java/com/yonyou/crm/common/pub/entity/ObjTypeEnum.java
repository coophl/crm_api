package com.yonyou.crm.common.pub.entity;

/**
 * 一套枚举值，适用PC后台，PC前台，移动端，三端务必保持一致
 * @author litcb
 *
 */
public enum ObjTypeEnum {
	
	CUSTOMER(1,"客户"),
	LEAD(2,"线索"),
	CONTACT(3,"联系人"),
	OPPORTUNITY(4,"商机"),
	USER(5,"用户"),
	CUSTOMER_G(6,"集团客户"),
	DYNAMIC(7,"动态");
	
	//枚举项对应int值
	Integer value;
	//枚举项显示值
	String name;
	private ObjTypeEnum(Integer value, String name) {
		this.value = value;
		this.name = name;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
