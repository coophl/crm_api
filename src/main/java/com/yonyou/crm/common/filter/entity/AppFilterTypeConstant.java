package com.yonyou.crm.common.filter.entity;

public class AppFilterTypeConstant {
	
	/**
	 * 单选
	 */
	public final static String Radio = "radio";
	
	/**
	 * 多选
	 */
	public final static String Multiple = "multiple";
	
	/**
	 * 省市区选择器
	 */
	public final static String PCDPicker = "picker";
	
	/**
	 * 树状结构参照
	 */
	public final static String TreeRefer = "class";
	
	/**
	 * 平铺结构参照
	 */
	public final static String Refer = "refer";
	
	/**
	 * 日期选择器
	 */
	public final static String DatePicker = "date";
	
	/**
	 * 日期区间
	 */
	public final static String DateRanger = "dateRanger";
	
	/**
	 * 数值区间
	 */
	public final static String NumberRanger = "numberRanger";
	
}
