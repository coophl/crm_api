package com.yonyou.crm.common.rowstate.constant;

public class RowStateConstant {

	public final static String ADD = "add"; 
	
	public final static String UPDATE = "update";
	
	public final static String DELETE = "delete";
}
