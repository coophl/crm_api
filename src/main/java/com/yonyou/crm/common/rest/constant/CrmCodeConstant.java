package com.yonyou.crm.common.rest.constant;

public class CrmCodeConstant {

	/**
	 * 成功
	 */
	public final static String Success = "0";
	/**
	 * 服务端错误
	 */
	public final static String ServerError = "1";
}
