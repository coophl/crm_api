package com.yonyou.crm.common.rest.constant;

/**
 * 提示信息方式
 * @author wutongh
 *
 */
public class PCCodeConstant extends CrmCodeConstant {

	public static String Info = "2";
	public static String Warn = "3";
	public static String Notification = "4";
	public static String ServiceFormVaild = "5";
	
}
