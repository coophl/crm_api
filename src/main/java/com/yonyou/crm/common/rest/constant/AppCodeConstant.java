package com.yonyou.crm.common.rest.constant;

public class AppCodeConstant extends CrmCodeConstant {
 
	/**
	 * 客户端错误
	 * 接口调用异常，返回错误描述，移动端做友好性提示
	 */
	public final static String ClientError = "-1";
	/**
	 * 登录错误
	 * 需要做重登陆处理的，如session过期
	 */
	public final static String LoginError = "2";
	/**
	 * 移动端缓存，不解析data数据，读本地缓存数据
	 */
	public final static String LocalCache = "3";
	/**
	 * 强制更新提示，做强制升级处理
	 */
	public final static String ForceUpdate = "4";
}
