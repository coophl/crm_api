package com.yonyou.crm.sys.queryplan.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sys.queryplan.entity.QueryPlanVO;

public interface IQueryPlanRmSerivice {
	
	public List<QueryPlanVO> getQueryPlanList(Map<String, Object> param);
}
