package com.yonyou.crm.sys.queryplan.entity;

import java.io.Serializable;

public class QueryPlanVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 终端类型
     */
    private Integer clientType;

    /**
     * 菜单id
     */
    private Integer menuId;

    /**
     * 业务对象ID
     */
    private Integer objId;

    /**
     * 查询方案名称
     */
    private String name;

    /**
     * 查询方案处理类
     */
    private String defClass;

    /**
     * 查询方案描述
     */
    private String remark;
    
    /**
     * 是否选中，1选中2未选中
     */
    private Integer isSelected;

    /**
     * sys_query_plan
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 角色ID
     * @return role_id 角色ID
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 角色ID
     * @param roleId 角色ID
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * 终端类型
     * @return client_type 终端类型
     */
    public Integer getClientType() {
        return clientType;
    }

    /**
     * 终端类型
     * @param clientType 终端类型
     */
    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

    /**
     * 菜单id
     * @return menu_id 菜单id
     */
    public Integer getMenuId() {
        return menuId;
    }

    /**
     * 菜单id
     * @param menuId 菜单id
     */
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    /**
     * 业务对象ID
     * @return obj_id 业务对象ID
     */
    public Integer getObjId() {
        return objId;
    }

    /**
     * 业务对象ID
     * @param objId 业务对象ID
     */
    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    /**
     * 查询方案名称
     * @return name 查询方案名称
     */
    public String getName() {
        return name;
    }

    /**
     * 查询方案名称
     * @param name 查询方案名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 查询方案处理类
     * @return def_class 查询方案处理类
     */
    public String getDefClass() {
        return defClass;
    }

    /**
     * 查询方案处理类
     * @param defClass 查询方案处理类
     */
    public void setDefClass(String defClass) {
        this.defClass = defClass;
    }

    /**
     * 查询方案描述
     * @return remark 查询方案描述
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 查询方案描述
     * @param remark 查询方案描述
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

	/**
	 * 是否选中，1选中2未选中
	 * @return isSelected
	 */
	public Integer getIsSelected() {
		return isSelected;
	}

	/**
	 * @param isSelected 是否选中，1选中2未选中
	 */
	public void setIsSelected(Integer isSelected) {
		this.isSelected = isSelected;
	}
}