package com.yonyou.crm.sys.login.rmitf;

import com.yonyou.crm.sys.user.entity.UserVO;

public interface ILoginRmService {
	
	public UserVO login(String code);

}
