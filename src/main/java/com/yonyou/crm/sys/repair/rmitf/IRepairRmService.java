package com.yonyou.crm.sys.repair.rmitf;


import java.util.Map;

import com.yonyou.crm.sys.repair.entity.RepairVO;


public interface IRepairRmService {
	RepairVO insertRepair(RepairVO vo);
	void deleteRepair(Long id);
	RepairVO updateRepair(RepairVO vo);
	RepairVO idGetRepair(Long id);
	
}
