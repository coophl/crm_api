package com.yonyou.crm.sys.repair.entity;

import java.io.Serializable;

public class RepairVO implements Serializable {
    /**
     * 培训课程id
     */
    private Long id;

    /**
     * 培训主题
     */
    private String subject;

    /**
     * 创建人id
     */
    private Long userId;

    /**
     * 创建人姓名
     */
    private String userName;

    /**
     * test
     */
    private static final long serialVersionUID = 1L;

    /**
     * 培训课程id
     * @return id 培训课程id
     */
    public Long getId() {
        return id;
    }

    /**
     * 培训课程id
     * @param id 培训课程id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 培训主题
     * @return subject 培训主题
     */
    public String getSubject() {
        return subject;
    }

    /**
     * 培训主题
     * @param subject 培训主题
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * 创建人id
     * @return user_id 创建人id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 创建人id
     * @param userId 创建人id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 创建人姓名
     * @return user_name 创建人姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 创建人姓名
     * @param userName 创建人姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}