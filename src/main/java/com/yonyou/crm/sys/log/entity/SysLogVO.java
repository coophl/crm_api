package com.yonyou.crm.sys.log.entity;

import java.io.Serializable;
import java.util.Date;

public class SysLogVO implements Serializable {
    /**
     * 自增id
     */
    private Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 日志模块
     */
    private String module;

    /**
     * 状态 0 正常 其他不正常
     */
    private String status;

    /**
     * 时间
     */
    private Date createTime;

    /**
     * 日志级别 默认业务日志 trace
     */
    private String level;

    /**
     * 冗余特殊字段，可用于特殊节点
     */
    private String extraflag;

    /**
     * 模块为 login 时，为ip地址
     */
    private String extrainfo;

    /**
     * 详情
     */
    private String info;

    /**
     * sys_log
     */
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     * @return id 自增id
     */
    public Long getId() {
        return id;
    }

    /**
     * 自增id
     * @param id 自增id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 日志模块
     * @return module 日志模块
     */
    public String getModule() {
        return module;
    }

    /**
     * 日志模块
     * @param module 日志模块
     */
    public void setModule(String module) {
        this.module = module;
    }

    /**
     * 状态 0 正常 其他不正常
     * @return status 状态 0 正常 其他不正常
     */
    public String getStatus() {
        return status;
    }

    /**
     * 状态 0 正常 其他不正常
     * @param status 状态 0 正常 其他不正常
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 时间
     * @return create_time 时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 时间
     * @param createTime 时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 日志级别 默认业务日志 trace
     * @return level 日志级别 默认业务日志 trace
     */
    public String getLevel() {
        return level;
    }

    /**
     * 日志级别 默认业务日志 trace
     * @param level 日志级别 默认业务日志 trace
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 冗余特殊字段，可用于特殊节点
     * @return extraflag 冗余特殊字段，可用于特殊节点
     */
    public String getExtraflag() {
        return extraflag;
    }

    /**
     * 冗余特殊字段，可用于特殊节点
     * @param extraflag 冗余特殊字段，可用于特殊节点
     */
    public void setExtraflag(String extraflag) {
        this.extraflag = extraflag;
    }

    /**
     * 模块为 login 时，为ip地址
     * @return extrainfo 模块为 login 时，为ip地址
     */
    public String getExtrainfo() {
        return extrainfo;
    }

    /**
     * 模块为 login 时，为ip地址
     * @param extrainfo 模块为 login 时，为ip地址
     */
    public void setExtrainfo(String extrainfo) {
        this.extrainfo = extrainfo;
    }

    /**
     * 详情
     * @return info 详情
     */
    public String getInfo() {
        return info;
    }

    /**
     * 详情
     * @param info 详情
     */
    public void setInfo(String info) {
        this.info = info;
    }
}