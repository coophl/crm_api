package com.yonyou.crm.sys.log.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.appmenu.entity.MenuVO;
import com.yonyou.crm.sys.log.entity.*;

public interface ISysLogRmService {

	public SysLogVO insert(SysLogVO user);
	public Page<SysLogVO> getList(Page<SysLogVO> page,Map<String, Object> paraMap);
}
