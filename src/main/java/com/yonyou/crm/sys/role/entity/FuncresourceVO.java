package com.yonyou.crm.sys.role.entity;

import java.io.Serializable;

public class FuncresourceVO implements Serializable {
    /**
     * 功能资源主键
     */
    private Long id;

    /**
     * 资源标识
     */
    private String resRemark;

    /**
     * 功能节点编码
     */
    private String funcnode;

    /**
     * 功能权限主键
     */
    private Long funcperId;

    /**
     * 资源类型（1：菜单；2：按钮；3：其他）
     */
    private Integer type;

    /**
     * 客户端类型（1：pc；2：移动）
     */
    private Integer device;

    /**
     * 菜单id
     */
    private Long menuId;

    /**
     * 请求资源url
     */
    private String url;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 是否显示
     */
    private Integer isDisplay;

    /**
     * sys_funcresource
     */
    private static final long serialVersionUID = 1L;

    /**
     * 功能资源主键
     * @return id 功能资源主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 功能资源主键
     * @param id 功能资源主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 资源标识
     * @return res_remark 资源标识
     */
    public String getResRemark() {
        return resRemark;
    }

    /**
     * 资源标识
     * @param resRemark 资源标识
     */
    public void setResRemark(String resRemark) {
        this.resRemark = resRemark;
    }

    /**
     * 功能节点编码
     * @return funcnode 功能节点编码
     */
    public String getFuncnode() {
        return funcnode;
    }

    /**
     * 功能节点编码
     * @param funcnode 功能节点编码
     */
    public void setFuncnode(String funcnode) {
        this.funcnode = funcnode;
    }

    /**
     * 功能权限主键
     * @return funcper_id 功能权限主键
     */
    public Long getFuncperId() {
        return funcperId;
    }

    /**
     * 功能权限主键
     * @param funcperId 功能权限主键
     */
    public void setFuncperId(Long funcperId) {
        this.funcperId = funcperId;
    }

    /**
     * 资源类型（1：菜单；2：按钮；3：其他）
     * @return type 资源类型（1：菜单；2：按钮；3：其他）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 资源类型（1：菜单；2：按钮；3：其他）
     * @param type 资源类型（1：菜单；2：按钮；3：其他）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 客户端类型（1：pc；2：移动）
     * @return device 客户端类型（1：pc；2：移动）
     */
    public Integer getDevice() {
        return device;
    }

    /**
     * 客户端类型（1：pc；2：移动）
     * @param device 客户端类型（1：pc；2：移动）
     */
    public void setDevice(Integer device) {
        this.device = device;
    }

    /**
     * 菜单id
     * @return menu_id 菜单id
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 菜单id
     * @param menuId 菜单id
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 请求资源url
     * @return url 请求资源url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 请求资源url
     * @param url 请求资源url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否显示
     * @return isDisplay 是否显示
     */
    public Integer getIsDisplay() {
        return isDisplay;
    }

    /**
     * 是否显示
     * @param isDisplay 是否显示
     */
    public void setIsDisplay(Integer isDisplay) {
        this.isDisplay = isDisplay;
    }
}