package com.yonyou.crm.sys.role.constant;

/**
 * 预制角色类型 常量
 */
public class RoleTypeConstant {
    public final static String SALESPERSON = "83";//销售员
    public final static String SALESMANGER= "84";//销售经理
    public final static String COMPANYOFFICE= "85";//公司内勤
    public final static String GROUPOFFICE= "86";//集团内勤
}

