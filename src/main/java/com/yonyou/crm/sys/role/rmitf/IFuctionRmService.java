package com.yonyou.crm.sys.role.rmitf;

import java.util.List;

import com.yonyou.crm.sys.role.entity.FuncresourceVO;
import com.yonyou.crm.sys.role.entity.FunctionVO;
import com.yonyou.crm.sys.role.entity.RoleFuncPerVO;

public interface IFuctionRmService {
	
	public List<FunctionVO>  getFunctiuonList(Long roleId);

	public List<FuncresourceVO> getUnhaveFuncResourceList(long roleId);

	public void assignFuncPer(List<RoleFuncPerVO> roleFuncPerVOs, String checked);

}
