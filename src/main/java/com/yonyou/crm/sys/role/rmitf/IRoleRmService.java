package com.yonyou.crm.sys.role.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.role.entity.RoleVO;
import com.yonyou.crm.sys.user.entity.UserVO;

public interface IRoleRmService {

	List<RoleVO> getRoleListByTenantId(Map<String, Object> param);

	RoleVO getRoleDetail(Long id);

	RoleVO insertRole(RoleVO role);

	List<RoleVO> getUserRole(Long id);

	RoleVO updateRole(RoleVO role);

	int deleteRole(Long id);

    boolean assignUser(long roleId, String[] userIds);

	Page<UserVO> getPersonals(Page<UserVO> requestPage, Long roleId);

	boolean unAssignUser(long roleId, String[] userIds);

	List<Map<String,Object>> getRoleTypes();

	List<RoleVO> getRefList(String param);

	List<UserVO> getAddPersonals(String name, String orgId, Long roleId);
}
