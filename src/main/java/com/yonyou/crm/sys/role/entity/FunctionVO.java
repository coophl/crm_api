package com.yonyou.crm.sys.role.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FunctionVO implements Serializable{
	
	 /**
     * 功能权限主键
     */
    private Long id;

    /**
     * 功能权限编码
     */
    private String code;

    /**
     * 功能权限名称
     */
    private String name;

    /**
     * 租户id
     */
    private Long tenantId;
    
    private List<FuncpermissionVO> child = new ArrayList<>();
    
    
	public List<FuncpermissionVO> getChild() {
		return child;
	}

	public void setChild(List<FuncpermissionVO> child) {
		this.child = child;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
