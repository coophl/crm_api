package com.yonyou.crm.sys.role.entity;

import java.io.Serializable;
import java.util.Date;

public class RoleVO implements Serializable {
	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 编码
	 */
	private String code;

	/**
	 * 类型
	 */
	private Integer type;

	private String typeName;
	
	
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * 是否预置,1是2否
	 */
	private Byte isPreseted;

	private String isPresetedName;

	/**
	 * 行业
	 */
	private Long industryId;

	/**
	 * 组织
	 */
	private Long orgId;
	/**
	 * 组织名称 为了显示参照添加的
	 */
	private String orgName;

	/**
	 * 租户
	 */
	private Long tenantId;

	/**
	 * 是否已删除
	 */
	private Byte isDeleted;

	/**
	 * 删除人
	 */
	private Long deletedUserId;

	/**
	 * 删除时间
	 */
	private Date deletedTime;

	/**
	 * 记录的创建时间
	 */
	private Date sysCreatedTime;

	/**
	 * 记录的创建人
	 */
	private Long sysCreatedUserId;

	/**
	 * 记录的修改时间
	 */
	private Date sysModifiedTime;

	/**
	 * 记录的修改人
	 */
	private Long sysModifiedUserId;

	/**
	 * 时间戳
	 */
	private Date ts;

	/**
	 * 描述
	 */
	private String description;

	/**
	 * sys_role
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 * 
	 * @return id 主键
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 主键
	 * 
	 * @param id
	 *            主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 名称
	 * 
	 * @return name 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 名称
	 * 
	 * @param name
	 *            名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 编码
	 * 
	 * @return code 编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 编码
	 * 
	 * @param code
	 *            编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 类型
	 * 
	 * @return type 类型
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 类型
	 * 
	 * @param type
	 *            类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	public Byte getIsPreseted() {
		return isPreseted;
	}

	public void setIsPreseted(Byte isPreseted) {
		this.isPreseted = isPreseted;
	}

	public String getIsPresetedName() {
		return isPresetedName;
	}

	public void setIsPresetedName(String isPresetedName) {
		this.isPresetedName = isPresetedName;
	}

	/**
	 * 行业
	 * 
	 * @return industry_id 行业
	 */
	public Long getIndustryId() {
		return industryId;
	}

	/**
	 * 行业
	 * 
	 * @param industryId
	 *            行业
	 */
	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}

	/**
	 * 组织
	 * 
	 * @return org_id 组织
	 */
	public Long getOrgId() {
		return orgId;
	}

	/**
	 * 组织
	 * 
	 * @param orgId
	 *            组织
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * 租户
	 * 
	 * @return tenant_id 租户
	 */
	public Long getTenantId() {
		return tenantId;
	}

	/**
	 * 租户
	 * 
	 * @param tenantId
	 *            租户
	 */
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * 是否已删除
	 * 
	 * @return is_deleted 是否已删除
	 */
	public Byte getIsDeleted() {
		return isDeleted;
	}

	/**
	 * 是否已删除
	 * 
	 * @param isDeleted
	 *            是否已删除
	 */
	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * 删除人
	 * 
	 * @return deleted_user_id 删除人
	 */
	public Long getDeletedUserId() {
		return deletedUserId;
	}

	/**
	 * 删除人
	 * 
	 * @param deletedUserId
	 *            删除人
	 */
	public void setDeletedUserId(Long deletedUserId) {
		this.deletedUserId = deletedUserId;
	}

	/**
	 * 删除时间
	 * 
	 * @return deleted_time 删除时间
	 */
	public Date getDeletedTime() {
		return deletedTime;
	}

	/**
	 * 删除时间
	 * 
	 * @param deletedTime
	 *            删除时间
	 */
	public void setDeletedTime(Date deletedTime) {
		this.deletedTime = deletedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @return sys_created_time 记录的创建时间
	 */
	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @param sysCreatedTime
	 *            记录的创建时间
	 */
	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}

	/**
	 * 记录的创建人
	 * 
	 * @return sys_created_user_id 记录的创建人
	 */
	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}

	/**
	 * 记录的创建人
	 * 
	 * @param sysCreatedUserId
	 *            记录的创建人
	 */
	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @return sys_modified_time 记录的修改时间
	 */
	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @param sysModifiedTime
	 *            记录的修改时间
	 */
	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}

	/**
	 * 记录的修改人
	 * 
	 * @return sys_modified_user_id 记录的修改人
	 */
	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	/**
	 * 记录的修改人
	 * 
	 * @param sysModifiedUserId
	 *            记录的修改人
	 */
	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	/**
	 * 时间戳
	 * 
	 * @return ts 时间戳
	 */
	public Date getTs() {
		return ts;
	}

	/**
	 * 时间戳
	 * 
	 * @param ts
	 *            时间戳
	 */
	public void setTs(Date ts) {
		this.ts = ts;
	}

	/**
	 * 描述
	 * 
	 * @return description 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 描述
	 * 
	 * @param description
	 *            描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}