package com.yonyou.crm.sys.role.entity;

import java.io.Serializable;

public class FuncpermissionVO implements Serializable{

   /**
	* 功能权限主键
	*/
   private Long id;

   /**
    * 功能权限编码
    */
   private String code;

   /**
    * 功能权限名称
    */
   private String name; 
   
   /**
    * 是否选中
    */
   private String checked;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getChecked() {
		return checked;
	}
	
	public void setChecked(String checked) {
		this.checked = checked;
	}
   
}
