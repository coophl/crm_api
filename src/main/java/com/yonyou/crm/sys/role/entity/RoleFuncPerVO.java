package com.yonyou.crm.sys.role.entity;

import java.io.Serializable;
/**
 * 
 * @author Administrator
 *
 */
public class RoleFuncPerVO implements Serializable{
	
	/**
     * 租户id
     */
    private Long tenantId;
    
    /**
     * 角色id
     */
    private Long roleId;
    
    /**
     * 功能权限id
     */
    private Long funcperId;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getFuncperId() {
		return funcperId;
	}

	public void setFuncperId(Long funcperId) {
		this.funcperId = funcperId;
	}
    
    
    
}
