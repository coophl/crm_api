package com.yonyou.crm.sys.appmenu.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AppMenuVO implements Serializable {
    
	/**
     * 对象Id
     */
    private String objid;
    
	/**
	 * APP菜单code
	 */
	//private String code;
	
	/**
     * APP菜单功能编码
     */
    private String funcode;
	
	/**
	 * 菜单名称
	 */
	private String name;
	
	/**
	 * 菜单位置
	 */
	private String position;
	
	/**
	 * 是否支持快速记录
	 */
	private String isfastrecord;
	
	/**
	 * 是否支持快速新增
	 */
	private String isfastadd;
	
	/**
	 * 子菜单列表
	 */
	private List<AppMenuItemVO> nextlist = new ArrayList<AppMenuItemVO>();
	
	/**
     * sys_appmenu
     */
    private static final long serialVersionUID = 1L;

    /*
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	*/

	public String getObjid() {
		return objid;
	}

	public void setObjid(String objid) {
		this.objid = objid;
	}

	public String getFuncode() {
		return funcode;
	}

	public void setFuncode(String funcode) {
		this.funcode = funcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getIsfastrecord() {
		return isfastrecord;
	}

	public void setIsfastrecord(String isfastrecord) {
		this.isfastrecord = isfastrecord;
	}

	public String getIsfastadd() {
		return isfastadd;
	}

	public void setIsfastadd(String isfastadd) {
		this.isfastadd = isfastadd;
	}

	public List<AppMenuItemVO> getNextlist() {
		return nextlist;
	}

	public void setNextlist(List<AppMenuItemVO> nextlist) {
		this.nextlist = nextlist;
	}
	
}