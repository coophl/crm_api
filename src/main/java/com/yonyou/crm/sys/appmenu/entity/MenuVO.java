package com.yonyou.crm.sys.appmenu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MenuVO implements Serializable {
    /**
     * 菜单项主键
     */
    private Long id;

    /**
     * 租户Id
     */
    private Long tenantId;

    /**
     * 公司Id
     */
    private Long orgId;
    
    /**
     * 对象Id
     */
    private Integer objId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;
    
    /**
     * 功能编码
     */
    private String funcode;
    
    /**
     * 父菜单
     */
    private Long parentId;

    /**
     * 
     */
    private String icon;

    /**
     * 菜单位置
     */
    private String position;

    /**
     * 排序号
     */
    private Integer orderNum;
    
    /**
	 * 是否支持快速记录
	 */
	private Integer isFastrecord;
	
	/**
	 * 是否支持快速新增
	 */
	private Integer isFastadd;
	
	/**
	 * 是否是系统预置
	 */
	private Integer isSystem;
	
	/**
	 * 是否显示
	 */
	private Integer isDisplay;

    /**
     * 
     */
    private Integer enableState;

    /**
     * 
     */
    private Long enableUserId;

    /**
     * 
     */
    private Date enableTime;

    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 删除标志，0未删除、1已删除
     */
    private Integer isDeleted;

    /**
     * 
     */
    private Date ts;
    
    /**
	 * 子菜单列表
	 */
	private List<MenuVO> nextlist;

    /**
     * sys_appmenu
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜单项主键
     * @return id 菜单项主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 菜单项主键
     * @param id 菜单项主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户Id
     * @return tenant_id 租户Id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户Id
     * @param tenantId 租户Id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司Id
     * @return org_id 公司Id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司Id
     * @param orgId 公司Id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Integer getObjId() {
		return objId;
	}

	public void setObjId(Integer objId) {
		this.objId = objId;
	}

	/**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 编码
     * @return code 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 编码
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    public String getFuncode() {
		return funcode;
	}

	public void setFuncode(String funcode) {
		this.funcode = funcode;
	}

	/**
     * 父菜单
     * @return parent_id 父菜单
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父菜单
     * @param parentId 父菜单
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 
     * @return icon 
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon 
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 菜单位置
     * @return position 菜单位置
     */
    public String getPosition() {
        return position;
    }

    /**
     * 菜单位置
     * @param position 菜单位置
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 排序号
     * @return order_num 排序号
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * 排序号
     * @param orderNum 排序号
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getIsFastrecord() {
		return isFastrecord;
	}

	public void setIsFastrecord(Integer isFastrecord) {
		this.isFastrecord = isFastrecord;
	}

	public Integer getIsFastadd() {
		return isFastadd;
	}

	public void setIsFastadd(Integer isFastadd) {
		this.isFastadd = isFastadd;
	}

	public Integer getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}

	public Integer getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Integer isDisplay) {
		this.isDisplay = isDisplay;
	}

	/**
     * 
     * @return enable_state 
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 
     * @param enableState 
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 
     * @return enable_user_id 
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 
     * @param enableUserId 
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 
     * @return enable_time 
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 
     * @param enableTime 
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 删除标志，0未删除、1已删除
     * @return is_deleted 删除标志，0未删除、1已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 删除标志，0未删除、1已删除
     * @param isDeleted 删除标志，0未删除、1已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 
     * @return ts 
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 
     * @param ts 
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public List<MenuVO> getNextlist() {
		return nextlist;
	}

	public void setNextlist(List<MenuVO> nextlist) {
		this.nextlist = nextlist;
	}
}