package com.yonyou.crm.sys.appmenu.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.appmenu.entity.*;

public interface IAppMenuRmService {

	public Page<MenuVO> getList(Page<MenuVO> page,Map<String, Object> paraMap);
	public List<AppMenuVO> getList(Map<String, Object> paraMap);
	public MenuVO getDetail(Long id);
	public MenuVO insert(MenuVO user);
	public MenuVO update(MenuVO user);
	public int delete(Long id);
	public Page<MenuVO> batchDelete(String[] ids, Page<MenuVO> page,Map<String, Object> paraMap);
	public MenuVO updateEnableState(Long id, Integer enableState);

}
