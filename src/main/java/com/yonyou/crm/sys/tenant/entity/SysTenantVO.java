package com.yonyou.crm.sys.tenant.entity;

import java.io.Serializable;
import java.util.Date;

public class SysTenantVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 企业名称
     */
    private String companyName;

    /**
     * 企业简称
     */
    private String companySimpleName;

    /**
     * 企业类型
     */
    private Byte companyType;

    /**
     * 所属行业
     */
    private Long companyIndustry;
    /**
     * 所属行业
     */
    private String companyIndustryName;

    /**
     * 总部地址
     */
    private String companyAddress;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 创建时间
     */
    private Date sysCreatedTime;

    /**
     * 创建人
     */
    private Long sysCreatedUserId;

    /**
     * 修改时间
     */
    private Date sysModifiedTime;

    /**
     * 修改人
     */
    private Long sysModifiedUserId;

    /**
     * 是否删除(0代表未删除，1代表删除）
     */
    private Byte isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 租户对应根组织id
     */
    private Long orgId;
    
    /**
     * 租户对应根组织名称
     */
    private String orgName;

    /**
     * 租户是否初始化（1代表未初始化，2代表初始化）
     */
    private Byte isInit;
    
    /**
     * 公司创立时间
     */
    private Date companyCreatedTime;
    
    /**
     * 企业类型名称
     */
    private String companyTypeName;
    
    /**
     * sys_tenant
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 企业名称
     * @return company_name 企业名称
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 企业名称
     * @param companyName 企业名称
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 企业简称
     * @return company_simple_name 企业简称
     */
    public String getCompanySimpleName() {
        return companySimpleName;
    }

    /**
     * 企业简称
     * @param companySimpleName 企业简称
     */
    public void setCompanySimpleName(String companySimpleName) {
        this.companySimpleName = companySimpleName;
    }

    /**
     * 企业类型
     * @return company_type 企业类型
     */
    public Byte getCompanyType() {
        return companyType;
    }

    /**
     * 企业类型
     * @param companyType 企业类型
     */
    public void setCompanyType(Byte companyType) {
        this.companyType = companyType;
    }

    /**
     * 所属行业
     * @return company_industry 所属行业
     */
    public Long getCompanyIndustry() {
        return companyIndustry;
    }

    /**
     * 所属行业
     * @param companyIndustry 所属行业
     */
    public void setCompanyIndustryName(String companyIndustryName) {
        this.companyIndustryName = companyIndustryName;
    }
    /**
     * 所属行业
     * @return company_industry 所属行业
     */
    public String getCompanyIndustryName() {
        return companyIndustryName;
    }

    /**
     * 所属行业
     * @param companyIndustry 所属行业
     */
    public void setCompanyIndustry(Long companyIndustry) {
        this.companyIndustry = companyIndustry;
    }

    /**
     * 总部地址
     * @return company_address 总部地址
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * 总部地址
     * @param companyAddress 总部地址
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 创建时间
     * @return sys_created_time 创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 创建时间
     * @param sysCreatedTime 创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 创建人
     * @return sys_created_user_id 创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 创建人
     * @param sysCreatedUserId 创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 修改时间
     * @return sys_modified_time 修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 修改时间
     * @param sysModifiedTime 修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 修改人
     * @return sys_modified_user_id 修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 修改人
     * @param sysModifiedUserId 修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @return is_deleted 是否删除(0代表未删除，1代表删除）
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @param isDeleted 是否删除(0代表未删除，1代表删除）
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 租户对应根组织id
     * @return org_id 租户对应根组织id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 租户对应根组织id
     * @param orgId 租户对应根组织id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 租户是否初始化（1代表未初始化，2代表初始化）
     * @return is_init 租户是否初始化（1代表未初始化，2代表初始化）
     */
    public Byte getIsInit() {
        return isInit;
    }

    /**
     * 租户是否初始化（1代表未初始化，2代表初始化）
     * @param isInit 租户是否初始化（1代表未初始化，2代表初始化）
     */
    public void setIsInit(Byte isInit) {
        this.isInit = isInit;
    }

	public Date getCompanyCreatedTime() {
		return companyCreatedTime;
	}

	public void setCompanyCreatedTime(Date companyCreatedTime) {
		this.companyCreatedTime = companyCreatedTime;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getCompanyTypeName() {
		return companyTypeName;
	}

	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}
}