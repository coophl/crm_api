package com.yonyou.crm.sys.tenant.rmitf;

import com.yonyou.crm.sys.tenant.entity.SysTenantVO;

public interface ISysTenantRmService {
	
	public SysTenantVO getSysTenantById(Long id);
	
	public SysTenantVO updateSysTenant(SysTenantVO sysTenant);
}
