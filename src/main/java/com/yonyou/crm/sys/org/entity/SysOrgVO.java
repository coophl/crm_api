package com.yonyou.crm.sys.org.entity;

import java.io.Serializable;
import java.util.Date;

public class SysOrgVO implements Serializable {

    private static final long serialVersionUID = 1L;
	
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 简称
     */
    private String simpleName;

    /**
     * 助记码
     */
    private String simpleCode;

    /**
     * 上级组织
     */
    private Long fatherorgId;
    
    /**
     * 层级路径，逗号分隔
     */
    private String path;

    /**
     * 所属公司
     */
    private Long fathercompanyId;

    /**
     * 上级组织名称
     */
    private String fatherorgName;

    /**
     * 所属公司名称
     */
    private String fathercompanyName;
  
	/**
     * 组织类型
     */
    private Integer orgType;
    
    /**
     * 组织类型名称
     */
    private String orgTypeName;

    /**
     * 启用状态
     */
    private Integer enableState;
    
    /**
     * 启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用操作人
     */
    private Long enableUserId;

    /**
     * 停启用日期
     */
    private Date enableTime;
    
    /**
     * 负责人
     */
    private String respoPerson;

    /**
     * 其他负责人
     */
    private String otherRespoPerson;
    
    /**
     * 公司创立时间
     */
    private Date createTime;
    
    private String address;

	/**
     * 主键
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 编码
     * @return code 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 编码
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 简称
     * @return simple_name 简称
     */
    public String getSimpleName() {
        return simpleName;
    }

    /**
     * 简称
     * @param simpleName 简称
     */
    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    /**
     * 助记码
     * @return simple_code 助记码
     */
    public String getSimpleCode() {
        return simpleCode;
    }

    /**
     * 助记码
     * @param simpleCode 助记码
     */
    public void setSimpleCode(String simpleCode) {
        this.simpleCode = simpleCode;
    }

    /**
     * 上级组织
     * @return fatherorg_id 上级组织
     */
    public Long getFatherorgId() {
        return fatherorgId;
    }

    /**
     * 上级组织
     * @param fatherorgId 上级组织
     */
    public void setFatherorgId(Long fatherorgId) {
        this.fatherorgId = fatherorgId;
    }

    /**
     * 层级路径
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path 层级路径
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
     * 所属公司
     * @return fathercompany_id 所属公司
     */
    public Long getFathercompanyId() {
        return fathercompanyId;
    }

    /**
     * 所属公司
     * @param fathercompanyId 所属公司
     */
    public void setFathercompanyId(Long fathercompanyId) {
        this.fathercompanyId = fathercompanyId;
    }

    /**
     * 组织类型
     * @return org_type 组织类型
     */
    public Integer getOrgType() {
        return orgType;
    }

    /**
     * 组织类型
     * @param orgType 组织类型
     */
    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }
    
    /**
     * 组织类型名称
     * @return
     */
    public String getOrgTypeName() {
		return orgTypeName;
	}

    /**
     * 组织类型名称
     * @param orgTypeName
     */
	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}

    /**
     * 启用状态
     * @return enablestate 启用状态
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 启用状态
     * @param enablestate 启用状态
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }
    
    /**
     * 启用状态名称
     * @return
     */
	public String getEnablestateName() {
		return enableStateName;
	}

	/**
	 * 启用状态名称
	 * @param enablestateName
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}
    
    /**
     * 停启用操作人
     * @return enable_user_id 停启用操作人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用操作人
     * @param enableUserId 停启用操作人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

	/**
     * 停启用日期
     * @return enable_time 停启用日期
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用日期
     * @param enableTime 停启用日期
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }
    
    

	public String getRespoPerson() {
		return respoPerson;
	}

	public void setRespoPerson(String respoPerson) {
		this.respoPerson = respoPerson;
	}

	public String getOtherRespoPerson() {
		return otherRespoPerson;
	}

	public void setOtherRespoPerson(String otherRespoPerson) {
		this.otherRespoPerson = otherRespoPerson;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFatherorgName() {
		return fatherorgName;
	}

	public void setFatherorgName(String fatherorgName) {
		this.fatherorgName = fatherorgName;
	}

	public String getFathercompanyName() {
		return fathercompanyName;
	}

	public void setFathercompanyName(String fathercompanyName) {
		this.fathercompanyName = fathercompanyName;
	}
    
}