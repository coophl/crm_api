package com.yonyou.crm.sys.org.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.org.entity.SysOrgVO;

public interface ISysOrgRmService {

	Page<SysOrgVO> getPage(Page<SysOrgVO> page, Map<String, Object> condMap);

	public SysOrgVO getDetail(Long id);
	
	public SysOrgVO insert(SysOrgVO sysOrgVO);

	public SysOrgVO update(SysOrgVO sysOrg);

	public List<Map<String,Object>> getSysOrgTree();
	
	public List<Map<String,Object>> getTreeByOrgType(Integer orgType, Long fatherorgId,Long id);
		
	public Object selectFieldsByIds(Object[] ids);
	
	public int batchDelete(String[] idStrs);

	public String batchUpdateEnablestate(String[] idArray, Integer enableState);
}
