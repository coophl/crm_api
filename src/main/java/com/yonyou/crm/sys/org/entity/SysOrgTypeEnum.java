package com.yonyou.crm.sys.org.entity;

public enum SysOrgTypeEnum {
	GROUP(0,"集团"),COMPANY(1,"公司"),DEPT(2,"部门");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private SysOrgTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
