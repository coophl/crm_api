package com.yonyou.crm.sys.user.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.user.entity.UserVO;

public interface IUserRmService {

	Page<UserVO> page(Page<UserVO> page, Map<String, Object> paraMap);
	UserVO detail(Long id);
	UserVO insert(UserVO vo);
	UserVO update(UserVO vo);
	String batchDelete(String[] ids);
	String batchUpdateEnableState(String[] ids, Integer enableState);
	int getUserCountByJob(Long orgId);
	Object selectFieldsByIds(Object[] ids);
	
	List<Map<String, Object>> getUserByDeptId(Long deptId, String condition);
	
	List<UserVO> batchUpdateAdmin(List<UserVO> voList);
	List<UserVO> getAdminList(Map<String, Object> paramMap);
	
	Map<String, Object> getDeptAndUserList(Map<String, Object> paramMap);
	
	Page<UserVO> getOrgUserList(Page<UserVO> page, Map<String, Object> paraMap);
	
	List<UserVO> getUserListExport(Map<String, Object> paramMap);
	
	List<UserVO> getRefList(Map<String, Object> paraMap);
	Page<UserVO> getRefPage(Page<UserVO> page, Map<String, Object> paraMap);
    
	void allocateRole(String[] userIds, Long roleId);
    
	void updatePassword(Long userId, String password);

    List<Map<String,Object>> getSubdepts();
}
