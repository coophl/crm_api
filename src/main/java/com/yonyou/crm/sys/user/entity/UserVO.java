package com.yonyou.crm.sys.user.entity;

import java.io.Serializable;
import java.util.Date;

public class UserVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 公司
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 部门
     */
    private Long deptId;
    
    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 名称
     */
    private String name;
    
    /**
     * 名称拼音
     */
    private String namePinyin;
    
    /**
     * 用户名
     */
    private String code;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 性别名称
     */
    private String genderName;
    
    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 加密盐
     */
    private String salt;

    /**
     * 停启用状态，1启用、2停用
     */
    private Integer enableState;
    
    /**
     * 停启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用操作人
     */
    private Long enableUserId;

    /**
     * 停启用操作人名称
     */
    private String enableUserName;

    /**
     * 停启用日期
     */
    private Date enableTime;

    /**
     * 头像id
     */
    private String portraitId;
    
    /**
     * 职位，1员工2负责人3其他负责人
     */
    private Integer job;
    
    /**
     * 职位名称
     */
    private String jobName;
    
    /**
     * 职位，1系统管理员2集团管理员3公司管理员
     */
    private Integer type;
    
    /**
     * 职位名称
     */
    private String typeName;
    
    /**
     * 编辑状态，add、update、delete
     */
    private String editState;
    
    /**
     * 角色id
     */
    private Long roleId;
    
    /**
     * 角色类型
     */
    private Integer roleType;
    
    /**
     * 角色名称
     */
    private String roleName;
    
    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司
     * @return org_id 公司
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司
     * @param orgId 公司
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 公司名称
	 * @return orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * 公司名称
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}


	/**
     * 部门
     * @return dept_id 部门
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 部门
     * @param deptId 部门
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 部门名称
	 * @return deptName
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * 部门名称
	 * @param deptName
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 名称拼音
	 * @return namePinyin
	 */
	public String getNamePinyin() {
		return namePinyin;
	}

	/**
	 * @param namePinyin 名称拼音
	 */
	public void setNamePinyin(String namePinyin) {
		this.namePinyin = namePinyin;
	}

	/**
     * 用户名
     * @return name 用户名
     */
    public String getCode() {
        return code;
    }

    /**
     * 用户名
     * @param code 用户名
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     * 性别
     * @return gender 性别
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别
     * @param gender 性别
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 性别名称
     * @return gender 性别名称
     */
    public String getGenderName() {
		return genderName;
	}

    /**
     * 性别名称
     * @param gender 性别性别名称
     */
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}

	/**
     * 邮箱
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 手机
     * @return phone 手机
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机
     * @param phone 手机
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 密码
     * @return password 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 加密盐
     * @return salt 加密盐
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 加密盐
     * @param salt 加密盐
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * 停启用状态，0启用、1停用
     * @return enable_state 停启用状态，0启用、1停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
	 * @return enableStateName
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * @param enableStateName 要设置的 enableStateName
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 停启用状态，0启用、1停用
     * @param enableState 停启用状态，0启用、1停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用操作人
     * @return enable_user_id 停启用操作人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用操作人
     * @param enableUserId 停启用操作人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用操作人名称
	 * @return enableUserName
	 */
	public String getEnableUserName() {
		return enableUserName;
	}

	/**
	 * 停启用操作人名称
	 * @param enableUserName
	 */
	public void setEnableUserName(String enableUserName) {
		this.enableUserName = enableUserName;
	}

	/**
     * 停启用日期
     * @return enable_time 停启用日期
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用日期
     * @param enableTime 停启用日期
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

	/**
     * 头像id
     * @return portrait_id 头像id
     */
    public String getPortraitId() {
        return portraitId;
    }

    /**
     * 头像id
     * @param portraitId 头像id
     */
    public void setPortraitId(String portraitId) {
        this.portraitId = portraitId;
    }

	/**
	 * @return job
	 */
	public Integer getJob() {
		return job;
	}

	/**
	 * @param job 要设置的 job
	 */
	public void setJob(Integer job) {
		this.job = job;
	}

	/**
	 * @return jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName 要设置的 jobName
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * 人员类型，1系统管理。2集团管理员。3公司管理员。4用户， 数据库默认用户
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 人员类型
	 * @param type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 人员类型名称
	 * @return
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * 人员类型名称
	 * @param typeName
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * 编辑状态
	 * @return
	 */
	public String getEditState() {
		return editState;
	}

	/**
	 * 编辑状态
	 * @param editState
	 */
	public void setEditState(String editState) {
		this.editState = editState;
	}

	/**
	 * 角色id
	 * @return roleId
	 */
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId 角色id
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * 角色类型
	 * @return roleType
	 */
	public Integer getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType 角色类型
	 */
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	/**
	 * 角色名称
	 * @return roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName 角色名称
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
    
}