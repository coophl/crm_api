package com.yonyou.crm.sys.user.entity;

public enum JobEnum {
	STAFF(1,"员工"),LEADER(2,"负责人"),LEADER_V(3,"其他负责人");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private JobEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    
}
