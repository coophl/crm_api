package com.yonyou.crm.sys.user.entity;

public enum TypeEnum {
	XTADMIN(1,"系统管理员"),JTADMIN(2,"集团管理员"),GSADMIN(3,"公司管理员"),USER(4,"普通用户");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	// 构造方法
    private TypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    
}
