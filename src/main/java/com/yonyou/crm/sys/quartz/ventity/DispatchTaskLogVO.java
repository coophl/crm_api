package com.yonyou.crm.sys.quartz.ventity;

import java.io.Serializable;
import java.util.Date;

public class DispatchTaskLogVO implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private Date starttime;

    /**
     * 
     */
    private Date endtime;

    /**
     * 
     */
    private Integer visible;

    /**
     * 
     */
    private Integer flag;

    /**
     * 
     */
    private String taskid;

    /**
     * 
     */
    private byte[] content;
    
    private String logDetail;

    /**
     * dispatch_tasklog
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }
    
    public String getLogDetail() {
		return logDetail;
	}

	public void setLogDetail(String logDetail) {
		this.logDetail = logDetail;
	}

	/**
     * 
     * @return starttime 
     */
    public Date getStarttime() {
        return starttime;
    }

    /**
     * 
     * @param starttime 
     */
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    /**
     * 
     * @return endtime 
     */
    public Date getEndtime() {
        return endtime;
    }

    /**
     * 
     * @param endtime 
     */
    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    /**
     * 
     * @return visible 
     */
    public Integer getVisible() {
        return visible;
    }

    /**
     * 
     * @param visible 
     */
    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    /**
     * 
     * @return flag 
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * 
     * @param flag 
     */
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * 
     * @return taskid 
     */
    public String getTaskid() {
        return taskid;
    }

    /**
     * 
     * @param taskid 
     */
    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    /**
     * 
     * @return content 
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * 
     * @param content 
     */
    public void setContent(byte[] content) {
        this.content = content;
    }
}