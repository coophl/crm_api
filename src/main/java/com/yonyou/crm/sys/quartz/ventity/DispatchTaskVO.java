package com.yonyou.crm.sys.quartz.ventity;

import java.io.Serializable;
import java.util.Date;

public class DispatchTaskVO implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String sysid;

    /**
     * 
     */
    private String tenantid;

    /**
     * 
     */
    private String code;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private String groupid;
    /**
     * 任务分组名称
     */
    private String groupName;

    /**
     * 
     */
    private String cronexpression;

    /**
     * 
     */
    private String description;

    /**
     * 
     */
    private Integer enableStatus;
    /**
     * 
     */
    private Date firstime;
    
    private String url;
    
    private Byte is_deleted;
    /**
     * 创建人
     */
    private String created_user_id;
    /**
     * 创建时间
     */
    private String created_time;
    /**
     * 修改人
     */
    private String modified_user_id;
    
    private String modified_time;
    
    private Date ts;

    /**
     * dispatch_task
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return sysid 
     */
    public String getSysid() {
        return sysid;
    }

    /**
     * 
     * @param sysid 
     */
    public void setSysid(String sysid) {
        this.sysid = sysid;
    }

    /**
     * 
     * @return tenantid 
     */
    public String getTenantid() {
        return tenantid;
    }

    /**
     * 
     * @param tenantid 
     */
    public void setTenantid(String tenantid) {
        this.tenantid = tenantid;
    }

    /**
     * 
     * @return code 
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code 
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return name 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return groupid 
     */
    public String getGroupid() {
        return groupid;
    }

    /**
     * 
     * @param groupid 
     */
    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    /**
     * 
     * @return cronexpression 
     */
    public String getCronexpression() {
        return cronexpression;
    }

    /**
     * 
     * @param cronexpression 
     */
    public void setCronexpression(String cronexpression) {
        this.cronexpression = cronexpression;
    }


    /**
     * 
     * @return description 
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description 
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return enableStatus 
     */
    public Integer getEnableStatus() {
        return enableStatus;
    }

    /**
     * 
     * @param enablestatus 
     */
    public void setEnableStatus(Integer enableStatus) {
        this.enableStatus = enableStatus;
    }

    /**
     * 
     * @return firstime 
     */
    public Date getFirstime() {
        return firstime;
    }
    
	/**
     * 
     * @param firstime 
     */
    public void setFirstime(Date firstime) {
        this.firstime = firstime;
    }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated_user_id() {
		return created_user_id;
	}

	public void setCreated_user_id(String created_user_id) {
		this.created_user_id = created_user_id;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public String getModified_user_id() {
		return modified_user_id;
	}

	public void setModified_user_id(String modified_user_id) {
		this.modified_user_id = modified_user_id;
	}

	public String getModified_time() {
		return modified_time;
	}

	public void setModified_time(String modified_time) {
		this.modified_time = modified_time;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Byte getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Byte is_deleted) {
		this.is_deleted = is_deleted;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
}