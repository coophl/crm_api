package com.yonyou.crm.sys.quartz.rmitf;

import com.yonyou.crm.sys.quartz.ventity.DispatchTaskLogVO;

public interface IDispatchTaskLogRmService {
	/**
	 * 保存日志
	 * @param vo
	 * @return
	 */
	public int saveTaskLog(DispatchTaskLogVO vo);
	/**
	 * 更新任务日志
	 * @param vo
	 * @return
	 */
	public DispatchTaskLogVO updateTaskLog(DispatchTaskLogVO vo);
	/**
	 * 删除任务日志
	 * @param id
	 * @return
	 */
	public int deleteTaskLog(String id); 
}
