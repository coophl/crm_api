package com.yonyou.crm.sys.quartz.rmitf;


import com.yonyou.crm.sys.quartz.ventity.DispatchGroupVO;

import java.util.List;
import java.util.Map;

public interface IDispatchGroupRmService {
	/**
	 * 保存任务分组
	 * @param vo
	 * @return
	 */
	public DispatchGroupVO saveDispatchGroupVO(DispatchGroupVO vo);

	/**
	 * 查询任务分组
	 * @return
	 */
	public List<Map<String, Object>> selectDispatchGroupList();
}
