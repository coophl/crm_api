package com.yonyou.crm.sys.quartz.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sys.quartz.ventity.DispatchGroupVO;
import com.yonyou.crm.sys.quartz.ventity.DispatchTaskLogVO;
import com.yonyou.crm.sys.quartz.ventity.DispatchTaskVO;

public interface IDispatchTaskRmService {
	/**
	 * 保存任务
	 * 
	 * @param vo
	 * @return
	 */
	public DispatchTaskVO saveDispatchTaskVO(DispatchTaskVO vo);

	/**
	 * 查询任务列表
	 * 
	 * @param page
	 * @param paramMap
	 * @return
	 */
	public Page<DispatchTaskVO> selectTask(Page<DispatchTaskVO> page,
                                           Map<String, Object> paramMap);

	/**
	 * 删除任务
	 * 
	 * @param id
	 * @return
	 */
	public void deleteTask(String id,Page<DispatchTaskVO> page,
            Map<String, Object> paramMap);

	/**
	 * 更新任务
	 * 
	 * @param vo
	 * @return
	 */
	public DispatchTaskVO updateDispatchTaskVO(DispatchTaskVO vo);

	/**
	 * 获取任务详情
	 * 
	 * @param vo
	 * @param id
	 * @return
	 */
	public DispatchTaskVO getVOById(DispatchTaskVO vo, String id);

	/**
	 * 停启用
	 * 
	 * @param id
	 * @param enableStatus
	 * @return
	 */
	public DispatchTaskVO updateEnableStatus(String id, Integer enableStatus);

	/**
	 * 立即执行
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> triggerTask(String id);

	/**
	 * 获取日志详情
	 * 
	 * @param id
	 * @return
	 */
	public DispatchTaskLogVO getTaskLogDetail(String id);
	/**
	 * 获取参照（参照后台任务分组）
	 * @param page
	 * @param paraMap
	 * @return
	 */
	public Page<DispatchGroupVO> getRefPage(Page<DispatchGroupVO> page, Map<String, Object> paraMap);
	/**
	 * 获取参照列表
	 * @param paraMap
	 * @return
	 */
	public List<DispatchGroupVO> getRefList(Map<String, Object> paraMap) ;

}
