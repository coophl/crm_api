package com.yonyou.crm.sys.quartz.ventity;

import java.io.Serializable;

public class DispatchGroupVO implements Serializable {
    /**
     *
     */
    private String id;

    /**
     *
     */
    private String tenantid;

    /**
     *
     */
    private String name;

    /**
     * dispatch_group
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return tenantid
     */
    public String getTenantid() {
        return tenantid;
    }

    /**
     *
     * @param tenantid
     */
    public void setTenantid(String tenantid) {
        this.tenantid = tenantid;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}