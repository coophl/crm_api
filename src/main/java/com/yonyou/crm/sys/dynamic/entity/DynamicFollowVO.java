package com.yonyou.crm.sys.dynamic.entity;

import java.util.Date;

public class DynamicFollowVO {
	/**
	 * 租户
	 */
	private Long tenantId;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 动态主键
	 */
	private Long dynamicId;
	/**
	 * 关注类型
	 */
	private Integer followType;
	/**
	 * 关注人ID
	 */
	private Long userId;
	/**
	 * 负责人部门，当关注类型为负责人时才有值
	 */
	private Long deptId;
	/**
	 * 动态创建时间
	 */
	private Date createdTime;
	
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDynamicId() {
		return dynamicId;
	}
	public void setDynamicId(Long dynamicId) {
		this.dynamicId = dynamicId;
	}
	public Integer getFollowType() {
		return followType;
	}
	public void setFollowType(Integer followType) {
		this.followType = followType;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	
}
