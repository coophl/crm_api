package com.yonyou.crm.sys.dynamic.entity;

public enum DynamicFollowTypeEnum {
	
	OWNER(1,"负责人"),FOLLOW(2,"参与人");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private DynamicFollowTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
