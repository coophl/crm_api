package com.yonyou.crm.sys.dynamic.rmitf;

import java.util.List;

import com.yonyou.crm.sys.dynamic.entity.DynamicVO;

public interface IDynamicRmService {

	/**
	 * 生成新动态
	 * @param vo
	 * @param followers 参与人列表
	 */
	public void insert(DynamicVO vo,Long[] followers);
	
	/**
	 * 动态首页查询
	 * @return
	 */
	public DynamicVO[] getDynamic(Long userId,Long roleId,int pageIndex,int pageSize);
	
	/**
	 * 业务对象动态查询
	 * @param objType 对象类型
	 * @param userId 对象ID
	 * @return
	 */
	public DynamicVO[] getBusiDynamic(Long objType,Long objId);
	
	/**
	 * 我的动态
	 * @param userId
	 * @return
	 */
	public DynamicVO[] getMyDynamic(Long userId);
}
