package com.yonyou.crm.sys.dynamic.entity;

import java.io.Serializable;
import java.util.Date;

public class DynamicVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 租户
	 */
	private Long tenantId;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 动态所属组织
	 */
	private Long orgId;
	/**
	 * 动态触发人
	 */
	private Long ownerId;
	/**
	 * 动态类型：1.拜访、2.电话、3.邮件、4.变更历史
	 */
	private Integer type;
	/**
	 * 业务对象类型
	 */
	private Long objType;
	/**
	 * 业务对象ID
	 */
	private Long objId;
	/**
	 * 动态内容
	 */
	private String content;
	/**
	 * 照片
	 */
	private String photos;
	/**
	 * 动态创建时间
	 */
	private Date createdTime;
	
	private String formatTime;
	
	/**
	 * 动态触发人名称
	 */
	private String ownerName;
	
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getObjType() {
		return objType;
	}
	public void setObjType(Long objType) {
		this.objType = objType;
	}
	public Long getObjId() {
		return objId;
	}
	public void setObjId(Long objId) {
		this.objId = objId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPhotos() {
		return photos;
	}
	public void setPhotos(String photos) {
		this.photos = photos;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public String getFormatTime() {
		return formatTime;
	}
	public void setFormatTime(String formatTime) {
		this.formatTime = formatTime;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	@Override
	public String toString() {
		return "DynamicVO [tenantId=" + tenantId + ", id=" + id + ", orgId="
				+ orgId + ", ownerId=" + ownerId + ", type=" + type
				+ ", objType=" + objType + ", objId=" + objId
				+ ", content=" + content + ", photos=" + photos
				+ ", createdTime=" + createdTime + "]";
	}
	
	
}
