package com.yonyou.crm.sys.dataright.entity;

public enum RightActionEnum {
	VIEW(1,"查看"),EDIT(2,"编辑"),DELETE(3,"删除");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private RightActionEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}