package com.yonyou.crm.sys.dataright.entity;

public enum RightTypeEnum {
	OWNER(1,"负责人"),SELFDEPT(2,"本部门"),SELFDEPTANDCHILD(3,"本部门及下级部门"),
	SELFORG(4,"本公司"),SELFORGANDCHILD(5,"本公司及下级公司"),TENANT(6,"集团");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private RightTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
