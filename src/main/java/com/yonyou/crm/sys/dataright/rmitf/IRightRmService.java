package com.yonyou.crm.sys.dataright.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sys.dataright.entity.RightConfig;
import com.yonyou.crm.sys.dataright.entity.RightTypeVO;
import com.yonyou.crm.sys.dataright.entity.SuperVO;

public interface IRightRmService {
	/**
	 * 获取权限sql
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @return
	 */
	String getRightSql(Long roleId,Long objId, Long actionId);
	/**
	 * 判断id的数据是否有权访问
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @param id 数据的主键
	 * @return
	 */
	boolean hasRight(Long roleId,Long objId, Long actionId,Long id);
	/**
	 * 判断vo数据是否有权访问
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @param vo 数据
	 * @return
	 */
	boolean hasRight(Long roleId,Long objId, Long actionId,SuperVO vo);
	/**
	 * 对ids对应的数据进行过滤，返回有权访问的部分
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @param ids 数据的主键数组
	 * @return
	 */
	Long[] filterWithRight(Long roleId,Long objId, Long actionId,Long[] ids);
	/**
	 * 对数据进行过滤，返回有权访问的部分
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @param ids 数据数组
	 * @return
	 */
	SuperVO[] filterWithRight(Long roleId,Long objId, Long actionId,SuperVO[] ids);
	/**
	 * 获取权限的配置信息
	 * @param roleId 角色
	 * @param objId 对象
	 * @param actionId 动作
	 * @return
	 */
	RightConfig getRightConfig(Long roleId,Long objId, Long actionId);
	/**
	 * 分配数据资源
	 * @param roleId
	 * @param checked
	 * @param typeId
	 */
	void assign(Long roleId,String typeId);
	/**
	 * 获取数据权限
	 * @param roleId
	 * @return
	 */
	List<RightTypeVO> right(Long roleId);
}
