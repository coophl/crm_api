package com.yonyou.crm.sys.dataright.entity;

import java.io.Serializable;

public class RightObject  implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	private String id;
	/**
	 * 租户
	 */
	private String tenantId;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 类型：对应业务对象表
	 */
	private String type;
	/**
	 * 对象对应表名
	 */
	private String tableName;
	/**
	 * 主键字段
	 */
	private String pkField;
	/**
	 * 负责人字段
	 */
	private String ownerField;
	/**
	 * 部门字段
	 */
	private String deptField;
	/**
	 * 组织字段
	 */
	private String orgField;
	/**
	 * 租户字段
	 */
	private String tenantField;
	/**
	 * 参与人表名
	 */
	private String relUserTableName;
	/**
	 * 参与人表数据ID字段名
	 */
	private String relDataIdField;
	/**
	 * 参与人表参与人ID字段名
	 */
	private String relUserField;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getPkField() {
		return pkField;
	}
	public void setPkField(String pkField) {
		this.pkField = pkField;
	}
	public String getOwnerField() {
		return ownerField;
	}
	public void setOwnerField(String ownerField) {
		this.ownerField = ownerField;
	}
	public String getDeptField() {
		return deptField;
	}
	public void setDeptField(String deptField) {
		this.deptField = deptField;
	}
	public String getOrgField() {
		return orgField;
	}
	public void setOrgField(String orgField) {
		this.orgField = orgField;
	}
	public String getTenantField() {
		return tenantField;
	}
	public void setTenantField(String tenantField) {
		this.tenantField = tenantField;
	}
	public String getRelUserTableName() {
		return relUserTableName;
	}
	public void setRelUserTableName(String relUserTableName) {
		this.relUserTableName = relUserTableName;
	}
	public String getRelDataIdField() {
		return relDataIdField;
	}
	public void setRelDataIdField(String relDataIdField) {
		this.relDataIdField = relDataIdField;
	}
	public String getRelUserField() {
		return relUserField;
	}
	public void setRelUserField(String relUserField) {
		this.relUserField = relUserField;
	}
	
}
