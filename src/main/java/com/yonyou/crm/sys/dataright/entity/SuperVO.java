package com.yonyou.crm.sys.dataright.entity;

public abstract class SuperVO {
	public abstract Long getOwner();
	public abstract Long getDeptId();
	public abstract Long getOrgId();
	public abstract Long getTenantId();
}
