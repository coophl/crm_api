package com.yonyou.crm.sys.dataright.entity;

import java.io.Serializable;
import java.util.Date;

public class RightConfig implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 租户
	 */
	private Long tenantId;
	/**
	 * 创建人
	 */
	private Long sysCreatedUserId;
	/**
	 * 创建时间
	 */
	private Date sysCreatedTime;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 角色
	 */
	private Long roleId;
	/**
	 * 对象
	 */
	private RightObject rightObject;
	/**
	 * 动作
	 */
	private Long actionId;
	/**
	 * 权限类型
	 */
	private Long typeId;
	/**
	 * 权限扩展类型
	 */
	private Long exTypeId;
	/**
	 * 权限扩展属性
	 */
	private String exAttr;
	
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}
	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}
	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}
	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public RightObject getRightObject() {
		return rightObject;
	}
	public void setRightObject(RightObject rightObject) {
		this.rightObject = rightObject;
	}
	public Long getActionId() {
		return actionId;
	}
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Long getExTypeId() {
		return exTypeId;
	}
	public void setExTypeId(Long exTypeId) {
		this.exTypeId = exTypeId;
	}
	public String getExAttr() {
		return exAttr;
	}
	public void setExAttr(String exAttr) {
		this.exAttr = exAttr;
	}
	
	
}
