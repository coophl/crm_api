package com.yonyou.crm.sys.dataright.entity;

/**
 * 
 * 记录权限对象的枚举，权限对象上额外记录着负责人，部门。组织。参与表等信息。
 * 
 * @see ObjTypeEnum 枚举值要与ObjTypeEnum保持一致
 * 参照表sys_rightobject
 * @author litcb
 *
 */
public enum RightObjectTypeEnum {
	CUSTOMER(1,"客户"),
	LEAD(2,"线索"),
	CONTACT(3,"联系人"),
	OPPORTUNITY(4,"商机"),
	USER(5,"用户"),
	
	DYNAMIC(7,"动态");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private RightObjectTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
