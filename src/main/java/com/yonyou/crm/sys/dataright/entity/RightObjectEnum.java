package com.yonyou.crm.sys.dataright.entity;

public enum RightObjectEnum {
	CUSTOMEROBJECT(1,"客户-公司");
	
	//枚举项对应int值
	long value;
	//枚举项显示值
	String name;

	// 构造方法
	private RightObjectEnum(long value, String name) {
		this.value = value;
		this.name = name;
	}
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
