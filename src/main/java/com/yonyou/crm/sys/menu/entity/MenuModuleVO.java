package com.yonyou.crm.sys.menu.entity;

import java.io.Serializable;


/**
 * 模块
 * @author biyda
 *
 */
public class MenuModuleVO implements Serializable{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 8085507717696664875L;

	private Long id;
	 
	 /**
	  * 名称
	  */
	 private String name;
	 
	 /**
	  * 编码
	  */
	 private String code;
	 
	 /**
	  * 分组
	  */
	private MenuGroupVO[] child;
	 
	 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public MenuGroupVO[] getChild() {
		return child;
	}

	public void setChild(MenuGroupVO[] child) {
		this.child = child;
	}

	 
	 
	 
	 
}
