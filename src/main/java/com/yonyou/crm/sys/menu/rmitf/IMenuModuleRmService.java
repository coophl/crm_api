package com.yonyou.crm.sys.menu.rmitf;

import java.util.List;

import com.yonyou.crm.sys.menu.entity.MenuModuleVO;

public interface IMenuModuleRmService {

	public List<MenuModuleVO> getModuleList();
}
