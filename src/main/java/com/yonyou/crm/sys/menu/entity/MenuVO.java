package com.yonyou.crm.sys.menu.entity;

import java.io.Serializable;

/**
 * 菜单
 * @author biyda
 *
 */
public class MenuVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7182277143954910372L;

	/**
     * 菜单项主键
     */
    private Long id;
    
    /**
     * 分组ID
     */
    private Long menuGroupId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    
    /**
     * 路由标识
     */
    private String webId;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMenuGroupId() {
		return menuGroupId;
	}

	public void setMenuGroupId(Long menuGroupId) {
		this.menuGroupId = menuGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWebId() {
		return webId;
	}

	public void setWebId(String webId) {
		this.webId = webId;
	}

	 
    
    
    
	
}
