package com.yonyou.crm.sys.menu.entity;

import java.io.Serializable;

/**
 * 分组
 * @author biyda
 *
 */
public class MenuGroupVO implements Serializable{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 637321329722511051L;

	private Long id;
	 
	 /**
	  * 模块
	  */
	 private Long moduleId;
	 
	 /**
	  * 名称
	  */
	 private String name;
	 
	 /**
	  * 编码
	  */
	 private String code;
	 
	 /**
	  * 菜单
	  */
	private MenuVO[] child;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	 

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public MenuVO[] getChild() {
		return child;
	}

	public void setChild(MenuVO[] child) {
		this.child = child;
	}

	 
	 

	 
	 
	 
	 
}
