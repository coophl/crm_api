package com.yonyou.crm.sys.region.entity;

import java.io.Serializable;
import java.util.*;

public class RegionsVO implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    //private String code;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private String shortName;

    /**
     * 
     */
    //private String parentCode;

    /**
     * 
     */
    //private Long parentId;

    /**
     * 
     */
    //private Integer level;

    /**
     * 
     */
    private String cityCode;

    /**
     * 
     */
    private String zipCode;

    /**
     * 
     */
    //private String pinyin;
    
    /*
     * 下属子目录
     */
    private ArrayList<RegionsVO> sub;

    /**
     * sys_region
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return code 
     
    public String getCode() {
        return code;
    }
    */

    /**
     * 
     * @param code 

    public void setCode(String code) {
        this.code = code;
    }
    */

    /**
     * 
     * @return name 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return short_name 
     
    public String getShortName() {
        return shortName;
    }
    */

    /**
     * 
     * @param shortName 
     
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    */

    /**
     * 
     * @return parent_code 
     
    public String getParentCode() {
        return parentCode;
    }
    */

    /**
     * 
     * @param parentCode 
     
    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
    */

    /**
     * 
     * @return parent_id 
     
    public Long getParentId() {
        return parentId;
    }
    */

    /**
     * 
     * @param parentId 
     
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    */

    /**
     * 
     * @return level 
     
    public Integer getLevel() {
        return level;
    }
    */

    /**
     * 
     * @param level 
     
    public void setLevel(Integer level) {
        this.level = level;
    }
    */

    /**
     * 
     * @return city_code 
     
    public String getCityCode() {
        return cityCode;
    }
    */

    /**
     * 
     * @param cityCode 
     
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    */

    /**
     * 
     * @return zip_code 
     
    public String getZipCode() {
        return zipCode;
    }
    */

    /**
     * 
     * @param zipCode 
     
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    */

    /**
     * 
     * @return pinyin 
     
    public String getPinyin() {
        return pinyin;
    }
    */

    /**
     * 
     * @param pinyin 
     
    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }
    */

	public ArrayList<RegionsVO> getSub() {
		return sub;
	}

	public void setSub(ArrayList<RegionsVO> sub) {
		this.sub = sub;
	}
}