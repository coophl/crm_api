package com.yonyou.crm.sys.region.rmitf;

import java.util.*;

public interface IRegionsRmService {

	List<Map<String, Object>> getRegionCityDistrictData();

    void syncRegionJW();
}
