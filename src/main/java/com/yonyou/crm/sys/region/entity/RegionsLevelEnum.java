package com.yonyou.crm.sys.region.entity;

public enum RegionsLevelEnum {
	COUNTRY(0,"国家"),PROVINCE(1,"省"),CITY(2,"市"),DISTRICT(3,"区/县");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private RegionsLevelEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
