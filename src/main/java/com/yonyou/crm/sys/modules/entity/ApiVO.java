package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class ApiVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;

    /**
     * ID,保留1,000,000以内作为系统预置备用
     */
    private Long id;
    
    /**
     * 对象模块ID
     */
    private Long objId;
    
    /**
     * api地址
     */
    private String api;
    
    /**
     * 接口条件过滤要用到的字段
     */
    private String[] filterFields;
    
    private String filterFieldsStr;
    private String filter;

    /**
     * 接口URL上要求必须替换的字段
     */
    private String[] apiURLSubstituteFields;
    
    /**
     * 终端类型
     */
    private Integer clientType;
  
    /**
     * 是否默认
     */
    private Integer isDefault;
    
    /**
     * sys_mt_api
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @return tenant_id 租户ID,租户ID为0的表明是系统预置对象
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @param tenantId 租户ID,租户ID为0的表明是系统预置对象
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 业务类型,保留1,000,000以内作为系统预置备用
     * @return id 业务类型,保留1,000,000以内作为系统预置备用
     */
    public Long getId() {
        return id;
    }

    /**
     * 保留1,000,000以内作为系统预置备用
     * @param id 业务类型,保留1,000,000以内作为系统预置备用
     */
    public void setId(Long id) {
        this.id = id;
    }

	public String getFilterFieldsStr() {
		return filterFieldsStr;
	}

	public void setFilterFieldsStr(String filterFieldsStr) {
		this.filterFieldsStr = filterFieldsStr;
	}

	public Integer getClientType() {
		return clientType;
	}

	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String[] getFilterFields() {
		return filterFields;
	}

	public void setFilterFields(String[] filterFields) {
		this.filterFields = filterFields;
	}

	public String[] getApiURLSubstituteFields() {
		return apiURLSubstituteFields;
	}

	public void setApiURLSubstituteFields(String[] apiURLSubstituteFields) {
		this.apiURLSubstituteFields = apiURLSubstituteFields;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

}