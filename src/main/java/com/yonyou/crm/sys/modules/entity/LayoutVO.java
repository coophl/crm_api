package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LayoutVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;

    /**
     * 布局ID,保留1,000,000以内作为系统预置备用
     */
    private Long id;
    
    /**
     * 对象类型
     */
    private Long objId;
    
    /**
     * 业务类型ID
     */
    private Long biztypeId;
    
    /**
     * 业务类型名称
     */
    private String biztypeName;
    
    /**
     * 模板名称
     */
    private String name;
    
    /**
     * 页面布局描述
     */
    private String description;
    
    /**
     * 排序
     */
    private Long sortOrder;
    /**
     * 模板类型，1.详情，2.编辑
     */
    private Integer type;
    
    /**
     * 终端类型，1.PC，2.移动
     */
    private Integer clientType;
    
    private List<BlockVO> mainLayout;
    
    private List<ItemLayoutItemVO> itemLayout;
    
    private List<RelationLayoutVO> relationLayout;
 
    /**
     * 明细布局，二维数组
     */
    private List<List<ItemLayoutItemVO>> itemLayoutForClient;
    

    /**
     * 是否是默认
     */
    private Integer isDefault;
    
    /**
     * 是否已默认
     */
    private Integer isDeleted;

    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;
    
    /**
     * 停启用状态，1启用2停用
     */
    private Integer enableState;

    /**
     * 停用时间
     */
    private Date enableTime;
    
    /**
     * 启用状态（启用，停用）
     */
    private String enableStateName;
    
    /**
     * 停启用人
     */
    private Long enableUserId;
    
    /**
     * 是否选中状态（前端使用，虚拟属性）
     */
    private Integer checked;
    /**
     * sys_mt_layout
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @return tenant_id 租户ID,租户ID为0的表明是系统预置对象
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @param tenantId 租户ID,租户ID为0的表明是系统预置对象
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 业务类型名称
     * @return name 业务类型名称
     */
    public String getName() {
        return name;
    }

    /**
     * 业务类型名称
     * @param name 业务类型名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 是否是默认
     * @return is_default 是否是默认
     */
    public Integer getIsDefault() {
        return isDefault;
    }
    
    /**
     * 是否是默认
     * @param isDefault 是否是默认
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getClientType() {
		return clientType;
	}

	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	public List<BlockVO> getMainLayout() {
		return mainLayout;
	}

	public void setMainLayout(List<BlockVO> mainLayout) {
		this.mainLayout = mainLayout;
	}

	public List<RelationLayoutVO> getRelationLayout() {
		return relationLayout;
	}

	public void setRelationLayout(List<RelationLayoutVO> relationLayout) {
		this.relationLayout = relationLayout;
	}

	public List<ItemLayoutItemVO> getItemLayout() {
		return itemLayout;
	}

	public void setItemLayout(List<ItemLayoutItemVO> itemLayout) {
		this.itemLayout = itemLayout;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<List<ItemLayoutItemVO>> getItemLayoutForClient() {
		return itemLayoutForClient;
	}

	public void setItemLayoutForClient(List<List<ItemLayoutItemVO>> itemLayoutForClient) {
		this.itemLayoutForClient = itemLayoutForClient;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Long getBiztypeId() {
		return biztypeId;
	}

	public void setBiztypeId(Long biztypeId) {
		this.biztypeId = biztypeId;
	}

	public Integer getEnableState() {
		return enableState;
	}

	public void setEnableState(Integer enableState) {
		this.enableState = enableState;
	}

	public Date getEnableTime() {
		return enableTime;
	}

	public void setEnableTime(Date enableTime) {
		this.enableTime = enableTime;
	}

	public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	public Long getEnableUserId() {
		return enableUserId;
	}

	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}

	public String getBiztypeName() {
		return biztypeName;
	}

	public void setBiztypeName(String biztypeName) {
		this.biztypeName = biztypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getChecked() {
		return checked;
	}

	public void setChecked(Integer checked) {
		this.checked = checked;
	}

}