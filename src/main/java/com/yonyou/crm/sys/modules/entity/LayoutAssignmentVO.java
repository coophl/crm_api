package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class LayoutAssignmentVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;

    /**
     * 终端类型，1.PC，2.移动
     */
    private Integer clientType;
    
    /**
     * 布局类型
     */
    private Integer layoutType;
    
    /**
     * 业务类型
     */
    private Long biztypeId;
    
    /**
     * 角色ID
     */
    private Long roleId;
    
    /**
     * 布局ID
     */
    private Long layoutId;
    
    /**
     * sys_mt_layout
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @return tenant_id 租户ID,租户ID为0的表明是系统预置对象
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @param tenantId 租户ID,租户ID为0的表明是系统预置对象
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

	public Integer getClientType() {
		return clientType;
	}

	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	public Long getBiztypeId() {
		return biztypeId;
	}

	public void setBiztypeId(Long biztypeId) {
		this.biztypeId = biztypeId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	public Integer getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(Integer layoutType) {
		this.layoutType = layoutType;
	}

}