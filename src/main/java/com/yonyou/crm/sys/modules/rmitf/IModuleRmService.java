package com.yonyou.crm.sys.modules.rmitf;

import java.util.List;

import com.yonyou.crm.sys.modules.entity.ModuleVO;

public interface IModuleRmService {
	
	public List<ModuleVO> getModuleList();

	public ModuleVO getModule(Long objId);

}
