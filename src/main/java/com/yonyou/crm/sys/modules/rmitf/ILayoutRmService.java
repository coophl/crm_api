package com.yonyou.crm.sys.modules.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sys.modules.entity.BiztypeRoleVO;
import com.yonyou.crm.sys.modules.entity.LayoutAssignmentVO;
import com.yonyou.crm.sys.modules.entity.LayoutCatalogVO;
import com.yonyou.crm.sys.modules.entity.LayoutVO;

public interface ILayoutRmService {
	
	public List<LayoutVO> getLayoutList(Long objId);
	
	public LayoutVO getLayout(Long id );
	
	public LayoutVO addLayout(LayoutVO layoutVO);
	
	public LayoutVO updateLayout(LayoutVO layoutVO);
	
	public void assignLayouts(Integer clientType, Integer layoutType,List<LayoutAssignmentVO> assignmentVOs);

	public void deleteLayout(Long id);
	
	public List<LayoutCatalogVO> getLayoutCatalogList(Long objId);
	
	public List<Map<String, Object>>  getRelatedObjects(Long objId);
	
	public Map<String, Object> getAssignmentTemplate(Long objId, Integer clientType,Integer layoutType);
	
	public LayoutVO getDefaultLayout(Long objId, Integer clientType, Integer layoutType);
	
	public void disableLayout(Long layoutId);
	
	public void enableLayout(Long layoutId);
	
	public List<BiztypeRoleVO> getAssignRoles(Map<String, Object> param);

}
