package com.yonyou.crm.sys.modules.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sys.modules.entity.BiztypeVO;

public interface IBiztypeRmService {
	
	public List<BiztypeVO> getBiztypeList(Long objId);
	
	public BiztypeVO addBiztype(BiztypeVO biztypeVO);

	public BiztypeVO getBiztype(Long biztypeId);

	public BiztypeVO updateBiztype(BiztypeVO biztypeVO);
	
	public void deleteBiztype(Long biztypeId);

	public void enableBiztype(Long biztypeId);
	
	public void disableBiztype(Long biztypeId);

	public Map<String, Object> getModuleBiztypesForCurrentRole(String objName);
	
	public List<BiztypeVO> batchUpdateEnableState(String[] idArray, Integer enableState, Long objId);
	
}
