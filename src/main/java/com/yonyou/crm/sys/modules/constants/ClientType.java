package com.yonyou.crm.sys.modules.constants;

public enum ClientType {
     
    PC("PC端",1) ,  MOBILE("移动端", 2);
     
    private String name ;
    private int index ;
     
    private ClientType( String name , int index ){
        this.name = name ;
        this.index = index ;
    }
 
    public int getIndex() {
        return index;
    }
    
    public String getName() {
        return name;
    }
    
    public   static ClientType getClientTypeByIndex(int idx) {
    	for (ClientType cType : values()) {
    		if (cType.index == idx) {
    			return cType;
    		}
    	}
    	// TODO 如果不在范围内则抛出异常
    	return null;
    }
     
 
}