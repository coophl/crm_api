package com.yonyou.crm.sys.modules.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sys.modules.entity.FieldVO;

public interface IFieldRmService {
	
	public List<FieldVO> getFieldList(Long objId);
	
	public FieldVO getField(String fullname );
	
	public FieldVO addField(FieldVO fieldVO);

	public FieldVO updateField(FieldVO fieldVO);
	
	public void deleteField(String fullname );

	public  List<Object>  getCustomFieldTypes(Long objId);
	
	public Map<String, Object> getBaseDocList(Map<String, Object> param);
}
