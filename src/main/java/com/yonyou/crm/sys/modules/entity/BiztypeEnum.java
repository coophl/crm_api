package com.yonyou.crm.sys.modules.entity;

public enum BiztypeEnum {
	CONBIZTYPE_1((long)1,"默认业务类型1"),LEADBIZTYPE_1((long)3,"教育行业"),OPPOBIZTYPE_1((long)6,"自定义业务类型1"),
	OPPOBIZTYPE_2((long)7,"n2"),COMPBIZTYPE_1((long)8,"大型教育"),CUSTOMER_1((long)11,"客户默认业务类型"),
	PRODUCT_1((long)12,"产品默认业务类型");
	//枚举项对应int值
	Long value;
	//枚举项显示值
	String name;
	private BiztypeEnum(Long value, String name) {
		this.value = value;
		this.name = name;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
