package com.yonyou.crm.sys.modules.constants;

public enum LayoutType {
     
    VIEW("查看",1) ,  EDIT("编辑", 2) ;
     
    private String name ;
    private int index ;
     
    private LayoutType( String name , int index ){
        this.name = name ;
        this.index = index ;
    }
 
    public int getIndex() {
        return index;
    }
    public String getName() {
        return name;
    }
    
    public   static LayoutType getLayoutTypeByIndex(int idx) {
    	for (LayoutType cType : values()) {
    		if (cType.index == idx) {
    			return cType;
    		}
    	}
    	// TODO 如果不在范围内则抛出异常
    	return null;
    }
     
 
}