package com.yonyou.crm.sys.modules.constants;

import java.util.HashMap;
import java.util.Map;

public class FieldType {

	// 单行文本
	public final static Integer Text = 1; 
	// 单选
	public final static Integer Radio = 2;
	// 多行文本
	public final static Integer TextArea = 3;
	// 复选框
	public final static Integer CheckBox = 4;
	//整型
	public final static Integer Integer = 5;
	//浮点型
	public final static Integer Decimal = 6;
	//多选
	public final static Integer MultiSelect = 7;
	//图像
	public final static Integer Image = 8;
	//货币
	public final static Integer Currency = 9;
	//日期
	public final static Integer Date = 10;
	//日期时间
	public final static Integer DateTime = 11;
	//电话
	public final static Integer Tel = 12;
	//邮箱
	public final static Integer Email = 13;
	//网址
	public final static Integer URL = 14;
	//位置
	public final static Integer GeoLocation = 15;
	//查找型参照
	public final static Integer Lookup = 16;
	//主从型参照
	public final static Integer MasterDetail = 17;
	//百分数
	public final static Integer Percent = 18;
	// 富文本类型
	public final static Integer RichText = 19;
	// 省市区
	public final static Integer AdministrativeDivision = 20;
	
	private static Map<Integer, String> mobileUiTypeMap;
	private static Map<Integer, String> pcUiTypeMap;
	static {
		mobileUiTypeMap = new HashMap<Integer, String>();
		mobileUiTypeMap.put(Text, "singlelinetext");
		mobileUiTypeMap.put(Radio, "enum");
		mobileUiTypeMap.put(TextArea, "largetext");
		mobileUiTypeMap.put(CheckBox, "boolean");
		mobileUiTypeMap.put(Integer, "decimal");
		mobileUiTypeMap.put(Decimal, "decimal");
		mobileUiTypeMap.put(MultiSelect, "enum");
		mobileUiTypeMap.put(Image, "pic");
		mobileUiTypeMap.put(Currency, "money");
		mobileUiTypeMap.put(Date, "date");
		mobileUiTypeMap.put(DateTime, "date");
		mobileUiTypeMap.put(Tel, "phone");
		mobileUiTypeMap.put(Email, "email");
		mobileUiTypeMap.put(URL, "link");
		// TODO 待确定
		mobileUiTypeMap.put(GeoLocation, "address");//是否为address？
		mobileUiTypeMap.put(Lookup, "refertype");
		mobileUiTypeMap.put(MasterDetail, "refertype");
		mobileUiTypeMap.put(Percent, "percent");
		mobileUiTypeMap.put(RichText, "largetext");
		mobileUiTypeMap.put(AdministrativeDivision, "address_pcd");
		
		pcUiTypeMap = new HashMap<Integer, String>();
		pcUiTypeMap.put(Text, "");
		pcUiTypeMap.put(Radio, "");
		pcUiTypeMap.put(TextArea, "");
		pcUiTypeMap.put(CheckBox, "");
		pcUiTypeMap.put(Integer, "");
		pcUiTypeMap.put(Decimal, "");
		pcUiTypeMap.put(MultiSelect, "");
		pcUiTypeMap.put(Image, "");
		pcUiTypeMap.put(Currency, "");
		pcUiTypeMap.put(Date, "");
		pcUiTypeMap.put(DateTime, "");
		pcUiTypeMap.put(Tel, "");
		pcUiTypeMap.put(Email, "");
		pcUiTypeMap.put(URL, "");
		pcUiTypeMap.put(GeoLocation, "");
		pcUiTypeMap.put(Lookup, "");
		pcUiTypeMap.put(MasterDetail, "");
		pcUiTypeMap.put(Percent, "");
		pcUiTypeMap.put(RichText, "");
		pcUiTypeMap.put(AdministrativeDivision, "");
	}
	
	public static String getMobileUiType(Integer fieldType) {
		if (mobileUiTypeMap.containsKey(fieldType)) {
			return mobileUiTypeMap.get(fieldType);
		} else {
			return "";
		}
	}
	
	public static String getPcUiType(Integer fieldType) {
		if (pcUiTypeMap.containsKey(fieldType)) {
			return pcUiTypeMap.get(fieldType);
		} else {
			return "";
		}
	}
}
