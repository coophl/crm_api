package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

import com.yonyou.crm.sys.role.entity.RoleVO;

import java.util.Date;
import java.util.List;

public class BiztypeVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;

    /**
     * 业务类型,保留1,000,000以内作为系统预置备用
     */
    private Long id;
    
    /**
     * 对象模块ID
     */
    private Long objId;

    /**
     * 业务类型名称
     */
    private String name;
    
    /**
     * 角色ID，新增或修改时使用
     */
    private List<Long> roleIds;

    /**
     * 是否是用户自定义
     */
    private Integer isCustom;

    /**
     * 是否是默认
     */
    private Integer isDefault;

    /**
     * 是否启用
     */
    private Integer isEnabled;

    /**
     * 排序
     */
    private Long sortOrder;
    /**
     * 是否已删除
     */
    private Integer isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 添加时的版本
     */
    private String version;

    /**
     * 描述
     */
    private String description;
    
    /**
     * 适用角色（自定义组装字符串） 
     */
    private List<RoleVO> roles;
    
    /**
     * sys_mt_biztype
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @return tenant_id 租户ID,租户ID为0的表明是系统预置对象
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID,租户ID为0的表明是系统预置对象
     * @param tenantId 租户ID,租户ID为0的表明是系统预置对象
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 业务类型,保留1,000,000以内作为系统预置备用
     * @return id 业务类型,保留1,000,000以内作为系统预置备用
     */
    public Long getId() {
        return id;
    }

    /**
     * 业务类型,保留1,000,000以内作为系统预置备用
     * @param id 业务类型,保留1,000,000以内作为系统预置备用
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * 业务类型名称
     * @return name 业务类型名称
     */
    public String getName() {
        return name;
    }

    /**
     * 业务类型名称
     * @param name 业务类型名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 是否是用户自定义
     * @return is_custom 是否是用户自定义
     */
    public Integer getIsCustom() {
        return isCustom;
    }

    /**
     * 是否是用户自定义
     * @param i 是否是用户自定义
     */
    public void setIsCustom(Integer i) {
        this.isCustom = i;
    }

    /**
     * 是否是默认
     * @return is_default 是否是默认
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * 是否是默认
     * @param isDefault 是否是默认
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 是否启用
     * @return is_enabled 是否启用
     */
    public Integer getIsEnabled() {
        return isEnabled;
    }

    /**
     * 是否启用
     * @param isEnabled 是否启用
     */
    public void setIsEnabled(Integer isEnabled) {
        this.isEnabled = isEnabled;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 添加时的版本
     * @return version 添加时的版本
     */
    public String getVersion() {
        return version;
    }

    /**
     * 添加时的版本
     * @param version 添加时的版本
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 描述
     * @return description 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

	public List<RoleVO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleVO>  roles) {
		this.roles = roles;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

}