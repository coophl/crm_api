package com.yonyou.crm.sys.modules.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.bpub.list.entity.AppListVO;
import com.yonyou.crm.sys.modules.entity.LayoutCatalogVO;
import com.yonyou.crm.sys.modules.entity.ListLayoutAssignmentVO;
import com.yonyou.crm.sys.modules.entity.ListLayoutVO;

public interface IListLayoutRmService {
	
	public List<ListLayoutVO> getListLayoutList(Long objId);
	
	public ListLayoutVO getListLayout(Long id );
	
	public ListLayoutVO addListLayout(ListLayoutVO listLayoutVO);

	public ListLayoutVO updateListLayout(ListLayoutVO listLayoutVO);
	
	public void assignListLayouts(Integer clientType,List<ListLayoutAssignmentVO> listLayoutAssignmentVOs);
	
	public void deleteListLayout(Long id);
	
	public List<LayoutCatalogVO> getListLayoutCatalogList(Long objId);
	
	public Map<String, Object> getAssignmentTemplate(Long objId, Integer clientType );

	public AppListVO getAppListVOForCurrentUser(Long objId );
	
	public void disableListLayout(Long listLayoutId);
	
	public void enableListLayout(Long listLayoutId);
}
