package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class CurrencyVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置货币
     */
    private Long tenantId;
    
    
    /**
     * 货币ID
     */
    private Long id;

    /**
     * 货币名称
     */
    private String name;
    
    /**
     * 货币符号
     */
    private String symbol;
    
    /**
     * 货币代码
     */
    private String code;
    
    /**
     * 显示顺序
     */
    private Integer sortOrder;
    
    /**
     * sys_currency
     */
    private static final long serialVersionUID = 1L;

	public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getSorOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
}