package com.yonyou.crm.sys.modules.entity;
/**
 * 用于表单配置相关使用的枚举类
 * @author Administrator
 *
 */
public enum MtObjTypeEnum {
	CONTACT((long)1,"contact"),LEAD((long)2,"lead"),OPPORTUNITY((long)3,"opportunity"),COMPETITIVE((long)4,"competitor"),
	CUSTOMER((long)5,"customer"),PRODUCT((long)6,"product");
	//枚举项对应int值
	Long value;
	//枚举项显示值
	String name;
	private MtObjTypeEnum(Long value, String name) {
		this.value = value;
		this.name = name;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
