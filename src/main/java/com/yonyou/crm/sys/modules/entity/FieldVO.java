package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.doc.entity.BaseDocVO;

public class FieldVO implements Serializable,Cloneable {
	/**
     * 租户ID,租户ID为0的表明是系统预置属性
     */
    private Long tenantId;
    
    /**
     * 属性所在对象类型
     */
    private Long objId;
    
    /**
     * 属性类型
     */
    private Integer type;

    /**
     * 属性API名称
     * 字段名称，以字母开头，后面可以跟字母，数字和下划线, 自定义字段在后面自动加一个_cus后缀，该字段不允许修改
     */
    private String apiName;

    /**
     * 属性名全称
     *  模块名+属性名，如Customer.name
     */
    private String fullname;
    
    /**
     * 属性显示名称
     */
    private String name;
    
    /**
     * 是否是自定义
     */
    private Integer isCustom;
    
    /**
     * 是否可作为过滤条件
     */
    private Integer isFilterable;
    
    
    /**
     * 是否允许从相册选择
     */
    private Integer isSupportAlbum;
    
    /**
     * 是否可显示给用户
     */
    private Integer isDisplayable;
    
    /**
     * 字段长度
     */
    private Integer length;
    
    /**
     * 字段精度
     */
    private Integer precision;
    
    /**
     * 最小值
     */
    private String minValue;
    
    /**
     * 最大值
     */
    private String maxValue;
  
    /**
	 * 字段类型及所在的位置，从0开始
	 * 如果是自定义字段，格式为int23（第23个int型备用自定义字段)和text10(第10个text类型备用自定义字段），
	 * 如果是系统预置字段，则就是字段名,比如name。
     */
    private String slot;

    /**
     * 货币
     */
    private CurrencyVO currency;
    
    /**
     * 货币ID
     */
    private Long currencyId;
    
    /**
     * 是否只读
     */
    private Integer isReadonly;
    
    /**
     * web api
     */
    private Long pcApiId;
    
    private ApiVO pcApi;
   
    /**
     * mobile api
     */
    private Long mobileApiId;
    
    private ApiVO mobileApi;
  
    private String pcUiType;
    private String mobileUiType;
    
    /**
     * 表达式类型，0不是表达式，1是表达式
     */
    private Integer expressionType;
 
    /**
     * 表达式,表达式支持变量和字面量
     */
    private String expression;
    
    /**
     * 是否必填
     */
    private Integer isRequired;
    
    /**
     * 描述
     */
    private String description;
    
    /**
     * 枚举来源，1.系统预置， 2.档案
     */
    private Integer enumSource;

    /**
     * 枚举编码
     */
    private String enumCode;
    
    /**
     * 枚举
     */
    private List<Map<String, Object>> enums;
    
    /**
     * 档案参照，包括档案本身信息，及items(具体的key和value)
     */
    private Map<String, Object> refDoc;
    
    private BaseDocVO originalDoc;

    private String[] controlledBy;
    
    /**
     *   由哪些字段控制, 逗号分隔字符串,如果明细是由主对象的字段控制，则用__Main__.name这种形式表示
     */
    private String controlledByStr;
    /**
     * 关联档案ID
     */
    private Long refDocId;
    
    /**
     * 关联的对象类型
     */
    private Long refObjId;
    
    /**
     *  是否默认选中
     */
    private Integer defaultChecked;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;
    
    /**
     * 是否已删除
     */
    private Integer isDeleted;
    
    /**
     * 删除人
     */
    private Long deletedUserId;
    
    /**
     * 记录的删除时间
     */
    private Date deletedTime;
    
    /**
     * 显示顺序
     */
    private Long sortOrder;
    
    /**
     * sys_mt_object
     */
    private static final long serialVersionUID = 1L;
    
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameAttr(String nameAttr) {
        this.name = nameAttr;
    }

    public Long getSorOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

	public Integer getDefaultChecked() {
		return defaultChecked;
	}

	public void setDefaultChecked(Integer defaultChecked) {
		this.defaultChecked = defaultChecked;
	}


	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getPrecision() {
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public Integer getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public CurrencyVO getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyVO currency) {
		this.currency = currency;
	}



	public Integer getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Integer isCustom) {
		this.isCustom = isCustom;
	}

	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}

	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}

	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}

	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}

	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}

	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Integer getIsDisplayable() {
		return isDisplayable;
	}

	public void setIsDisplayable(Integer isDisplayable) {
		this.isDisplayable = isDisplayable;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public Long getDeletedUserId() {
		return deletedUserId;
	}

	public void setDeletedUserId(Long deletedUserId) {
		this.deletedUserId = deletedUserId;
	}

	public Date getDeletedTime() {
		return deletedTime;
	}

	public void setDeletedTime(Date deletedTime) {
		this.deletedTime = deletedTime;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getExpressionType() {
		return expressionType;
	}

	public void setExpressionType(Integer expressionType) {
		this.expressionType = expressionType;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}
	
	@Override
	public Object clone() {
		FieldVO cloneFieldVO = null;
		try {
			cloneFieldVO = (FieldVO) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return cloneFieldVO;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public Long getRefObjId() {
		return refObjId;
	}

	public void setRefObjId(Long refObjId) {
		this.refObjId = refObjId;
	}

	public Integer getIsReadonly() {
		return isReadonly;
	}

	public void setIsReadonly(Integer isReadonly) {
		this.isReadonly = isReadonly;
	}

	public Integer getIsFilterable() {
		return isFilterable;
	}

	public void setIsFilterable(Integer isFilterable) {
		this.isFilterable = isFilterable;
	}

	public Integer getEnumSource() {
		return enumSource;
	}

	public void setEnumSource(Integer enumSource) {
		this.enumSource = enumSource;
	}

	public String getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}

	public List<Map<String, Object>> getEnums() {
		return enums;
	}

	public void setEnums(List<Map<String, Object>> enums) {
		this.enums = enums;
	}

	public Long getRefDocId() {
		return refDocId;
	}

	public void setRefDocId(Long refDocId) {
		this.refDocId = refDocId;
	}

	public Map<String, Object> getRefDoc() {
		return refDoc;
	}

	public void setRefDoc(Map<String, Object> refDoc) {
		this.refDoc = refDoc;
	}

	public Long getPcApiId() {
		return pcApiId;
	}

	public void setPcApiId(Long pcApiId) {
		this.pcApiId = pcApiId;
	}

	public Long getMobileApiId() {
		return mobileApiId;
	}

	public void setMobileApiId(Long mobileApiId) {
		this.mobileApiId = mobileApiId;
	}

	public ApiVO getPcApi() {
		return pcApi;
	}

	public void setPcApi(ApiVO pcApi) {
		this.pcApi = pcApi;
	}

	public ApiVO getMobileApi() {
		return mobileApi;
	}

	public void setMobileApi(ApiVO mobileApi) {
		this.mobileApi = mobileApi;
	}

	public String[] getControlledBy() {
		return controlledBy;
	}

	public void setControlledBy(String[] controlledBy) {
		this.controlledBy = controlledBy;
	}

	public String getControlledByStr() {
		return controlledByStr;
	}

	public void setControlledByStr(String controlledByStr) {
		this.controlledByStr = controlledByStr;
	}

	public String getPcUiType() {
		return pcUiType;
	}

	public void setPcUiType(String pcUiType) {
		this.pcUiType = pcUiType;
	}

	public String getMobileUiType() {
		return mobileUiType;
	}

	public void setMobileUiType(String mobileUiType) {
		this.mobileUiType = mobileUiType;
	}

	public Integer getIsSupportAlbum() {
		return isSupportAlbum;
	}

	public void setIsSupportAlbum(Integer isSupportAlbum) {
		this.isSupportAlbum = isSupportAlbum;
	}

	public BaseDocVO getOriginalDoc() {
		return originalDoc;
	}

	public void setOriginalDoc(BaseDocVO originalDoc) {
		this.originalDoc = originalDoc;
	}
    
}