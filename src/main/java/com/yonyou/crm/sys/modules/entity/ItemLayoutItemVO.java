package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;
import java.util.Date;

public class ItemLayoutItemVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置属性
     */
    private Long tenantId;
    
    /**
     * 明细对象ID
     */
    private Long itemObjId;
    /**
     * 属性所在布局
     */
    private Long layoutId;

    /**
     * 属性名称,注意可能不同于字段上面的名称，比如可能有关联字段
     */
    private String apiName;
    
    /**
     * 属性显示名称
     */
    private String name;
    
    /**
     * 是否是空元素
     */
    private Integer isBlank;
    
    /**
     * 字段本身
     */
    private FieldVO actualField;
    
    /**
     * 字段本身全称
     */
    private String actualFieldFullname;
    
    /**
     * 是否必填
     */
    private Integer isRequired;
    
    /**
     * 是否只读
     */
    private Integer isReadonly;
    
    /**
     * 宽度
     */
    private String width;
    
    /**
     * 高度
     */
    private String height;
    
    /**
     * 在第几行
     */
    private Integer rowPosition;
    
    /**
     * 在行中的第几列
     */
    private Integer columnPosition;
    
    
    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;
    
    private static final long serialVersionUID = 1L;
    
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}

	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}

	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}

	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}

	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}

	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	public Integer getIsReadonly() {
		return isReadonly;
	}

	public void setIsReadonly(Integer isReadonly) {
		this.isReadonly = isReadonly;
	}

	public Integer getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}

	public FieldVO getActualField() {
		return actualField;
	}

	public void setActualField(FieldVO actualField) {
		this.actualField = actualField;
	}

	public Long getItemObjId() {
		return itemObjId;
	}

	public void setItemObjId(Long itemObjId) {
		this.itemObjId = itemObjId;
	}

	public Long getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getActualFieldFullname() {
		return actualFieldFullname;
	}

	public void setActualFieldFullname(String actualFieldFullname) {
		this.actualFieldFullname = actualFieldFullname;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public Integer getRowPosition() {
		return rowPosition;
	}

	public void setRowPosition(Integer rowPosition) {
		this.rowPosition = rowPosition;
	}

	public Integer getColumnPosition() {
		return columnPosition;
	}

	public void setColumnPosition(Integer columnPosition) {
		this.columnPosition = columnPosition;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsBlank() {
		return isBlank;
	}

	public void setIsBlank(Integer isBlank) {
		this.isBlank = isBlank;
	}

}