package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class ModuleVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;
    
    /**
     * 对象ID
     */
    private Long id;

    /**
     * 对象API名称
     */
    private String apiName;

    /**
     * 对象显示名称
     */
    private String name;
    
    /**
     * 是否可自定义字段
     */
    private Integer isExtensible;
    
    /**
     * 主对象Id
     */
    private Long masterObjId;
    
    /**
     * 是否有业务类型
     */
    private Integer withBizTypes;
    
    /**
     * 是否有明细对象
     */
    private Integer withItem;
    
    /**
     * 对象主表
     */
    private String mainTable;
    
    /**
     * 显示顺序
     */
    private Integer sortOrder;
    
    /**
     *  主表上int型字段的总可用数
     */
   private Integer mainCustomIntNum;
   
   /**
    *  主表上decimal型字段的总可用数
    */
  private Integer mainCustomDecimalNum;
  
  /**
   *  主表上datetime型字段的总可用数
   */
 private Integer mainCustomDatetimeNum;
 
 /**
  *  主表上varchar型字段的总可用数
  */
private Integer mainCustomCharNum;

/**
 *  主表上text型字段的总可用数
 */
private Integer mainCustomTextNum;

/**
 *  主表上blob型字段的总可用数
 */
private Integer mainCustomBlobNum;
   
 /**
    * 如果是主对象的话，则其会带有明细对象
 */
 private ModuleVO item;
   
    
    /**
     * sys_mt_object
     */
    private static final long serialVersionUID = 1L;
    
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer withBizTypes() {
        return withBizTypes;
    }

    public void setWithBizTypes(Integer withBizTypes) {
        this.withBizTypes = withBizTypes;
    }

    public Integer getSorOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    
	public Integer getIsExtensible() {
		return isExtensible;
	}

	public void setIsExtensible(Integer isExtensible) {
		this.isExtensible = isExtensible;
	}

	public Long getMasterObjId() {
		return masterObjId;
	}

	public void setMasterObjId(Long masterObjId) {
		this.masterObjId = masterObjId;
	}

	public Integer getMainCustomIntNum() {
		return mainCustomIntNum;
	}

	public void setMainCustomIntNum(Integer mainCustomIntNum) {
		this.mainCustomIntNum = mainCustomIntNum;
	}

	public Integer getMainCustomDecimalNum() {
		return mainCustomDecimalNum;
	}

	public void setMainCustomDecimalNum(Integer mainCustomDecimalNum) {
		this.mainCustomDecimalNum = mainCustomDecimalNum;
	}

	public Integer getMainCustomDatetimeNum() {
		return mainCustomDatetimeNum;
	}

	public void setMainCustomDatetimeNum(Integer mainCustomDatetimeNum) {
		this.mainCustomDatetimeNum = mainCustomDatetimeNum;
	}

	public Integer getMainCustomCharNum() {
		return mainCustomCharNum;
	}

	public void setMainCustomCharNum(Integer mainCustomCharNum) {
		this.mainCustomCharNum = mainCustomCharNum;
	}

	public Integer getMainCustomTextNum() {
		return mainCustomTextNum;
	}

	public void setMainCustomTextNum(Integer mainCustomTextNum) {
		this.mainCustomTextNum = mainCustomTextNum;
	}

	public Integer getMainCustomBlobNum() {
		return mainCustomBlobNum;
	}

	public void setMainCustomBlobNum(Integer mainCustomBlobNum) {
		this.mainCustomBlobNum = mainCustomBlobNum;
	}

	public ModuleVO getItem() {
		return item;
	}

	public void setItem(ModuleVO item) {
		this.item = item;
	}

	public Integer getWithItem() {
		return withItem;
	}

	public void setWithItem(Integer withItem) {
		this.withItem = withItem;
	}

	public String getMainTable() {
		return mainTable;
	}

	public void setMainTable(String mainTable) {
		this.mainTable = mainTable;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
    
}