package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class LayoutCatalogVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;
    
    /**
     * 对象类型
     */
    private Long objId;

    /**
     * 模板类型，1.详情，2.编辑 ,3 列表
     */
    private Integer layoutType;
    
    /**
     * 终端类型，1.PC，2.移动
     */
    private Integer clientType;
    
    /**
     * sys_mt_layout_catalog
     */
    private static final long serialVersionUID = 1L;

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	public Integer getClientType() {
		return clientType;
	}

	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	public Integer getLayoutType() {
		return layoutType;
	}

	public void setLayoutType(Integer layoutType) {
		this.layoutType = layoutType;
	}
    
   

}