package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class FieldTypeVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置
     */
    private Long tenantId;
    
    /**
     * 字段类型
     */
    private Integer type;
    
    /**
     * 名称
     */
    private String name;
    
    /**
     *  对应的mysql数据类型
     */
    private String mysqlDatatype;
    
    /**
     *  0 可用于所有字段,1.只能用于预置字段
     */
    private Integer onlyPresetAvailable;
    
    /**
     * 显示顺序
     */
    private Integer sortOrder;
    
    /**
     * 字段长度
     */
    private Integer length;
    
    /**
     * 字段精度
     */
    private Integer precision;
    
    /**
     * 最小值
     */
    private String minValue;
    
    /**
     * 最大值
     */
    private String maxValue;
  
    
    /**
     * sys_mt_field_type
     */
    private static final long serialVersionUID = 1L;
    
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getSorOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMysqlDatatype() {
		return mysqlDatatype;
	}

	public void setMysqlDatatype(String mysqlDatatype) {
		this.mysqlDatatype = mysqlDatatype;
	}

	public Integer getOnlyPresetAvailable() {
		return onlyPresetAvailable;
	}

	public void setOnlyPresetAvailable(Integer onlyPresetAvailable) {
		this.onlyPresetAvailable = onlyPresetAvailable;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getPrecision() {
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}
    
}