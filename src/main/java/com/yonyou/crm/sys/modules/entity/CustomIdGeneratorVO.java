package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class CustomIdGeneratorVO implements Serializable {
	/**
     * 租户ID
     */
    private Long tenantId;
    
    /**
     * ID
     */
    private Long id;
    
    private static final long serialVersionUID = 1L;
    
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
   

}