package com.yonyou.crm.sys.modules.entity;

import java.io.Serializable;

public class BiztypeRoleVO implements Serializable {
	/**
     * 租户ID,租户ID为0的表明是系统预置对象
     */
    private Long tenantId;
    
    /**
     * 业务类型ID
     */
    private Long biztypeId;
    
    /**
     * 角色ID
     */
    private Long roleId;
    
    /**
     * 角色名称
     */
    private String roleName;
    
    /**
     * 组织ID
     */
    private Long orgId;
    
    /**
     * 组织名称
     */
    private String orgName;
    
    /**
     * sys_mt_biztype_role
     */
    private static final long serialVersionUID = 1L;
    
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getBiztypeId() {
		return biztypeId;
	}

	public void setBiztypeId(Long biztypeId) {
		this.biztypeId = biztypeId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}