package com.yonyou.crm.sact.taskcard.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sact.taskcard.entity.SactTaskcardVO;

public interface ISactTaskcardRmService {
	
	public SactTaskcardVO insertTaskcard(SactTaskcardVO sactTaskcard);
	
	public List<SactTaskcardVO> getSactTaskcardList(Map<String, Object> condMap);
	
	public int deleteSactTaskcard(Long id);
	
	public SactTaskcardVO updateSactTaskcard(SactTaskcardVO sactTaskcard);
	
	
	public List<SactTaskcardVO> deleteSactTaskcardByIDs(String[] idStrs, Map<String, Object> condMap);
	
	public SactTaskcardVO getTaskcardDetailById(Long id);
	
	public List<SactTaskcardVO> batchUpdateEnableState(String[] idArray, Integer enableState, Map<String, Object> searchMap);
	
	public Map<String, Object> getBiztypeListByModule(Long id);
}
