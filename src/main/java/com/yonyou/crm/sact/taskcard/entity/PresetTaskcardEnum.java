package com.yonyou.crm.sact.taskcard.entity;

public enum PresetTaskcardEnum {
	VISIT_SUMMARY(1,"拜访小结"),LOCATIONS(2,"签到"),CUS_DETAIL(3,"客户详情");
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private PresetTaskcardEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
