package com.yonyou.crm.sact.visitroute.entity;

import java.io.Serializable;

public class VisitrouteNodeVO implements Serializable {
   
	/**
     * sact_visitroute_node
     */
    private static final long serialVersionUID = 1L;
	
	/**
     * 拜访路线id
     */
    private Long visitrouteId;

    /**
     * 网点id
     */
    private Long nodeId;
    
    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 拜访路线id
     * @return visitroute_id 拜访路线id
     */
    public Long getVisitrouteId() {
        return visitrouteId;
    }

    /**
     * 拜访路线id
     * @param visitrouteId 拜访路线id
     */
    public void setVisitrouteId(Long visitrouteId) {
        this.visitrouteId = visitrouteId;
    }

    /**
     * 网点id
     * @return node_id 网点id
     */
    public Long getNodeId() {
        return nodeId;
    }

    /**
     * 网点id
     * @param nodeId 网点id
     */
    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }
    
    /**
     * 显示顺序
     * @return order_num 显示顺序
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * 显示顺序
     * @param orderNum 显示顺序
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
    
}