package com.yonyou.crm.sact.visitroute.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class VisitrouteVO implements Serializable {
	
    /**
     * sact_visitroute
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 路线id
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 路线名称
     */
    private String name;

    /**
     * 负责人
     */
    private Long ownerUserId;

    /**
     * 公司id
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 部门
     */
    private Long deptId;
    
    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 覆盖网点数
     */
    private Long coverNodeNum;

    /**
     * 负责网点数
     */
    private Long ownerNodeNum;

    /**
     * 网点
     */
    private Long nodeId;
    
    /**
     * 网点名称
     */
    private String nodeName;

    /**
     * 启用状态,1启用2停用
     */
    private Byte enableState;
    
    /**
     * 停启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 备注
     */
    private String remarks;
    
    /**
     * 网点列表
     */
    private List<VisitrouteNodeVO> nodeList;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 路线名称
     * @return name 路线名称
     */
    public String getName() {
        return name;
    }

    /**
     * 路线名称
     * @param name 路线名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 负责人
     * @return owner_user_id 负责人
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人
     * @param ownerUserId 负责人
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 公司名称
	 * @return orgName 公司名称
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * 公司名称
	 * @param orgName 公司名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
     * 所属部门
     * @return dept_id 所属部门
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 所属部门
     * @param deptId 所属部门
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 部门名称
	 * @return deptName 部门名称
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * 部门名称
	 * @param deptName 部门名称
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
     * 覆盖网点数
     * @return cover_node_num 覆盖网点数
     */
    public Long getCoverNodeNum() {
        return coverNodeNum;
    }

    /**
     * 覆盖网点数
     * @param coverNodeNum 覆盖网点数
     */
    public void setCoverNodeNum(Long coverNodeNum) {
        this.coverNodeNum = coverNodeNum;
    }

    /**
     * 负责网点数
     * @return owner_node_num 负责网点数
     */
    public Long getOwnerNodeNum() {
        return ownerNodeNum;
    }

    /**
     * 负责网点数
     * @param ownerNodeNum 负责网点数
     */
    public void setOwnerNodeNum(Long ownerNodeNum) {
        this.ownerNodeNum = ownerNodeNum;
    }

    /**
     * 网点
     * @return node_id 网点
     */
    public Long getNodeId() {
        return nodeId;
    }

    /**
     * 网点
     * @param nodeId 网点
     */
    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * 网点名称
	 * @return nodeName 网点名称
	 */
	public String getNodeName() {
		return nodeName;
	}

	/**
	 * 网点名称
	 * @param nodeName 网点名称
	 */
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	/**
     * 启用状态,1启用2停用
     * @return enable_state 启用状态,1启用2停用
     */
    public Byte getEnableState() {
        return enableState;
    }

    /**
     * 启用状态,1启用2停用
     * @param enableState 启用状态,1启用2停用
     */
    public void setEnableState(Byte enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用状态名称
	 * @return enableStateName 停启用状态名称
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * 停启用状态名称
	 * @param enableStateName 停启用状态名称
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 显示顺序
     * @return order_num 显示顺序
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * 显示顺序
     * @param orderNum 显示顺序
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * 备注
     * @return remarks 备注
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 备注
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

	/**
	 * 网点列表
	 * @return nodeList	网点列表
	 */
	public List<VisitrouteNodeVO> getNodeList() {
		return nodeList;
	}

	/**
	 * 网点列表
	 * @param nodeList 网点列表
	 */
	public void setNodeList(List<VisitrouteNodeVO> nodeList) {
		this.nodeList = nodeList;
	}
}