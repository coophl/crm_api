package com.yonyou.crm.sact.visitroute.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sact.visitroute.entity.VisitrouteVO;

public interface IVisitrouteRmService {
	
	public Page<VisitrouteVO> getList(Page<VisitrouteVO> page,Map<String, Object> paraMap);
	
	public VisitrouteVO insert(VisitrouteVO vo);
	
	public VisitrouteVO update(VisitrouteVO vo);
	
	public Page<VisitrouteVO> batchDelete(String[] ids, Page<VisitrouteVO> page,Map<String, Object> paraMap);
}
