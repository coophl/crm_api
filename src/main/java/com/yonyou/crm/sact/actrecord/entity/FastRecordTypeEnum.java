package com.yonyou.crm.sact.actrecord.entity;

public enum FastRecordTypeEnum {
	PHONE_RECORD(1,"phone"),MAIL_RECORD(2,"mail"),OTHER_RECORD(3,"other");
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private FastRecordTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
