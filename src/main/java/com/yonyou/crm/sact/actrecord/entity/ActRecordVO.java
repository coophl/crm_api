package com.yonyou.crm.sact.actrecord.entity;

import java.io.Serializable;
import java.util.Date;

public class ActRecordVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 行动记录类型（1拜访，2任务，3快速新增）
     */
    private Integer recordType;

    /**
     * 快速新增类型(1电话记录、2邮件记录、3其他记录)
     */
    private Integer quickAddType;
    
    /**
     * 行动记录对应的任务或者拜访id
     */
    private Integer relativeId;
    /**
     * 关联业务对象id
     */
    private Integer objType;

    /**
     * 关联具体业务id
     */
    private Long objId;

    /**
     * 关联客户id
     */
    private Long cumId;

    /**
     * 内容
     */
    private String content;

    /**
     * 下次跟进日期
     */
    private Date nextDate;

    /**
     * 下次跟进内容
     */
    private String nextContent;

    /**
     * 电话号码
     */
    private String phoneNumber;

    /**
     * 通话时长
     */
    private Integer talkTime;

    /**
     * 邮箱地址
     */
    private String mailAddress;

    /**
     * 邮件发送时间
     */
    private Date mailSendTime;

    /**
     * 行动记录执行人
     */
    private Long ownerUserId;

    /**
     * 所属部门
     */
    private Long deptId;

    /**
     * 行动记录日期
     */
    private Date recordDate;

    /**
     * 是否删除（0未删除，1删除）
     */
    private Integer isDeleted;

    /**
     * 系统删除人
     */
    private Long deletedUserId;

    /**
     * 系统删除时间
     */
    private Date deletedTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * sact_actrecord
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 行动记录类型（1拜访，2任务，3快速新增）
     * @return record_type 行动记录类型（1拜访，2任务，3快速新增）
     */
    public Integer getRecordType() {
        return recordType;
    }

    /**
     * 行动记录类型（1拜访，2任务，3快速新增）
     * @param recordType 行动记录类型（1拜访，2任务，3快速新增）
     */
    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }

    /**
     * 快速新增类型(1电话记录、2邮件记录、3其他记录)
     * @return quick_add_type 快速新增类型(1电话记录、2邮件记录、3其他记录)
     */
    public Integer getQuickAddType() {
        return quickAddType;
    }

    /**
     * 快速新增类型(1电话记录、2邮件记录、3其他记录)
     * @param quickAddType 快速新增类型(1电话记录、2邮件记录、3其他记录)
     */
    public void setQuickAddType(Integer quickAddType) {
        this.quickAddType = quickAddType;
    }

    /**
     * 关联业务对象id
     * @return obj_type 关联业务对象id
     */
    public Integer getObjType() {
        return objType;
    }

    /**
     * 关联业务对象id
     * @param objType 关联业务对象id
     */
    public void setObjType(Integer objType) {
        this.objType = objType;
    }

    /**
     * 关联具体业务id
     * @return obj_id 关联具体业务id
     */
    public Long getObjId() {
        return objId;
    }

    /**
     * 关联具体业务id
     * @param objId 关联具体业务id
     */
    public void setObjId(Long objId) {
        this.objId = objId;
    }

    /**
     * 关联客户id
     * @return cum_id 关联客户id
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 关联客户id
     * @param cumId 关联客户id
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    /**
     * 内容
     * @return content 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 内容
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 下次跟进日期
     * @return next_date 下次跟进日期
     */
    public Date getNextDate() {
        return nextDate;
    }

    /**
     * 下次跟进日期
     * @param nextDate 下次跟进日期
     */
    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    /**
     * 下次跟进内容
     * @return next_content 下次跟进内容
     */
    public String getNextContent() {
        return nextContent;
    }

    /**
     * 下次跟进内容
     * @param nextContent 下次跟进内容
     */
    public void setNextContent(String nextContent) {
        this.nextContent = nextContent;
    }

    /**
     * 电话号码
     * @return phone_number 电话号码
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * 电话号码
     * @param phoneNumber 电话号码
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * 通话时长
     * @return talk_time 通话时长
     */
    public Integer getTalkTime() {
        return talkTime;
    }

    /**
     * 通话时长
     * @param talkTime 通话时长
     */
    public void setTalkTime(Integer talkTime) {
        this.talkTime = talkTime;
    }

    /**
     * 邮箱地址
     * @return mail_address 邮箱地址
     */
    public String getMailAddress() {
        return mailAddress;
    }

    /**
     * 邮箱地址
     * @param mailAddress 邮箱地址
     */
    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    /**
     * 邮件发送时间
     * @return mail_send_time 邮件发送时间
     */
    public Date getMailSendTime() {
        return mailSendTime;
    }

    /**
     * 邮件发送时间
     * @param mailSendTime 邮件发送时间
     */
    public void setMailSendTime(Date mailSendTime) {
        this.mailSendTime = mailSendTime;
    }

    /**
     * 行动记录执行人
     * @return owner_user_id 行动记录执行人
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 行动记录执行人
     * @param ownerUserId 行动记录执行人
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    /**
     * 所属部门
     * @return dept_id 所属部门
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 所属部门
     * @param deptId 所属部门
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 行动记录日期
     * @return record_date 行动记录日期
     */
    public Date getRecordDate() {
        return recordDate;
    }

    /**
     * 行动记录日期
     * @param recordDate 行动记录日期
     */
    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * 是否删除（0未删除，1删除）
     * @return is_deleted 是否删除（0未删除，1删除）
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除（0未删除，1删除）
     * @param isDeleted 是否删除（0未删除，1删除）
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 系统删除人
     * @return deleted_user_id 系统删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 系统删除人
     * @param deletedUserId 系统删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 系统删除时间
     * @return deleted_time 系统删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 系统删除时间
     * @param deletedTime 系统删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public Integer getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Integer relativeId) {
		this.relativeId = relativeId;
	}
}