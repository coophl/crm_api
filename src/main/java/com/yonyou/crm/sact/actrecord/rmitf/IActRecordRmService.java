package com.yonyou.crm.sact.actrecord.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sact.actrecord.entity.ActRecordVO;

public interface IActRecordRmService {
	
	public Page<ActRecordVO> getPage(Page<ActRecordVO> page, Map<String, Object> paramMap);
	
	public ActRecordVO save(ActRecordVO vo);
	
	public ActRecordVO update(ActRecordVO vo);
	
	public Page<ActRecordVO> batchDelete(String[] ids, Page<ActRecordVO> page, Map<String, Object> paramMap);
	
	public Object detail(Long id);
}
