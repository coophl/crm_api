package com.yonyou.crm.sact.schedule.entity;

import java.io.Serializable;
import java.util.Date;

public class ScheduleGoodjobVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 是否点赞（1代表是，2代表否）
     */
    private Integer isGoodjob;

    /**
     * 拜访ID
     */
    private Long visitId;

    /**
     * 评论人ID
     */
    private Long userId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 创建时间
     */
    private Date sysCreatedTime;

    /**
     * 创建人
     */
    private Long sysCreatedUserId;

    /**
     * 修改时间
     */
    private Date sysModifiedTime;

    /**
     * 修改人
     */
    private Long sysModifiedUserId;

    /**
     * 是否删除(0代表未删除，1代表删除）
     */
    private Integer isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * sact_schedule_goodjob
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否点赞（1代表是，2代表否）
     * @return is_goodjob 是否点赞（1代表是，2代表否）
     */
    public Integer getIsGoodjob() {
        return isGoodjob;
    }

    /**
     * 是否点赞（1代表是，2代表否）
     * @param isGoodjob 是否点赞（1代表是，2代表否）
     */
    public void setIsGoodjob(Integer isGoodjob) {
        this.isGoodjob = isGoodjob;
    }

    /**
     * 拜访ID
     * @return visit_id 拜访ID
     */
    public Long getVisitId() {
        return visitId;
    }

    /**
     * 拜访ID
     * @param visitId 拜访ID
     */
    public void setVisitId(Long visitId) {
        this.visitId = visitId;
    }

    /**
     * 评论人ID
     * @return user_id 评论人ID
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 评论人ID
     * @param userId 评论人ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 创建时间
     * @return sys_created_time 创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 创建时间
     * @param sysCreatedTime 创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 创建人
     * @return sys_created_user_id 创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 创建人
     * @param sysCreatedUserId 创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 修改时间
     * @return sys_modified_time 修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 修改时间
     * @param sysModifiedTime 修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 修改人
     * @return sys_modified_user_id 修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 修改人
     * @param sysModifiedUserId 修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @return is_deleted 是否删除(0代表未删除，1代表删除）
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @param isDeleted 是否删除(0代表未删除，1代表删除）
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }
}