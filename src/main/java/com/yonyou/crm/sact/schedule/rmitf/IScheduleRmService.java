package com.yonyou.crm.sact.schedule.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sact.schedule.entity.ScheduleCommentVO;
import com.yonyou.crm.sact.schedule.entity.ScheduleGoodjobVO;
import com.yonyou.crm.sact.schedule.entity.ScheduleVO;

public interface IScheduleRmService {

	public List<ScheduleVO> getScheduleList(Map<String, Object> paraMap);
	public List<String> getScheduleDateList(Map<String, Object> paraMap);
	
	public List<ScheduleCommentVO> getCommentList(Map<String, Object> paraMap);
	
	public List<ScheduleGoodjobVO> getGoodjobList(Map<String, Object> paraMap);
	
	public List<ScheduleCommentVO> deleteCommentByIDs(String[] ids,Map<String, Object> paraMap);
	
	public List<ScheduleGoodjobVO> deleteGoodjobByIDs(String[] ids,Map<String, Object> paraMap);
	
	public List<Map<String, Object>> getCumVisitFinishedList(Long id, Long objType, Long objBiztype, String scheduleType);
}
