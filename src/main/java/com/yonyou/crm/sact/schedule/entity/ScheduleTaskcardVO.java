package com.yonyou.crm.sact.schedule.entity;

import java.io.Serializable;

public class ScheduleTaskcardVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 日程ID
     */
    private Long scheduleId;

    /**
     * 日程类型（1客户拜访，2网点拜访，3任务）
     */
    private Integer scheduleType;

    /**
     * 任务卡ID
     */
    private Long taskcardId;

    /**
     * 是否必输（1是，2否）
     */
    private Integer required;

    /**
     * 显示顺序
     */
    private Integer orderNum;
    
    /**
     * 是否填写
     */
    private Integer written;
    
    /**
     * 新增业务对象记录对应业务对象ID
     */
    private Long objType;

    /**
     * 新增业务对象记录对应业务类型ID
     */
    private Long objBiztype;
    
    /**
     * 填写条目数
     */
    private Integer writtenNum;
    
    /**
     * sact_schedule_taskcard
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 日程ID
     * @return schedule_id 日程ID
     */
    public Long getScheduleId() {
        return scheduleId;
    }

    /**
     * 日程ID
     * @param scheduleId 日程ID
     */
    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * 日程类型（1客户拜访，2网点拜访，3任务）
     * @return schedule_type 日程类型（1客户拜访，2网点拜访，3任务）
     */
    public Integer getScheduleType() {
        return scheduleType;
    }

    /**
     * 日程类型（1客户拜访，2网点拜访，3任务）
     * @param scheduleType 日程类型（1客户拜访，2网点拜访，3任务）
     */
    public void setScheduleType(Integer scheduleType) {
        this.scheduleType = scheduleType;
    }

    /**
     * 任务卡ID
     * @return taskcard_id 任务卡ID
     */
    public Long getTaskcardId() {
        return taskcardId;
    }

    /**
     * 任务卡ID
     * @param taskcardId 任务卡ID
     */
    public void setTaskcardId(Long taskcardId) {
        this.taskcardId = taskcardId;
    }

    /**
     * 是否必输（1是，2否）
     * @return required 是否必输（1是，2否）
     */
    public Integer getRequired() {
        return required;
    }

    /**
     * 是否必输（1是，2否）
     * @param required 是否必输（1是，2否）
     */
    public void setRequired(Integer required) {
        this.required = required;
    }

    /**
     * 显示顺序
     * @return order_num 显示顺序
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * 显示顺序
     * @param orderNum 显示顺序
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

	public Integer getWritten() {
		return written;
	}

	public void setWritten(Integer written) {
		this.written = written;
	}

	public Long getObjType() {
		return objType;
	}

	public void setObjType(Long objType) {
		this.objType = objType;
	}

	public Long getObjBiztype() {
		return objBiztype;
	}

	public void setObjBiztype(Long objBiztype) {
		this.objBiztype = objBiztype;
	}

	public Integer getWrittenNum() {
		return writtenNum;
	}

	public void setWrittenNum(Integer writtenNum) {
		this.writtenNum = writtenNum;
	}
	
}