package com.yonyou.crm.sact.schedule.entity;

public enum ScheduleStatusEnum {
	NOT_START(1,"未开始"),IN_HAND(2,"处理中"),COMPLETED(3,"已完成");
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private ScheduleStatusEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
