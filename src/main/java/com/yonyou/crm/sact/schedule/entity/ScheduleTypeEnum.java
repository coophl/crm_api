package com.yonyou.crm.sact.schedule.entity;

public enum ScheduleTypeEnum {
	CUSVISIT(1,"CustomerVisit"),NETVISIT(2,"NetVisit"),TASK(3,"Task");
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private ScheduleTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
