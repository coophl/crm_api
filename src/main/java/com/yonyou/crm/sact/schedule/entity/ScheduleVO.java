package com.yonyou.crm.sact.schedule.entity;

import java.io.Serializable;
import java.util.Date;

import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sact.visitrule.entity.VisitruleVO;
import com.yonyou.crm.sys.user.entity.UserVO;

public class ScheduleVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 公司id
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 客户id
     */
    private Long cumId;
    
    /**
     * 客户信息
     */
    private CustomerVO cumInfo;

    /**
     * 拜访日期
     */
    private Date date;
    
    /**
     * 负责人id
     */
    private Long ownerUserId;
    
    /**
     * 负责人名称
     */
    private UserVO ownerInfo;

    /**
     * 类型
     */
    private String type;
    
    /**
     * 状态
     */
    private Integer status;
    
    /**
     * 状态名称
     */
    private String statusName;
    
    /**
     * 总结已录标识
     */
    private String summaryFlag;
    
    /**
     * 签到已签标识
     */
    private String traceFlag;
    
    /**
     * 任务卡信息
     */
    private VisitruleVO visitRule;

    /**
     * sact_visit
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
     * 客户id
     * @return cum_id 客户id
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户id
     * @param cumId 客户id
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

	public CustomerVO getCumInfo() {
		return cumInfo;
	}

	public void setCumInfo(CustomerVO cumInfo) {
		this.cumInfo = cumInfo;
	}

	/**
     * 拜访日期
     * @return date 拜访日期
     */
    public Date getDate() {
		return date;
	}

    /**
     * 拜访日期
     * @param date 拜访日期
     */
	public void setDate(Date date) {
		this.date = date;
	}

    /**
     * 负责人id
     * @return owner_user_id 负责人id
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人id
     * @param ownerUserId 负责人id
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public UserVO getOwnerInfo() {
		return ownerInfo;
	}

	public void setOwnerInfo(UserVO ownerInfo) {
		this.ownerInfo = ownerInfo;
	}

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getSummaryFlag() {
		return summaryFlag;
	}

	public void setSummaryFlag(String summaryFlag) {
		this.summaryFlag = summaryFlag;
	}

	public String getTraceFlag() {
		return traceFlag;
	}

	public void setTraceFlag(String traceFlag) {
		this.traceFlag = traceFlag;
	}

	public VisitruleVO getVisitRule() {
		return visitRule;
	}

	public void setVisitRule(VisitruleVO visitRule) {
		this.visitRule = visitRule;
	}

}