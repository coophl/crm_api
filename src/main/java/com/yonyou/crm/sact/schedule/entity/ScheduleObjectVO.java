package com.yonyou.crm.sact.schedule.entity;

import java.io.Serializable;
import java.util.Date;

public class ScheduleObjectVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 日程ID
     */
    private Long scheduleId;

    /**
     * 新增业务对象记录ID
     */
    private Long objId;
    
    /**
     * 任务卡ID
     */
    private Long taskcardId;

    /**
     * 新增业务对象记录对应业务对象ID
     */
    private Long objType;

    /**
     * 新增业务对象记录对应业务类型ID
     */
    private Long objBiztype;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 是否删除（0代表未删除，1代表删除）
     */
    private Integer isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 时间戳
     */
    private Date ts;
    
    private Integer scheduleType;
    /**
     * sact_schedule_object
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 日程ID
     * @return schedule_id 日程ID
     */
    public Long getScheduleId() {
        return scheduleId;
    }

    /**
     * 日程ID
     * @param scheduleId 日程ID
     */
    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * 新增业务对象记录ID
     * @return obj_id 新增业务对象记录ID
     */
    public Long getObjId() {
        return objId;
    }

    /**
     * 新增业务对象记录ID
     * @param objId 新增业务对象记录ID
     */
    public void setObjId(Long objId) {
        this.objId = objId;
    }

    /**
     * 新增业务对象记录对应业务对象ID
     * @return obj_type 新增业务对象记录对应业务对象ID
     */
    public Long getObjType() {
        return objType;
    }

    /**
     * 新增业务对象记录对应业务对象ID
     * @param objType 新增业务对象记录对应业务对象ID
     */
    public void setObjType(Long objType) {
        this.objType = objType;
    }

    /**
     * 新增业务对象记录对应业务类型ID
     * @return obj_biztype 新增业务对象记录对应业务类型ID
     */
    public Long getObjBiztype() {
        return objBiztype;
    }

    /**
     * 新增业务对象记录对应业务类型ID
     * @param objBiztype 新增业务对象记录对应业务类型ID
     */
    public void setObjBiztype(Long objBiztype) {
        this.objBiztype = objBiztype;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 是否删除（0代表未删除，1代表删除）
     * @return is_deleted 是否删除（0代表未删除，1代表删除）
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除（0代表未删除，1代表删除）
     * @param isDeleted 是否删除（0代表未删除，1代表删除）
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public Integer getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}

	public Long getTaskcardId() {
		return taskcardId;
	}

	public void setTaskcardId(Long taskcardId) {
		this.taskcardId = taskcardId;
	}
}