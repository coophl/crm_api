package com.yonyou.crm.sact.visit.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.cum.contact.entity.ContactVO;
import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sact.outwork.entity.TraceVO;
import com.yonyou.crm.sact.visitrule.entity.VisitruleVO;
import com.yonyou.crm.sprc.opportunity.entity.OpportunityVO;
import com.yonyou.crm.sys.user.entity.UserVO;

public class VisitsVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 公司id
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 客户id
     */
    private Long cumId;
    
    /**
     * 客户信息
     */
    private CustomerVO cumInfo;

    /**
     * 联系人id
     */
    private String contactId;
    
    /**
     * 联系人信息
     */
    private List<ContactVO> contactInfo;

    /**
     * 商机id
     */
    private Long opportunityId;
    
    /**
     * 商机名称
     */
    private OpportunityVO opportunityInfo;

    /**
     * 拜访日期
     */
    private Date date;
    
    /**
     * 下次拜访日期
     */
    private Date nextDate;

    /**
     * 负责人id
     */
    private Long ownerUserId;
    
    /**
     * 负责人名称
     */
    private UserVO ownerInfo;

    /**
     * 签到Id
     */
    private Long traceId;

    /**
     * 签到信息
     */
    private TraceVO traceInfo;
    
    /**
     * 拜访目的
     */
    private String purpose;

    /**
     * 拜访总结
     */
    private String summary;
    
    /**
     * 类型
     */
    private String type;
    
    /**
     * 类型名称
     */
    private String typeName;
    
    /**
     * 状态
     */
    private Integer status;
    
    /**
     * 状态名称
     */
    private String statusName;


    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 是否删除
     */
    private Byte isDeleted;

    /**
     * 时间戳
     */
    private Date ts;
    
    /**
     * 拜访规则信息
     */
    private VisitruleVO visitRule;

    /**
     * sact_visit
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
     * 客户id
     * @return cum_id 客户id
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户id
     * @param cumId 客户id
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

	public CustomerVO getCumInfo() {
		return cumInfo;
	}

	public void setCumInfo(CustomerVO cumInfo) {
		this.cumInfo = cumInfo;
	}

	/**
     * 联系人id
     * @return contact_id 联系人id
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * 联系人id
     * @param contactId 联系人id
     */
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

	public List<ContactVO> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(List<ContactVO> contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
     * 商机id
     * @return bizopp_id 商机id
     */
    public Long getOpportunityId() {
        return opportunityId;
    }

    /**
     * 商机id
     * @param opportunityId 商机id
     */
    public void setOpportunityId(Long opportunityId) {
        this.opportunityId = opportunityId;
    }

	public OpportunityVO getOpportunityInfo() {
		return opportunityInfo;
	}

	public void setOpportunityInfo(OpportunityVO opportunityInfo) {
		this.opportunityInfo = opportunityInfo;
	}

	/**
     * 拜访日期
     * @return date 拜访日期
     */
    public Date getDate() {
		return date;
	}

    /**
     * 拜访日期
     * @param date 拜访日期
     */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
     * 下次拜访日期
     * @return next_date 下次拜访日期
     */
    public Date getNextDate() {
        return nextDate;
    }

    /**
     * 下次拜访日期
     * @param nextDate 下次拜访日期
     */
    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    /**
     * 负责人id
     * @return owner_user_id 负责人id
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人id
     * @param ownerUserId 负责人id
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public UserVO getOwnerInfo() {
		return ownerInfo;
	}

	public void setOwnerInfo(UserVO ownerInfo) {
		this.ownerInfo = ownerInfo;
	}
	
	/**
     * 签到Id
     * @return trace_id 签到Id
     */
	public Long getTraceId() {
		return traceId;
	}

	/**
     * 签到Id
     * @param traceId 签到Id
     */
	public void setTraceId(Long traceId) {
		this.traceId = traceId;
	}

	/**
     * 签到信息
     * @return traceInfo 签到信息
     */
	public TraceVO getTraceInfo() {
		return traceInfo;
	}

	/**
     * 签到信息
     * @param traceInfo 签到信息
     */
	public void setTraceInfo(TraceVO traceInfo) {
		this.traceInfo = traceInfo;
	}

    /**
     * 拜访目的
     * @return purpose 拜访目的
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * 拜访目的
     * @param purpose 拜访目的
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * 拜访总结
     * @return summary 拜访总结
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 拜访总结
     * @param summary 拜访总结
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 是否删除
     * @return is_deleted 是否删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public VisitruleVO getVisitRule() {
		return visitRule;
	}

	public void setVisitRule(VisitruleVO visitRule) {
		this.visitRule = visitRule;
	}
}