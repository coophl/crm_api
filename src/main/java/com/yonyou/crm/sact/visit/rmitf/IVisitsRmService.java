package com.yonyou.crm.sact.visit.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sact.outwork.entity.TraceVO;
import com.yonyou.crm.sact.visit.entity.VisitsVO;


public interface IVisitsRmService {

	public Page<VisitsVO> getList(Page<VisitsVO> page,Map<String, Object> paraMap);
	public VisitsVO getDetail(Long id);
	public VisitsVO insert(VisitsVO visit);
	public VisitsVO update(VisitsVO visit);
	public int delete(Long id);
	public VisitsVO location(Long id, TraceVO trace);
	public Page<VisitsVO> batchDelete(String[] ids, Page<VisitsVO> page,Map<String, Object> paraMap);
	public Object selectFieldsByIds(Object[] ids);
	public List<VisitsVO> getScheduleList(Map<String, Object> paraMap);
	public List<String> getScheduleDateList(Map<String, Object> paraMap);
	public VisitsVO summary(VisitsVO visit);
	public VisitsVO getLocation(Long id);
	public VisitsVO getSummary(Long id);
	
	
}
