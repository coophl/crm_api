package com.yonyou.crm.sact.visitrule.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class VisitruleTaskcardVO implements Serializable {
    
    /**
     * 拜访规则id
     */
    private Long visitruleId;

    /**
     * 拜访卡id
     */
    private Long taskcardId;
    
    /**
     * 拜访卡名称
     */
    private String taskcardName;
    
    /**
     * 业务对象ID
     */
    private Long mtObjId;
    
    /**
     * 业务对象名称
     */
    private String mtObjName;
    
    /**
     * 业务类型ID
     */
    private Long mtBiztypeId;
    
    /**
     * 业务类型名称
     */
    private String mtBiztypeName;
	
	/**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 是否必输，1是2否
     */
    private Integer required;
    
    /**
     * 是否必输名称
     */
    private String requiredName;
    
    /**
     * 是否预置，1是2否
     */
    private Integer isPreseted;
    
    /**
     * 任务卡对应商机列表
     */
    private List<Map<String, Object>> opporList;
    
    /**
     * 任务卡对应线索列表
     */
    private List<Map<String, Object>> leadList;
    
    /**
     * 任务卡对应联系人列表
     */
    private List<Map<String, Object>> contactList;
    
    /**
     * 任务卡对应竞品采集列表
     */
    private List<Map<String, Object>> competiveList;
    
    /**
     * 备注信息（简介）
     */
    private String remark;
    
    /**
     * 任务卡是否填写
     */
    private String writeFlag;
    
    private static final long serialVersionUID = 1L;
    /**
     * 拜访规则id
     * @return visitrule_id 拜访规则id
     */
    public Long getVisitruleId() {
        return visitruleId;
    }

    /**
     * 拜访规则id
     * @param visitruleId 拜访规则id
     */
    public void setVisitruleId(Long visitruleId) {
        this.visitruleId = visitruleId;
    }

    /**
     * 拜访卡id
     * @return taskcard_id 拜访卡id
     */
    public Long getTaskcardId() {
        return taskcardId;
    }

    /**
     * 拜访卡id
     * @param taskcardId 拜访卡id
     */
    public void setTaskcardId(Long taskcardId) {
        this.taskcardId = taskcardId;
    }
    
    /**
     * 拜访卡名称
	 * @return taskcardName
	 */
	public String getTaskcardName() {
		return taskcardName;
	}

	/**
	 * @param taskcardName 拜访卡名称
	 */
	public void setTaskcardName(String taskcardName) {
		this.taskcardName = taskcardName;
	}

	/**
	 * 业务对象ID
	 * @return mtObjId
	 */
	public Long getMtObjId() {
		return mtObjId;
	}

	/**
	 * @param mtObjId 业务对象ID
	 */
	public void setMtObjId(Long mtObjId) {
		this.mtObjId = mtObjId;
	}

	/**
	 * 业务对象名称
	 * @return mtObjName
	 */
	public String getMtObjName() {
		return mtObjName;
	}

	/**
	 * @param mtObjName 业务对象名称
	 */
	public void setMtObjName(String mtObjName) {
		this.mtObjName = mtObjName;
	}

	/**
     * 显示顺序
     * @return order_num 显示顺序
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * 显示顺序
     * @param orderNum 显示顺序
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * 是否必输，1是2否
     * @return required 是否必输，1是2否
     */
    public Integer getRequired() {
        return required;
    }

    /**
     * 是否必输，1是2否
     * @param required 是否必输，1是2否
     */
    public void setRequired(Integer required) {
        this.required = required;
    }

	/**
	 * 是否必输名称
	 * @return requiredName
	 */
	public String getRequiredName() {
		return requiredName;
	}

	/**
	 * @param requiredName 是否必输名称
	 */
	public void setRequiredName(String requiredName) {
		this.requiredName = requiredName;
	}

	/**
	 * 是否预置，1是2否
	 * @return isPreseted
	 */
	public Integer getIsPreseted() {
		return isPreseted;
	}

	/**
	 * @param isPreseted 是否预置，1是2否
	 */
	public void setIsPreseted(Integer isPreseted) {
		this.isPreseted = isPreseted;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<Map<String, Object>> getOpporList() {
		return opporList;
	}

	public void setOpporList(List<Map<String, Object>> opporList) {
		this.opporList = opporList;
	}

	public List<Map<String, Object>> getLeadList() {
		return leadList;
	}

	public void setLeadList(List<Map<String, Object>> leadList) {
		this.leadList = leadList;
	}

	public List<Map<String, Object>> getContactList() {
		return contactList;
	}

	public void setContactList(List<Map<String, Object>> contactList) {
		this.contactList = contactList;
	}

	public List<Map<String, Object>> getCompetiveList() {
		return competiveList;
	}

	public void setCompetiveList(List<Map<String, Object>> competiveList) {
		this.competiveList = competiveList;
	}

	public Long getMtBiztypeId() {
		return mtBiztypeId;
	}

	public void setMtBiztypeId(Long mtBiztypeId) {
		this.mtBiztypeId = mtBiztypeId;
	}

	public String getMtBiztypeName() {
		return mtBiztypeName;
	}

	public void setMtBiztypeName(String mtBiztypeName) {
		this.mtBiztypeName = mtBiztypeName;
	}

	public String getWriteFlag() {
		return writeFlag;
	}

	public void setWriteFlag(String writeFlag) {
		this.writeFlag = writeFlag;
	}
	
}