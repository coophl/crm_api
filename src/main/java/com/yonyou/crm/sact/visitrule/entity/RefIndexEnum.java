package com.yonyou.crm.sact.visitrule.entity;

public enum RefIndexEnum {

	CUMLEVEL(1,"客户等级"),CUMTYPE(2,"客户类型"),CUMSTATE(3,"客户状态");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private RefIndexEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}

}
