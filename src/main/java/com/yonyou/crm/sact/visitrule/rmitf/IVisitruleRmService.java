package com.yonyou.crm.sact.visitrule.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.sact.visitrule.entity.VisitruleTaskcardVO;
import com.yonyou.crm.sact.visitrule.entity.VisitruleVO;

public interface IVisitruleRmService {

	public Page<VisitruleVO> list(Page<VisitruleVO> page,Map<String, Object> paraMap);
	public VisitruleVO detail(Long id);
	public VisitruleVO insert(VisitruleVO vo);
	public void update(VisitruleVO vo);
	public Page<VisitruleVO> batchDelete(String[] ids, Page<VisitruleVO> page,Map<String, Object> paraMap);
	public List<VisitruleVO> batchUpdateEnableState(String[] ids, Integer enableState,Map<String, Object> paraMap);
	public List<VisitruleVO> refList(Map<String, Object> paraMap);
	
	/*******拜访规则2.0*******/
	public List<VisitruleVO> list(Map<String, Object> paraMap);
	public void saveVT(List<VisitruleTaskcardVO> vtVOs, Long visitruleId);
	public void savePresetedVisitrule(Long orgId);
}
