package com.yonyou.crm.sact.visitrule.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class VisitruleVO implements Serializable {
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 任务卡列表
     */
    private List<VisitruleTaskcardVO> taskcardList;

    /**
     * 未选中任务卡列表
     */
    private List<VisitruleTaskcardVO> uncheckedList;
    
    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 规则名称
     */
    private String name;

    /**
     * 公司id
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 参考指标，1客户等级2客户类型3客户状态
     */
    private Integer refIndex;
    
    /**
     * 参考指标名称
     */
    private String refIndexName;
    
    /**
     * 客户等级
     */
    private Integer cumLevel;
    
    /**
     * 客户等级名称
     */
    private String cumLevelName;
    
    /**
     * 客户类型
     */
    private Integer cumType;
    
    /**
     * 客户类型名称
     */
    private String cumTypeName;
    
    /**
     * 客户状态
     */
    private Integer cumState;
    
    /**
     * 客户状态名称
     */
    private String cumStateName;
    
    /**
     * 客户相关枚举值
     */
    private Integer cumEnumValue;
    
    /**
     * 客户相关枚举值名称
     */
    private String cumEnumValueName;

    /**
     * 启用状态,1启用2停用
     */
    private Integer enableState;

    /**
     * 启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;
    
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 任务卡列表
	 * @return taskcardList
	 */
	public List<VisitruleTaskcardVO> getTaskcardList() {
		return taskcardList;
	}

	/**
	 * @param taskcardList 任务卡列表
	 */
	public void setTaskcardList(List<VisitruleTaskcardVO> taskcardList) {
		this.taskcardList = taskcardList;
	}

	/**
	 * 未选中任务卡列表
	 * @return uncheckedList
	 */
	public List<VisitruleTaskcardVO> getUncheckedList() {
		return uncheckedList;
	}

	/**
	 * @param uncheckedList 未选中任务卡列表
	 */
	public void setUncheckedList(List<VisitruleTaskcardVO> uncheckedList) {
		this.uncheckedList = uncheckedList;
	}

	/**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 规则名称
     * @return name 规则名称
     */
    public String getName() {
        return name;
    }

    /**
     * 规则名称
     * @param name 规则名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 公司名称
	 * @return orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName 公司名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * 参考指标，1客户等级2客户类型3客户状态
	 * @return refIndex
	 */
	public Integer getRefIndex() {
		return refIndex;
	}

	/**
	 * @param refIndex 参考指标，1客户等级2客户类型3客户状态
	 */
	public void setRefIndex(Integer refIndex) {
		this.refIndex = refIndex;
	}

	/**
	 * 参考指标名称
	 * @return refIndexName
	 */
	public String getRefIndexName() {
		return refIndexName;
	}

	/**
	 * @param refIndexName 参考指标名称
	 */
	public void setRefIndexName(String refIndexName) {
		this.refIndexName = refIndexName;
	}

	/**
	 * 客户等级
	 * @return cumLevel
	 */
	public Integer getCumLevel() {
		return cumLevel;
	}

	/**
	 * @param cumLevel 客户等级
	 */
	public void setCumLevel(Integer cumLevel) {
		this.cumLevel = cumLevel;
	}

	/**
	 * 客户等级名称
	 * @return cumLevelName
	 */
	public String getCumLevelName() {
		return cumLevelName;
	}

	/**
	 * @param cumLevelName 客户等级名称
	 */
	public void setCumLevelName(String cumLevelName) {
		this.cumLevelName = cumLevelName;
	}

	/**
	 * 客户类型
	 * @return cumType
	 */
	public Integer getCumType() {
		return cumType;
	}

	/**
	 * @param cumType 客户类型
	 */
	public void setCumType(Integer cumType) {
		this.cumType = cumType;
	}

	/**
	 * 客户类型名称
	 * @return cumTypeName
	 */
	public String getCumTypeName() {
		return cumTypeName;
	}

	/**
	 * @param cumTypeName 客户类型名称
	 */
	public void setCumTypeName(String cumTypeName) {
		this.cumTypeName = cumTypeName;
	}

	/**
	 * 客户状态
	 * @return cumState
	 */
	public Integer getCumState() {
		return cumState;
	}

	/**
	 * @param cumState 客户状态
	 */
	public void setCumState(Integer cumState) {
		this.cumState = cumState;
	}

	/**
	 * 客户状态名称
	 * @return cumStateName
	 */
	public String getCumStateName() {
		return cumStateName;
	}

	/**
	 * @param cumStateName 客户状态名称
	 */
	public void setCumStateName(String cumStateName) {
		this.cumStateName = cumStateName;
	}

	/**
	 * 客户相关枚举值
	 * @return cumEnumValue
	 */
	public Integer getCumEnumValue() {
		return cumEnumValue;
	}

	/**
	 * @param cumEnumValue 客户相关枚举值
	 */
	public void setCumEnumValue(Integer cumEnumValue) {
		this.cumEnumValue = cumEnumValue;
	}

	/**
	 * 客户相关枚举值名称
	 * @return cumEnumValueName
	 */
	public String getCumEnumValueName() {
		return cumEnumValueName;
	}

	/**
	 * @param cumEnumValueName 客户相关枚举值名称
	 */
	public void setCumEnumValueName(String cumEnumValueName) {
		this.cumEnumValueName = cumEnumValueName;
	}

	/**
     * 启用状态,1启用2停用
     * @return enable_state 启用状态,1启用2停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 启用状态,1启用2停用
     * @param enableState 启用状态,1启用2停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 启用状态名称
	 * @return enableStateName
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * @param enableStateName 启用状态名称
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }
}