package com.yonyou.crm.sact.visitrule.entity;

import java.io.Serializable;

public class VisitruleDeptVO implements Serializable {

	/**
	 * 拜访规则id
	 */
	private Long visitruleId;
	
	/**
	 * 部门id
	 */
	private Long deptId;
	
	private static final long serialVersionUID = 1L;
	/**
	 * 拜访规则id
	 * @return
	 */
	public Long getVisitruleId() {
		return visitruleId;
	}

	/**
	 * 拜访规则id
	 * @param visitruleId
	 */
	public void setVisitruleId(Long visitruleId) {
		this.visitruleId = visitruleId;
	}

	/**
	 * 部门id
	 * @return
	 */
	public Long getDeptId() {
		return deptId;
	}

	/**
	 * 部门id
	 * @param deptId
	 */
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

}
