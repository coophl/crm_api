package com.yonyou.crm.sact.outwork.entity;

import java.io.Serializable;
import java.util.Date;

public class TraceVO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 租户id(集团id)
     */
    private Long tenantId;

    /**
     * 
     */
    private Long orgId;

    /**
     * 11：位置签到，12：客户，13：拜访, 21.内勤
     */
    private Integer signType;

    /**
     * 打卡或签到人ID
     */
    private Long userId;

    /**
     * 外勤客户签到时，为客户account_id； 内勤打卡记录时，为考勤点point_id,拜访时为visit_id
     */
    private Long relObjId;

    /**
     * 地址经度
     */
    private String longitude;

    /**
     * 地址维度
     */
    private String latitude;

    /**
     * 海拔
     */
    private String altitude;

    /**
     * 地址名称
     */
    private String addrName;

    /**
     * 详细地址
     */
    private String address;

    /**
     * geohash
     */
    private String geohash;

    /**
     * 外勤附件信息，json字符串
     */
    private String noteFile;

    /**
     * 外勤签到内容的类型：1.纯文字、2.含图片，3含语音
     */
    private int noteType;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备型号
     */
    private String deviceModel;

    /**
     * 设备码
     */
    private String deviceId;

    /**
     * 
     */
    private Long deptId;

    /**
     * 打卡时间
     */
    private Date signTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 
     */
    private Date ts;

    /**
     * 外勤签到内容
     */
    private String note;

    /**
     * sact_trace
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id(集团id)
     * @return tenant_id 租户id(集团id)
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id(集团id)
     * @param tenantId 租户id(集团id)
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 
     * @return org_id 
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 
     * @param orgId 
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 1.外勤，2内勤
     * @return sign_type 1.外勤，2内勤
     */
    public Integer getSignType() {
        return signType;
    }

    /**
     * 1.外勤，2内勤
     * @param signType 1.外勤，2内勤
     */
    public void setSignType(Integer signType) {
        this.signType = signType;
    }

    /**
     * 打卡或签到人ID
     * @return user_id 打卡或签到人ID
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 打卡或签到人ID
     * @param userId 打卡或签到人ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 外勤客户签到时，为客户account_id； 内勤打卡记录时，为考勤点point_id
     * @return rel_obj_id 外勤客户签到时，为客户account_id； 内勤打卡记录时，为考勤点point_id
     */
    public Long getRelObjId() {
        return relObjId;
    }

    /**
     * 外勤客户签到时，为客户account_id； 内勤打卡记录时，为考勤点point_id
     * @param relObjId 外勤客户签到时，为客户account_id； 内勤打卡记录时，为考勤点point_id
     */
    public void setRelObjId(Long relObjId) {
        this.relObjId = relObjId;
    }

    /**
     * 地址经度
     * @return jlongitude 地址经度
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 地址经度
     * @param jlongitude 地址经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 地址维度
     * @return wlatitude 地址维度
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 地址维度
     * @param wlatitude 地址维度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAltitude() {
		return altitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	/**
     * 地址名称
     * @return addr_name 地址名称
     */
    public String getAddrName() {
        return addrName;
    }

    /**
     * 地址名称
     * @param addrName 地址名称
     */
    public void setAddrName(String addrName) {
        this.addrName = addrName;
    }

    /**
     * 详细地址
     * @return address 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 详细地址
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * geohash
     * @return geohash geohash
     */
    public String getGeohash() {
        return geohash;
    }

    /**
     * geohash
     * @param geohash geohash
     */
    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    /**
     * 外勤附件信息，json字符串
     * @return note_file 外勤附件信息，json字符串
     */
    public String getNoteFile() {
        return noteFile;
    }

    /**
     * 外勤附件信息，json字符串
     * @param noteFile 外勤附件信息，json字符串
     */
    public void setNoteFile(String noteFile) {
        this.noteFile = noteFile;
    }

    /**
     * 外勤签到内容的类型：1.纯文字、2.含图片，3含语音
     * @return note_type 外勤签到内容的类型：1.纯文字、2.含图片，3含语音
     */
    public int getNoteType() {
        return noteType;
    }

    /**
     * 外勤签到内容的类型：1.纯文字、2.含图片，3含语音
     * @param noteType 外勤签到内容的类型：1.纯文字、2.含图片，3含语音
     */
    public void setNoteType(int noteType) {
        this.noteType = noteType;
    }

    /**
     * 设备名称
     * @return device_name 设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 设备名称
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * 设备型号
     * @return device_model 设备型号
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * 设备型号
     * @param deviceModel 设备型号
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /**
     * 设备码
     * @return device_id 设备码
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 设备码
     * @param deviceId 设备码
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 
     * @return dept_id 
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 
     * @param deptId 
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 打卡时间
     * @return sign_time 打卡时间
     */
    public Date getSignTime() {
        return signTime;
    }

    /**
     * 打卡时间
     * @param signTime 打卡时间
     */
    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }

	/**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 
     * @return ts 
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 
     * @param ts 
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 外勤签到内容
     * @return note 外勤签到内容
     */
    public String getNote() {
        return note;
    }

    /**
     * 外勤签到内容
     * @param note 外勤签到内容
     */
    public void setNote(String note) {
        this.note = note;
    }
}