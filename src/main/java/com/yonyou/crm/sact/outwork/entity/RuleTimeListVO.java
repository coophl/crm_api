package com.yonyou.crm.sact.outwork.entity;

import java.io.Serializable;

public class RuleTimeListVO implements Serializable {

	/*
	 * 格式 hh-mm
	 */
	private String starttime;
	
	private String endtime;
	
	private static final long serialVersionUID = 1L;
	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
}
