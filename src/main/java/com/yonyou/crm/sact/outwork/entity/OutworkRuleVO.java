package com.yonyou.crm.sact.outwork.entity;

import java.io.Serializable;
import java.util.Date;

public class OutworkRuleVO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 租户id(集团id)
     */
    private Long tenantId;

    /**
     * 公司id
     */
    private Long orgId;

    /**
     * 外勤签到时间范围
     */
    private String timeRange;

    /**
     * 规则生效日期
     */
    private Date startDate;

    /**
     * 规则失效日期
     */
    private Date terminalDate;

    /**
     * 停启用状态，1启用、2停用
     */
    private Integer enableState;

    /**
     * 停启用操作人
     */
    private Long enableUserId;

    /**
     * 停启用日期
     */
    private Date enableTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * sact_outwork_rule
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户id(集团id)
     * @return tenant_id 租户id(集团id)
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id(集团id)
     * @param tenantId 租户id(集团id)
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 外勤签到时间范围
     * @return time_range 外勤签到时间范围
     */
    public String getTimeRange() {
        return timeRange;
    }

    /**
     * 外勤签到时间范围
     * @param timeRange 外勤签到时间范围
     */
    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    /**
     * 规则生效时间
     * @return start_time 规则生效时间
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * 规则生效时间
     * @param startDate 规则生效时间
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 规则失效时间
     * @return terminalDate 规则失效时间
     */
    public Date getTerminalDate() {
        return terminalDate;
    }

    /**
     * 规则失效时间
     * @param terminalDate 规则失效时间
     */
    public void setTerminalDate(Date terminalDate) {
        this.terminalDate = terminalDate;
    }

    public Integer getEnableState() {
		return enableState;
	}

	public void setEnableState(Integer enableState) {
		this.enableState = enableState;
	}

	public Long getEnableUserId() {
		return enableUserId;
	}

	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}

	public Date getEnableTime() {
		return enableTime;
	}

	public void setEnableTime(Date enableTime) {
		this.enableTime = enableTime;
	}

	/**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}
}