package com.yonyou.crm.sact.outwork.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.sact.outwork.entity.OutworkRuleVO;
import com.yonyou.crm.sact.outwork.entity.RuleTimeListVO;
import com.yonyou.crm.sact.outwork.entity.TraceListVO;
import com.yonyou.crm.sact.outwork.entity.TraceVO;

public interface IOutworkRmService {
	
	public Map<String, Object> saveRule(OutworkRuleVO vo);

	public List<RuleTimeListVO> getRuleList(Map<String, Object> paraMap);
	
	public Map<String, Object> getRule(Map<String, Object> paraMap);
	
	public TraceVO saveTrace(TraceVO vo);
	
	public List<TraceListVO> getTraceList(Map<String, Object> paraMap); 
	
	public List<String> getAbsenceDates(Map<String, Object> paraMap);
	
	public Map<String, Object> getSubOutworkList(Map<String, Object> paraMap);
	
}
