package com.yonyou.crm.sact.outwork.entity;

import java.io.Serializable;
import java.util.List;

import com.yonyou.crm.bpub.detail.entity.LocationVO;

public class TraceListVO implements Serializable {
	
	private Long id;
	
	private String time;
	
	private LocationVO location;
	
	private String note;
	
	private List<String> piclist;
	
	private static final long serialVersionUID = 1L;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public LocationVO getLocation() {
		return location;
	}

	public void setLocation(LocationVO location) {
		this.location = location;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<String> getPiclist() {
		return piclist;
	}

	public void setPiclist(List<String> piclist) {
		this.piclist = piclist;
	}

}
