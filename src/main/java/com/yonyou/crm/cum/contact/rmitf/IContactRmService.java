package com.yonyou.crm.cum.contact.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.bpub.list.entity.AppListVO;
import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.contact.entity.ContactVO;


public interface IContactRmService {
	
	public Page<ContactVO> getList(Page<ContactVO> page,Map<String, Object> paraMap);
	
	public ContactVO getDetail(Long id);
	
	public ContactVO insert(ContactVO contact);
	
	public ContactVO insert(ContactVO contact, Map<String, Object> param);
	
	public ContactVO update(ContactVO contact);
	
	public int delete(Long id);
	
	public Page<ContactVO> batchDelete(String[] ids, Page<ContactVO> page,Map<String, Object> paraMap);
	
	List<AppListVO> getListForApp(Map<String, Object> map);
	
	public int insertFollow(Long id);
	
	public int deleteFollow(Long id);
	
	public List<String> getContactNameById(String[] ids);
}
