package com.yonyou.crm.cum.contact.entity;

import java.io.Serializable;
import java.util.Date;

import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sys.user.entity.UserVO;

public class ContactVO implements Serializable {
	
    /**
     * cum_contact
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 联系人id
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;
    
    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;
    
    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;
    
    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 所属集团
     */
    private Long groupId;
    
    /**
     * 集团名称
     */
    private String groupName;

    /**
     * 所属公司
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 联系人编码
     */
    private String code;

    /**
     * 联系人照片
     */
    private String photo;

    /**
     * 姓名
     */
    private String name;

    /**
     * 职务
     */
    private Long post;

    /**
     * 部门
     */
    private Long deptId;
    
    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 客户
     */
    private Long customer;
    
    /**
     * 客户信息
     */
    private CustomerVO customerInfo;

    /**
     * 出生年份
     */
    private Byte birthYear;

    /**
     * 生日
     */
    private Byte birthday;

    /**
     * 性别(男：1，女：2，未指定：3)
     */
    private Byte gender;
    
    /**
     * 性别名称
     */
    private String genderName;

    /**
     * 婚姻(是：1，否：2，不确定：3)
     */
    private Byte marriage;
    
    /**
     * 婚姻名称
     */
    private String marriageName;

    /**
     * 上级联系人
     */
    private Long supContactId;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 家庭电话
     */
    private String homePhone;

    /**
     * 办公电话
     */
    private String officePhone;

    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 微信
     */
    private String wechat;
    
    /**
     * 微博
     */
    private String blog;

    /**
     * QQ
     */
    private String qq;
    
    /**
     * 国家
     */
    private int country;

    /**
     * 省
     */
    private int province;
    
    /**
     * 省名称
     */
    private String provinceName;

    /**
     * 市
     */
    private int city;
    
    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 区
     */
    private int district;
    
    /**
     * 区县名称
     */
    private String districtName;

    /**
     * 街道号码
     */
    private String street;

    /**
     * 邮编
     */
    private String zipCode;
    
    /**
     * 详细地址
     */
    private String address;

    /**
     * 主要联系人标识
     */
    private Byte mainContact;
    
    /**
     * 主要联系人标识名称
     */
    private String mainContactName;

    /**
     * 负责人
     */
    private Long ownerUserId;
    
    /**
     * 负责人信息
     */
    private UserVO ownerUserInfo;

    /**
     * 参与人
     */
    private Long relUserId;

    /**
     * 角色
     */
    private String role;
    
    /**
     * 角色名称
     */
    private String roleName;
    
    /**
     * 态度
     */
    private String attitude;
    
    /**
     * 态度名称
     */
    private String attitudeName;
    
    /**
     * 兴趣爱好
     */
    private String hobby;

    /**
     * 启用标识(启用1，停用2)
     */
    private Byte enableState;
    
    /**
     * 停启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 最近跟进人
     */
    private Long followUserId;

    /**
     * 最近跟进时间
     */
    private Date followTime;
    
    /**
     * 关注标识
     */
    private String followFlag;

    /**
     * 自定义项1
     */
    private String def1;

    /**
     * 自定义项2
     */
    private String def2;

    /**
     * 自定义项3
     */
    private String def3;

    /**
     * 自定义项4
     */
    private String def4;

    /**
     * 自定义项5
     */
    private String def5;

    /**
     * 自定义项6
     */
    private String def6;

    /**
     * 自定义项7
     */
    private String def7;

    /**
     * 自定义项8
     */
    private String def8;

    /**
     * 自定义项9
     */
    private String def9;

    /**
     * 自定义项10
     */
    private String def10;

    /**
     * 自定义项11
     */
    private String def11;

    /**
     * 自定义项12
     */
    private String def12;

    /**
     * 自定义项13
     */
    private String def13;

    /**
     * 自定义项14
     */
    private String def14;

    /**
     * 自定义项15
     */
    private String def15;

    /**
     * 自定义项16
     */
    private String def16;

    /**
     * 自定义项17
     */
    private String def17;

    /**
     * 自定义项18
     */
    private String def18;

    /**
     * 自定义项19
     */
    private String def19;

    /**
     * 自定义项20
     */
    private String def20;

    /**
     * 自定义项21
     */
    private String def21;

    /**
     * 自定义项22
     */
    private String def22;

    /**
     * 自定义项23
     */
    private String def23;

    /**
     * 自定义项24
     */
    private String def24;

    /**
     * 自定义项25
     */
    private String def25;

    /**
     * 自定义项26
     */
    private String def26;

    /**
     * 自定义项27
     */
    private String def27;

    /**
     * 自定义项28
     */
    private String def28;

    /**
     * 自定义项29
     */
    private String def29;

    /**
     * 自定义项30
     */
    private String def30;

    /**
     * 联系人id
     * @return id 联系人id
     */
    public Long getId() {
        return id;
    }

    /**
     * 联系人id
     * @param id 联系人id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }
    
    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }
    
    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }
    
    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }
    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }
    
    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 所属集团
     * @return group_id 所属集团
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * 所属集团
     * @param groupId 所属集团
     */
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    
    /**
     * 集团名称
	 * @return groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName 集团名称
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	/**
     * 所属公司
     * @return org_id 所属公司
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 所属公司
     * @param orgId 所属公司
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    
	/**
	 * 公司名称
	 * @return orgName 公司名称
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * 公司名称
	 * @param orgName 公司名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
    /**
     * 联系人编码
     * @return code 联系人编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 联系人编码
     * @param code 联系人编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 联系人照片
     * @return photo 联系人照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 联系人照片
     * @param photo 联系人照片
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 职务
     * @return post 职务
     */
    public Long getPost() {
        return post;
    }

    /**
     * 职务
     * @param post 职务
     */
    public void setPost(Long post) {
        this.post = post;
    }

    /**
     * 部门
     * @return dept_id 部门
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 部门
     * @param deptId 部门
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }
	
	/**
	 * 部门名称
	 * @return deptName	部门名称
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * 部门名称
	 * @param deptName 部门名称
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
    /**
     * 客户
     * @return customer 客户
     */
    public Long getCustomer() {
        return customer;
    }

    /**
     * 客户
     * @param customer 客户
     */
    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    public CustomerVO getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerVO customerInfo) {
		this.customerInfo = customerInfo;
	}

	/**
     * 出生年份
     * @return birth_year 出生年份
     */
    public Byte getBirthYear() {
        return birthYear;
    }

    /**
     * 出生年份
     * @param birthYear 出生年份
     */
    public void setBirthYear(Byte birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * 生日
     * @return birthday 生日
     */
    public Byte getBirthday() {
        return birthday;
    }

    /**
     * 生日
     * @param birthday 生日
     */
    public void setBirthday(Byte birthday) {
        this.birthday = birthday;
    }

    /**
     * 性别(男：1，女：2)
     * @return gender 性别(男：1，女：2)
     */
    public Byte getGender() {
        return gender;
    }

    /**
     * 性别(男：1，女：2)
     * @param gender 性别(男：1，女：2)
     */
    public void setGender(Byte gender) {
        this.gender = gender;
    }
    
	/**
	 * 性别名称
	 * @return genderName 性别名称
	 */
	public String getGenderName() {
		return genderName;
	}

	/**
	 * 性别名称
	 * @param genderName 性别名称
	 */
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	
    /**
     * 婚姻(是：1，否：2)
     * @return marriage 婚姻(是：1，否：2)
     */
    public Byte getMarriage() {
        return marriage;
    }

    /**
     * 婚姻(是：1，否：2)
     * @param marriage 婚姻(是：1，否：2)
     */
    public void setMarriage(Byte marriage) {
        this.marriage = marriage;
    }
    
	/**
	 * 婚姻名称
	 * @return marriageName 婚姻名称
	 */
	public String getMarriageName() {
		return marriageName;
	}

	/**
	 * 婚姻名称
	 * @param marriageName 婚姻名称
	 */
	public void setMarriageName(String marriageName) {
		this.marriageName = marriageName;
	}

    /**
     * 上级联系人
     * @return sup_contact_id 上级联系人
     */
    public Long getSupContactId() {
        return supContactId;
    }

    /**
     * 上级联系人
     * @param supContactId 上级联系人
     */
    public void setSupContactId(Long supContactId) {
        this.supContactId = supContactId;
    }

    /**
     * 备注
     * @return remarks 备注
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 备注
     * @param remarks 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 手机
     * @return mobile 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 家庭电话
     * @return home_phone 家庭电话
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * 家庭电话
     * @param homePhone 家庭电话
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * 办公电话
     * @return office_phone 办公电话
     */
    public String getOfficePhone() {
        return officePhone;
    }

    /**
     * 办公电话
     * @param officePhone 办公电话
     */
    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    /**
     * 传真
     * @return fax 传真
     */
    public String getFax() {
        return fax;
    }

    /**
     * 传真
     * @param fax 传真
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * 邮箱
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 微信
     * @return wechat 微信
     */
    public String getWechat() {
        return wechat;
    }

    /**
     * 微信
     * @param wechat 微信
     */
    public void setWechat(String wechat) {
        this.wechat = wechat;
    }
    
	/**
	 * 微博
	 * @return blog	微博
	 */
	public String getBlog() {
		return blog;
	}

	/**
	 * @param blog 微博
	 */
	public void setBlog(String blog) {
		this.blog = blog;
	}
	
    /**
     * QQ
     * @return qq QQ
     */
    public String getQq() {
        return qq;
    }

    /**
     * QQ
     * @param qq QQ
     */
    public void setQq(String qq) {
        this.qq = qq;
    }

    /**
     * 国家
	 * @return country	国家
	 */
	public int getCountry() {
		return country;
	}

	/**
	 * 国家
	 * @param country 国家
	 */
	public void setCountry(int country) {
		this.country = country;
	}

	/**
	 * 省
	 * @return province	省
	 */
	public int getProvince() {
		return province;
	}

	/**
	 * @param province 省
	 */
	public void setProvince(int province) {
		this.province = province;
	}

	/**
	 * 省份名称
	 * @return provinceName	省份名称
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * @param provinceName 省份名称
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * 市
	 * @return city	市
	 */
	public int getCity() {
		return city;
	}

	/**
	 * @param city 市
	 */
	public void setCity(int city) {
		this.city = city;
	}

	/**
	 * 城市名称
	 * @return cityName	城市名称
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName 城市名称
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * 区
	 * @return district	区
	 */
	public int getDistrict() {
		return district;
	}

	/**
	 * @param district 区
	 */
	public void setDistrict(int district) {
		this.district = district;
	}

	/**
	 * 区县名称
	 * @return districtName	区县名称
	 */
	public String getDistrictName() {
		return districtName;
	}

	/**
	 * @param districtName 区县名称
	 */
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
	 * 街道号码
	 * @return street 街道号码
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street 街道号码
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * 邮编
	 * @return zipCode 邮编
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode 邮编
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

    /**
     * 详细地址
     * @return address 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 详细地址
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 主要联系人标识
     * @return main_contact 主要联系人标识
     */
    public Byte getMainContact() {
        return mainContact;
    }

    /**
     * 主要联系人标识
     * @param mainContact 主要联系人标识
     */
    public void setMainContact(Byte mainContact) {
        this.mainContact = mainContact;
    }

    /**
     * 主要联系人标识名称
	 * @return mainContactName 主要联系人标识名称
	 */
	public String getMainContactName() {
		return mainContactName;
	}

	/**
	 * 主要联系人标识名称
	 * @param mainContactName 主要联系人标识名称
	 */
	public void setMainContactName(String mainContactName) {
		this.mainContactName = mainContactName;
	}

	/**
     * 负责人
     * @return owner_user_id 负责人
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人
     * @param ownerUserId 负责人
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public UserVO getOwnerUserInfo() {
		return ownerUserInfo;
	}

	public void setOwnerUserInfo(UserVO ownerUserInfo) {
		this.ownerUserInfo = ownerUserInfo;
	}

	/**
     * 参与人
	 * @return relUserId 参与人
	 */
	public Long getRelUserId() {
		return relUserId;
	}

	/**
	 * 参与人
	 * @param relUserId 参与人
	 */
	public void setRelUserId(Long relUserId) {
		this.relUserId = relUserId;
	}

	/**
	 * @return role	角色
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role 角色
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return roleName 角色名称
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName 角色名称
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return attitude	态度
	 */
	public String getAttitude() {
		return attitude;
	}

	/**
	 * @param attitude 态度
	 */
	public void setAttitude(String attitude) {
		this.attitude = attitude;
	}
	
    /**
	 * @return attitudeName 态度名称
	 */
	public String getAttitudeName() {
		return attitudeName;
	}

	/**
	 * @param attitudeName 态度名称
	 */
	public void setAttitudeName(String attitudeName) {
		this.attitudeName = attitudeName;
	}

	/**
     * 兴趣爱好
     * @return hobby 兴趣爱好
     */
    public String getHobby() {
        return hobby;
    }

    /**
     * 兴趣爱好
     * @param hobby 兴趣爱好
     */
    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    /**
     * 启用标识(启用1，停用2)
     * @return enable_state 启用标识(启用1，停用2)
     */
    public Byte getEnableState() {
        return enableState;
    }

    /**
     * 启用标识(启用1，停用2)
     * @param enableState 启用标识(启用1，停用2)
     */
    public void setEnableState(Byte enableState) {
        this.enableState = enableState;
    }
    
	/**
	 * 停启用状态名称
	 * @return enableStateName 停启用状态名称
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * 停启用状态名称
	 * @param enableStateName 停启用状态名称
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

    /**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 	最近跟进人
	 * @return followUserId	最近跟进人
	 */
	public Long getFollowUserId() {
		return followUserId;
	}

	/**
	 * 	最近跟进人
	 * @param followUserId 	最近跟进人
	 */
	public void setFollowUserId(Long followUserId) {
		this.followUserId = followUserId;
	}

	/**
	 * 	最近跟进时间
	 * @return followTime 最近跟进时间
	 */
	public Date getFollowTime() {
		return followTime;
	}

	/**
	 * 最近跟进时间
	 * @param followTime 最近跟进时间
	 */
	public void setFollowTime(Date followTime) {
		this.followTime = followTime;
	}

	public String getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(String followFlag) {
		this.followFlag = followFlag;
	}

	/**
     * 自定义项1
     * @return def1 自定义项1
     */
    public String getDef1() {
        return def1;
    }

    /**
     * 自定义项1
     * @param def1 自定义项1
     */
    public void setDef1(String def1) {
        this.def1 = def1;
    }

    /**
     * 自定义项2
     * @return def2 自定义项2
     */
    public String getDef2() {
        return def2;
    }

    /**
     * 自定义项2
     * @param def2 自定义项2
     */
    public void setDef2(String def2) {
        this.def2 = def2;
    }

    /**
     * 自定义项3
     * @return def3 自定义项3
     */
    public String getDef3() {
        return def3;
    }

    /**
     * 自定义项3
     * @param def3 自定义项3
     */
    public void setDef3(String def3) {
        this.def3 = def3;
    }

    /**
     * 自定义项4
     * @return def4 自定义项4
     */
    public String getDef4() {
        return def4;
    }

    /**
     * 自定义项4
     * @param def4 自定义项4
     */
    public void setDef4(String def4) {
        this.def4 = def4;
    }

    /**
     * 自定义项5
     * @return def5 自定义项5
     */
    public String getDef5() {
        return def5;
    }

    /**
     * 自定义项5
     * @param def5 自定义项5
     */
    public void setDef5(String def5) {
        this.def5 = def5;
    }

    /**
     * 自定义项6
     * @return def6 自定义项6
     */
    public String getDef6() {
        return def6;
    }

    /**
     * 自定义项6
     * @param def6 自定义项6
     */
    public void setDef6(String def6) {
        this.def6 = def6;
    }

    /**
     * 自定义项7
     * @return def7 自定义项7
     */
    public String getDef7() {
        return def7;
    }

    /**
     * 自定义项7
     * @param def7 自定义项7
     */
    public void setDef7(String def7) {
        this.def7 = def7;
    }

    /**
     * 自定义项8
     * @return def8 自定义项8
     */
    public String getDef8() {
        return def8;
    }

    /**
     * 自定义项8
     * @param def8 自定义项8
     */
    public void setDef8(String def8) {
        this.def8 = def8;
    }

    /**
     * 自定义项9
     * @return def9 自定义项9
     */
    public String getDef9() {
        return def9;
    }

    /**
     * 自定义项9
     * @param def9 自定义项9
     */
    public void setDef9(String def9) {
        this.def9 = def9;
    }

    /**
     * 自定义项10
     * @return def10 自定义项10
     */
    public String getDef10() {
        return def10;
    }

    /**
     * 自定义项10
     * @param def10 自定义项10
     */
    public void setDef10(String def10) {
        this.def10 = def10;
    }

    /**
     * 自定义项11
     * @return def11 自定义项11
     */
    public String getDef11() {
        return def11;
    }

    /**
     * 自定义项11
     * @param def11 自定义项11
     */
    public void setDef11(String def11) {
        this.def11 = def11;
    }

    /**
     * 自定义项12
     * @return def12 自定义项12
     */
    public String getDef12() {
        return def12;
    }

    /**
     * 自定义项12
     * @param def12 自定义项12
     */
    public void setDef12(String def12) {
        this.def12 = def12;
    }

    /**
     * 自定义项13
     * @return def13 自定义项13
     */
    public String getDef13() {
        return def13;
    }

    /**
     * 自定义项13
     * @param def13 自定义项13
     */
    public void setDef13(String def13) {
        this.def13 = def13;
    }

    /**
     * 自定义项14
     * @return def14 自定义项14
     */
    public String getDef14() {
        return def14;
    }

    /**
     * 自定义项14
     * @param def14 自定义项14
     */
    public void setDef14(String def14) {
        this.def14 = def14;
    }

    /**
     * 自定义项15
     * @return def15 自定义项15
     */
    public String getDef15() {
        return def15;
    }

    /**
     * 自定义项15
     * @param def15 自定义项15
     */
    public void setDef15(String def15) {
        this.def15 = def15;
    }

    /**
     * 自定义项16
     * @return def16 自定义项16
     */
    public String getDef16() {
        return def16;
    }

    /**
     * 自定义项16
     * @param def16 自定义项16
     */
    public void setDef16(String def16) {
        this.def16 = def16;
    }

    /**
     * 自定义项17
     * @return def17 自定义项17
     */
    public String getDef17() {
        return def17;
    }

    /**
     * 自定义项17
     * @param def17 自定义项17
     */
    public void setDef17(String def17) {
        this.def17 = def17;
    }

    /**
     * 自定义项18
     * @return def18 自定义项18
     */
    public String getDef18() {
        return def18;
    }

    /**
     * 自定义项18
     * @param def18 自定义项18
     */
    public void setDef18(String def18) {
        this.def18 = def18;
    }

    /**
     * 自定义项19
     * @return def19 自定义项19
     */
    public String getDef19() {
        return def19;
    }

    /**
     * 自定义项19
     * @param def19 自定义项19
     */
    public void setDef19(String def19) {
        this.def19 = def19;
    }

    /**
     * 自定义项20
     * @return def20 自定义项20
     */
    public String getDef20() {
        return def20;
    }

    /**
     * 自定义项20
     * @param def20 自定义项20
     */
    public void setDef20(String def20) {
        this.def20 = def20;
    }

    /**
     * 自定义项21
     * @return def21 自定义项21
     */
    public String getDef21() {
        return def21;
    }

    /**
     * 自定义项21
     * @param def21 自定义项21
     */
    public void setDef21(String def21) {
        this.def21 = def21;
    }

    /**
     * 自定义项22
     * @return def22 自定义项22
     */
    public String getDef22() {
        return def22;
    }

    /**
     * 自定义项22
     * @param def22 自定义项22
     */
    public void setDef22(String def22) {
        this.def22 = def22;
    }

    /**
     * 自定义项23
     * @return def23 自定义项23
     */
    public String getDef23() {
        return def23;
    }

    /**
     * 自定义项23
     * @param def23 自定义项23
     */
    public void setDef23(String def23) {
        this.def23 = def23;
    }

    /**
     * 自定义项24
     * @return def24 自定义项24
     */
    public String getDef24() {
        return def24;
    }

    /**
     * 自定义项24
     * @param def24 自定义项24
     */
    public void setDef24(String def24) {
        this.def24 = def24;
    }

    /**
     * 自定义项25
     * @return def25 自定义项25
     */
    public String getDef25() {
        return def25;
    }

    /**
     * 自定义项25
     * @param def25 自定义项25
     */
    public void setDef25(String def25) {
        this.def25 = def25;
    }

    /**
     * 自定义项26
     * @return def26 自定义项26
     */
    public String getDef26() {
        return def26;
    }

    /**
     * 自定义项26
     * @param def26 自定义项26
     */
    public void setDef26(String def26) {
        this.def26 = def26;
    }

    /**
     * 自定义项27
     * @return def27 自定义项27
     */
    public String getDef27() {
        return def27;
    }

    /**
     * 自定义项27
     * @param def27 自定义项27
     */
    public void setDef27(String def27) {
        this.def27 = def27;
    }

    /**
     * 自定义项28
     * @return def28 自定义项28
     */
    public String getDef28() {
        return def28;
    }

    /**
     * 自定义项28
     * @param def28 自定义项28
     */
    public void setDef28(String def28) {
        this.def28 = def28;
    }

    /**
     * 自定义项29
     * @return def29 自定义项29
     */
    public String getDef29() {
        return def29;
    }

    /**
     * 自定义项29
     * @param def29 自定义项29
     */
    public void setDef29(String def29) {
        this.def29 = def29;
    }

    /**
     * 自定义项30
     * @return def30 自定义项30
     */
    public String getDef30() {
        return def30;
    }

    /**
     * 自定义项30
     * @param def30 自定义项30
     */
    public void setDef30(String def30) {
        this.def30 = def30;
    }

}