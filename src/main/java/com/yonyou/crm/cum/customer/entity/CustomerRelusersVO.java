package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.util.Date;

public class CustomerRelusersVO implements Serializable {
	
    /**
	 * cum_customer_relusers
	 */
	private static final long serialVersionUID = 1L;

	/**
     * ID
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 公司ID
     */
    private Long orgId;

    /**
     * 客户ID
     */
    private Long cumId;

    /**
     * 参与人部门ID
     */
    private Long deptId;
    
    /**
     * 参与人部门名称
     */
    private String deptName;

    /**
     * 参与人ID
     */
    private Long userId;
    
    /**
     * 参与人名称
     */
    private String userName;

    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * ID
     * @return id ID
     */
    public Long getId() {
        return id;
    }

    /**
     * ID
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司ID
     * @return org_id 公司ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司ID
     * @param orgId 公司ID
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 客户ID
     * @return cum_id 客户ID
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户ID
     * @param cumId 客户ID
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    /**
     * 参与人部门ID
     * @return dept_id 参与人部门ID
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 参与人部门ID
     * @param deptId 参与人部门ID
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 参与人ID
     * @return user_id 参与人ID
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 参与人ID
     * @param userId 参与人ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	/**
	 * @return deptName
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * @param deptName 要设置的 deptName
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * @return userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName 要设置的 userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}