package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;

public class AppCustomerListValueVO implements Serializable{
	
	private String itemkey;
	
	private Object displayvalue;

	public String getItemkey() {
		return itemkey;
	}

	public void setItemkey(String itemkey) {
		this.itemkey = itemkey;
	}

	public Object getDisplayvalue() {
		return displayvalue;
	}

	public void setDisplayvalue(Object displayvalue) {
		this.displayvalue = displayvalue;
	}

}
