package com.yonyou.crm.cum.customer.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerVO;

public interface ICustomerChartRmService {
	
	public Object getOwnerDatas(Long deptId, Long userId);
	
	public Page<CustomerVO> getCustomers(Page<CustomerVO> page, Map<String, Object> paramMap);

	public Object getRatio(Long deptId, Long userId);
}
