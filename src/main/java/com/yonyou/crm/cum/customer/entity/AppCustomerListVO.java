package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.util.List;

public class AppCustomerListVO implements Serializable {
	
	private Long id;
	
	private List<AppCustomerListValueVO> valuelist;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<AppCustomerListValueVO> getValuelist() {
		return valuelist;
	}

	public void setValuelist(List<AppCustomerListValueVO> valuelist) {
		this.valuelist = valuelist;
	}

}
