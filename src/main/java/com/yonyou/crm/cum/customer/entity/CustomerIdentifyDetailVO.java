package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;

public class CustomerIdentifyDetailVO implements Serializable{

	private String key;
	
	private String name;
	
	private Object value;

	public CustomerIdentifyDetailVO (String key, String name, Object value) {
		this.key = key;
		this.name = name;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	

}
