package com.yonyou.crm.cum.customer.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerFollowVO;
import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sys.dynamic.entity.DynamicVO;

public interface IGroupCustomerRmService {
	
	public CustomerVO getDetail(Long id);
	
	public Page<CustomerVO> getList(Page<CustomerVO> page, Map<String, Object> paramMap);
	
	public Map<String, Object> getViewUserList(Long cumId, Map<String, Object> paramMap);
	
	public Map<String, Object> getFollowUserList(Long cumId, Map<String, Object> paramMap);
	
	public CustomerVO insert(CustomerVO customer);
	
	public CustomerVO update(CustomerVO customer);
	
	public int delete(Long id);
	
	public Page<CustomerVO> batchDelete(String[] ids, Page<CustomerVO> page, Map<String, Object> paraMap);
	
	public int updateState(Long id, int enableState);
	
	public Page<CustomerVO> batchUpdateState(String[] ids, Integer enableState, Page<CustomerVO> page, Map<String, Object> paraMap);
	
	public CustomerFollowVO saveFollow(Long cumId);
	
	public int deleteFollow(Long cumId);
	
	public int checkNameExist(CustomerVO customer);
	
	public int isFollow(Long cumId);
	
	public List<CustomerVO> getRefList(Map<String, Object> paraMap);

	public Page<CustomerVO> getRefPage(Page<CustomerVO> page, Map<String, Object> paraMap);
	
	public CustomerVO identify(CustomerVO customer);
	
	public CustomerVO cancelIdentify(CustomerVO customer);
	
	public boolean getIdentifyFlag(Map<String, Object> paraMap);
	
	public boolean checkVerifyNameExist(String id, String verifyfullname);
	
	public DynamicVO[] getCumDynamic(Long cumId);

}
