package com.yonyou.crm.cum.customer.entity;

public enum CumGroupTypeEnum {
	COMPANY(1,"公司客户"),
	GROUP(2,"集团客户");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;

	// 构造方法
	private CumGroupTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
