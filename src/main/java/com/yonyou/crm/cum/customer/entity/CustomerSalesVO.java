package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CustomerSalesVO implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 客户ID
     */
    private Long cumId;

    /**
     * 公司ID
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

	/**
     * 负责人所属部门ID
     */
    private Long ownerDeptId;
    
    /**
     * 负责人所属部门名称
     */
    private String ownerDeptName;

    /**
     * 负责人ID
     */
    private Long ownerUserId;
    
    /**
     * 负责人名称
     */
    private String ownerUserName;
    
    /**
     * 客户等级
     */
    private Integer level;

    /**
     * 客户等级名称
     */
    private String levelName;
    
    /**
     * 客户状态
     */
    private Integer state;
    
    /**
     * 客户状态名称
     */
    private String stateName;
    
    /**
     * 最近跟进时间
     */
    private Date followTime; 
    
    /**
     * 首次跟进时间
     */
    private Date firstFollowTime;

    /**
     * 最近拜访时间
     */
    private Date visitTime;
    
    /**
     * 最近交易时间
     */
    private Date exchangeTime;
    
    /**
     * 最近交易金额
     */
    private BigDecimal exchangeMoney;
    
    /**
     * 启用标识(启用1，停用2)
     */
    private Integer enableState;
    
    /**
     * 启用标识名称(1启用，2停用)
     */
    private String enableStateName;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 自定义字段1
     */
    private String def1;

    /**
     * 自定义字段2
     */
    private String def2;

    /**
     * 自定义字段3
     */
    private String def3;

    /**
     * 自定义字段4
     */
    private String def4;

    /**
     * 自定义字段5
     */
    private String def5;

    /**
     * 自定义字段6
     */
    private String def6;

    /**
     * 自定义字段7
     */
    private String def7;

    /**
     * 自定义字段8
     */
    private String def8;

    /**
     * 自定义字段9
     */
    private String def9;

    /**
     * 自定义字段10
     */
    private String def10;

    /**
     * 自定义字段11
     */
    private String def11;

    /**
     * 自定义字段12
     */
    private String def12;

    /**
     * 自定义字段13
     */
    private String def13;

    /**
     * 自定义字段14
     */
    private String def14;

    /**
     * 自定义字段15
     */
    private String def15;

    /**
     * 自定义字段16
     */
    private String def16;

    /**
     * 自定义字段17
     */
    private String def17;

    /**
     * 自定义字段18
     */
    private String def18;

    /**
     * 自定义字段19
     */
    private String def19;

    /**
     * 自定义字段20
     */
    private String def20;

    /**
     * 自定义字段21
     */
    private String def21;

    /**
     * 自定义字段22
     */
    private String def22;

    /**
     * 自定义字段23
     */
    private String def23;

    /**
     * 自定义字段24
     */
    private String def24;

    /**
     * 自定义字段25
     */
    private String def25;

    /**
     * 自定义字段26
     */
    private String def26;

    /**
     * 自定义字段27
     */
    private String def27;

    /**
     * 自定义字段28
     */
    private String def28;

    /**
     * 自定义字段29
     */
    private String def29;

    /**
     * 自定义字段30
     */
    private String def30;
    
    /**
     * 参与人信息
     */
    private String[] relUsersID;
    
    /**
     * 参与人信息
     */
    private String[] relUsersName;
    
    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * cum_customer_sale
     */
    private static final long serialVersionUID = 1L;

    /**
     * ID
     * @return id ID
     */
    public Long getId() {
        return id;
    }

    /**
     * ID
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 客户ID
     * @return cum_id 客户ID
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户ID
     * @param cumId 客户ID
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    /**
     * 公司ID
     * @return org_id 公司ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司ID
     * @param orgId 公司ID
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 负责人所属部门ID
     * @return owner_dept_id 负责人所属部门ID
     */
    public Long getOwnerDeptId() {
        return ownerDeptId;
    }

    /**
     * 负责人所属部门ID
     * @param ownerDeptId 负责人所属部门ID
     */
    public void setOwnerDeptId(Long ownerDeptId) {
        this.ownerDeptId = ownerDeptId;
    }

    public String getOwnerDeptName() {
		return ownerDeptName;
	}

	public void setOwnerDeptName(String ownerDeptName) {
		this.ownerDeptName = ownerDeptName;
	}

	/**
     * 负责人ID
     * @return owner_user_id 负责人ID
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人ID
     * @param ownerUserId 负责人ID
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getOwnerUserName() {
		return ownerUserName;
	}

	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getFollowTime() {
		return followTime;
	}

	public void setFollowTime(Date followTime) {
		this.followTime = followTime;
	}

	public Date getFirstFollowTime() {
		return firstFollowTime;
	}

	public void setFirstFollowTime(Date firstFollowTime) {
		this.firstFollowTime = firstFollowTime;
	}

	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}

	public Date getExchangeTime() {
		return exchangeTime;
	}

	public void setExchangeTime(Date exchangeTime) {
		this.exchangeTime = exchangeTime;
	}

	public BigDecimal getExchangeMoney() {
		return exchangeMoney;
	}

	public void setExchangeMoney(BigDecimal exchangeMoney) {
		this.exchangeMoney = exchangeMoney;
	}

	/**
     * 自定义字段1
     * @return def1 自定义字段1
     */
    public String getDef1() {
        return def1;
    }

    /**
     * 自定义字段1
     * @param def1 自定义字段1
     */
    public void setDef1(String def1) {
        this.def1 = def1;
    }

    /**
     * 自定义字段2
     * @return def2 自定义字段2
     */
    public String getDef2() {
        return def2;
    }

    /**
     * 自定义字段2
     * @param def2 自定义字段2
     */
    public void setDef2(String def2) {
        this.def2 = def2;
    }

    /**
     * 自定义字段3
     * @return def3 自定义字段3
     */
    public String getDef3() {
        return def3;
    }

    /**
     * 自定义字段3
     * @param def3 自定义字段3
     */
    public void setDef3(String def3) {
        this.def3 = def3;
    }

    /**
     * 自定义字段4
     * @return def4 自定义字段4
     */
    public String getDef4() {
        return def4;
    }

    /**
     * 自定义字段4
     * @param def4 自定义字段4
     */
    public void setDef4(String def4) {
        this.def4 = def4;
    }

    /**
     * 自定义字段5
     * @return def5 自定义字段5
     */
    public String getDef5() {
        return def5;
    }

    /**
     * 自定义字段5
     * @param def5 自定义字段5
     */
    public void setDef5(String def5) {
        this.def5 = def5;
    }

    /**
     * 自定义字段6
     * @return def6 自定义字段6
     */
    public String getDef6() {
        return def6;
    }

    /**
     * 自定义字段6
     * @param def6 自定义字段6
     */
    public void setDef6(String def6) {
        this.def6 = def6;
    }

    /**
     * 自定义字段7
     * @return def7 自定义字段7
     */
    public String getDef7() {
        return def7;
    }

    /**
     * 自定义字段7
     * @param def7 自定义字段7
     */
    public void setDef7(String def7) {
        this.def7 = def7;
    }

    /**
     * 自定义字段8
     * @return def8 自定义字段8
     */
    public String getDef8() {
        return def8;
    }

    /**
     * 自定义字段8
     * @param def8 自定义字段8
     */
    public void setDef8(String def8) {
        this.def8 = def8;
    }

    /**
     * 自定义字段9
     * @return def9 自定义字段9
     */
    public String getDef9() {
        return def9;
    }

    /**
     * 自定义字段9
     * @param def9 自定义字段9
     */
    public void setDef9(String def9) {
        this.def9 = def9;
    }

    /**
     * 自定义字段10
     * @return def10 自定义字段10
     */
    public String getDef10() {
        return def10;
    }

    /**
     * 自定义字段10
     * @param def10 自定义字段10
     */
    public void setDef10(String def10) {
        this.def10 = def10;
    }

    /**
     * 自定义字段11
     * @return def11 自定义字段11
     */
    public String getDef11() {
        return def11;
    }

    /**
     * 自定义字段11
     * @param def11 自定义字段11
     */
    public void setDef11(String def11) {
        this.def11 = def11;
    }

    /**
     * 自定义字段12
     * @return def12 自定义字段12
     */
    public String getDef12() {
        return def12;
    }

    /**
     * 自定义字段12
     * @param def12 自定义字段12
     */
    public void setDef12(String def12) {
        this.def12 = def12;
    }

    /**
     * 自定义字段13
     * @return def13 自定义字段13
     */
    public String getDef13() {
        return def13;
    }

    /**
     * 自定义字段13
     * @param def13 自定义字段13
     */
    public void setDef13(String def13) {
        this.def13 = def13;
    }

    /**
     * 自定义字段14
     * @return def14 自定义字段14
     */
    public String getDef14() {
        return def14;
    }

    /**
     * 自定义字段14
     * @param def14 自定义字段14
     */
    public void setDef14(String def14) {
        this.def14 = def14;
    }

    /**
     * 自定义字段15
     * @return def15 自定义字段15
     */
    public String getDef15() {
        return def15;
    }

    /**
     * 自定义字段15
     * @param def15 自定义字段15
     */
    public void setDef15(String def15) {
        this.def15 = def15;
    }

    /**
     * 自定义字段16
     * @return def16 自定义字段16
     */
    public String getDef16() {
        return def16;
    }

    /**
     * 自定义字段16
     * @param def16 自定义字段16
     */
    public void setDef16(String def16) {
        this.def16 = def16;
    }

    /**
     * 自定义字段17
     * @return def17 自定义字段17
     */
    public String getDef17() {
        return def17;
    }

    /**
     * 自定义字段17
     * @param def17 自定义字段17
     */
    public void setDef17(String def17) {
        this.def17 = def17;
    }

    /**
     * 自定义字段18
     * @return def18 自定义字段18
     */
    public String getDef18() {
        return def18;
    }

    /**
     * 自定义字段18
     * @param def18 自定义字段18
     */
    public void setDef18(String def18) {
        this.def18 = def18;
    }

    /**
     * 自定义字段19
     * @return def19 自定义字段19
     */
    public String getDef19() {
        return def19;
    }

    /**
     * 自定义字段19
     * @param def19 自定义字段19
     */
    public void setDef19(String def19) {
        this.def19 = def19;
    }

    /**
     * 自定义字段20
     * @return def20 自定义字段20
     */
    public String getDef20() {
        return def20;
    }

    /**
     * 自定义字段20
     * @param def20 自定义字段20
     */
    public void setDef20(String def20) {
        this.def20 = def20;
    }

    /**
     * 自定义字段21
     * @return def21 自定义字段21
     */
    public String getDef21() {
        return def21;
    }

    /**
     * 自定义字段21
     * @param def21 自定义字段21
     */
    public void setDef21(String def21) {
        this.def21 = def21;
    }

    /**
     * 自定义字段22
     * @return def22 自定义字段22
     */
    public String getDef22() {
        return def22;
    }

    /**
     * 自定义字段22
     * @param def22 自定义字段22
     */
    public void setDef22(String def22) {
        this.def22 = def22;
    }

    /**
     * 自定义字段23
     * @return def23 自定义字段23
     */
    public String getDef23() {
        return def23;
    }

    /**
     * 自定义字段23
     * @param def23 自定义字段23
     */
    public void setDef23(String def23) {
        this.def23 = def23;
    }

    /**
     * 自定义字段24
     * @return def24 自定义字段24
     */
    public String getDef24() {
        return def24;
    }

    /**
     * 自定义字段24
     * @param def24 自定义字段24
     */
    public void setDef24(String def24) {
        this.def24 = def24;
    }

    /**
     * 自定义字段25
     * @return def25 自定义字段25
     */
    public String getDef25() {
        return def25;
    }

    /**
     * 自定义字段25
     * @param def25 自定义字段25
     */
    public void setDef25(String def25) {
        this.def25 = def25;
    }

    /**
     * 自定义字段26
     * @return def26 自定义字段26
     */
    public String getDef26() {
        return def26;
    }

    /**
     * 自定义字段26
     * @param def26 自定义字段26
     */
    public void setDef26(String def26) {
        this.def26 = def26;
    }

    /**
     * 自定义字段27
     * @return def27 自定义字段27
     */
    public String getDef27() {
        return def27;
    }

    /**
     * 自定义字段27
     * @param def27 自定义字段27
     */
    public void setDef27(String def27) {
        this.def27 = def27;
    }

    /**
     * 自定义字段28
     * @return def28 自定义字段28
     */
    public String getDef28() {
        return def28;
    }

    /**
     * 自定义字段28
     * @param def28 自定义字段28
     */
    public void setDef28(String def28) {
        this.def28 = def28;
    }

    /**
     * 自定义字段29
     * @return def29 自定义字段29
     */
    public String getDef29() {
        return def29;
    }

    /**
     * 自定义字段29
     * @param def29 自定义字段29
     */
    public void setDef29(String def29) {
        this.def29 = def29;
    }

    /**
     * 自定义字段30
     * @return def30 自定义字段30
     */
    public String getDef30() {
        return def30;
    }

    /**
     * 自定义字段30
     * @param def30 自定义字段30
     */
    public void setDef30(String def30) {
        this.def30 = def30;
    }
    
    public String[] getRelUsersID() {
		return relUsersID;
	}

	public void setRelUsersID(String[] relUsersID) {
		this.relUsersID = relUsersID;
	}

	public String[] getRelUsersName() {
		return relUsersName;
	}

	public void setRelUsersName(String[] relUsersName) {
		this.relUsersName = relUsersName;
	}

	/**
     * 自定义字段30
     * @return def30 自定义字段30
     */
    public String[] getRelUserId() {
        return relUsersID;
    }

    /**
     * 自定义字段30
     * @param def30 自定义字段30
     */
    public void setRelUserId(String[] relUsersID) {
        this.relUsersID = relUsersID;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getEnableState() {
		return enableState;
	}

	public void setEnableState(Integer enableState) {
		this.enableState = enableState;
	}

	public Long getEnableUserId() {
		return enableUserId;
	}

	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}

	public Date getEnableTime() {
		return enableTime;
	}

	public void setEnableTime(Date enableTime) {
		this.enableTime = enableTime;
	}

	public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}
	
}