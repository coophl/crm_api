package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.util.Date;

public class CustomerOwnersVO implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 公司ID
     */
    private Long orgId;

    /**
     * 客户ID
     */
    private Long cumId;
    
    /**
     * 客户名称
     */
    private Long cumName;

    /**
     * 原负责人部门ID
     */
    private Long oldDeptId;
    
    /**
     * 原负责人部门名称
     */
    private Long oldDeptName;

    /**
     * 原负责人ID
     */
    private Long oldOwnerId;
    
    /**
     * 原负责人名称
     */
    private Long oldOwnerName;

    /**
     * 新负责人部门ID
     */
    private Long newDeptId;
    
    /**
     * 新负责人部门名称
     */
    private Long newDeptName;

    /**
     * 新负责人ID
     */
    private Long newOwnerId;
    
    /**
     * 新负责人Name
     */
    private Long newOwnerName;
    
    /**
     * 创建人
     */
    private Long createdUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * cum_customer_owner
     */
    private static final long serialVersionUID = 1L;

    /**
     * ID
     * @return id ID
     */
    public Long getId() {
        return id;
    }

    /**
     * ID
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 公司ID
     * @return org_id 公司ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司ID
     * @param orgId 公司ID
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 客户ID
     * @return cum_id 客户ID
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户ID
     * @param cumId 客户ID
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    public Long getCumName() {
		return cumName;
	}

	public void setCumName(Long cumName) {
		this.cumName = cumName;
	}

	/**
     * 原负责人部门ID
     * @return old_dept_id 原负责人部门ID
     */
    public Long getOldDeptId() {
        return oldDeptId;
    }

    /**
     * 原负责人部门ID
     * @param oldDeptId 原负责人部门ID
     */
    public void setOldDeptId(Long oldDeptId) {
        this.oldDeptId = oldDeptId;
    }

    public Long getOldDeptName() {
		return oldDeptName;
	}

	public void setOldDeptName(Long oldDeptName) {
		this.oldDeptName = oldDeptName;
	}

	/**
     * 原负责人ID
     * @return old_owner_id 原负责人ID
     */
    public Long getOldOwnerId() {
        return oldOwnerId;
    }

    /**
     * 原负责人ID
     * @param oldOwnerId 原负责人ID
     */
    public void setOldOwnerId(Long oldOwnerId) {
        this.oldOwnerId = oldOwnerId;
    }

    public Long getOldOwnerName() {
		return oldOwnerName;
	}

	public void setOldOwnerName(Long oldOwnerName) {
		this.oldOwnerName = oldOwnerName;
	}

	/**
     * 新负责人部门ID
     * @return new_dept_id 新负责人部门ID
     */
    public Long getNewDeptId() {
        return newDeptId;
    }

    /**
     * 新负责人部门ID
     * @param newDeptId 新负责人部门ID
     */
    public void setNewDeptId(Long newDeptId) {
        this.newDeptId = newDeptId;
    }

    public Long getNewDeptName() {
		return newDeptName;
	}

	public void setNewDeptName(Long newDeptName) {
		this.newDeptName = newDeptName;
	}

	/**
     * 新负责人ID
     * @return new_owner_id 新负责人ID
     */
    public Long getNewOwnerId() {
        return newOwnerId;
    }

    /**
     * 新负责人ID
     * @param newOwnerId 新负责人ID
     */
    public void setNewOwnerId(Long newOwnerId) {
        this.newOwnerId = newOwnerId;
    }

    public Long getNewOwnerName() {
		return newOwnerName;
	}

	public void setNewOwnerName(Long newOwnerName) {
		this.newOwnerName = newOwnerName;
	}

	/**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

}