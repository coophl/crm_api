package com.yonyou.crm.cum.customer.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerRelusersVO;

public interface ICustomerRelusersRmService {
	
	public Page<CustomerRelusersVO> getList(Page<CustomerRelusersVO> page,Map<String, Object> paramMap);
	
	public CustomerRelusersVO insert(CustomerRelusersVO vo);
	
	public int delete(Long id);
	
}