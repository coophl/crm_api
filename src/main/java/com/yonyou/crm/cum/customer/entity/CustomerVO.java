package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CustomerVO implements Serializable {

    /**
     * cum_customer
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 主键
     */
    private Long id;
    
    /**
     * 业务类型
     */
    private Long biztypeId;
    
    /**
     * 业务类型名称
     */
    private String biztypeName;

    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 创建人id
     */
    private Long createdUserId;

    /**
     * 创建人名称
     */
    private String createdUserName;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 修改人
     */
    private Long modifiedUserId;

    /**
     * 修改时间
     */
    private Date modifiedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 是否删除
     */
    private Integer isDeleted;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 时间戳
     */
    private Date ts;
    
    /**
     * 所属集团
     */
    private Long groupId;

    /**
     * 公司id
     */
    private Long orgId;

    /**
     * 客户编码
     */
    private String code;

    /**
     * 客户名称
     */
    private String name;

    /**
     * 客户全称
     */
    private String fullname;

    /**
     * 助记码
     */
    private String memCode;

    /**
     * 客户图标
     */
    private String pic;

    /**
     * 上级客户id
     */
    private Long parentId;
    
    /**
     * 上级客户名称
     */
    private String parentName;
    
    /**
     * 营业额(万元)
     */
    private BigDecimal turnover;

    /**
     * 购买潜力
     */
    private BigDecimal buyPotential;

    /**
     * 员工人数
     */
    private Long employeeNum;
    
    /**
     * 主营业务
     */
    private String mainBiz;

    /**
     * 网址
     */
    private String website;

    /**
     * 微博
     */
    private String blog;

    /**
     * 股票代码
     */
    private String stockCode;
    
    /**
     * 备注
     */
    private String remark;

    /**
     * 客户类型
     */
    private Integer type;
    
    /**
     * 客户类型名称
     */
    private String typeName;
    
    /**
     * 行业
     */
    private Integer industry;
    
    /**
     * 行业名称
     */
    private String industryName;
    
    /**
     * 渠道类型
     */
    private Integer cannelType;

    /**
     * 渠道类型名称
     */
    private String cannelTypeName;
    
    /**
     * 客户来源
     */
    private Integer source;

    /**
     * 客户来源名称
     */
    private String sourceName;
    
	/**
	 * 线索id（唯一标识）
	 */
	private Integer leadId;

	/**
     * 线索名称
     */
    private String leadName;
    
    /**
     * 客户等级
     */
    private Integer level;

    /**
     * 客户等级名称
     */
    private String levelName;
    
    /**
     * 客户状态
     */
    private Integer state;
    
    /**
     * 客户状态名称
     */
    private String stateName;

    /**
     * 营销区域
     */
    private Integer saleArea;

    /**
     * 活跃度
     */
    private Integer liveness;

    /**
     * 贡献度
     */
    private Integer contribution;

    /**
     * 忠诚度
     */
    private Integer loyalty;

    /**
     * 潜力度
     */
    private Integer potential;

    /**
     * 总机
     */
    private String switchboard;

    /**
     * 电话
     */
    private String tel;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 传真
     */
    private String fax;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 国家
     */
    private Integer country;

    /**
     * 省
     */
    private Integer province;
    
    private String provinceName;

    /**
     * 市
     */
    private Integer city;
    
    private String cityName;

    /**
     * 区
     */
    private Integer district;
    
    private String districtName;

    /**
     * 详细地址
     */
    private String street;
    
    /**
     * 地址
     */
    private String address;

    /**
     * 邮编
     */
    private String zipCode;

    /**
     * GPS信息(经纬度逗号分隔)
     */
    private String latlng;
    
    /**
     * 经度
     */
    private String longitude;
    
    /**
     * 纬度
     */
    private String latitude;
    
    /**
     * 高度
     */
    private String altitude;
    

    /**
     * 客户身份(法定代表人1、自然人2,默认为法定代表人)
     */
    private Integer identityType;

    /**
     * 企业核实标识，已认证1，未认证0，默认0
     */
    private Integer isIdentified;
    
    /**
     * 企业核实
     */
    private String verifyId;
    /**
     * 企业核实
     */
    private String verifyFullname;

    /**
     * 注册资金币种
     */
    private Integer regCapitaltype;

    /**
     * 注册资金
     */
    private String regCapital;

    /**
     * 法定代表人
     */
    private String legalRepresent;

    /**
     * 注册电话
     */
    private String regTel;

    /**
     * 纳税人识别号
     */
    private String taxpayerNo;

    /**
     * 工商注册号
     */
    private String bizRegno;

    /**
     * 组织机构代码
     */
    private String orgCode;

    /**
     * 统一信用代码
     */
    private String creditCode;

    /**
     * 企业类型
     */
    private Integer companyType;

    /**
     * 营业期限
     */
    private String businessTerm;

    /**
     * 登记机关
     */
    private String regOrg;

    /**
     * 注册地址
     */
    private String regAddr;

    /**
     * 经营范围
     */
    private String bizScope;
    
    /**
     * 税务登记证(图片)
     */
    private String taxCertificate;

    /**
     * 工商营业执照(图片)
     */
    private String bizLicense;

    /**
     * 组织机构代码证(图片)
     */
    private String orgCertificate;

    /**
     * 证件类型
     */
    private Integer docType;

    /**
     * 证件号
     */
    private String docNum;

    /**
     * 出生年
     */
    private Integer birthYear;

    /**
     * 出生月
     */
    private Integer birthMonth;

    /**
     * 出生日
     */
    private Integer birthDay;

    /**
     * 性别(未选择0， 男1 ，女2)
     */
    private Integer gender;

    /**
     * 证件正面(图片)
     */
    private String docFront;

    /**
     * 证件反面(图片)
     */
    private String docReverse;

    /**
     * 启用标识(1启用，2停用)
     */
    private Integer enableState;

    /**
     * 启用标识名称(1启用，2停用)
     */
    private String enableStateName;

    /**
     * 集团客户标识
     */
    private Integer isGroup;
    
    /**
     * 集团客户标识名称
     */
    private String isGroupName;
    
    /**
     * 销售信息(包含负责人信息等)
     */
    private List<CustomerSalesVO> salesVOs;
    
    /**
     * 首次跟进时间
     */
    private Date firstFollowTime;
    
    /**
     * 最近跟进时间
     */
    private Date followTime;
    
	/**
     * 负责人id
     */
    private Long ownerUserId;
    
    /**
     * 公司名称（逗号分隔）
     */
    private String orgNames;

    public Date getFirstFollowTime() {
		return firstFollowTime;
	}

	public void setFirstFollowTime(Date firstFollowTime) {
		this.firstFollowTime = firstFollowTime;
	}

	public Date getFollowTime() {
		return followTime;
	}

	public void setFollowTime(Date followTime) {
		this.followTime = followTime;
	}

	public Long getOwnerUserId() {
		return ownerUserId;
	}

	public void setOwnerUserId(Long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

    /**
     * 自定义项
     */
    private Long int0;

    /**
     * 自定义项
     */
    private Long int1;

    /**
     * 自定义项
     */
    private Long int2;

    /**
     * 自定义项
     */
    private Long int3;

    /**
     * 自定义项
     */
    private Long int4;

    /**
     * 自定义项
     */
    private Long int5;

    /**
     * 自定义项
     */
    private Long int6;

    /**
     * 自定义项
     */
    private Long int7;

    /**
     * 自定义项
     */
    private Long int8;

    /**
     * 自定义项
     */
    private Long int9;

    /**
     * 自定义项
     */
    private BigDecimal decimal0;

    /**
     * 自定义项
     */
    private BigDecimal decimal1;

    /**
     * 自定义项
     */
    private BigDecimal decimal2;

    /**
     * 自定义项
     */
    private BigDecimal decimal3;

    /**
     * 自定义项
     */
    private BigDecimal decimal4;

    /**
     * 自定义项
     */
    private BigDecimal decimal5;

    /**
     * 自定义项
     */
    private BigDecimal decimal6;

    /**
     * 自定义项
     */
    private BigDecimal decimal7;

    /**
     * 自定义项
     */
    private BigDecimal decimal8;

    /**
     * 自定义项
     */
    private BigDecimal decimal9;

    /**
     * 自定义项
     */
    private String char0;

    /**
     * 自定义项
     */
    private String char1;

    /**
     * 自定义项
     */
    private String char2;

    /**
     * 自定义项
     */
    private String char3;

    /**
     * 自定义项
     */
    private String char4;

    /**
     * 自定义项
     */
    private String char5;

    /**
     * 自定义项
     */
    private String char6;

    /**
     * 自定义项
     */
    private String char7;

    /**
     * 自定义项
     */
    private String char8;

    /**
     * 自定义项
     */
    private String char9;

    /**
     * 自定义项
     */
    private Date datetime0;

    /**
     * 自定义项
     */
    private Date datetime1;

    /**
     * 自定义项
     */
    private Date datetime2;

    /**
     * 自定义项
     */
    private Date datetime3;

    /**
     * 自定义项
     */
    private Date datetime4;

    /**
     * 自定义项
     */
    private Date datetime5;

    /**
     * 自定义项
     */
    private Date datetime6;

    /**
     * 自定义项
     */
    private Date datetime7;

    /**
     * 自定义项
     */
    private Date datetime8;

    /**
     * 自定义项
     */
    private Date datetime9;

    /**
     * 自定义项
     */
    private String text0;

    /**
     * 自定义项
     */
    private String text1;

    /**
     * 自定义项
     */
    private String text2;

    /**
     * 自定义项
     */
    private String text3;

    /**
     * 自定义项
     */
    private String text4;

    /**
     * 自定义项
     */
    private String text5;

    /**
     * 自定义项
     */
    private String text6;

    /**
     * 自定义项
     */
    private String text7;

    /**
     * 自定义项
     */
    private String text8;
    
    /**
     * 自定义项
     */
    private String text9;

    public Long getInt0() {
		return int0;
	}

	public void setInt0(Long int0) {
		this.int0 = int0;
	}

	public Long getInt1() {
		return int1;
	}

	public void setInt1(Long int1) {
		this.int1 = int1;
	}

	public Long getInt2() {
		return int2;
	}

	public void setInt2(Long int2) {
		this.int2 = int2;
	}

	public Long getInt3() {
		return int3;
	}

	public void setInt3(Long int3) {
		this.int3 = int3;
	}

	public Long getInt4() {
		return int4;
	}

	public void setInt4(Long int4) {
		this.int4 = int4;
	}

	public Long getInt5() {
		return int5;
	}

	public void setInt5(Long int5) {
		this.int5 = int5;
	}

	public Long getInt6() {
		return int6;
	}

	public void setInt6(Long int6) {
		this.int6 = int6;
	}

	public Long getInt7() {
		return int7;
	}

	public void setInt7(Long int7) {
		this.int7 = int7;
	}

	public Long getInt8() {
		return int8;
	}

	public void setInt8(Long int8) {
		this.int8 = int8;
	}

	public Long getInt9() {
		return int9;
	}

	public void setInt9(Long int9) {
		this.int9 = int9;
	}

	public BigDecimal getDecimal0() {
		return decimal0;
	}

	public void setDecimal0(BigDecimal decimal0) {
		this.decimal0 = decimal0;
	}

	public BigDecimal getDecimal1() {
		return decimal1;
	}

	public void setDecimal1(BigDecimal decimal1) {
		this.decimal1 = decimal1;
	}

	public BigDecimal getDecimal2() {
		return decimal2;
	}

	public void setDecimal2(BigDecimal decimal2) {
		this.decimal2 = decimal2;
	}

	public BigDecimal getDecimal3() {
		return decimal3;
	}

	public void setDecimal3(BigDecimal decimal3) {
		this.decimal3 = decimal3;
	}

	public BigDecimal getDecimal4() {
		return decimal4;
	}

	public void setDecimal4(BigDecimal decimal4) {
		this.decimal4 = decimal4;
	}

	public BigDecimal getDecimal5() {
		return decimal5;
	}

	public void setDecimal5(BigDecimal decimal5) {
		this.decimal5 = decimal5;
	}

	public BigDecimal getDecimal6() {
		return decimal6;
	}

	public void setDecimal6(BigDecimal decimal6) {
		this.decimal6 = decimal6;
	}

	public BigDecimal getDecimal7() {
		return decimal7;
	}

	public void setDecimal7(BigDecimal decimal7) {
		this.decimal7 = decimal7;
	}

	public BigDecimal getDecimal8() {
		return decimal8;
	}

	public void setDecimal8(BigDecimal decimal8) {
		this.decimal8 = decimal8;
	}

	public BigDecimal getDecimal9() {
		return decimal9;
	}

	public void setDecimal9(BigDecimal decimal9) {
		this.decimal9 = decimal9;
	}

	public String getChar0() {
		return char0;
	}

	public void setChar0(String char0) {
		this.char0 = char0;
	}

	public String getChar1() {
		return char1;
	}

	public void setChar1(String char1) {
		this.char1 = char1;
	}

	public String getChar2() {
		return char2;
	}

	public void setChar2(String char2) {
		this.char2 = char2;
	}

	public String getChar3() {
		return char3;
	}

	public void setChar3(String char3) {
		this.char3 = char3;
	}

	public String getChar4() {
		return char4;
	}

	public void setChar4(String char4) {
		this.char4 = char4;
	}

	public String getChar5() {
		return char5;
	}

	public void setChar5(String char5) {
		this.char5 = char5;
	}

	public String getChar6() {
		return char6;
	}

	public void setChar6(String char6) {
		this.char6 = char6;
	}

	public String getChar7() {
		return char7;
	}

	public void setChar7(String char7) {
		this.char7 = char7;
	}

	public String getChar8() {
		return char8;
	}

	public void setChar8(String char8) {
		this.char8 = char8;
	}

	public String getChar9() {
		return char9;
	}

	public void setChar9(String char9) {
		this.char9 = char9;
	}

	public Date getDatetime0() {
		return datetime0;
	}

	public void setDatetime0(Date datetime0) {
		this.datetime0 = datetime0;
	}

	public Date getDatetime1() {
		return datetime1;
	}

	public void setDatetime1(Date datetime1) {
		this.datetime1 = datetime1;
	}

	public Date getDatetime2() {
		return datetime2;
	}

	public void setDatetime2(Date datetime2) {
		this.datetime2 = datetime2;
	}

	public Date getDatetime3() {
		return datetime3;
	}

	public void setDatetime3(Date datetime3) {
		this.datetime3 = datetime3;
	}

	public Date getDatetime4() {
		return datetime4;
	}

	public void setDatetime4(Date datetime4) {
		this.datetime4 = datetime4;
	}

	public Date getDatetime5() {
		return datetime5;
	}

	public void setDatetime5(Date datetime5) {
		this.datetime5 = datetime5;
	}

	public Date getDatetime6() {
		return datetime6;
	}

	public void setDatetime6(Date datetime6) {
		this.datetime6 = datetime6;
	}

	public Date getDatetime7() {
		return datetime7;
	}

	public void setDatetime7(Date datetime7) {
		this.datetime7 = datetime7;
	}

	public Date getDatetime8() {
		return datetime8;
	}

	public void setDatetime8(Date datetime8) {
		this.datetime8 = datetime8;
	}

	public Date getDatetime9() {
		return datetime9;
	}

	public void setDatetime9(Date datetime9) {
		this.datetime9 = datetime9;
	}

	public String getText0() {
		return text0;
	}

	public void setText0(String text0) {
		this.text0 = text0;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public String getText3() {
		return text3;
	}

	public void setText3(String text3) {
		this.text3 = text3;
	}

	public String getText4() {
		return text4;
	}

	public void setText4(String text4) {
		this.text4 = text4;
	}

	public String getText5() {
		return text5;
	}

	public void setText5(String text5) {
		this.text5 = text5;
	}

	public String getText6() {
		return text6;
	}

	public void setText6(String text6) {
		this.text6 = text6;
	}

	public String getText7() {
		return text7;
	}

	public void setText7(String text7) {
		this.text7 = text7;
	}

	public String getText8() {
		return text8;
	}

	public void setText8(String text8) {
		this.text8 = text8;
	}

	public String getText9() {
		return text9;
	}

	public void setText9(String text9) {
		this.text9 = text9;
	}
    
    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Long getBiztypeId() {
		return biztypeId;
	}

	public void setBiztypeId(Long biztypeId) {
		this.biztypeId = biztypeId;
	}

	/**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 创建人
     * @return created_user_id 创建人
     */
    public Long getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 创建人
     * @param createdUserId 创建人
     */
    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getCreatedUserName() {
		return createdUserName;
	}

	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}

	/**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 修改人
     * @return modified_user_id 修改人
     */
    public Long getModifiedUserId() {
        return modifiedUserId;
    }

    /**
     * 修改人
     * @param modifiedUserId 修改人
     */
    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /**
     * 修改时间
     * @return modified_time 修改时间
     */
    public Date getModifiedTime() {
        return modifiedTime;
    }

    /**
     * 修改时间
     * @param modifiedTime 修改时间
     */
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 是否删除
     * @return is_deleted 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 启用人
     * @return enable_user_id 启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 启用人
     * @param enableUserId 启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 启用时间
     * @return enable_time 启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 启用时间
     * @param enableTime 启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 所属集团
	 * @return groupId 所属集团
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * 所属集团
	 * @param groupId 所属集团
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 客户编码
     * @return code 客户编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 客户编码
     * @param code 客户编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 客户名称
     * @return name 客户名称
     */
    public String getName() {
        return name;
    }

    /**
     * 客户名称
     * @param name 客户名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 客户全称
     * @return fullname 客户全称
     */
    public String getFullname() {
        return fullname;
    }

    /**
     * 客户全称
     * @param fullname 客户全称
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     * 助记码
     * @return mem_code 助记码
     */
    public String getMemCode() {
        return memCode;
    }

    /**
     * 助记码
     * @param memCode 助记码
     */
    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    /**
     * 客户图标
     * @return pic 客户图标
     */
    public String getPic() {
        return pic;
    }

    /**
     * 客户图标
     * @param pic 客户图标
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 上级客户id
     * @return parent_id 上级客户id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 上级客户id
     * @param parentId 上级客户id
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 上级客户名称
	 * @return parentName 上级客户名称
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * 上级客户名称
	 * @param parentName 上级客户名称
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
     * 营业额(万元)
     * @return turnover 营业额(万元)
     */
    public BigDecimal getTurnover() {
        return turnover;
    }

    /**
     * 营业额(万元)
     * @param turnover 营业额(万元)
     */
    public void setTurnover(BigDecimal turnover) {
        this.turnover = turnover;
    }

    /**
     * 购买潜力
     * @return buy_potential 购买潜力
     */
    public BigDecimal getBuyPotential() {
        return buyPotential;
    }

    /**
     * 购买潜力
     * @param buyPotential 购买潜力
     */
    public void setBuyPotential(BigDecimal buyPotential) {
        this.buyPotential = buyPotential;
    }

    /**
     * 员工人数
     * @return employee_num 员工人数
     */
    public Long getEmployeeNum() {
        return employeeNum;
    }

    /**
     * 员工人数
     * @param employeeNum 员工人数
     */
    public void setEmployeeNum(Long employeeNum) {
        this.employeeNum = employeeNum;
    }

    /**
     * 网址
     * @return website 网址
     */
    public String getWebsite() {
        return website;
    }

    /**
     * 网址
     * @param website 网址
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * 微博
     * @return blog 微博
     */
    public String getBlog() {
        return blog;
    }

    /**
     * 微博
     * @param blog 微博
     */
    public void setBlog(String blog) {
        this.blog = blog;
    }

    /**
     * 股票代码
     * @return stock_code 股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 股票代码
     * @param stockCode 股票代码
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 客户类型
     * @return type 客户类型
     */
    public Integer getType() {
        return type;
    }

    /**
     * 客户类型
     * @param type 客户类型
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 客户类型名称
	 * @return typeName 客户类型名称
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * 客户类型名称
	 * @param typeName 客户类型名称
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
    /**
     * 行业
     * @return industry 行业
     */
    public Integer getIndustry() {
        return industry;
    }

    /**
     * 行业
     * @param industry 行业
     */
    public void setIndustry(Integer industry) {
        this.industry = industry;
    }
    
	/**
	 * 行业名称
	 * @return industryName 行业名称
	 */
	public String getIndustryName() {
		return industryName;
	}

	/**
	 * 行业名称
	 * @param industryName 行业名称
	 */
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	/**
     * 渠道类型
     * @return cannel_type 渠道类型
     */
    public Integer getCannelType() {
        return cannelType;
    }

    /**
     * 渠道类型
     * @param cannelType 渠道类型
     */
    public void setCannelType(Integer cannelType) {
        this.cannelType = cannelType;
    }

    /**
     * 渠道类型名称
	 * @return cannelTypeName 渠道类型名称
	 */
	public String getCannelTypeName() {
		return cannelTypeName;
	}

	/**
	 * 渠道类型名称
	 * @param cannelTypeName 渠道类型名称
	 */
	public void setCannelTypeName(String cannelTypeName) {
		this.cannelTypeName = cannelTypeName;
	}

	/**
	 * 客户来源
	 * @return source 客户来源
	 */
	public Integer getSource() {
		return source;
	}

	/**
	 * 客户来源
	 * @param source 客户来源
	 */
	public void setSource(Integer source) {
		this.source = source;
	}

	/**
	 * 客户来源名称
	 * @return sourceName 客户来源名称
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * 客户来源名称
	 * @param sourceName 客户来源名称
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
     * 客户等级
     * @return level 客户等级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 客户等级
     * @param level 客户等级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

	/**
	 * 客户等级名称
	 * @return levelName 客户等级名称
	 */
	public String getLevelName() {
		return levelName;
	}

	/**
	 * 客户等级名称
	 * @param levelName 客户等级名称
	 */
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	
    /**
     * 客户状态
	 * @return state 客户状态
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * 客户状态
	 * @param state 客户状态
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
     * 营销区域
     * @return sale_area 营销区域
     */
    public Integer getSaleArea() {
        return saleArea;
    }

    /**
     * 营销区域
     * @param saleArea 营销区域
     */
    public void setSaleArea(Integer saleArea) {
        this.saleArea = saleArea;
    }

	/**
     * 活跃度
     * @return liveness 活跃度
     */
    public Integer getLiveness() {
        return liveness;
    }

    /**
     * 活跃度
     * @param liveness 活跃度
     */
    public void setLiveness(Integer liveness) {
        this.liveness = liveness;
    }

    /**
     * 贡献度
     * @return contribution 贡献度
     */
    public Integer getContribution() {
        return contribution;
    }

    /**
     * 贡献度
     * @param contribution 贡献度
     */
    public void setContribution(Integer contribution) {
        this.contribution = contribution;
    }

    /**
     * 忠诚度
     * @return loyalty 忠诚度
     */
    public Integer getLoyalty() {
        return loyalty;
    }

    /**
     * 忠诚度
     * @param loyalty 忠诚度
     */
    public void setLoyalty(Integer loyalty) {
        this.loyalty = loyalty;
    }

    /**
     * 潜力度
     * @return potential 潜力度
     */
    public Integer getPotential() {
        return potential;
    }

    /**
     * 潜力度
     * @param potential 潜力度
     */
    public void setPotential(Integer potential) {
        this.potential = potential;
    }

    /**
     * 总机
     * @return switchboard 总机
     */
    public String getSwitchboard() {
        return switchboard;
    }

    /**
     * 总机
     * @param switchboard 总机
     */
    public void setSwitchboard(String switchboard) {
        this.switchboard = switchboard;
    }

    /**
     * 电话
     * @return tel 电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 电话
     * @param tel 电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 手机
     * @return mobile 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 传真
     * @return fax 传真
     */
    public String getFax() {
        return fax;
    }

    /**
     * 传真
     * @param fax 传真
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * 邮箱
     * @return email 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 国家
     * @return country 国家
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 国家
     * @param country 国家
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 省
     * @return province 省
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 省
     * @param province 省
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
     * 市
     * @return city 市
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 市
     * @param city 市
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
     * 区
     * @return district 区
     */
    public Integer getDistrict() {
        return district;
    }

    /**
     * 区
     * @param district 区
     */
    public void setDistrict(Integer district) {
        this.district = district;
    }

    public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
     * 详细地址
     * @return street 详细地址
     */
    public String getStreet() {
        return street;
    }

    /**
     * 街道
     * @param street 街道
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * 详细地址
	 * @return address 详细地址
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 详细地址
	 * @param address 详细地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
     * 邮编
     * @return zip_code 邮编
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * 邮编
     * @param zipCode 邮编
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * GPS信息(经纬度逗号分隔)
     * @return latlng GPS信息(经纬度逗号分隔)
     */
    public String getLatlng() {
        return latlng;
    }

    /**
     * GPS信息(经纬度逗号分隔)
     * @param latlng GPS信息(经纬度逗号分隔)
     */
    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAltitude() {
		return altitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	/**
     * 客户身份(法定代表人1、自然人2,默认为法定代表人)
     * @return identity_type 客户身份(法定代表人1、自然人2,默认为法定代表人)
     */
    public Integer getIdentityType() {
        return identityType;
    }

    /**
     * 客户身份(法定代表人1、自然人2,默认为法定代表人)
     * @param identityType 客户身份(法定代表人1、自然人2,默认为法定代表人)
     */
    public void setIdentityType(Integer identityType) {
        this.identityType = identityType;
    }

    /**
     * 身份认证标示，已认证1，未认证0，默认0
     * @return is_identified 身份认证标示，已认证1，未认证0，默认0
     */
    public Integer getIsIdentified() {
        return isIdentified;
    }

    /**
     * 身份认证标示，已认证1，未认证0，默认0
     * @param isIdentified 身份认证标示，已认证1，未认证0，默认0
     */
    public void setIsIdentified(Integer isIdentified) {
        this.isIdentified = isIdentified;
    }

    /**
     * 企业核实
	 * @return verifyId 企业核实
	 */
	public String getVerifyId() {
		return verifyId;
	}

	/**
	 * 企业核实
	 * @param verifyId 企业核实
	 */
	public void setVerifyId(String verifyId) {
		this.verifyId = verifyId;
	}
	
	/**
     * 企业核实名称
	 * @return verifyFullname 企业核实名称
	 */
	public String getVerifyFullname() {
		return verifyFullname;
	}

	/**
	 * 企业核实名称
	 * @param verifyId 企业核实名称
	 */
	public void setVerifyFullname(String verifyFullname) {
		this.verifyFullname = verifyFullname;
	}

	/**
     * 注册资金币种
     * @return reg_capitaltype 注册资金币种
     */
    public Integer getRegCapitaltype() {
        return regCapitaltype;
    }

    /**
     * 注册资金币种
     * @param regCapitaltype 注册资金币种
     */
    public void setRegCapitaltype(Integer regCapitaltype) {
        this.regCapitaltype = regCapitaltype;
    }

    /**
     * 注册资金
     * @return reg_capital 注册资金
     */
    public String getRegCapital() {
        return regCapital;
    }

    /**
     * 注册资金
     * @param regCapital 注册资金
     */
    public void setRegCapital(String regCapital) {
        this.regCapital = regCapital;
    }

    /**
     * 法定代表人
     * @return legal_represent 法定代表人
     */
    public String getLegalRepresent() {
        return legalRepresent;
    }

    /**
     * 法定代表人
     * @param legalRepresent 法定代表人
     */
    public void setLegalRepresent(String legalRepresent) {
        this.legalRepresent = legalRepresent;
    }

    /**
     * 注册电话
     * @return reg_tel 注册电话
     */
    public String getRegTel() {
        return regTel;
    }

    /**
     * 注册电话
     * @param regTel 注册电话
     */
    public void setRegTel(String regTel) {
        this.regTel = regTel;
    }

    /**
     * 纳税人识别号
     * @return taxpayer_no 纳税人识别号
     */
    public String getTaxpayerNo() {
        return taxpayerNo;
    }

    /**
     * 纳税人识别号
     * @param taxpayerNo 纳税人识别号
     */
    public void setTaxpayerNo(String taxpayerNo) {
        this.taxpayerNo = taxpayerNo;
    }

    /**
     * 工商注册号
     * @return biz_regno 工商注册号
     */
    public String getBizRegno() {
        return bizRegno;
    }

    /**
     * 工商注册号
     * @param bizRegno 工商注册号
     */
    public void setBizRegno(String bizRegno) {
        this.bizRegno = bizRegno;
    }

    /**
     * 组织机构代码
     * @return org_code 组织机构代码
     */
    public String getOrgCode() {
        return orgCode;
    }

    /**
     * 组织机构代码
     * @param orgCode 组织机构代码
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    /**
     * 统一信用代码
     * @return credit_code 统一信用代码
     */
    public String getCreditCode() {
        return creditCode;
    }

    /**
     * 统一信用代码
     * @param creditCode 统一信用代码
     */
    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    /**
     * 企业类型
     * @return company_type 企业类型
     */
    public Integer getCompanyType() {
        return companyType;
    }

    /**
     * 企业类型
     * @param companyType 企业类型
     */
    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    /**
     * 营业期限
     * @return business_term 营业期限
     */
    public String getBusinessTerm() {
        return businessTerm;
    }

    /**
     * 营业期限
     * @param businessTerm 营业期限
     */
    public void setBusinessTerm(String businessTerm) {
        this.businessTerm = businessTerm;
    }

    /**
     * 登记机关
     * @return reg_org 登记机关
     */
    public String getRegOrg() {
        return regOrg;
    }

    /**
     * 登记机关
     * @param regOrg 登记机关
     */
    public void setRegOrg(String regOrg) {
        this.regOrg = regOrg;
    }

    /**
     * 注册地址
     * @return reg_addr 注册地址
     */
    public String getRegAddr() {
        return regAddr;
    }

    /**
     * 注册地址
     * @param regAddr 注册地址
     */
    public void setRegAddr(String regAddr) {
        this.regAddr = regAddr;
    }

    /**
     * 税务登记证(图片)
     * @return tax_certificate 税务登记证(图片)
     */
    public String getTaxCertificate() {
        return taxCertificate;
    }

    /**
     * 税务登记证(图片)
     * @param taxCertificate 税务登记证(图片)
     */
    public void setTaxCertificate(String taxCertificate) {
        this.taxCertificate = taxCertificate;
    }

    /**
     * 工商营业执照(图片)
     * @return biz_license 工商营业执照(图片)
     */
    public String getBizLicense() {
        return bizLicense;
    }

    /**
     * 工商营业执照(图片)
     * @param bizLicense 工商营业执照(图片)
     */
    public void setBizLicense(String bizLicense) {
        this.bizLicense = bizLicense;
    }

    /**
     * 组织机构代码证(图片)
     * @return org_certificate 组织机构代码证(图片)
     */
    public String getOrgCertificate() {
        return orgCertificate;
    }

    /**
     * 组织机构代码证(图片)
     * @param orgCertificate 组织机构代码证(图片)
     */
    public void setOrgCertificate(String orgCertificate) {
        this.orgCertificate = orgCertificate;
    }

    /**
     * 证件类型
     * @return doc_type 证件类型
     */
    public Integer getDocType() {
        return docType;
    }

    /**
     * 证件类型
     * @param docType 证件类型
     */
    public void setDocType(Integer docType) {
        this.docType = docType;
    }

    /**
     * 证件号
     * @return doc_num 证件号
     */
    public String getDocNum() {
        return docNum;
    }

    /**
     * 证件号
     * @param docNum 证件号
     */
    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    /**
     * 出生年
     * @return birth_year 出生年
     */
    public Integer getBirthYear() {
        return birthYear;
    }

    /**
     * 出生年
     * @param birthYear 出生年
     */
    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * 出生月
     * @return birth_month 出生月
     */
    public Integer getBirthMonth() {
        return birthMonth;
    }

    /**
     * 出生月
     * @param birthMonth 出生月
     */
    public void setBirthMonth(Integer birthMonth) {
        this.birthMonth = birthMonth;
    }

    /**
     * 出生日
     * @return birth_day 出生日
     */
    public Integer getBirthDay() {
        return birthDay;
    }

    /**
     * 出生日
     * @param birthDay 出生日
     */
    public void setBirthDay(Integer birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * 性别(未选择0， 男1 ，女2)
     * @return gender 性别(未选择0， 男1 ，女2)
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别(未选择0， 男1 ，女2)
     * @param gender 性别(未选择0， 男1 ，女2)
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 证件正面(图片)
     * @return doc_front 证件正面(图片)
     */
    public String getDocFront() {
        return docFront;
    }

    /**
     * 证件正面(图片)
     * @param docFront 证件正面(图片)
     */
    public void setDocFront(String docFront) {
        this.docFront = docFront;
    }

    /**
     * 证件反面(图片)
     * @return doc_reverse 证件反面(图片)
     */
    public String getDocReverse() {
        return docReverse;
    }

    /**
     * 证件反面(图片)
     * @param docReverse 证件反面(图片)
     */
    public void setDocReverse(String docReverse) {
        this.docReverse = docReverse;
    }

    /**
     * 启用标识(1启用，2停用)
     * @return enable_state 启用标识(1启用，2停用)
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 启用标识名称(1启用，2停用)
     * @param enableState 启用标识(1启用，2停用)
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 集团客户标识(公司客户1，集团客户2)
     * @return is_group 集团客户标识(公司客户1，集团客户2)
     */
    public Integer getIsGroup() {
        return isGroup;
    }

    /**
     * 集团客户标识(公司客户1，集团客户2)
     * @param isGroup 集团客户标识(公司客户1，集团客户2)
     */
    public void setIsGroup(Integer isGroup) {
        this.isGroup = isGroup;
    }

    /**
     * 集团客户标识名称
	 * @return isGroupName 集团客户标识名称
	 */
	public String getIsGroupName() {
		return isGroupName;
	}

	/**
	 * 集团客户标识名称
	 * @param isGroupName 集团客户标识名称
	 */
	public void setIsGroupName(String isGroupName) {
		this.isGroupName = isGroupName;
	}

	/**
     * 主营业务
     * @return main_biz 主营业务
     */
    public String getMainBiz() {
        return mainBiz;
    }

    /**
     * 主营业务
     * @param mainBiz 主营业务
     */
    public void setMainBiz(String mainBiz) {
        this.mainBiz = mainBiz;
    }

    /**
     * 备注
     * @return remark 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 经营范围
     * @return biz_scope 经营范围
     */
    public String getBizScope() {
        return bizScope;
    }

    /**
     * 经营范围
     * @param bizScope 经营范围
     */
    public void setBizScope(String bizScope) {
        this.bizScope = bizScope;
    }

	public List<CustomerSalesVO> getSalesVOs() {
		return salesVOs;
	}

	public void setSalesVOs(List<CustomerSalesVO> salesVOs) {
		this.salesVOs = salesVOs;
	}

	/**
	 * 线索id
	 * @return leadId 线索id
	 */
	public Integer getLeadId() {
		return leadId;
	}

	/**
	 * 线索id
	 * @param leadId 线索id
	 */
	public void setLeadId(Integer leadId) {
		this.leadId = leadId;
	}

	/**
	 * 线索名称
	 * @return leadName 线索名称
	 */
	public String getLeadName() {
		return leadName;
	}

	/**
	 * 线索名称
	 * @param leadName 线索名称
	 */
	public void setLeadName(String leadName) {
		this.leadName = leadName;
	}

	public String getOrgNames() {
		return orgNames;
	}

	public void setOrgNames(String orgNames) {
		this.orgNames = orgNames;
	}

	public String getBiztypeName() {
		return biztypeName;
	}

	public void setBiztypeName(String biztypeName) {
		this.biztypeName = biztypeName;
	}
}