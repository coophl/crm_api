package com.yonyou.crm.cum.customer.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerFollowVO;
import com.yonyou.crm.cum.customer.entity.CustomerVO;
import com.yonyou.crm.sys.dynamic.entity.DynamicVO;

public interface ICustomerRmService {
	
	CustomerVO getDetail(Long id);
	
	Page<CustomerVO> getList(Page<CustomerVO> page, Map<String, Object> paramMap);
	
	Map<String, Object> getViewUserList(Long cumId, Map<String, Object> paramMap);
	
	Map<String, Object> getFollowUserList(Long cumId, Map<String, Object> paramMap);
	
	CustomerVO insert(CustomerVO customer);
	
	CustomerVO update(CustomerVO customer);
	
	int delete(Long id);
	
	Page<CustomerVO> batchDelete(String[] ids, Page<CustomerVO> page, Map<String, Object> paraMap);
	
	int updateState(Long id, int enableState);
	
	Page<CustomerVO> batchUpdateState(String[] ids, Integer enableState, Page<CustomerVO> page, Map<String, Object> paraMap);
	
	CustomerFollowVO saveFollow(Long cumId);
	
	int deleteFollow(Long cumId);
	
	int checkNameExist(CustomerVO customer);
	
	int isFollow(Long cumId);
	
	List<CustomerVO> getRefList(Map<String, Object> paraMap);

	Page<CustomerVO> getRefPage(Page<CustomerVO> page, Map<String, Object> paraMap);
	
	CustomerVO identify(CustomerVO customer);
	
	CustomerVO cancelIdentify(CustomerVO customer);
	
	boolean getIdentifyFlag(Map<String, Object> paraMap);
	
	void changeOwner(Map<String, Object> paraMap);
	
	boolean checkVerifyNameExist(String id, String verifyfullname);

	List<Map<String,Object>> getCustomerRelatedlist(String customerId, String type);

	List<Map<String,Object>> getCustomerMap(String deptid, List<String> personIDs);

    void delrelate(String id, String relateid , String type);
    
    public DynamicVO[] getCumDynamic(Long cumId);
   
    void checkVerifyIdExist(CustomerVO vo);
    
    Map<String, List<String>> getCustomerFuncList(Map<String, Object> param);

    void addrelate(String id, List<String> relateArry);
}
