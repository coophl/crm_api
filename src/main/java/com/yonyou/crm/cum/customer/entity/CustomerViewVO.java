package com.yonyou.crm.cum.customer.entity;

import java.io.Serializable;
import java.util.Date;

public class CustomerViewVO implements Serializable {

    /**
     * cum_customer_view
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 公司ID
     */
    private Long orgId;

    /**
     * 客户ID
     */
    private Long cumId;

    /**
     * 查看人部门ID
     */
    private Long deptId;

    /**
     * 查看人ID
     */
    private Long userId;

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 公司ID
     * @return org_id 公司ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 公司ID
     * @param orgId 公司ID
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 客户ID
     * @return cum_id 客户ID
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户ID
     * @param cumId 客户ID
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    /**
     * 查看人部门ID
     * @return dept_id 查看人部门ID
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 查看人部门ID
     * @param deptId 查看人部门ID
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 查看人ID
     * @return user_id 查看人ID
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 查看人ID
     * @param userId 查看人ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}