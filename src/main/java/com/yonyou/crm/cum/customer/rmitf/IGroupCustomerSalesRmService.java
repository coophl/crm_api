package com.yonyou.crm.cum.customer.rmitf;

import java.util.*;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.customer.entity.CustomerSalesVO;

public interface IGroupCustomerSalesRmService {
	
	public Page<CustomerSalesVO> getList(Page<CustomerSalesVO> page,Map<String, Object> paramMap);
	
	public CustomerSalesVO getDetail(Long id);
	
	public CustomerSalesVO insert(CustomerSalesVO customerSale);
	
	public CustomerSalesVO update(CustomerSalesVO customerSale);
	
	public int delete(Long id);
	
	public int deleteByIDs(Long[] ids);
	
	public void batchAllocation(String orgIdStr, String cumIdStr);
	
	public void batchResume(String orgIdStr, String cumIdStr);
	
	public List<CustomerSalesVO> getExistOrg(String cumIdStr);
	
}