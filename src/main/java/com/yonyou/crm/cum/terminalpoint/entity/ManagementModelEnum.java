package com.yonyou.crm.cum.terminalpoint.entity;

public enum ManagementModelEnum {
	OWNMANAGE(1,"自营"),DISTRIBUTORMANAGE(2,"经销商经营");
	
	//枚举项对应int值
	Integer value;
	//枚举项显示值
	String name;
	//构造方法
	private ManagementModelEnum(Integer value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
