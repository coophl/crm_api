package com.yonyou.crm.cum.terminalpoint.rmitf;

import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.terminalpoint.entity.CumChnlnodeVO;

public interface ICumChnlnodeRmService {
	
	public Page<CumChnlnodeVO> getPage(Page<CumChnlnodeVO> page, Map<String, Object> paramMap);
	
	public CumChnlnodeVO save(CumChnlnodeVO cumChnlnode);
	
	public Page<CumChnlnodeVO> deleteCumChnlnodeByIDs(String[] ids, Map<String, Object> paramMap);
	
	public CumChnlnodeVO update(CumChnlnodeVO cumChnlnode);
	
	public CumChnlnodeVO detail(Long id);
}
