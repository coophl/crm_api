package com.yonyou.crm.cum.terminalpoint.entity;

import java.io.Serializable;
import java.util.Date;

public class CumChnlnodeVO implements Serializable {
	
	/**
    * cum_chnlnode
    */
    private static final long serialVersionUID = 1L;
    
    /**
     * 主键
     */
    private Long id;

    /**
     * 网点编号
     */
    private String chnlnodeCode;

    /**
     * 网点名称
     */
    private String chnlnodeName;

    /**
     * 助记码
     */
    private String chnlnodeMnemonic;

    /**
     * 网点类型（引用自定义档案-网点类型）
     */
    private Long type;
    
    /**
     * 网点类型详情id
     */
    private Long typeDetail;
    
    /**
     * 网点类型详情名称
     */
    private String typeDetailName;
    
    /**
     * 经营模式（1代表自营，2代表经销商经营）
     */
    private Integer managementModel;
    
    /**
     * 经营模式名称（1代表自营，2代表经销商经营）
     */
    private String managementModelName;

    /**
     * 等级（引用自定义档案-网点等级）
     */
    private Long level;
    
    /**
     * 等级详情id
     */
    private Long levelDetail;
    
    /**
     * 等级详情名称
     */
    private String levelDetailName;
    
    /**
     * 联系人
     */
    private Long contactId;
    
    /**
     * 联系人名称
     */
    private String contactName;

    /**
     * 手机
     */
    private String phone;

    /**
     * 座机
     */
    private String landline;

    /**
     * 供货客户
     */
    private Long supplyCustomer;

    /**
     * 所属部门
     */
    private Long deptId;
    
    /**
     * 所属部门名称
     */
    private String deptName;

    /**
     * 所属公司
     */
    private Long orgId;
    
    /**
     * 所属公司名称
     */
    private String orgName;
    
    /**
     * 负责人
     */
    private Long ownerUserId;
    
    /**
     * 负责人名称
     */
    private String ownerUserName;
    
    /**
     * 拜访频次
     */
    private Integer visitFrequency;

    /**
     * 频率（1代表周，2代表双周，3代表月）
     */
    private Integer rate;
    
    /**
     * 频率名称（1代表周，2代表双周，3代表月）
     */
    private String rateName;

    /**
     * 行政区域-国家
     */
    private String administrationCountry;

    /**
     * 行政区域-省/州
     */
    private String administrationProvince;

    /**
     * 行政区域-城市
     */
    private String administrationCity;

    /**
     * 行政区域-区/县
     */
    private String administrationCounty;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 经度
     */
    private String gpsPosition;

    /**
     * 开店日期
     */
    private Date shopDate;

    /**
     * 照片
     */
    private String photo;

    /**
     * 附件
     */
    private String attachment;

    /**
     * 对应ERP
     */
    private String erpCode;

    /**
     * 停启用状态，1启用2停用
     */
    private Integer enableState;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 停启用人
     */
    private Long enableUserId;

    /**
     * 创建时间
     */
    private Date sysCreatedTime;

    /**
     * 创建人
     */
    private Long sysCreatedUserId;

    /**
     * 修改时间
     */
    private Date sysModifiedTime;

    /**
     * 修改人
     */
    private Long sysModifiedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 是否删除(0代表未删除，1代表删除）
     */
    private Integer isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 自定义项1
     */
    private String def1;

    /**
     * 自定义项2
     */
    private String def2;

    /**
     * 自定义项3
     */
    private String def3;

    /**
     * 自定义项4
     */
    private String def4;

    /**
     * 自定义项5
     */
    private String def5;

    /**
     * 自定义项6
     */
    private String def6;

    /**
     * 自定义项7
     */
    private String def7;

    /**
     * 自定义项8
     */
    private String def8;

    /**
     * 自定义项9
     */
    private String def9;

    /**
     * 自定义项10
     */
    private String def10;

    /**
     * 自定义项11
     */
    private String def11;

    /**
     * 自定义项12
     */
    private String def12;

    /**
     * 自定义项13
     */
    private String def13;

    /**
     * 自定义项14
     */
    private String def14;

    /**
     * 自定义项15
     */
    private String def15;

    /**
     * 自定义项16
     */
    private String def16;

    /**
     * 自定义项17
     */
    private String def17;

    /**
     * 自定义项18
     */
    private String def18;

    /**
     * 自定义项19
     */
    private String def19;

    /**
     * 自定义项20
     */
    private String def20;

    /**
     * 自定义项21
     */
    private String def21;

    /**
     * 自定义项22
     */
    private String def22;

    /**
     * 自定义项23
     */
    private String def23;

    /**
     * 自定义项24
     */
    private String def24;

    /**
     * 自定义项25
     */
    private String def25;

    /**
     * 自定义项26
     */
    private String def26;

    /**
     * 自定义项27
     */
    private String def27;

    /**
     * 自定义项28
     */
    private String def28;

    /**
     * 自定义项29
     */
    private String def29;

    /**
     * 自定义项30
     */
    private String def30;
    
    /**
     * 停启用字符
     */
    private String enableStateName;
    
    
    public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 网点编号
     * @return chnlnode_code 网点编号
     */
    public String getChnlnodeCode() {
        return chnlnodeCode;
    }

    /**
     * 网点编号
     * @param chnlnodeCode 网点编号
     */
    public void setChnlnodeCode(String chnlnodeCode) {
        this.chnlnodeCode = chnlnodeCode;
    }

    /**
     * 网点名称
     * @return chnlnode_name 网点名称
     */
    public String getChnlnodeName() {
        return chnlnodeName;
    }

    /**
     * 网点名称
     * @param chnlnodeName 网点名称
     */
    public void setChnlnodeName(String chnlnodeName) {
        this.chnlnodeName = chnlnodeName;
    }

    /**
     * 助记码
     * @return chnlnode_mnemonic 助记码
     */
    public String getChnlnodeMnemonic() {
        return chnlnodeMnemonic;
    }

    /**
     * 助记码
     * @param chnlnodeMnemonic 助记码
     */
    public void setChnlnodeMnemonic(String chnlnodeMnemonic) {
        this.chnlnodeMnemonic = chnlnodeMnemonic;
    }

    /**
     * 网点类型（引用自定义档案-网点类型）_
     * @return type 网点类型（引用自定义档案-网点类型）_
     */
    public Long getType() {
        return type;
    }

    /**
     * 网点类型（引用自定义档案-网点类型）_
     * @param type 网点类型（引用自定义档案-网点类型）_
     */
    public void setType(Long type) {
        this.type = type;
    }

    /**
     * 经营模式（1代表自营，2代表经销商经营）
     * @return management model 经营模式（1代表自营，2代表经销商经营）
     */
    public Integer getManagementModel() {
        return managementModel;
    }

    /**
     * 经营模式（1代表自营，2代表经销商经营）
     * @param managementModel 经营模式（1代表自营，2代表经销商经营）
     */
    public void setManagementModel(Integer managementModel) {
        this.managementModel = managementModel;
    }

    /**
     * 等级（引用自定义档案-网点等级）
     * @return level 等级（引用自定义档案-网点等级）
     */
    public Long getLevel() {
        return level;
    }

    /**
     * 等级（引用自定义档案-网点等级）
     * @param level 等级（引用自定义档案-网点等级）
     */
    public void setLevel(Long level) {
        this.level = level;
    }

    /**
     * 联系人
     * @return contact 联系人
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * 联系人
     * @param contact 联系人
     */
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    /**
     * 手机
     * @return phone 手机
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机
     * @param phone 手机
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 座机
     * @return landline 座机
     */
    public String getLandline() {
        return landline;
    }

    /**
     * 座机
     * @param landline 座机
     */
    public void setLandline(String landline) {
        this.landline = landline;
    }

    /**
     * 供货客户
     * @return supply_customer 供货客户
     */
    public Long getSupplyCustomer() {
        return supplyCustomer;
    }

    /**
     * 供货客户
     * @param supplyCustomer 供货客户
     */
    public void setSupplyCustomer(Long supplyCustomer) {
        this.supplyCustomer = supplyCustomer;
    }

    /**
     * 所属部门
     * @return dept_id 所属部门
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 所属部门
     * @param deptId 所属部门
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 所属公司
     * @return org_id 所属公司
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 所属公司
     * @param orgId 所属公司
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 负责人
     * @return charge_person 负责人
     */
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 负责人
     * @param chargePerson 负责人
     */
    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    /**
     * 拜访频次
     * @return visit_frequency 拜访频次
     */
    public Integer getVisitFrequency() {
        return visitFrequency;
    }

    /**
     * 拜访频次
     * @param visitFrequency 拜访频次
     */
    public void setVisitFrequency(Integer visitFrequency) {
        this.visitFrequency = visitFrequency;
    }

    /**
     * 频率（1代表周，2代表双周，3代表月）
     * @return rate 频率（1代表周，2代表双周，3代表月）
     */
    public Integer getRate() {
        return rate;
    }

    /**
     * 频率（1代表周，2代表双周，3代表月）
     * @param rate 频率（1代表周，2代表双周，3代表月）
     */
    public void setRate(Integer rate) {
        this.rate = rate;
    }

    /**
     * 行政区域-国家
     * @return administration_country 行政区域-国家
     */
    public String getAdministrationCountry() {
        return administrationCountry;
    }

    /**
     * 行政区域-国家
     * @param administrationCountry 行政区域-国家
     */
    public void setAdministrationCountry(String administrationCountry) {
        this.administrationCountry = administrationCountry;
    }

    /**
     * 行政区域-省/州
     * @return administration_province 行政区域-省/州
     */
    public String getAdministrationProvince() {
        return administrationProvince;
    }

    /**
     * 行政区域-省/州
     * @param administrationProvince 行政区域-省/州
     */
    public void setAdministrationProvince(String administrationProvince) {
        this.administrationProvince = administrationProvince;
    }

    /**
     * 行政区域-城市
     * @return administration_city 行政区域-城市
     */
    public String getAdministrationCity() {
        return administrationCity;
    }

    /**
     * 行政区域-城市
     * @param administrationCity 行政区域-城市
     */
    public void setAdministrationCity(String administrationCity) {
        this.administrationCity = administrationCity;
    }

    /**
     * 行政区域-区/县
     * @return administration_county 行政区域-区/县
     */
    public String getAdministrationCounty() {
        return administrationCounty;
    }

    /**
     * 行政区域-区/县
     * @param administrationCounty 行政区域-区/县
     */
    public void setAdministrationCounty(String administrationCounty) {
        this.administrationCounty = administrationCounty;
    }

    /**
     * 详细地址
     * @return address 详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 详细地址
     * @param address 详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 经度
     * @return gps_position 经度
     */
    public String getGpsPosition() {
        return gpsPosition;
    }

    /**
     * 经度
     * @param gpsPosition 经度
     */
    public void setGpsPosition(String gpsPosition) {
        this.gpsPosition = gpsPosition;
    }

    /**
     * 开店日期
     * @return shop_date 开店日期
     */
    public Date getShopDate() {
        return shopDate;
    }

    /**
     * 开店日期
     * @param shopDate 开店日期
     */
    public void setShopDate(Date shopDate) {
        this.shopDate = shopDate;
    }

    /**
     * 照片
     * @return photo 照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 照片
     * @param photo 照片
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 附件
     * @return attachment 附件
     */
    public String getAttachment() {
        return attachment;
    }

    /**
     * 附件
     * @param attachment 附件
     */
    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    /**
     * 对应ERP
     * @return erp_code 对应ERP
     */
    public String getErpCode() {
        return erpCode;
    }

    /**
     * 对应ERP
     * @param erpCode 对应ERP
     */
    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * 停启用状态，1启用2停用
     * @return enable_state 停启用状态，1启用2停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 停启用状态，1启用2停用
     * @param enableState 停启用状态，1启用2停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 创建时间
     * @return sys_created_time 创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 创建时间
     * @param sysCreatedTime 创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 创建人
     * @return sys_created_user_id 创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 创建人
     * @param sysCreatedUserId 创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 修改时间
     * @return sys_modified_time 修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 修改时间
     * @param sysModifiedTime 修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 修改人
     * @return sys_modified_user_id 修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 修改人
     * @param sysModifiedUserId 修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @return is_deleted 是否删除(0代表未删除，1代表删除）
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @param isDeleted 是否删除(0代表未删除，1代表删除）
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 自定义项1
     * @return def1 自定义项1
     */
    public String getDef1() {
        return def1;
    }

    /**
     * 自定义项1
     * @param def1 自定义项1
     */
    public void setDef1(String def1) {
        this.def1 = def1;
    }

    /**
     * 自定义项2
     * @return def2 自定义项2
     */
    public String getDef2() {
        return def2;
    }

    /**
     * 自定义项2
     * @param def2 自定义项2
     */
    public void setDef2(String def2) {
        this.def2 = def2;
    }

    /**
     * 自定义项3
     * @return def3 自定义项3
     */
    public String getDef3() {
        return def3;
    }

    /**
     * 自定义项3
     * @param def3 自定义项3
     */
    public void setDef3(String def3) {
        this.def3 = def3;
    }

    /**
     * 自定义项4
     * @return def4 自定义项4
     */
    public String getDef4() {
        return def4;
    }

    /**
     * 自定义项4
     * @param def4 自定义项4
     */
    public void setDef4(String def4) {
        this.def4 = def4;
    }

    /**
     * 自定义项5
     * @return def5 自定义项5
     */
    public String getDef5() {
        return def5;
    }

    /**
     * 自定义项5
     * @param def5 自定义项5
     */
    public void setDef5(String def5) {
        this.def5 = def5;
    }

    /**
     * 自定义项6
     * @return def6 自定义项6
     */
    public String getDef6() {
        return def6;
    }

    /**
     * 自定义项6
     * @param def6 自定义项6
     */
    public void setDef6(String def6) {
        this.def6 = def6;
    }

    /**
     * 自定义项7
     * @return def7 自定义项7
     */
    public String getDef7() {
        return def7;
    }

    /**
     * 自定义项7
     * @param def7 自定义项7
     */
    public void setDef7(String def7) {
        this.def7 = def7;
    }

    /**
     * 自定义项8
     * @return def8 自定义项8
     */
    public String getDef8() {
        return def8;
    }

    /**
     * 自定义项8
     * @param def8 自定义项8
     */
    public void setDef8(String def8) {
        this.def8 = def8;
    }

    /**
     * 自定义项9
     * @return def9 自定义项9
     */
    public String getDef9() {
        return def9;
    }

    /**
     * 自定义项9
     * @param def9 自定义项9
     */
    public void setDef9(String def9) {
        this.def9 = def9;
    }

    /**
     * 自定义项10
     * @return def10 自定义项10
     */
    public String getDef10() {
        return def10;
    }

    /**
     * 自定义项10
     * @param def10 自定义项10
     */
    public void setDef10(String def10) {
        this.def10 = def10;
    }

    /**
     * 自定义项11
     * @return def11 自定义项11
     */
    public String getDef11() {
        return def11;
    }

    /**
     * 自定义项11
     * @param def11 自定义项11
     */
    public void setDef11(String def11) {
        this.def11 = def11;
    }

    /**
     * 自定义项12
     * @return def12 自定义项12
     */
    public String getDef12() {
        return def12;
    }

    /**
     * 自定义项12
     * @param def12 自定义项12
     */
    public void setDef12(String def12) {
        this.def12 = def12;
    }

    /**
     * 自定义项13
     * @return def13 自定义项13
     */
    public String getDef13() {
        return def13;
    }

    /**
     * 自定义项13
     * @param def13 自定义项13
     */
    public void setDef13(String def13) {
        this.def13 = def13;
    }

    /**
     * 自定义项14
     * @return def14 自定义项14
     */
    public String getDef14() {
        return def14;
    }

    /**
     * 自定义项14
     * @param def14 自定义项14
     */
    public void setDef14(String def14) {
        this.def14 = def14;
    }

    /**
     * 自定义项15
     * @return def15 自定义项15
     */
    public String getDef15() {
        return def15;
    }

    /**
     * 自定义项15
     * @param def15 自定义项15
     */
    public void setDef15(String def15) {
        this.def15 = def15;
    }

    /**
     * 自定义项16
     * @return def16 自定义项16
     */
    public String getDef16() {
        return def16;
    }

    /**
     * 自定义项16
     * @param def16 自定义项16
     */
    public void setDef16(String def16) {
        this.def16 = def16;
    }

    /**
     * 自定义项17
     * @return def17 自定义项17
     */
    public String getDef17() {
        return def17;
    }

    /**
     * 自定义项17
     * @param def17 自定义项17
     */
    public void setDef17(String def17) {
        this.def17 = def17;
    }

    /**
     * 自定义项18
     * @return def18 自定义项18
     */
    public String getDef18() {
        return def18;
    }

    /**
     * 自定义项18
     * @param def18 自定义项18
     */
    public void setDef18(String def18) {
        this.def18 = def18;
    }

    /**
     * 自定义项19
     * @return def19 自定义项19
     */
    public String getDef19() {
        return def19;
    }

    /**
     * 自定义项19
     * @param def19 自定义项19
     */
    public void setDef19(String def19) {
        this.def19 = def19;
    }

    /**
     * 自定义项20
     * @return def20 自定义项20
     */
    public String getDef20() {
        return def20;
    }

    /**
     * 自定义项20
     * @param def20 自定义项20
     */
    public void setDef20(String def20) {
        this.def20 = def20;
    }

    /**
     * 自定义项21
     * @return def21 自定义项21
     */
    public String getDef21() {
        return def21;
    }

    /**
     * 自定义项21
     * @param def21 自定义项21
     */
    public void setDef21(String def21) {
        this.def21 = def21;
    }

    /**
     * 自定义项22
     * @return def22 自定义项22
     */
    public String getDef22() {
        return def22;
    }

    /**
     * 自定义项22
     * @param def22 自定义项22
     */
    public void setDef22(String def22) {
        this.def22 = def22;
    }

    /**
     * 自定义项23
     * @return def23 自定义项23
     */
    public String getDef23() {
        return def23;
    }

    /**
     * 自定义项23
     * @param def23 自定义项23
     */
    public void setDef23(String def23) {
        this.def23 = def23;
    }

    /**
     * 自定义项24
     * @return def24 自定义项24
     */
    public String getDef24() {
        return def24;
    }

    /**
     * 自定义项24
     * @param def24 自定义项24
     */
    public void setDef24(String def24) {
        this.def24 = def24;
    }

    /**
     * 自定义项25
     * @return def25 自定义项25
     */
    public String getDef25() {
        return def25;
    }

    /**
     * 自定义项25
     * @param def25 自定义项25
     */
    public void setDef25(String def25) {
        this.def25 = def25;
    }

    /**
     * 自定义项26
     * @return def26 自定义项26
     */
    public String getDef26() {
        return def26;
    }

    /**
     * 自定义项26
     * @param def26 自定义项26
     */
    public void setDef26(String def26) {
        this.def26 = def26;
    }

    /**
     * 自定义项27
     * @return def27 自定义项27
     */
    public String getDef27() {
        return def27;
    }

    /**
     * 自定义项27
     * @param def27 自定义项27
     */
    public void setDef27(String def27) {
        this.def27 = def27;
    }

    /**
     * 自定义项28
     * @return def28 自定义项28
     */
    public String getDef28() {
        return def28;
    }

    /**
     * 自定义项28
     * @param def28 自定义项28
     */
    public void setDef28(String def28) {
        this.def28 = def28;
    }

    /**
     * 自定义项29
     * @return def29 自定义项29
     */
    public String getDef29() {
        return def29;
    }

    /**
     * 自定义项29
     * @param def29 自定义项29
     */
    public void setDef29(String def29) {
        this.def29 = def29;
    }

    /**
     * 自定义项30
     * @return def30 自定义项30
     */
    public String getDef30() {
        return def30;
    }

    /**
     * 自定义项30
     * @param def30 自定义项30
     */
    public void setDef30(String def30) {
        this.def30 = def30;
    }

	public String getManagementModelName() {
		return managementModelName;
	}

	public void setManagementModelName(String managementModelName) {
		this.managementModelName = managementModelName;
	}

	public String getRateName() {
		return rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}

	public Long getTypeDetail() {
		return typeDetail;
	}

	public void setTypeDetail(Long typeDetail) {
		this.typeDetail = typeDetail;
	}

	public Long getLevelDetail() {
		return levelDetail;
	}

	public void setLevelDetail(Long levelDetail) {
		this.levelDetail = levelDetail;
	}

	public String getTypeDetailName() {
		return typeDetailName;
	}

	public void setTypeDetailName(String typeDetailName) {
		this.typeDetailName = typeDetailName;
	}

	public String getLevelDetailName() {
		return levelDetailName;
	}

	public void setLevelDetailName(String levelDetailName) {
		this.levelDetailName = levelDetailName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOwnerUserName() {
		return ownerUserName;
	}

	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
}