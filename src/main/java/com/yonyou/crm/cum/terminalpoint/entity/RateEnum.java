package com.yonyou.crm.cum.terminalpoint.entity;

public enum RateEnum {
	WEEK(1,"周"),TWOWEEK(2,"双周"),MONTH(3,"月");
	
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private RateEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
