package com.yonyou.crm.cum.cumprmt.entity;

import java.io.Serializable;
import java.util.Date;

import com.yonyou.crm.bpub.approval.entity.SuperVO;


@SuppressWarnings("serial")
public class CumprmtVO extends SuperVO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;
    
    /**
     * 删除人名称
     */
    private String deletedUserName;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 公司id
     */
    private Long orgId;
    
    /**
     * 公司名称
     */
    private String orgName;

    /**
     * 部门id
     */
    private Long deptId;
    
    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 申请人
     */
    private Long applyUserId;
    
    /**
     * 申请人名称
     */
    private String applyUserName;

    /**
     * 单据号
     */
    private String billCode;

    /**
     * 客户编码
     */
    private Long cumId;

    /**
     * 客户名称
     */
    private String cumName;

    /**
     * 客户全称
     */
    private String cumFullname;

    /**
     * 创建人
     */
    private Long sysCreatedUserId;

    /**
     * 创建时间
     */
    private Date sysCreatedTime;

    /**
     * 最后修改人
     */
    private Long sysModifiedUserId;

    /**
     * 最后修改时间
     */
    private Date sysModifiedTime;

    /**
     * 提交人
     */
    private Long commitUserId;
    
    /**
     * 提交人名称
     */
    private String commitUserName;

    /**
     * 提交时间
     */
    private Date commitTime;

    /**
     * 最后审批人
     */
    private Long approvalUserId;
    
    /**
     * 最后审批人名称
     */
    private String approvalUserName;

    /**
     * 最后审批时间
     */
    private Date approvalTime;

    /**
     * 审批状态
     */
    private Integer approvalState;
    
    /**
     * 审批状态名称
     */
    private String approvalStateName;
    
    /*
     * 审批动作
     */
    private String approvalAction;
    
    /**
     * 备注
     */
    private String description;

    /**
     * 审批批语
     */
    private String approvalComment;

    /**
     * 自定义项1
     */
    private String def1;

    /**
     * 自定义项2
     */
    private String def2;

    /**
     * 自定义项3
     */
    private String def3;

    /**
     * 自定义项4
     */
    private String def4;

    /**
     * 自定义项5
     */
    private String def5;

    /**
     * 自定义项6
     */
    private String def6;

    /**
     * 自定义项7
     */
    private String def7;

    /**
     * 自定义项8
     */
    private String def8;

    /**
     * 自定义项9
     */
    private String def9;

    /**
     * 自定义项10
     */
    private String def10;

    /**
     * 自定义项11
     */
    private String def11;

    /**
     * 自定义项12
     */
    private String def12;

    /**
     * 自定义项13
     */
    private String def13;

    /**
     * 自定义项14
     */
    private String def14;

    /**
     * 自定义项15
     */
    private String def15;

    /**
     * 自定义项16
     */
    private String def16;

    /**
     * 自定义项17
     */
    private String def17;

    /**
     * 自定义项18
     */
    private String def18;

    /**
     * 自定义项19
     */
    private String def19;

    /**
     * 自定义项20
     */
    private String def20;

    /**
     * 
     * @return id 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除人名称
	 * @return deletedUserName
	 */
	public String getDeletedUserName() {
		return deletedUserName;
	}

	/**
	 * @param deletedUserName 删除人名称
	 */
	public void setDeletedUserName(String deletedUserName) {
		this.deletedUserName = deletedUserName;
	}

	/**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 公司id
     * @return org_id 公司id
     */
    public Long getOrgId() {
        return orgId;
    }
    /**
     * 公司id
     * @param orgId 公司id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 公司名称
	 * @return orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName 公司名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
     * 部门id
     * @return dept_id 部门id
     */
    public Long getDeptId() {
        return deptId;
    }

    /**
     * 部门id
     * @param deptId 部门id
     */
    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    /**
     * 部门名称
	 * @return deptName
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * @param deptName 部门名称
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
     * 申请人
     * @return apply_user_id 申请人
     */
    public Long getApplyUserId() {
        return applyUserId;
    }

    /**
     * 申请人
     * @param applyUserId 申请人
     */
    public void setApplyUserId(Long applyUserId) {
        this.applyUserId = applyUserId;
    }

    /**
     * 申请人名称
	 * @return applyUserName
	 */
	public String getApplyUserName() {
		return applyUserName;
	}

	/**
	 * @param applyUserName 申请人名称
	 */
	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}

	/**
     * 单据号
     * @return bill_code 单据号
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * 单据号
     * @param billCode 单据号
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * 客户编码
     * @return cum_id 客户编码
     */
    public Long getCumId() {
        return cumId;
    }

    /**
     * 客户编码
     * @param cumId 客户编码
     */
    public void setCumId(Long cumId) {
        this.cumId = cumId;
    }

    /**
     * 客户名称
     * @return cum_name 客户名称
     */
    public String getCumName() {
        return cumName;
    }

    /**
     * 客户名称
     * @param cumName 客户名称
     */
    public void setCumName(String cumName) {
        this.cumName = cumName;
    }

    /**
     * 客户全称
     * @return cum_fullname 客户全称
     */
    public String getCumFullname() {
        return cumFullname;
    }

    /**
     * 客户全称
     * @param cumFullname 客户全称
     */
    public void setCumFullname(String cumFullname) {
        this.cumFullname = cumFullname;
    }

    /**
     * 创建人
     * @return sys_created_user_id 创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 创建人
     * @param sysCreatedUserId 创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 创建时间
     * @return sys_created_time 创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 创建时间
     * @param sysCreatedTime 创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 最后修改人
     * @return sys_modified_user_id 最后修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 最后修改人
     * @param sysModifiedUserId 最后修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 最后修改时间
     * @return sys_modified_time 最后修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 最后修改时间
     * @param sysModifiedTime 最后修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 提交人
     * @return commit_user_id 提交人
     */
    public Long getCommitUserId() {
        return commitUserId;
    }

    /**
     * 提交人
     * @param commitUserId 提交人
     */
    public void setCommitUserId(Long commitUserId) {
        this.commitUserId = commitUserId;
    }

    /**
     * 提交人名称
	 * @return commitUserName
	 */
	public String getCommitUserName() {
		return commitUserName;
	}

	/**
	 * @param commitUserName 提交人名称
	 */
	public void setCommitUserName(String commitUserName) {
		this.commitUserName = commitUserName;
	}

	/**
     * 提交时间
     * @return commit_time 提交时间
     */
    public Date getCommitTime() {
        return commitTime;
    }

    /**
     * 提交时间
     * @param commitTime 提交时间
     */
    public void setCommitTime(Date commitTime) {
        this.commitTime = commitTime;
    }

    /**
     * 最后审批人
     * @return approval_user_id 最后审批人
     */
    public Long getApprovalUserId() {
        return approvalUserId;
    }

    /**
     * 最后审批人
     * @param approvalUserId 最后审批人
     */
    public void setApprovalUserId(Long approvalUserId) {
        this.approvalUserId = approvalUserId;
    }

    /**
     * 最后审批人名称
	 * @return approvalUserName
	 */
	public String getApprovalUserName() {
		return approvalUserName;
	}

	/**
	 * @param approvalUserName 最后审批人名称
	 */
	public void setApprovalUserName(String approvalUserName) {
		this.approvalUserName = approvalUserName;
	}

	/**
     * 最后审批时间
     * @return approval_time 最后审批时间
     */
    public Date getApprovalTime() {
        return approvalTime;
    }

    /**
     * 最后审批时间
     * @param approvalTime 最后审批时间
     */
    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    /**
     * 审批状态
     * @return approval_state 审批状态
     */
    public Integer getApprovalState() {
        return approvalState;
    }

    /**
     * 审批状态
     * @param approvalState 审批状态
     */
    public void setApprovalState(Integer approvalState) {
        this.approvalState = approvalState;
    }
    
    /**
     * 审批状态名称
	 * @return approvalStateName
	 */
	public String getApprovalStateName() {
		return approvalStateName;
	}

	/**
	 * @param approvalStateName 审批状态名称
	 */
	public void setApprovalStateName(String approvalStateName) {
		this.approvalStateName = approvalStateName;
	}

	public String getApprovalAction() {
		return approvalAction;
	}

	public void setApprovalAction(String approvalAction) {
		this.approvalAction = approvalAction;
	}

	/**
     * 备注
     * @return description 备注
     */
    public String getDescription() {
        return description;
    }

    /**
     * 备注
     * @param description 备注
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 审批批语
     * @return approval_comment 审批批语
     */
    public String getApprovalComment() {
        return approvalComment;
    }

    /**
     * 审批批语
     * @param approvalComment 审批批语
     */
    public void setApprovalComment(String approvalComment) {
        this.approvalComment = approvalComment;
    }

    /**
     * 自定义项1
     * @return def1 自定义项1
     */
    public String getDef1() {
        return def1;
    }

    /**
     * 自定义项1
     * @param def1 自定义项1
     */
    public void setDef1(String def1) {
        this.def1 = def1;
    }

    /**
     * 自定义项2
     * @return def2 自定义项2
     */
    public String getDef2() {
        return def2;
    }

    /**
     * 自定义项2
     * @param def2 自定义项2
     */
    public void setDef2(String def2) {
        this.def2 = def2;
    }

    /**
     * 自定义项3
     * @return def3 自定义项3
     */
    public String getDef3() {
        return def3;
    }

    /**
     * 自定义项3
     * @param def3 自定义项3
     */
    public void setDef3(String def3) {
        this.def3 = def3;
    }

    /**
     * 自定义项4
     * @return def4 自定义项4
     */
    public String getDef4() {
        return def4;
    }

    /**
     * 自定义项4
     * @param def4 自定义项4
     */
    public void setDef4(String def4) {
        this.def4 = def4;
    }

    /**
     * 自定义项5
     * @return def5 自定义项5
     */
    public String getDef5() {
        return def5;
    }

    /**
     * 自定义项5
     * @param def5 自定义项5
     */
    public void setDef5(String def5) {
        this.def5 = def5;
    }

    /**
     * 自定义项6
     * @return def6 自定义项6
     */
    public String getDef6() {
        return def6;
    }

    /**
     * 自定义项6
     * @param def6 自定义项6
     */
    public void setDef6(String def6) {
        this.def6 = def6;
    }

    /**
     * 自定义项7
     * @return def7 自定义项7
     */
    public String getDef7() {
        return def7;
    }

    /**
     * 自定义项7
     * @param def7 自定义项7
     */
    public void setDef7(String def7) {
        this.def7 = def7;
    }

    /**
     * 自定义项8
     * @return def8 自定义项8
     */
    public String getDef8() {
        return def8;
    }

    /**
     * 自定义项8
     * @param def8 自定义项8
     */
    public void setDef8(String def8) {
        this.def8 = def8;
    }

    /**
     * 自定义项9
     * @return def9 自定义项9
     */
    public String getDef9() {
        return def9;
    }

    /**
     * 自定义项9
     * @param def9 自定义项9
     */
    public void setDef9(String def9) {
        this.def9 = def9;
    }

    /**
     * 自定义项10
     * @return def10 自定义项10
     */
    public String getDef10() {
        return def10;
    }

    /**
     * 自定义项10
     * @param def10 自定义项10
     */
    public void setDef10(String def10) {
        this.def10 = def10;
    }

    /**
     * 自定义项11
     * @return def11 自定义项11
     */
    public String getDef11() {
        return def11;
    }

    /**
     * 自定义项11
     * @param def11 自定义项11
     */
    public void setDef11(String def11) {
        this.def11 = def11;
    }

    /**
     * 自定义项12
     * @return def12 自定义项12
     */
    public String getDef12() {
        return def12;
    }

    /**
     * 自定义项12
     * @param def12 自定义项12
     */
    public void setDef12(String def12) {
        this.def12 = def12;
    }

    /**
     * 自定义项13
     * @return def13 自定义项13
     */
    public String getDef13() {
        return def13;
    }

    /**
     * 自定义项13
     * @param def13 自定义项13
     */
    public void setDef13(String def13) {
        this.def13 = def13;
    }

    /**
     * 自定义项14
     * @return def14 自定义项14
     */
    public String getDef14() {
        return def14;
    }

    /**
     * 自定义项14
     * @param def14 自定义项14
     */
    public void setDef14(String def14) {
        this.def14 = def14;
    }

    /**
     * 自定义项15
     * @return def15 自定义项15
     */
    public String getDef15() {
        return def15;
    }

    /**
     * 自定义项15
     * @param def15 自定义项15
     */
    public void setDef15(String def15) {
        this.def15 = def15;
    }

    /**
     * 自定义项16
     * @return def16 自定义项16
     */
    public String getDef16() {
        return def16;
    }

    /**
     * 自定义项16
     * @param def16 自定义项16
     */
    public void setDef16(String def16) {
        this.def16 = def16;
    }

    /**
     * 自定义项17
     * @return def17 自定义项17
     */
    public String getDef17() {
        return def17;
    }

    /**
     * 自定义项17
     * @param def17 自定义项17
     */
    public void setDef17(String def17) {
        this.def17 = def17;
    }

    /**
     * 自定义项18
     * @return def18 自定义项18
     */
    public String getDef18() {
        return def18;
    }

    /**
     * 自定义项18
     * @param def18 自定义项18
     */
    public void setDef18(String def18) {
        this.def18 = def18;
    }

    /**
     * 自定义项19
     * @return def19 自定义项19
     */
    public String getDef19() {
        return def19;
    }

    /**
     * 自定义项19
     * @param def19 自定义项19
     */
    public void setDef19(String def19) {
        this.def19 = def19;
    }

    /**
     * 自定义项20
     * @return def20 自定义项20
     */
    public String getDef20() {
        return def20;
    }

    /**
     * 自定义项20
     * @param def20 自定义项20
     */
    public void setDef20(String def20) {
        this.def20 = def20;
    }

	@Override
	public String getPrimaryKey() {
		// TODO 自动生成的方法存根
		return null;
	}
}