package com.yonyou.crm.cum.cumprmt.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.common.page.entity.Page;
import com.yonyou.crm.cum.cumprmt.entity.CumprmtVO;

public interface ICumprmtRmService {
	
	public Page<CumprmtVO> list(Page<CumprmtVO> page,Map<String, Object> paraMap);
	public CumprmtVO detail(Long id);
	public CumprmtVO save(CumprmtVO vo);
	public CumprmtVO update(CumprmtVO vo);
	public Page<CumprmtVO> batchDelete(String[] ids, Page<CumprmtVO> page,Map<String, Object> paraMap);
	
	public void submit(Long cumId);
	
	public List<CumprmtVO> getRelCumprmtList(Map<String, Object> paraMap);
}
