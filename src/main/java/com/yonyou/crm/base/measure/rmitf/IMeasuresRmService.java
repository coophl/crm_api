package com.yonyou.crm.base.measure.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.measure.entity.MeasuresVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IMeasuresRmService {

	public Page<MeasuresVO> getList(Page<MeasuresVO> page,Map<String, Object> paraMap);
	public MeasuresVO getDetail(Long id);
	public MeasuresVO insert(MeasuresVO measure);
	public MeasuresVO update(MeasuresVO measure);
	public int delete(Long id);
	public String batchDelete(String[] ids);
	public MeasuresVO updateEnableState(Long id, Integer enableState);
	public Page<MeasuresVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<MeasuresVO> page,Map<String, Object> paraMap);
	public Object selectFieldsByIds(Object[] ids);

	public List<MeasuresVO> getRefList(Map<String, Object> paraMap);
	public Page<MeasuresVO> getRefPage(Page<MeasuresVO> page, Map<String, Object> paraMap);
	
}
