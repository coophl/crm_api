package com.yonyou.crm.base.brand.entity;

import java.io.Serializable;
import java.util.Date;

public class BrandVO implements Serializable {
	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 英文名称
	 */
	private String enName;

	/**
	 * 编码~
	 */
	private String code;

	/**
	 * 启用标识(启用1，未启用2)
	 */
	private Byte enableState;

	/**
	 * 停启用状态名称
	 */
	private String enableStateName;

	/**
	 * 行业
	 */
	private Long industryId;

	/**
	 * 组织
	 */
	private Long orgId;
	/**
	 * 组织名称， 为了参照显示添加
	 */
	private String orgName;

	/**
	 * 对应ERP
	 */
	private String erpCode;

	/**
	 * 租户
	 */
	private Long tenantId;

	/**
	 * 是否已删除
	 */
	private Byte isDeleted;

	/**
	 * 删除人
	 */
	private Long deletedUserId;

	/**
	 * 删除时间
	 */
	private Date deletedTime;

	/**
	 * 记录的创建时间
	 */
	private Date sysCreatedTime;

	/**
	 * 记录的创建人
	 */
	private Long sysCreatedUserId;

	/**
	 * 记录的修改时间
	 */
	private Date sysModifiedTime;

	/**
	 * 记录的修改人
	 */
	private Long sysModifiedUserId;

	/**
	 * 停启用人
	 */
	private Long enableUserId;

	/**
	 * 停启用时间
	 */
	private Date enableTime;

	/**
	 * 时间戳
	 */
	private Date ts;

	/**
	 * 备注
	 */
	private String description;

	/**
	 * base_brand
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 * 
	 * @return id 主键
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 主键
	 * 
	 * @param id
	 *            主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 名称
	 * 
	 * @return name 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 名称
	 * 
	 * @param name
	 *            名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 英文名称
	 * 
	 * @return en_name 英文名称
	 */
	public String getEnName() {
		return enName;
	}

	/**
	 * 英文名称
	 * 
	 * @param enName
	 *            英文名称
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}

	/**
	 * 编码
	 * 
	 * @return code 编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 编码
	 * 
	 * @param code
	 *            编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 启用标识(启用1，未启用2)
	 * 
	 * @return enable_state 启用标识(启用1，未启用2)
	 */
	public Byte getEnableState() {
		return enableState;
	}

	/**
	 * 启用标识(启用1，未启用2)
	 * 
	 * @param enableState
	 *            启用标识(启用1，未启用2)
	 */
	public void setEnableState(Byte enableState) {
		this.enableState = enableState;
	}

	public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
	 * 行业
	 * 
	 * @return industry_id 行业
	 */
	public Long getIndustryId() {
		return industryId;
	}

	/**
	 * 行业
	 * 
	 * @param industryId
	 *            行业
	 */
	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}

	/**
	 * 组织
	 * 
	 * @return org_id 组织
	 */
	public Long getOrgId() {
		return orgId;
	}

	/**
	 * 组织
	 * 
	 * @param orgId
	 *            组织
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * 对应ERP
	 * 
	 * @return erp_code 对应ERP
	 */
	public String getErpCode() {
		return erpCode;
	}

	/**
	 * 对应ERP
	 * 
	 * @param erpCode
	 *            对应ERP
	 */
	public void setErpCode(String erpCode) {
		this.erpCode = erpCode;
	}

	/**
	 * 租户
	 * 
	 * @return tenant_id 租户
	 */
	public Long getTenantId() {
		return tenantId;
	}

	/**
	 * 租户
	 * 
	 * @param tenantId
	 *            租户
	 */
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * 是否已删除
	 * 
	 * @return is_deleted 是否已删除
	 */
	public Byte getIsDeleted() {
		return isDeleted;
	}

	/**
	 * 是否已删除
	 * 
	 * @param isDeleted
	 *            是否已删除
	 */
	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * 删除人
	 * 
	 * @return deleted_user_id 删除人
	 */
	public Long getDeletedUserId() {
		return deletedUserId;
	}

	/**
	 * 删除人
	 * 
	 * @param deletedUserId
	 *            删除人
	 */
	public void setDeletedUserId(Long deletedUserId) {
		this.deletedUserId = deletedUserId;
	}

	/**
	 * 删除时间
	 * 
	 * @return deleted_time 删除时间
	 */
	public Date getDeletedTime() {
		return deletedTime;
	}

	/**
	 * 删除时间
	 * 
	 * @param deletedTime
	 *            删除时间
	 */
	public void setDeletedTime(Date deletedTime) {
		this.deletedTime = deletedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @return sys_created_time 记录的创建时间
	 */
	public Date getSysCreatedTime() {
		return sysCreatedTime;
	}

	/**
	 * 记录的创建时间
	 * 
	 * @param sysCreatedTime
	 *            记录的创建时间
	 */
	public void setSysCreatedTime(Date sysCreatedTime) {
		this.sysCreatedTime = sysCreatedTime;
	}

	/**
	 * 记录的创建人
	 * 
	 * @return sys_created_user_id 记录的创建人
	 */
	public Long getSysCreatedUserId() {
		return sysCreatedUserId;
	}

	/**
	 * 记录的创建人
	 * 
	 * @param sysCreatedUserId
	 *            记录的创建人
	 */
	public void setSysCreatedUserId(Long sysCreatedUserId) {
		this.sysCreatedUserId = sysCreatedUserId;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @return sys_modified_time 记录的修改时间
	 */
	public Date getSysModifiedTime() {
		return sysModifiedTime;
	}

	/**
	 * 记录的修改时间
	 * 
	 * @param sysModifiedTime
	 *            记录的修改时间
	 */
	public void setSysModifiedTime(Date sysModifiedTime) {
		this.sysModifiedTime = sysModifiedTime;
	}

	/**
	 * 记录的修改人
	 * 
	 * @return sys_modified_user_id 记录的修改人
	 */
	public Long getSysModifiedUserId() {
		return sysModifiedUserId;
	}

	/**
	 * 记录的修改人
	 * 
	 * @param sysModifiedUserId
	 *            记录的修改人
	 */
	public void setSysModifiedUserId(Long sysModifiedUserId) {
		this.sysModifiedUserId = sysModifiedUserId;
	}

	/**
	 * 停启用人
	 * 
	 * @return enable_user_id 停启用人
	 */
	public Long getEnableUserId() {
		return enableUserId;
	}

	/**
	 * 停启用人
	 * 
	 * @param enableUserId
	 *            停启用人
	 */
	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}

	/**
	 * 停启用时间
	 * 
	 * @return enable_time 停启用时间
	 */
	public Date getEnableTime() {
		return enableTime;
	}

	/**
	 * 停启用时间
	 * 
	 * @param enableTime
	 *            停启用时间
	 */
	public void setEnableTime(Date enableTime) {
		this.enableTime = enableTime;
	}

	/**
	 * 时间戳
	 * 
	 * @return ts 时间戳
	 */
	public Date getTs() {
		return ts;
	}

	/**
	 * 时间戳
	 * 
	 * @param ts
	 *            时间戳
	 */
	public void setTs(Date ts) {
		this.ts = ts;
	}

	/**
	 * 备注
	 * 
	 * @return description 备注
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 备注
	 * 
	 * @param description
	 *            备注
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}