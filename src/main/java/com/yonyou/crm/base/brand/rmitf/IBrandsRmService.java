package com.yonyou.crm.base.brand.rmitf;

import com.yonyou.crm.base.brand.entity.BrandVO;
import com.yonyou.crm.common.page.entity.Page;

import java.util.List;
import java.util.Map;

public interface IBrandsRmService {

	List<BrandVO> getList();
	BrandVO getDetail(Long id);
	BrandVO insert(BrandVO brand);
	BrandVO update(BrandVO brand);
	int delete(Long id);
	String batchDelete(String[] ids);
	BrandVO updateEnableState(Long id, Integer enableState);
	Page<BrandVO> batchUpdateEnableState(String[] idArray, Integer enableState, Page<BrandVO> page, Map<String, Object> searchMap);

    Page<BrandVO> getPageList(Page<BrandVO> requestPage, Map<String, Object> searchMap);
    
    List<BrandVO> getRefList(Map<String, Object> paraMap);
    Page<BrandVO> getRefPage(Page<BrandVO> page, Map<String, Object> paraMap);
}
