package com.yonyou.crm.base.industry.rmitf;

import java.util.*;

import com.yonyou.crm.base.industry.entity.IndustryVO;

public interface IIndustryRmService {

	public List<Map<String, Object>> getRefTree();

	public List<IndustryVO> getList(Map<String, Object> condMap);
	
	public IndustryVO getDetail(Long id,Long fatherId);
}
