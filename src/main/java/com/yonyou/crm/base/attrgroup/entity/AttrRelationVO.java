package com.yonyou.crm.base.attrgroup.entity;

import java.io.Serializable;

public class AttrRelationVO implements Serializable {

	/**
	 * 属性组id
	 */
	private Long attrGroupId;
	
	/**
	 * 属性id
	 */
	private Long attrId;
	
	/**
	 * 属性值id
	 */
	private Long attrBId;
	
    /**
     * 编辑状态 add：新增，update：修改，delete:删除
     */
    private String editState;
	
	/**
	 * 属性组id
	 * @return attrGroupId
	 */
	public Long getAttrGroupId() {
		return attrGroupId;
	}
	
	/**
	 * @param attrGroupId 属性组id
	 */
	public void setAttrGroupId(Long attrGroupId) {
		this.attrGroupId = attrGroupId;
	}
	
	/**
	 * 属性id
	 * @return attrId
	 */
	public Long getAttrId() {
		return attrId;
	}
	
	/**
	 * @param attrId 属性id
	 */
	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	/**
	 * 属性值id
	 * @return attrBId
	 */
	public Long getAttrBId() {
		return attrBId;
	}

	/**
	 * 属性值id
	 * @param attrBId 要设置的 attrBId
	 */
	public void setAttrBId(Long attrBId) {
		this.attrBId = attrBId;
	}

	/**
	 * 编辑状态 add：新增，update：修改，delete:删除
	 * @return editState
	 */
	public String getEditState() {
		return editState == null ? "" : editState;
	}

	/**
	 * @param editState 编辑状态 add：新增，update：修改，delete:删除
	 */
	public void setEditState(String editState) {
		this.editState = editState;
	}
	
}
