package com.yonyou.crm.base.attrgroup.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.attrgroup.entity.AttrGroupVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IAttrGroupRmService {
	
	public Page<AttrGroupVO> page(Page<AttrGroupVO> page,Map<String, Object> paraMap);
	public AttrGroupVO detail(Long id);
	public AttrGroupVO insert(AttrGroupVO vo);
	public AttrGroupVO update(AttrGroupVO vo);
	public String batchDelete(String[] ids);
	public Page<AttrGroupVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<AttrGroupVO> page,Map<String, Object> paraMap);
	public boolean isCitedByProductOrPrdtype(Long attrGroupId);
	public List<AttrGroupVO> getRefList(Map<String, Object> paraMap);
	public Page<AttrGroupVO> getRefPage(Page<AttrGroupVO> page, Map<String, Object> paraMap);
	public Map<String, Object> getCheckedAttr(Long attrGroupId);
	public Map<String, Object> getCheckedAttrValue(Long attrGroupId, Long attrId);
}
