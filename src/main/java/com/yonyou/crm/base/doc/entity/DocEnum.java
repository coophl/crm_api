package com.yonyou.crm.base.doc.entity;

public enum DocEnum {
	CUMTYPE(1,"客户类型"),CHNLTYPE(2,"渠道类型"),CUMSOURCE(3,"客户来源"),CUMIDENTITY(4,"客户身份"),
	CUMLEVEL(5,"客户等级"),CUMSTATUS(6,"客户状态"),CHNLNODETYPE(7,"网点类型"),
	CHNLNODELEVEL(8,"网点等级");
	//枚举项对应int值
	int value;
	//枚举项显示值
	String name;
	private DocEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
