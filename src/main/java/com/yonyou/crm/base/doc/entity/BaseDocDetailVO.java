package com.yonyou.crm.base.doc.entity;

import java.io.Serializable;
import java.util.Date;

public class BaseDocDetailVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 名称
     */
    private String name;

    /**
     * 停启用状态，1启用2停用
     */
    private Byte enableState;

    /**
     * 停用时间
     */
    private Date enableTime;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 创建时间
     */
    private Date sysCreatedTime;

    /**
     * 创建人
     */
    private Long sysCreatedUserId;

    /**
     * 修改时间
     */
    private Date sysModifiedTime;

    /**
     * 修改人
     */
    private Long sysModifiedUserId;

    /**
     * 是否删除(0代表未删除，1代表删除）
     */
    private Byte isDeleted;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 对应档案ID
     */
    private Long baseDocId;
    
    /**
     * 是否启用（启用，停用）
     */
    private String enableStateName;
    
    /**
     * 修改状态（add代表新增，update代表编辑，delete代表删除）
     */
    private String editState;
    
    /**
     * 停启用人
     */
    private Long enableUserId;
    
    /**
     * sys_doc_detail
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 停启用状态，1启用2停用
     * @return enable_state 停启用状态，1启用2停用
     */
    public Byte getEnableState() {
        return enableState;
    }

    /**
     * 停启用状态，1启用2停用
     * @param enableState 停启用状态，1启用2停用
     */
    public void setEnableState(Byte enableState) {
        this.enableState = enableState;
    }

    /**
     * 停用时间
     * @return enable_time 停用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停用时间
     * @param enableTime 停用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 创建时间
     * @return sys_created_time 创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 创建时间
     * @param sysCreatedTime 创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 创建人
     * @return sys_created_user_id 创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 创建人
     * @param sysCreatedUserId 创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 修改时间
     * @return sys_modified_time 修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 修改时间
     * @param sysModifiedTime 修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 修改人
     * @return sys_modified_user_id 修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 修改人
     * @param sysModifiedUserId 修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @return is_deleted 是否删除(0代表未删除，1代表删除）
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除(0代表未删除，1代表删除）
     * @param isDeleted 是否删除(0代表未删除，1代表删除）
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 对应档案ID
     * @return sys_doc_id 对应档案ID
     */
    public Long getBaseDocId() {
        return baseDocId;
    }

    /**
     * 对应档案ID
     * @param sysDocId 对应档案ID
     */
    public void setBaseDocId(Long baseDocId) {
        this.baseDocId = baseDocId;
    }

	public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	public String getEditState() {
		return editState;
	}

	public void setEditState(String editState) {
		this.editState = editState;
	}

	public Long getEnableUserId() {
		return enableUserId;
	}

	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}
	
}