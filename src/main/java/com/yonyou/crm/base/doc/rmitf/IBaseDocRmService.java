package com.yonyou.crm.base.doc.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.doc.entity.BaseDocVO;
import com.yonyou.crm.common.page.entity.Page;



public interface IBaseDocRmService {
	
	public Page<BaseDocVO> getBaseDocPage(Page<BaseDocVO> page, Map<String, Object> paramMap);
	
	public BaseDocVO insertBaseDoc(BaseDocVO sysDoc);
	
	public Page<BaseDocVO> deleteBaseDocByIDs(String[] ids, Page<BaseDocVO> page, Map<String, Object> paramMap);
	
	public BaseDocVO updateBaseDoc(BaseDocVO sysDoc);
	
	public BaseDocVO detail(Long id);
	
	public Map<String, Object> getDocAndDetailList(Map<String, Object> idsMap);
	
	public List<BaseDocVO> getDocByIds(String[] ids);
}
