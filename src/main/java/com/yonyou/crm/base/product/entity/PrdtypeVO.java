package com.yonyou.crm.base.product.entity;

import java.io.Serializable;
import java.util.Date;

public class PrdtypeVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 编码
     */
    private String code;

    /**
     * 上级分类
     */
    private Long fatherTypeId;
    
    /**
     * 层级路径
     */
    private String path;
    
    /**
     * 层级
     */
    private Integer level;

    /**
     * 产品属性组
     */
    private Long attrGroupId;
    
    /**
     * 产品属性组名称
     */
    private String attrGroupName;

    /**
     * 是否删除
     */
    private Byte isDeleted;

    /**
     * 停启用状态，1启用2停用
     */
    private Integer enableState;

    /**
     * 启用时间
     */
    private Date enableTime;
    
    /**
     * 停用时间
     */
    private Date disableTime;
    
    /**
     * 停用人
     */
    private Long disableUserId;

    /**
     * 对应ERP
     */
    private String erpCode;

    /**
     * 名称
     */
    private String name;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;
    
    /**
     * 上级分类名称
     */
    private String fatherTypeName;
    
    /**
     * 启用/停用状态
     */
    private String enableStateName;
    
    /**
     * 停启用人
     */
    private Long enableUserId;
    
    /**
     * base_prdtype
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 编码
     * @return code 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 编码
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 上级分类
     * @return father_type_id 上级分类
     */
    public Long getFatherTypeId() {
        return fatherTypeId;
    }

    /**
     * 上级分类
     * @param fatherTypeId 上级分类
     */
    public void setFatherTypeId(Long fatherTypeId) {
        this.fatherTypeId = fatherTypeId;
    }

    /**
     * 层级路径
	 * @return path
	 */
	public String getPath() {
		return path == null ? "" : path;
	}

	/**
	 * @param path 层级路径
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * 层级
	 * @return level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level 层级
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
     * 产品属性组
     * @return attr_group_id 产品属性组
     */
    public Long getAttrGroupId() {
        return attrGroupId;
    }

    /**
     * 产品属性组
     * @param attrGroupId 产品属性组
     */
    public void setAttrGroupId(Long attrGroupId) {
        this.attrGroupId = attrGroupId;
    }

    /**
     * 产品属性组名称
	 * @return attrGroupName
	 */
	public String getAttrGroupName() {
		return attrGroupName;
	}

	/**
	 * @param attrGroupName 产品属性组名称
	 */
	public void setAttrGroupName(String attrGroupName) {
		this.attrGroupName = attrGroupName;
	}

	/**
     * 是否删除
     * @return is_deleted 是否删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 停启用状态，1启用2停用
     * @return enable_state 停启用状态，1启用2停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 停启用状态，1启用2停用
     * @param enableState 停启用状态，1启用2停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 启用时间
     * @return enable_time 启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 启用时间
     * @param enableTime 启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 停用时间
	 * @return disableTime
	 */
	public Date getDisableTime() {
		return disableTime;
	}

	/**
	 * @param disableTime 停用时间
	 */
	public void setDisableTime(Date disableTime) {
		this.disableTime = disableTime;
	}

	/**
	 * 停用人
	 * @return disableUserId
	 */
	public Long getDisableUserId() {
		return disableUserId;
	}

	/**
	 * @param disableUserId 停用人
	 */
	public void setDisableUserId(Long disableUserId) {
		this.disableUserId = disableUserId;
	}

	/**
     * 对应ERP
     * @return erp_code 对应ERP
     */
    public String getErpCode() {
        return erpCode;
    }

    /**
     * 对应ERP
     * @param erpCode 对应ERP
     */
    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录修改时间
     * @return sys_modified_time 记录修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录修改时间
     * @param sysModifiedTime 记录修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录修改人
     * @return sys_modified_user_id 记录修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录修改人
     * @param sysModifiedUserId 记录修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

	public String getFatherTypeName() {
		return fatherTypeName;
	}

	public void setFatherTypeName(String fatherTypeName) {
		this.fatherTypeName = fatherTypeName;
	}

	public String getEnableStateName() {
		return enableStateName;
	}

	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	public Long getEnableUserId() {
		return enableUserId;
	}

	public void setEnableUserId(Long enableUserId) {
		this.enableUserId = enableUserId;
	}
	
}