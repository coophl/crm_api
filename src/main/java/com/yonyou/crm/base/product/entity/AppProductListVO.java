package com.yonyou.crm.base.product.entity;

import java.io.Serializable;
import java.util.List;

public class AppProductListVO implements Serializable {
	
	private String code;
	
	private List<AppProductListValueVO> valuelist;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<AppProductListValueVO> getValuelist() {
		return valuelist;
	}

	public void setValuelist(List<AppProductListValueVO> valuelist) {
		this.valuelist = valuelist;
	}

}
