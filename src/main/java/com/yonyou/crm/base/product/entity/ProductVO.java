package com.yonyou.crm.base.product.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.yonyou.crm.base.attr.entity.AttrVO;

public class ProductVO implements Serializable {
	
	/**
	 * 主键
	 */
	private Long id;
	
    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 编码
     */
    private String code;
    /**
     * 是否已删除
     */
    private Byte isDeleted;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 记录的创建时间
     */
    private Date sysCreatedTime;

    /**
     * 记录的创建人
     */
    private Long sysCreatedUserId;

    /**
     * 记录的修改时间
     */
    private Date sysModifiedTime;

    /**
     * 记录的修改人
     */
    private Long sysModifiedUserId;

    /**
     * 时间戳
     */
    private Date ts;

    /**
     * 名称
     */
    private String name;

    /**
     * 助记码
     */
    private String memCode;

    /**
     * 规格
     */
    private String spec;

    /**
     * 产品分类
     */
    private Long prdtypeId;
    
    /**
     * 产品分类名称
     */
    private String prdtypeName;

    /**
     * 主单位
     */
    private Long measureId;
    
    /**
     * 主单位名称
     */
    private String measureName;

    /**
     * 品牌
     */
    private Long brandId;
    
    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 参考售价
     */
    private BigDecimal price;

    /**
     * 属性组
     */
    private Long attrGroupId;
    
    /**
     * 属性组名称
     */
    private String attrGroupName;
    
    /**
     * 属性列表
     */
    private List<AttrVO> attrList;
    
    /**
     * 使用组织
     */
    private String orgId;
    
    /**
     * 适用组织名称
     */
    private String orgName;

    /**
     * 产品照片
     */
    private String photo;

    /**
     * 停启用状态，1启用2停用
     */
    private Integer enableState;
    
    /**
     * 停启用状态名称
     */
    private String enableStateName;

    /**
     * 停启用时间
     */
    private Date enableTime;

    /**
     * 停启用人
     */
    private Long enableUserId;
    
    /**
     * 停用时间
     */
    private Date disableTime;
    
    /**
     * 停用人
     */
    private Long disableUserId;

    /**
     * 描述
     */
    private String description;

    /**
     * 销售单位
     */
    private List<SaleUnitVO> saleUnits;
    
    /**
     * 销售单位名称
     */
    private String saleUnitName;
    
    /**
     * 关注标识
     */
    private String followFlag;
    
    /**
     * 主键
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 主键
	 * @param id 主键
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
     * 租户ID
     * @return tenant_id 租户ID
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID
     * @param tenantId 租户ID
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 编码
     * @return code 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 编码
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * 是否已删除
     * @return is_deleted 是否已删除
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否已删除
     * @param isDeleted 是否已删除
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 记录的创建时间
     * @return sys_created_time 记录的创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 记录的创建时间
     * @param sysCreatedTime 记录的创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 记录的创建人
     * @return sys_created_user_id 记录的创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 记录的创建人
     * @param sysCreatedUserId 记录的创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 记录的修改时间
     * @return sys_modified_time 记录的修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 记录的修改时间
     * @param sysModifiedTime 记录的修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 记录的修改人
     * @return sys_modified_user_id 记录的修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 记录的修改人
     * @param sysModifiedUserId 记录的修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 助记码
     * @return mem_code 助记码
     */
    public String getMemCode() {
        return memCode;
    }

    /**
     * 助记码
     * @param memCode 助记码
     */
    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    /**
     * 规格
     * @return spec 规格
     */
    public String getSpec() {
        return spec;
    }

    /**
     * 规格
     * @param spec 规格
     */
    public void setSpec(String spec) {
        this.spec = spec;
    }

    /**
     * 产品分类
     * @return prdtype_id 产品分类
     */
    public Long getPrdtypeId() {
        return prdtypeId;
    }

    /**
     * 产品分类
     * @param prdtypeId 产品分类
     */
    public void setPrdtypeId(Long prdtypeId) {
        this.prdtypeId = prdtypeId;
    }

    /**
     * 产品分类名称
	 * @return prdtypeName
	 */
	public String getPrdtypeName() {
		return prdtypeName;
	}

	/**
	 * @param prdtypeName 产品分类名称
	 */
	public void setPrdtypeName(String prdtypeName) {
		this.prdtypeName = prdtypeName;
	}

	/**
     * 主单位
     * @return measure_id 主单位
     */
    public Long getMeasureId() {
        return measureId;
    }

    /**
     * 主单位
     * @param measureId 主单位
     */
    public void setMeasureId(Long measureId) {
        this.measureId = measureId;
    }

    /**
     * 主单位名称
	 * @return measureName
	 */
	public String getMeasureName() {
		return measureName;
	}

	/**
	 * @param measureName 主单位名称
	 */
	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}

	/**
     * 品牌
     * @return brand_id 品牌
     */
    public Long getBrandId() {
        return brandId;
    }

    /**
     * 品牌
     * @param brandId 品牌
     */
    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    /**
     * 品牌名称
	 * @return brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName 品牌名称
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
     * 参考售价
     * @return price 参考售价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 参考售价
     * @param price 参考售价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 属性组
     * @return attr_group_id 属性组
     */
    public Long getAttrGroupId() {
        return attrGroupId;
    }

    /**
     * 属性组
     * @param attrGroupId 属性组
     */
    public void setAttrGroupId(Long attrGroupId) {
        this.attrGroupId = attrGroupId;
    }

    /**
     * 属性组名称
	 * @return attrGroupName
	 */
	public String getAttrGroupName() {
		return attrGroupName;
	}

	/**
	 * @param attrGroupName 属性组名称
	 */
	public void setAttrGroupName(String attrGroupName) {
		this.attrGroupName = attrGroupName;
	}

	/**
	 * 属性列表
	 * @return attrList
	 */
	public List<AttrVO> getAttrList() {
		return attrList;
	}

	/**
	 * @param attrList 属性列表
	 */
	public void setAttrList(List<AttrVO> attrList) {
		this.attrList = attrList;
	}

	/**
	 * 使用组织
	 * @return orgId
	 */
	public String getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId 使用组织
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	/**
	 * 适用组织名称
	 * @return orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName 适用组织名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
     * 产品照片
     * @return photo 产品照片
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 产品照片
     * @param photo 产品照片
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 停启用状态，1启用2停用
     * @return enable_state 停启用状态，1启用2停用
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 停启用状态，1启用2停用
     * @param enableState 停启用状态，1启用2停用
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 停启用名称
	 * @return enableStateName
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * @param enableStateName 停启用名称
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 停启用时间
     * @return enable_time 停启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 停启用时间
     * @param enableTime 停启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 停启用人
     * @return enable_user_id 停启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 停启用人
     * @param enableUserId 停启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 停用时间
	 * @return disableTime
	 */
	public Date getDisableTime() {
		return disableTime;
	}

	/**
	 * @param disableTime 停用时间
	 */
	public void setDisableTime(Date disableTime) {
		this.disableTime = disableTime;
	}

	/**
	 * 停用人
	 * @return disableUserId
	 */
	public Long getDisableUserId() {
		return disableUserId;
	}

	/**
	 * @param disableUserId 停用人
	 */
	public void setDisableUserId(Long disableUserId) {
		this.disableUserId = disableUserId;
	}

	/**
     * 描述
     * @return description 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

	/**
	 * 销售单位
	 * @return saleUnits
	 */
	public List<SaleUnitVO> getSaleUnits() {
		return saleUnits;
	}

	/**
	 * @param saleUnits 销售单位
	 */
	public void setSaleUnits(List<SaleUnitVO> saleUnits) {
		this.saleUnits = saleUnits;
	}

	/**
	 * 销售单位名称
	 * @return saleUnitName
	 */
	public String getSaleUnitName() {
		return saleUnitName;
	}

	/**
	 * @param saleUnitName 销售单位名称
	 */
	public void setSaleUnitName(String saleUnitName) {
		this.saleUnitName = saleUnitName;
	}

	public String getFollowFlag() {
		return followFlag;
	}

	public void setFollowFlag(String followFlag) {
		this.followFlag = followFlag;
	}
}