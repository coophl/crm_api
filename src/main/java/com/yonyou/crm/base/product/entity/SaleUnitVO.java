package com.yonyou.crm.base.product.entity;

import java.io.Serializable;

public class SaleUnitVO implements Serializable {
	
	/**
	 * 产品主键
	 */
	private Long productId;
	/**
	 * 计量单位
	 */
	private Long measureId;
	
	/**
	 * 计量单位名称
	 */
	private String measureName;
	
	/**
	 * 换算率
	 */
	private String convertRate;
	
	/**
	 * 固定换算，1是2否
	 */
	private Integer fixedConvert;
	
	/**
	 * 固定换算名称
	 */
	private String fixedConvertName;
	
	/**
	 * 产品主键
	 * @return productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId 产品主键
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	/**
	 * 计量单位
	 * @return measureId
	 */
	public Long getMeasureId() {
		return measureId;
	}
	/**
	 * @param measureId 计量单位
	 */
	public void setMeasureId(Long measureId) {
		this.measureId = measureId;
	}
	
	/**
	 * 计量单位名称
	 * @return measureName
	 */
	public String getMeasureName() {
		return measureName;
	}
	/**
	 * @param measureName 计量单位名称
	 */
	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}
	/**
	 * 换算率
	 * @return convertRate
	 */
	public String getConvertRate() {
		return convertRate;
	}
	/**
	 * @param convertRate 换算率
	 */
	public void setConvertRate(String convertRate) {
		this.convertRate = convertRate;
	}
	
	/**
	 * 固定换算，1是2否
	 * @return fixedConvert
	 */
	public Integer getFixedConvert() {
		return fixedConvert;
	}
	/**
	 * @param fixedConvert 固定换算，1是2否
	 */
	public void setFixedConvert(Integer fixedConvert) {
		this.fixedConvert = fixedConvert;
	}
	
	/**
	 * 固定换算名称
	 * @return fixedConvertName
	 */
	public String getFixedConvertName() {
		return fixedConvertName;
	}
	
	/**
	 * @param fixedConvertName 固定换算名称
	 */
	public void setFixedConvertName(String fixedConvertName) {
		this.fixedConvertName = fixedConvertName;
	}
}
