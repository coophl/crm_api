package com.yonyou.crm.base.product.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.product.entity.PrdtypeVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IPrdtypeRmService {
	
	public PrdtypeVO insert(PrdtypeVO prdtype);
	
	public Page<PrdtypeVO> getPage(Page<PrdtypeVO> page, Map<String, Object> paraMap);
	
	public List<PrdtypeVO> getList(Map<String, Object> condMap);
	
	public PrdtypeVO getDetail(Long id);
			
	public PrdtypeVO update(PrdtypeVO prdtype);
	
	public List<Map<String,Object>> getTree();
	
	public String batchDeleteByIDs(String[] ids);
	
	public List<Map<String, Object>> getRefTree(Map<String, Object> paraMap);
	
	public Page<PrdtypeVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<PrdtypeVO> page, Map<String, Object> paraMap);
}
