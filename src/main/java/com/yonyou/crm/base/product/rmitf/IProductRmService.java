package com.yonyou.crm.base.product.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.product.entity.ProductVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IProductRmService {

	public Page<ProductVO> page(Page<ProductVO> page,Map<String, Object> paraMap);
	public ProductVO detail(Long id);
	public ProductVO insert(ProductVO vo);
	public ProductVO update(ProductVO vo);
	public String batchDelete(String[] ids);
	public Page<ProductVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<ProductVO> page,Map<String, Object> paraMap);
	public int insertProductFollow(Long id);
	public int deleteProductFollow(Long id);
	public Page<ProductVO> getRefPage(Page<ProductVO> page, Map<String, Object> paraMap);
	public List<ProductVO> getRefList(Map<String, Object> paraMap);
	public boolean isCitedByBObj(Long id);
	public void allocateOrg(Long productId, String[] orgIds);
}
