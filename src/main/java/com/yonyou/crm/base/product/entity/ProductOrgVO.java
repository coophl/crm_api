package com.yonyou.crm.base.product.entity;

import java.io.Serializable;

public class ProductOrgVO implements Serializable {

	/**
	 * 产品主键
	 */
	private Long productId;
	/**
	 * 组织id
	 */
	private Long orgId;
	
	/**
	 * 产品主键
	 * @return
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * 产品主键
	 * @param productId
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	/**
	 * 组织id
	 * @return
	 */
	public Long getOrgId() {
		return orgId;
	}
	/**
	 * 组织id
	 * @param orgId
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
}
