package com.yonyou.crm.base.attr.rmitf;

import java.util.List;
import java.util.Map;

import com.yonyou.crm.base.attr.entity.AttrBVO;
import com.yonyou.crm.base.attr.entity.AttrVO;
import com.yonyou.crm.common.page.entity.Page;

public interface IAttrRmService {

	public Page<AttrVO> page(Page<AttrVO> page,Map<String, Object> paraMap);
	public AttrVO detail(Long id);
	public AttrVO insert(AttrVO vo);
	public AttrVO update(AttrVO vo);
	public String batchDelete(String[] ids);
	public Page<AttrVO> batchUpdateEnableState(String[] ids, Integer enableState, Page<AttrVO> page,Map<String, Object> paraMap);
	public List<AttrVO> getAttrRefList();
	public List<AttrBVO> getAttrValueRefList(Long attrId);
	public boolean isCitedByAttrGroup(Long valueId);
	public List<String> getIdsByAttrGroup(Long attrId);
}
