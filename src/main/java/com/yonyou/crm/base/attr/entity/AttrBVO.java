package com.yonyou.crm.base.attr.entity;

import java.io.Serializable;
import java.util.Date;

public class AttrBVO implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 属性id
     */
    private Long attrId;
    
    /**
     * 租户id
     */
    private Long tenantId;

    /**
     * 属性值
     */
    private String value;

    /**
     * 对应ERP
     */
    private String erpCode;

    /**
     * 启用标识(启用1，未启用2)
     */
    private Integer enableState;
    
    /**
     * 启用标识名称
     */
    private String enableStateName;

    /**
     * 启用人
     */
    private Long enableUserId;

    /**
     * 启用时间
     */
    private Date enableTime;

    /**
     * 系统创建人
     */
    private Long sysCreatedUserId;

    /**
     * 系统创建时间
     */
    private Date sysCreatedTime;

    /**
     * 系统修改人
     */
    private Long sysModifiedUserId;

    /**
     * 系统修改时间
     */
    private Date sysModifiedTime;

    /**
     * 删除人
     */
    private Long deletedUserId;

    /**
     * 删除时间
     */
    private Date deletedTime;

    /**
     * 是否删除
     */
    private Integer isDeleted;

    /**
     * 编辑状态 add：新增，update：修改，delete:删除
     */
    private String editState;
    
    /**
     * 时间戳
     */
    private Date ts;

    /**
     * base_attr_b
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     * @return id 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 属性id
     * @return attr_id 属性id
     */
    public Long getAttrId() {
        return attrId;
    }

    /**
     * 属性id
     * @param attrId 属性id
     */
    public void setAttrId(Long attrId) {
        this.attrId = attrId;
    }

    /**
     * 租户id
     * @return tenant_id 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     * @param tenantId 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 属性值
     * @return value 属性值
     */
    public String getValue() {
        return value;
    }

    /**
     * 属性值
     * @param value 属性值
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 对应ERP
     * @return erp_code 对应ERP
     */
    public String getErpCode() {
        return erpCode;
    }

    /**
     * 对应ERP
     * @param erpCode 对应ERP
     */
    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    /**
     * 启用标识(启用1，未启用2)
     * @return enable_state 启用标识(启用1，未启用2)
     */
    public Integer getEnableState() {
        return enableState;
    }

    /**
     * 启用标识(启用1，未启用2)
     * @param enableState 启用标识(启用1，未启用2)
     */
    public void setEnableState(Integer enableState) {
        this.enableState = enableState;
    }

    /**
     * 启用标识名称
	 * @return enableStateName
	 */
	public String getEnableStateName() {
		return enableStateName;
	}

	/**
	 * @param enableStateName 启用标识名称
	 */
	public void setEnableStateName(String enableStateName) {
		this.enableStateName = enableStateName;
	}

	/**
     * 启用人
     * @return enable_user_id 启用人
     */
    public Long getEnableUserId() {
        return enableUserId;
    }

    /**
     * 启用人
     * @param enableUserId 启用人
     */
    public void setEnableUserId(Long enableUserId) {
        this.enableUserId = enableUserId;
    }

    /**
     * 启用时间
     * @return enable_time 启用时间
     */
    public Date getEnableTime() {
        return enableTime;
    }

    /**
     * 启用时间
     * @param enableTime 启用时间
     */
    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    /**
     * 系统创建人
     * @return sys_created_user_id 系统创建人
     */
    public Long getSysCreatedUserId() {
        return sysCreatedUserId;
    }

    /**
     * 系统创建人
     * @param sysCreatedUserId 系统创建人
     */
    public void setSysCreatedUserId(Long sysCreatedUserId) {
        this.sysCreatedUserId = sysCreatedUserId;
    }

    /**
     * 系统创建时间
     * @return sys_created_time 系统创建时间
     */
    public Date getSysCreatedTime() {
        return sysCreatedTime;
    }

    /**
     * 系统创建时间
     * @param sysCreatedTime 系统创建时间
     */
    public void setSysCreatedTime(Date sysCreatedTime) {
        this.sysCreatedTime = sysCreatedTime;
    }

    /**
     * 系统修改人
     * @return sys_modified_user_id 系统修改人
     */
    public Long getSysModifiedUserId() {
        return sysModifiedUserId;
    }

    /**
     * 系统修改人
     * @param sysModifiedUserId 系统修改人
     */
    public void setSysModifiedUserId(Long sysModifiedUserId) {
        this.sysModifiedUserId = sysModifiedUserId;
    }

    /**
     * 系统修改时间
     * @return sys_modified_time 系统修改时间
     */
    public Date getSysModifiedTime() {
        return sysModifiedTime;
    }

    /**
     * 系统修改时间
     * @param sysModifiedTime 系统修改时间
     */
    public void setSysModifiedTime(Date sysModifiedTime) {
        this.sysModifiedTime = sysModifiedTime;
    }

    /**
     * 删除人
     * @return deleted_user_id 删除人
     */
    public Long getDeletedUserId() {
        return deletedUserId;
    }

    /**
     * 删除人
     * @param deletedUserId 删除人
     */
    public void setDeletedUserId(Long deletedUserId) {
        this.deletedUserId = deletedUserId;
    }

    /**
     * 删除时间
     * @return deleted_time 删除时间
     */
    public Date getDeletedTime() {
        return deletedTime;
    }

    /**
     * 删除时间
     * @param deletedTime 删除时间
     */
    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    /**
     * 是否删除
     * @return is_deleted 是否删除
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * 是否删除
     * @param isDeleted 是否删除
     */
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 编辑状态 add：新增，update：修改，delete:删除
	 * @return editState
	 */
	public String getEditState() {
		return editState;
	}

	/**
	 * @param editState 编辑状态 add：新增，update：修改，delete:删除
	 */
	public void setEditState(String editState) {
		this.editState = editState;
	}

	/**
     * 时间戳
     * @return ts 时间戳
     */
    public Date getTs() {
        return ts;
    }

    /**
     * 时间戳
     * @param ts 时间戳
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }
}